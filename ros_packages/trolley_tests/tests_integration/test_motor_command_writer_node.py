#!/usr/bin/env python

"""Unit test for motor_command_writer_node"""

import rospy
import time
import unittest

from std_msgs.msg import Float64MultiArray

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_motor_command_writer_node'
MOTOR_PLUGIN_TOPIC = '/trolley/plugins/wheel_joint_effort_controller/command'
MOTOR_TORQUE_TOPIC = '/trolley/actuators/motor_torque'


#^ CLASS #######################################
class TestMotorCommandWriterNode(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_description = load_config('trolley_description')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']
		self.data_received = None

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(MOTOR_PLUGIN_TOPIC, Float64MultiArray, self.callback)
		rospy.loginfo('Subscribed to {}'.format(MOTOR_PLUGIN_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(MOTOR_PLUGIN_TOPIC), logger_name=NODE_NAME)
		self.data_received = msg.data

	#^ #######################################
	def test_if_publishing(self):
		timeout = rospy.get_time() + self.timeout

		while not rospy.is_shutdown() and self.data_received is None:
			if rospy.get_time() >= timeout:
				string = "Timeout: no messages received on {} for a duration of {} s".format(
					MOTOR_PLUGIN_TOPIC, self.timeout)
				rospy.logerr(string, logger_name=NODE_NAME)
				return

			rospy.sleep(self.loop_delay)

		string = "No messages received on topic {}".format(MOTOR_PLUGIN_TOPIC)
		self.assertFalse(self.data_received is None, string)

	#^ #######################################
	def test_if_motor_command_is_equal_for_both_wheels(self):
		while self.data_received is None:
			rospy.sleep(self.loop_delay)

		string = "RESULT: mtor command 1={}; motor command 2={}".format(*self.data_received)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertAlmostEqual(*self.data_received)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestMotorCommandWriterNode)
