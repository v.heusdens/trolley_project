#!/usr/bin/env python

"""Unit test for handle_force_generator_node's additional mass settings for handle force calculations"""

import rospy
import unittest

from std_msgs.msg import Float64
from trolley_msgs.srv import GetGeneratorForce
from python_scripts.dynamics.saved_symbolic_model_equations import ModelEquations
from toolbox.params import ParamsNumeric
from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_handle_force_generator_node_added_mass'
GET_FORCE_SERVICE = '/trolley/tests/get_generator_handle_force'


#^ CLASS #######################################
class TestHandleForceGeneratorNodeAddedMass(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_simulation = load_config('trolley_simulation')
		config_tests = load_config('trolley_tests')

		self.timeout = config_tests['timeout']
		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		support_wheel_mass_ratio = config_simulation['support_wheel_mass_ratio']
		target_vel = config_simulation['target_velocity']
		acc_duration = config_simulation['acceleration_duration']
		self.target_acc = target_vel / acc_duration

		#^ init model
		self.n = ParamsNumeric()
		self.n.set_params_nom()
		self.n.set_params_symbolic(['T_roll', 'F_hx', 'F_hy', 'dd_xw'])

		self.n.d_beta = 0
		self.n.d_xw = 1

		self.n.T_m = 0
		self.n.set_params_to_type('m_b', 'max')  #@ Add 20 kg of weight to bag
		self.n.m_w = self.n.m_w * (1 + support_wheel_mass_ratio)

		self.model_force = ModelEquations(self.n, acc_explicit=True).F_hx.all

		#^ init publishers/subscribers/services
		rospy.wait_for_service(GET_FORCE_SERVICE, timeout=self.timeout)
		self.get_force_srv = rospy.ServiceProxy(GET_FORCE_SERVICE, GetGeneratorForce)

	#^ #######################################
	def test_added_mass_during_acceleration(self):
		force_acc_exp = self.model_force.subs(self.n.dd_xw, self.target_acc)
		force_acc_gen = self.get_force_srv('acc').force

		rospy.loginfo('Vars set in model:\n \
			d_xw={}; dd_xw={}; mu={}; T_m={}; m_ft={}; m_w={}; R_w={}; m_acc={}; gamma={}'.format(
			self.n.d_xw, self.target_acc, self.n.mu, self.n.T_m, self.n.m_ft, self.n.m_w, self.n.R_w,
			self.n.m_acc, self.n.gamma),
			logger_name=NODE_NAME)

		string = "Acceleration force: expected={}; generated by node={} ({}% diff)".format(
			force_acc_exp, force_acc_gen, (force_acc_gen - force_acc_exp) / force_acc_exp * 100)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertAlmostEqual(force_acc_gen, force_acc_exp, msg=string)

	#^ #######################################
	def test_added_mass_during_steady_state(self):
		force_ss_exp = self.model_force.subs(self.n.dd_xw, 0)
		force_ss_gen = self.get_force_srv('ss').force

		rospy.loginfo('Vars set in model:\n \
			d_xw={}; dd_xw={}; mu={}; T_m={}; m_ft={}; m_w={}; R_w={}; m_acc={}; gamma={}'.format(
			self.n.d_xw, 0, self.n.mu, self.n.T_m, self.n.m_ft, self.n.m_w, self.n.R_w, self.n.m_acc,
			self.n.gamma),
			logger_name=NODE_NAME)

		string = "Steady-state force: expected={}; generated by node={} ({}% diff)".format(
			force_ss_exp, force_ss_gen, (force_ss_gen - force_ss_exp) / force_ss_exp * 100)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertAlmostEqual(force_ss_gen, force_ss_exp, msg=string)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	#unittest.main
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestHandleForceGeneratorNodeAddedMass)
