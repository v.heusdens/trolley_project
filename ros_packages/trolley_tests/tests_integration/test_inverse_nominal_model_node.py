#!/usr/bin/env python

"""Unit test for inverse_nominal_model_node"""

#TODO: Test for velocity calibration

import rospy
import unittest

from std_msgs.msg import Float64
from trolley_msgs.srv import SetAcceleration, SetMotorTorque, SetVelocity

from toolbox.params import ParamsNumeric
from toolbox.compute import isclose
from toolbox.ros import hold_ros, load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_inverse_nominal_model_node'
SUBSCRIBER_TOPIC = '/trolley/controller/handle_force_estimate'
VELOCITY_SERVICE = '/trolley/tests/set_velocity'
ACCELERATION_SERVICE = '/trolley/tests/set_acceleration'
MOTOR_TORQUE_SERVICE = '/trolley/tests/set_motor_torque'


#^ CLASS #######################################
class TestInverseNominalModelNode(unittest.TestCase):

	#^ #######################################
	def setUp(self):
		rospy.init_node(NODE_NAME)

		##^ init params
		self.wait_before_check = 0.1

		config_general = load_config('trolley_general')
		config_simulation = load_config('trolley_simulation')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']

		self.default_motor_torque = config_tests['motor_torque']
		self.default_velocity = config_simulation['target_velocity']
		acc_duration = config_simulation['acceleration_duration']
		self.default_acceleration = self.default_velocity / acc_duration

		self.force_estimate = None  #@ Save most recent force estimate from topic in this param

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		##^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self.callback)
		rospy.wait_for_service(VELOCITY_SERVICE, timeout=self.timeout)
		self.set_velocity_srv = rospy.ServiceProxy(VELOCITY_SERVICE, SetVelocity)
		rospy.wait_for_service(ACCELERATION_SERVICE, timeout=self.timeout)
		self.set_acceleration_srv = rospy.ServiceProxy(ACCELERATION_SERVICE, SetAcceleration)
		rospy.wait_for_service(MOTOR_TORQUE_SERVICE, timeout=self.timeout)
		self.set_motor_torque_srv = rospy.ServiceProxy(MOTOR_TORQUE_SERVICE, SetMotorTorque)

		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(VELOCITY_SERVICE), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(ACCELERATION_SERVICE), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(MOTOR_TORQUE_SERVICE), logger_name=NODE_NAME)

		##^ allow inverse nominal model node to calibrate
		self.set_sensor_values(0, 0, 0)
		self.hold(0.5)

	#^ #######################################
	def hold(self, duration):
		hold_ros(duration, self.loop_delay, NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		self.force_estimate = msg.data

	#^ #######################################
	def set_sensor_values(self, motor_torque, velocity, acceleration):
		self.set_motor_torque_srv(motor_torque)
		self.set_velocity_srv(velocity)
		self.set_acceleration_srv(acceleration)

	#^ TESTS #######################################
	def _test_act(self, motor_torque, velocity, acceleration):
		"""Use the function/class and get its result"""
		self.set_sensor_values(motor_torque, velocity, acceleration)

		while self.force_estimate is None:  #@ wait for value to be set
			rospy.sleep(self.loop_delay)

		self.hold(self.wait_before_check)  #@ wait for values to stabilize

	def _test_value_assert(self, motor_torque, velocity, acceleration):
		"""Check if the result is correct"""
		n = ParamsNumeric()
		n.set_params_nom()
		n.d_beta = 0
		n.T_m = motor_torque
		n.d_xw = velocity
		n.dd_xw = acceleration

		expected_handle_force = (n.m_f + n.m_b + 3 * n.m_w) * n.dd_xw - 2 / n.R_w * n.T_m
		diff = (self.force_estimate - expected_handle_force) / expected_handle_force * 100

		string = "Expected handle force: {} N; estimated handle force from inverse nominal model: {} N ({}% diff)".format(
			expected_handle_force, self.force_estimate, diff)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertTrue(isclose(self.force_estimate, expected_handle_force), string)

	#^ #######################################
	def test_if_publishing(self):
		timeout = rospy.get_time() + self.timeout
		while rospy.get_time() < timeout:
			if self.force_estimate is not None:
				self.assertTrue("Data is published on topic '{}'".format(SUBSCRIBER_TOPIC))
				return

		string = "Nothing published on topic '{}'".format(SUBSCRIBER_TOPIC)
		self.fail(string)

	#^ #######################################
	def test_force_estimate_for_zero_velocity_and_zero_torque(self):
		#% Arrange: setup of variables
		motor_torque = 0
		velocity = 0
		acceleration = self.default_acceleration

		#% Act: use the function/class and get its result
		self._test_act(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

		#% Assert: check if the result is correct
		self._test_value_assert(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

	#^ #######################################
	def test_force_estimate_for_positive_velocity_within_deadband_and_zero_torque(self):
		#% Arrange: setup of variables
		motor_torque = 0
		velocity = 0.005
		acceleration = self.default_acceleration

		#% Act: use the function/class and get its result
		self._test_act(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

		#% Assert: check if the result is correct
		self._test_value_assert(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

	#^ #######################################
	def test_relative_magnitude_of_force_estimate_for_positive_velocity_and_zero_torque(self):
		#% Arrange: setup of variables
		motor_torque = 0
		velocity = 1
		acceleration = self.default_acceleration

		#% Act: use the function/class and get its result
		self._test_act(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

		#% Assert: check if the result is correct
		n = ParamsNumeric()
		n.set_params_nom()
		n.d_beta = 0
		n.T_m = motor_torque
		n.d_xw = velocity
		n.dd_xw = acceleration

		exp_force_without_friction = (n.m_f + n.m_b + 3 * n.m_w) * n.dd_xw

		string = "Expected handle force: {} N; estimated handle force from inverse nominal model: {} N (should be larger)".format(
			exp_force_without_friction, self.force_estimate)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertTrue(self.force_estimate > exp_force_without_friction, string)

	#^ #######################################
	def test_force_estimate_for_zero_velocity_and_positive_torque(self):
		#% Arrange: setup of variables
		motor_torque = self.default_motor_torque
		velocity = 0
		acceleration = self.default_acceleration

		#% Act: use the function/class and get its result
		self._test_act(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

		#% Assert: check if the result is correct
		self._test_value_assert(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

	#^ #######################################
	def test_force_estimate_for_zero_velocity_and_negative_torque(self):
		#% Arrange: setup of variables
		motor_torque = -self.default_motor_torque
		velocity = 0
		acceleration = self.default_acceleration

		#% Act: use the function/class and get its result
		self._test_act(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

		#% Assert: check if the result is correct
		self._test_value_assert(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

	#^ #######################################
	def test_force_estimate_for_zero_velocity_and_positive_torque_and_negative_acceleration(self):
		#% Arrange: setup of variables
		motor_torque = self.default_motor_torque
		velocity = 0
		acceleration = -self.default_acceleration

		#% Act: use the function/class and get its result
		self._test_act(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)

		#% Assert: check if the result is correct
		self._test_value_assert(motor_torque=motor_torque, velocity=velocity, acceleration=acceleration)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	#unittest.main()
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestInverseNominalModelNode)
