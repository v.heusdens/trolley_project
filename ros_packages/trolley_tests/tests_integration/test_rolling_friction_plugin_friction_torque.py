#!/usr/bin/env python

"""Unit test for checking friction torque in joint_friction plugin"""

import rospy
import unittest

from trolley_msgs.msg import RollingFrictionTestMsg
from trolley_msgs.srv import SetFrictionCoefficient

from toolbox.params import ParamsNumeric
from toolbox.ros import hold_ros, load_config, wait_on_simulation_start
from toolbox.transform import convert_string_to_sympy
from toolbox.compute import isclose

PKG = 'trolley_tests'
NODE_NAME = 'test_rolling_friction_plugin_friction_torque'
SUBSCRIBER_TOPIC = '/trolley/tests/rolling_friction_plugin_test_msgs'


#^ BASE CLASS #######################################
class Base(unittest.TestCase):

	def _setUp(self):
		#^ init params
		config_general = load_config('trolley_general')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.duration = config_tests['gather_duration']

		self._trigger_message_received = False
		self.trigger_check_data = False
		self.trigger_failed = False

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, RollingFrictionTestMsg, self._callback)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _hold(self):
		duration = rospy.get_time() + self.duration
		while not rospy.is_shutdown() and rospy.get_time() < duration and not self.trigger_failed:
			rospy.sleep(self.loop_delay)

	#^ #######################################
	def _callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		self._trigger_message_received = True

		if self.trigger_check_data:
			rospy.loginfo_once('Checking data...', logger_name=NODE_NAME)

			if not isclose(msg.rolling_friction_torque, 0):
				string = "Test failed! Rolling friction should remain 0, was {} at rostime {}".format(
					msg.rolling_friction_torque, rospy.get_time())
				self.trigger_failed = True
				self.fail(string)

	#^ #######################################
	def _test_friction_torque(self):
		#^ gather data
		self.trigger_check_data = True
		self._hold()
		self.trigger_check_data = False

		#^ test data
		#@ No info on topic
		if not self._trigger_message_received:
			_string = "Timeout: no messages received on {} for a duration of {} s".format(
				SUBSCRIBER_TOPIC, self.duration)
			self.assertTrue(False, _string)
			return

		#@ No nonzero friction torque values have been set
		if not self.trigger_failed:
			self.assertTrue(True)


#^ TESTS #######################################
class NominalMassWithoutFriction(Base):

	def setUp(self):
		self._setUp()

	def test_nominal_mass_without_friction(self):
		self._test_friction_torque()


#^ #######################################
class NominalMassWithFriction(Base):

	def setUp(self):
		self._setUp()

	def test_nominal_mass_with_friction(self):
		self._test_friction_torque()


#^ #######################################
class MaximalMassWithoutFriction(Base):

	def setUp(self):
		self._setUp()

	def test_maximal_mass_without_friction(self):
		self._test_friction_torque()


#^ #######################################
class MaximalMassWithFriction(Base):

	def setUp(self):
		self._setUp()

	def test_maximal_mass_with_friction(self):
		self._test_friction_torque()


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest

	rospy.init_node(NODE_NAME)
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)

	#^ Get info from param server
	_friction = rospy.get_param('~friction')
	_added_mass = rospy.get_param('~added_mass')

	#^ Decide which test to run
	if not _friction and not _added_mass:
		test_case = NominalMassWithoutFriction
	elif _friction and not _added_mass:
		test_case = NominalMassWithFriction
	elif not _friction and _added_mass:
		test_case = MaximalMassWithoutFriction
	elif _friction and _added_mass:
		test_case = MaximalMassWithFriction
	else:
		raise Exception('Wrong test options given')

	#^ Run test
	rostest.rosrun(PKG, NODE_NAME, test_case)
