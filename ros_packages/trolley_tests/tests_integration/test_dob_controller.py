#!/usr/bin/env python

"""Unit test for checking complete controller structure"""

from numpy.core.numeric import NaN
import rospy
import unittest

from std_msgs.msg import Float64MultiArray
from trolley_msgs.srv import EmptyTrigger, SetAcceleration, SetHandleForce, SetVelocity

from toolbox.compute import isclose, mean
from toolbox.ros import hold_ros, load_config

PKG = 'trolley_tests'
NODE_NAME = 'test_dob_controller'
SUBSCRIBER_TOPIC = '/trolley/plugins/wheel_joint_effort_controller/command'
SET_VELOCITY_SERVICE = '/trolley/tests/set_velocity'
SET_ACCELERATION_SERVICE = '/trolley/tests/set_acceleration'
SET_HANDLE_FORCE_SERVICE = '/trolley/tests/set_handle_force'
RESET_Q_FILTER_SERVICE = '/trolley/tests/reset_q_filter'


#^ CLASS #######################################
class TestDOBController(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		self.trigger_message_received = False
		self.trigger_gather_data = False

		self.data_received = []
		self.callback_counter = 0

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64MultiArray, self.callback)
		rospy.wait_for_service(SET_VELOCITY_SERVICE, timeout=self.timeout)
		self.set_velocity_srv = rospy.ServiceProxy(SET_VELOCITY_SERVICE, SetVelocity)
		rospy.wait_for_service(SET_ACCELERATION_SERVICE, timeout=self.timeout)
		self.set_acceleration_srv = rospy.ServiceProxy(SET_ACCELERATION_SERVICE, SetAcceleration)
		rospy.wait_for_service(SET_HANDLE_FORCE_SERVICE, timeout=self.timeout)
		self.set_handle_force_srv = rospy.ServiceProxy(SET_HANDLE_FORCE_SERVICE, SetHandleForce)
		rospy.wait_for_service(RESET_Q_FILTER_SERVICE, timeout=self.timeout)
		self.reset_q_filter_srv = rospy.ServiceProxy(RESET_Q_FILTER_SERVICE, EmptyTrigger)

		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(SET_VELOCITY_SERVICE), logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(SET_ACCELERATION_SERVICE),
			logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(SET_HANDLE_FORCE_SERVICE),
			logger_name=NODE_NAME)
		rospy.loginfo('Service client initiated for {}'.format(RESET_Q_FILTER_SERVICE), logger_name=NODE_NAME)

	#^ #######################################
	def hold(self, duration):
		hold_ros(duration, self.loop_delay, NODE_NAME, loginfo=False)

	#^ #######################################
	def reset(self):
		self.trigger_gather_data = False
		self.data_received = []

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		self.trigger_message_received = True

		if not self.trigger_gather_data:
			return

		#^ Gather data until steady-state output is reached
		self.data_received.append(msg.data[0])

		if len(self.data_received) >= 2 and isclose(self.data_received[-1], self.data_received[-2]):
			self.callback_counter += 1
		else:
			self.callback_counter = 0

		if self.callback_counter >= 10:
			self.trigger_gather_data = False
			self.callback_counter = 0

	#^ TESTS #######################################
	def _test_arrange(self, velocity, acceleration, handle_force):  #% setup of variables
		self.reset()
		self.set_velocity_srv(velocity)
		self.set_acceleration_srv(acceleration)
		self.set_handle_force_srv(handle_force)
		self.hold(0.2)
		self.reset_q_filter_srv()

	#^ #######################################
	def _test_act(self):  #% use the function/class and get its result
		rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)
		self.trigger_gather_data = True

		#^ wait until enough data is received or timeout time is reached
		timeout = rospy.get_time() + self.timeout
		while self.trigger_gather_data:
			if rospy.get_time() > timeout:
				string = "Timeout of {} s exceeded for data gathering".format(self.timeout)
				self.fail(string)
				return

			self.hold(0.1)

	#^ #######################################
	def test_output_for_zero_inputs(self):
		#% Arrange: setup of variables
		self._test_arrange(velocity=0, acceleration=0, handle_force=0)

		#% Act: use the function/class and get its result
		self._test_act()

		#% Assert: check if the result is correct
		ss_expected = 0
		ss_measured = mean(self.data_received[-10:])
		if ss_expected != 0:
			diff = (ss_measured - ss_expected) / ss_expected * 100
		else:
			diff = "-"

		string = "Expected steady-state value={}; measured value={} ({}% off)".format(
			ss_expected, ss_measured, diff)
		rospy.loginfo(string, logger_name=NODE_NAME)
		self.assertAlmostEqual(ss_measured, ss_expected, msg=string)

	#TODO: more tests


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestDOBController)
