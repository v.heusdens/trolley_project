#!/usr/bin/env python

"""Dummy unit-test with specified duration, when run always returns true"""

import rospy
import unittest
from time import time

#& TEST SETUP
PKG = 'trolley_tests'
NODE_NAME = 'test_dummy'


class TestDummy(unittest.TestCase):

	def test_run(self):
		#^ init
		rospy.init_node(NODE_NAME)

		#^ set duration
		duration = 0.0
		if rospy.has_param('~duration'):
			duration = rospy.get_param('~duration')
			if duration < 1.0:
				raise rospy.ROSException(
					'Dummy test duration must be at least 1 sec to ensure all nodes can start')

		#^ loop
		rospy.loginfo('!Starting dummy test - duration = {}'.format(duration), logger_name=NODE_NAME)
		rate = rospy.Rate(100)
		timeout = time() + duration

		while time() < timeout:
			try:
				rate.sleep()
			except rospy.exceptions.ROSInterruptException as e_ros:
				rospy.loginfo(
					"!dummy test shutting down with the following interruption exception: {}".format(
					e_ros.message),
					logger_name=NODE_NAME)
				break

		#^ return true
		self.assertTrue(True)


#& MAIN
if __name__ == '__main__':
	import rostest
	rostest.rosrun(PKG, NODE_NAME, TestDummy)
