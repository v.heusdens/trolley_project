#!/usr/bin/env python

"""Unit tests for q_filter_node"""

import rospy
import unittest
import numpy as np

from std_msgs.msg import Float64
from trolley_msgs.srv import SetDisturbanceEstimate

from sim_ws.src.trolley_controller.src.q_filter_node import generate_q_filter_discrete
from toolbox.ros import hold_ros, load_config
from toolbox.transform import convert_string_to_sympy

PKG = 'trolley_tests'
NODE_NAME = 'test_q_filter_node'
SUBSCRIBER_TOPIC = '/trolley/controller/disturbance_estimate_filtered'
SERVICE_TOPIC = '/trolley/tests/set_disturbance_estimate_unfiltered'


#^ CLASS #######################################
class TestQFilterNode(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_general = load_config('trolley_general')
		config_controller = load_config('trolley_controller')
		config_tests = load_config('trolley_tests')

		self.loop_delay = config_general['loop_delay']
		self.timeout = config_tests['timeout']
		rate = config_controller['update_rate']

		#@ load Q-filter from controller config file
		q_num = [float(convert_string_to_sympy(x, np.pi, 'pi')) for x in config_controller['numerator']]
		q_denom = [float(convert_string_to_sympy(x, np.pi, 'pi')) for x in config_controller['denominator']]
		self.q_filter = generate_q_filter_discrete(
			q_num, q_denom, 1. / rate
		)  #@ transform num and denom to discrete Q-filter equation using the same function as used by the Q-filter node itself

		self.trigger_message_received = False
		self.trigger_gather_data = False

		self.output_received = []  #@ used for storing input data

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Float64, self.callback)
		rospy.wait_for_service(SERVICE_TOPIC, timeout=self.timeout)
		self.set_disturbance_estimate_srv = rospy.ServiceProxy(SERVICE_TOPIC, SetDisturbanceEstimate)

		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC))
		rospy.loginfo('Service client initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def hold(self, duration):
		"""Wait for a specified period of time in ROS sim-time."""
		hold_ros(duration, self.loop_delay, NODE_NAME, loginfo=False)

	#^ #######################################
	def callback(self, msg):
		"""Store incoming Q-filter output data."""
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC))
		self.trigger_message_received = True

		if not self.trigger_gather_data:  #@ wait until triggered
			return

		self.output_received.append(msg.data)  #@ store data

		if len(self.output_received) >= 5:  #@ gather 5 datapoints
			self.trigger_gather_data = False

	#^ TESTS #######################################
	def test_q_filter_difference_equation(self):
		"""Test output of Q-filter equation y[n] = a x[n-1] - b y [n-1] generated in the Q-filter node with expectations."""

		dt = 0.02  #@ time step
		var = 5 * 2 * np.pi  #@ transform from Hz to rad/s
		q_filter = generate_q_filter_discrete(
			[var], [1, var], dt
		)  #@ transform num and denom to discrete Q-filter equation using the same function as used by the Q-filter node itself

		#@ expected output values based on the discrete Q-filter fraction
		a = 0.46651191
		b = -0.53348809

		inputs = [(0, 0, 0), (1, 0, a), (0, 1, -b), (1, 1, a - b)]  #@ (x[n-1],  y[n-1], y[n]) for different test-scenarios
		for x_prev, y_prev, y_exp in inputs:  #@ loop over scenarios
			y_calc = q_filter(x_prev, y_prev)  #@ apply internal Q-filter

			string = "x[n-1]={}; y[n-1]={}; y[n] expected={}; y[n] calculated={} ({}% diff)".format(
				x_prev, y_prev, y_exp, y_calc, (y_calc - y_exp) / y_exp * 100)
			rospy.loginfo(string, logger_name=NODE_NAME)
			self.assertAlmostEqual(y_calc, y_exp, msg=string)  #@ test equality between calculated and expected output

	#^ #######################################
	def test_q_filter_node_IO(self):
		"""Test if Q-filter node processes input data correctly."""
		#@ let input-value publisher node publish a unit step on the Q-filter's input topic
		self.set_disturbance_estimate_srv(0)
		self.hold(0.5)
		self.set_disturbance_estimate_srv(1)
		self.hold(0.1)

		rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)
		self.trigger_gather_data = True  #@ start storing data

		#@ wait until enough data is received or timeout time is reached
		timeout = rospy.get_time() + self.timeout
		while self.trigger_gather_data:
			self.hold(0.1)

			if rospy.get_time() > timeout:
				self.fail("Timeout of {} s exceeded for data gathering".format(self.timeout))
				return

		#^ test received data
		for i in range(1, len(self.output_received)):  #@ loop over stored data
			output_exp = self.q_filter(1,
				self.output_received[i - 1])  #@ calculate expected output using the Q-filter function directly

			string = "y[n-1]={}; y[n] expected={}; y[n] received={} ({} % diff)".format(
				self.output_received[i - 1], output_exp, self.output_received[i],
				(self.output_received[i] - output_exp) / output_exp * 100)
			rospy.loginfo(string, logger_name=NODE_NAME)
			self.assertAlmostEqual(self.output_received[i], output_exp,
				msg=string)  #@ test equality between output received from Q-filter node and expected output.


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestQFilterNode)
