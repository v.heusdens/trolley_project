#!/usr/bin/env python

"""Unit test for checking if gazebo server is active"""

import os
import re
import rospy
import subprocess as sub
import unittest
from time import time

from toolbox.ros import load_config

#& TEST SETUP
PKG = 'trolley_tests'
TEST_NAME = 'gzserver_active_test'
NODE_NAME = 'test_gazebo_server_active'


#^ CLASS #######################################
class TestGzserverActive(unittest.TestCase):

	#^ Init params
	config_tests = load_config('trolley_tests')
	self.timeout = config_tests['timeout']

	NULL = open(os.devnull, 'w')
	gzserver_ok = False

	rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

	#^ #######################################
	def obtain_active_nodes(self):
		try:
			process = sub.Popen(['rosnode', 'ping', '-a'], stderr=self.NULL, stdout=sub.PIPE)
		except KeyboardInterrupt:
			return

		out, _ = process.communicate()
		re_ping_node = re.compile('pinging ([^ ]+) with a timeout of \d+.\d+s\s*xmlrpc reply from')
		ping_nodes = re_ping_node.findall(out)
		return ping_nodes

	#^ #######################################
	def test_if_gzserver_active(self):
		#^ init
		rospy.init_node(NODE_NAME)

		#^ Wait for simulation to start
		while rospy.get_time() == 0:
			pass

		timeout = time() + self.timeout
		while time() < timeout and not self.gzserver_ok:
			nodes_active = self.obtain_active_nodes()
			self.gzserver_ok = '/gazebo' in nodes_active

		self.assertTrue(self.gzserver_ok, 'timeout: Gazebo server not running.')


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, TEST_NAME, TestGzserverActive)
