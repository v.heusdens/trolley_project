#!/usr/bin/env python

"""Unit test for checking IMU plugin output"""

import rospy
import unittest

from sensor_msgs.msg import Imu

from toolbox.params import ParamsNumeric
from toolbox.ros import hold_ros, load_config, wait_on_simulation_start
from toolbox.transform import convert_string_to_sympy

PKG = 'trolley_tests'
NODE_NAME = 'test_imu_plugin'
SUBSCRIBER_TOPIC = '/trolley/plugins/imu_measurement'


#^ CLASS #######################################
class TestImuPlugin(unittest.TestCase):

	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		self.margin = 0.005  #@ relative to model_acc value

		config_general = load_config('trolley_general')
		config_tests = load_config('trolley_tests')
		config_simulation = load_config('trolley_simulation')

		self.loop_delay = config_general['loop_delay']
		self.duration = config_tests['gather_duration']
		self.timeout = config_tests['timeout']

		self.trigger_message_received = False
		self.trigger_gather_data = False

		self.data_acc = []

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init model
		p = ParamsNumeric()
		p.set_params_to_type(['L_h', 'm_b'], 'nom')
		p.d_xw = 0
		p.d_beta = 0
		p.mu = 0
		p.gamma = 0
		p.F_hx = 0
		p.T_m = config_tests['motor_torque']

		#@ Add sim support wheel mass
		support_wheel_mass_ratio = config_simulation['support_wheel_mass_ratio']
		p.m_w = (1 + support_wheel_mass_ratio) * p.m_w

		rospy.loginfo('Vars set in model:\n \
            T_m={}; \
            F_hx={}; \
            m_ft={}; \
            R_w={}; \
            m_acc={}; \
            gamma={}'.format(p.T_m, p.F_hx, p.m_ft, p.R_w, p.m_acc, p.gamma),
			logger_name=NODE_NAME)

		model = config_tests['model_ddxw']
		self.acc_calc = convert_string_to_sympy(model, p)

		self.upper_bound = self.acc_calc * (1 + self.margin)
		self.lower_bound = self.acc_calc * (1 - self.margin)

		#^ init publishers/subscribers/services
		rospy.Subscriber(SUBSCRIBER_TOPIC, Imu, self.callback)
		rospy.loginfo('Subscribed to {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.loop_delay, self.timeout, NODE_NAME)

	def hold_before_data_gathering(self):
		hold_ros(0.5, self.loop_delay, NODE_NAME)

	def hold_during_data_gathering(self):
		hold_ros(self.duration, self.loop_delay, NODE_NAME)

	#^ #######################################
	def callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(SUBSCRIBER_TOPIC), logger_name=NODE_NAME)
		self.trigger_message_received = True

		if self.trigger_gather_data:
			value = msg.linear_acceleration.x
			self.data_acc.append(value)

	#^ #######################################
	def test_imu_plugin_output(self):
		self.wait_on_simulation_start()
		self.hold_before_data_gathering()

		#^ gather data
		rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)
		self.trigger_gather_data = True
		self.hold_during_data_gathering()
		self.trigger_gather_data = False

		#^ test data
		#@ No info on topic
		if not self.trigger_message_received:
			string = "Timeout: no messages received on {} for a duration of {} s".format(SUBSCRIBER_TOPIC, self.duration)
			self.assertTrue(False, string)
			return

		#@ Proper data received
		data_avg = sum(self.data_acc) / len(self.data_acc)
		data_error = (data_avg - self.acc_calc) / self.acc_calc

		string = 'Calculated acceleration value: {:.5f} \n\
			Acceptable range:({:.5f}, {:.5f}) ({}% margin) \n\
			Average measured acceleration value: {:.5f} ({:.2f}% off)'.format(self.acc_calc, self.lower_bound, self.upper_bound,
			self.margin * 100, data_avg, data_error * 100)

		rospy.loginfo('\nTEST RESULT:\n' + string, logger_name=NODE_NAME)
		self.assertTrue(self.lower_bound <= data_avg <= self.upper_bound, string)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestImuPlugin)
