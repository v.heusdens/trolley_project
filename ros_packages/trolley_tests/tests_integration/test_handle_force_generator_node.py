#!/usr/bin/env python

"""Unit test for handle_force_generator_node"""

import numpy as np
import rospy
import unittest

from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Imu
from trolley_msgs.srv import GetGeneratorForce

from toolbox.compute import isclose
from toolbox.ros import hold_ros, load_config, wait_on_simulation_start

PKG = 'trolley_tests'
NODE_NAME = 'test_handle_force_generator_node'
TOPIC_VEL = '/gazebo/link_states'
TOPIC_ACC = '/trolley/plugins/imu_measurement'
TOPIC_FORCE = '/trolley/plugins/handle_force_measurement'
FORCE_SERVICE = '/trolley/tests/get_generator_handle_force'


#^ CLASS #######################################
class TestHandleForceGeneratorNode(unittest.TestCase):

	#^ #######################################
	def setUp(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		self.max_acceleration_error = 0.1 / 10.  #@ Allow maximum 0.1m/s difference over 10 seconds

		config_general = load_config('trolley_general')
		config_tests = load_config('trolley_tests')
		config_description = load_config('trolley_description')
		config_sensors = load_config('trolley_sensors')
		config_simulation = load_config('trolley_simulation')

		self.loop_delay = config_general['loop_delay']
		self.publish_delay = config_sensors['publish_delay']
		self.timeout = config_tests['timeout']

		self.acc_duration = config_simulation['acceleration_duration']
		self.gather_duration = 2 * self.acc_duration

		self.target_velocity = config_simulation['target_velocity']
		self.target_acceleration = self.target_velocity / self.acc_duration

		self.R_w = config_description['wheel_radius']

		self.trigger_vel_measured = False  #@ True if a velocity has been measured during data gathering phase
		self.trigger_acc_measured = False  #@ True if an acceleration has been measured during data gathering phase
		self.trigger_accelerating = False  #@ True if handle force generator is publishing acceleration force right now
		self.trigger_steady_state = False  #@ True if handle force generator is publishing steady-state force right now
		self.trigger_gather_data = False  #@ True if data gathering should take place right now

		self.force_during_acceleration = None  #@ Force during acceleration as emitted by handle_force_generator
		self.force_during_steady_state = None  #@ Force during steady state as emitted by handle_force_generator
		self.set_handle_force_node_settings_from_service()

		self.data_vel_accelerating = []  #@ Measured velocity during acceleration phase
		self.data_acc_accelerating = []  #@ Measured acceleration from IMU during acceleration phase
		self.data_vel_steady_state = []  #@ Measured velocity during steady-state phase
		self.data_acc_steady_state = []  #@ Measured acceleration from IMU during steady-state phase

		rospy.loginfo('Timeout set to {} s'.format(self.timeout), logger_name=NODE_NAME)

		#^ init publishers/subscribers/services
		rospy.Subscriber(TOPIC_VEL, LinkStates, self.callback_vel)
		rospy.Subscriber(TOPIC_ACC, Imu, self.callback_acc)
		rospy.Subscriber(TOPIC_FORCE, Vector3, self.callback_force)
		rospy.loginfo('Subscribed to {}'.format(TOPIC_VEL), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(TOPIC_ACC), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(TOPIC_FORCE), logger_name=NODE_NAME)

	#^ #######################################
	def wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.loop_delay, self.timeout, NODE_NAME)

	def hold_before_data_gathering(self):
		hold_ros(self.publish_delay, self.loop_delay, NODE_NAME)
		hold_ros(0.5, self.loop_delay, NODE_NAME)

	def hold_during_data_gathering(self):
		hold_ros(self.gather_duration, self.loop_delay, NODE_NAME)

	#^ #######################################
	def callback_vel(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(TOPIC_VEL), logger_name=NODE_NAME)
		if not self.trigger_gather_data:
			rospy.loginfo_throttle(0.5,
				"Gather data trigger set to false. Skipping velocity data point",
				logger_name=NODE_NAME)
			return

		self.trigger_vel_measured = True
		#^ Obtain index of target links
		i_l = msg.name.index('trolley::left_wheel_link')
		i_r = msg.name.index('trolley::right_wheel_link')

		#^ Obtain and combine velocity measurements
		ang_vel_l = msg.twist[i_l].angular.y
		ang_vel_r = msg.twist[i_r].angular.y
		ang_vel = (ang_vel_l + ang_vel_r) / 2
		lin_vel = ang_vel * self.R_w

		if self.trigger_steady_state:
			self.data_vel_steady_state.append(lin_vel)
			rospy.loginfo_throttle(0.5, "Appending steady-state velocity data", logger_name=NODE_NAME)
		elif self.trigger_accelerating:
			self.data_vel_accelerating.append(lin_vel)
			rospy.loginfo_throttle(0.5, "Appending accelerating velocity data", logger_name=NODE_NAME)
		else:
			rospy.logwarn_throttle(0.5, "Handle force generator state not as expected. Skipping velocity data point")

	#^ #######################################
	def callback_acc(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(TOPIC_ACC), logger_name=NODE_NAME)
		if not self.trigger_gather_data:
			rospy.loginfo_throttle(0.5,
				"Gather data trigger set to false. Skipping acceleration data point",
				logger_name=NODE_NAME)
			return

		self.trigger_acc_measured = True

		if self.trigger_steady_state:
			self.data_acc_steady_state.append(msg.linear_acceleration.x)
			rospy.loginfo_throttle(0.5, "Appending steady-state acceleration data", logger_name=NODE_NAME)
		elif self.trigger_accelerating:
			self.data_acc_accelerating.append(msg.linear_acceleration.x)
			rospy.loginfo_throttle(0.5, "Appending accelerating acceleration data", logger_name=NODE_NAME)
		else:
			rospy.logwarn_throttle(0.5, "Handle force generator state not as expected. Skipping acceleration data point")

	#^ #######################################
	def callback_force(self, msg):
		"""Based on the message, set the trigger_accelerating and trigger_steady_state flags"""
		self.trigger_accelerating = False
		self.trigger_steady_state = False

		if isclose(msg.x, self.force_during_acceleration):
			self.trigger_accelerating = True
		elif isclose(msg.x, self.force_during_steady_state):
			self.trigger_steady_state = True
		else:
			rospy.logerr(
				"Emitted handle force from node is not as expected: was {} but expected {} (acc) or {} (steady-state)".
				format(msg.x, self.force_during_acceleration, self.force_during_steady_state),
				logger_name=NODE_NAME)

	#^ #######################################
	def set_handle_force_node_settings_from_service(self):
		"""
		Set self.force_during_acceleration and self.force_during_steady_state by doing a service 
		call to the handle force generator node.
		"""
		rospy.wait_for_service(FORCE_SERVICE, timeout=self.timeout)
		try:
			get_force_setpoint = rospy.ServiceProxy(FORCE_SERVICE, GetGeneratorForce)
			self.force_during_acceleration = get_force_setpoint(type="acc").force
			self.force_during_steady_state = get_force_setpoint(type="ss").force
		except rospy.ServiceException:
			rospy.logerr("Service call on {} failed!".format(FORCE_SERVICE))

	#^ #######################################
	def test_acceleration_and_velocity_profile_due_to_handle_force_generator(self):
		self.wait_on_simulation_start()
		self.hold_before_data_gathering()

		#^ gather data
		rospy.loginfo_once('Gathering data...', logger_name=NODE_NAME)
		self.trigger_gather_data = True
		self.hold_during_data_gathering()
		self.trigger_gather_data = False

		#^ test data
		if not self.trigger_vel_measured or not self.trigger_acc_measured:
			string = "Received velocity on {}: {}, received acceleration on {}: {}, during {} seconds".format(
				TOPIC_VEL, self.trigger_vel_measured, TOPIC_ACC, self.trigger_acc_measured, self.gather_duration)
			rospy.logerr(string, logger_name=NODE_NAME)
			self.fail(string)

		#^ remove first and last 'cutoff' data points to remove transient/settling effects
		cutoff = 2
		min_data_size = 1 + 2 * cutoff
		if (len(self.data_acc_accelerating) < min_data_size or  #@ 5 because we cut off first and last two data points
			len(self.data_acc_steady_state) < min_data_size or
			len(self.data_vel_accelerating) < min_data_size or len(self.data_vel_steady_state) < min_data_size):
			string = "Did not receive enough data during both acceleration and steady-state phase"
			rospy.logerr(string, logger_name=NODE_NAME)
			self.fail(string)
		a_acceleration = np.array(self.data_acc_accelerating[cutoff:-cutoff])
		a_steady_state = np.array(self.data_acc_steady_state[cutoff:-cutoff])
		v_steady_state = np.array(self.data_vel_steady_state[cutoff:-cutoff])

		#^ calculate the averages and errors for accelerating, steady-state acceleration and steady-state velocity
		v_steady_state_avg = v_steady_state.mean()
		a_acceleration_avg = a_acceleration.mean()
		a_steady_state_avg = a_steady_state.mean()
		v_steady_state_error = v_steady_state_avg - self.target_velocity
		a_acceleration_error = a_acceleration_avg - self.target_acceleration
		a_steady_state_error = a_steady_state_avg - 0

		string_acc_acc = 'Initial target acceleration: {:.5f} m/s^2 \n\
			Acceptable range:({:.5f}, {:.5f}) m/s^2 ({}% margin) \n\
			Average measured acceleration value: {:.5f} m/s^2 ({:.2f}% off)'.format(
			self.target_acceleration, self.target_acceleration - self.max_acceleration_error,
			self.target_acceleration + self.max_acceleration_error,
			self.max_acceleration_error / self.target_acceleration * 100, a_acceleration_avg,
			a_acceleration_error / self.target_acceleration * 100)
		string_acc_ss = 'Steady-state target acceleration: {:f} m/s^2 \n\
			Acceptable range:({:.5f}, {:.5f}) m/s^2 \n\
			Average measured acceleration value: {:.5f} m/s^2'.format(0, 0 - self.max_acceleration_error,
			0 + self.max_acceleration_error, a_steady_state_avg)
		string_vel_ss = 'Steady-state velocity: {:.5f} m/s, target velocity: {:.2f} m/s ({:.2f}% off)'.format(
			v_steady_state_avg, self.target_velocity, v_steady_state_error / self.target_velocity * 100)

		rospy.loginfo('TEST RESULT:', logger_name=NODE_NAME)
		rospy.loginfo(string_acc_acc, logger_name=NODE_NAME)
		rospy.loginfo(string_acc_ss, logger_name=NODE_NAME)
		rospy.loginfo(string_vel_ss, logger_name=NODE_NAME)
		self.assertTrue(a_acceleration_error <= self.max_acceleration_error, string_acc_acc)
		self.assertTrue(a_steady_state_error <= self.max_acceleration_error, string_acc_ss)


#^ MAIN #######################################
if __name__ == '__main__':
	import rostest
	rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
	rostest.rosrun(PKG, NODE_NAME, TestHandleForceGeneratorNode)
