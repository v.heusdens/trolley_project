#!/usr/bin/env python

"""This script posts test data to the /trolley/sensors/acceleration topic"""

import rospy

from std_msgs.msg import Float64
from trolley_msgs.srv import SetAcceleration, SetAccelerationResponse

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'acceleration_publisher'
PUBLISHER_TOPIC = '/trolley/sensors/acceleration'
SERVICE_TOPIC = '/trolley/tests/set_acceleration'


#^ CLASS #######################################
class AccelerationPublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_sensors = load_config('trolley_sensors')
		self.update_rate = config_sensors['update_rate']

		if rospy.has_param('~acceleration'):
			acceleration = rospy.get_param('~acceleration')
			rospy.loginfo('acceleration set to {} m/s^2 from param server'.format(acceleration),
				logger_name=NODE_NAME)
		else:
			acceleration = 0
			rospy.loginfo('acceleration set to {} m/s^2'.format(acceleration), logger_name=NODE_NAME)

		#^ init msg
		self.msg = Float64()
		self.msg.data = acceleration

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Service(SERVICE_TOPIC, SetAcceleration, self.srv_handle)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def srv_handle(self, value):
		"""Service call handler"""
		self.msg.data = value.acceleration
		rospy.loginfo('SetAcceleration service called: acceleration set to {}'.format(value),
			logger_name=NODE_NAME)
		return SetAccelerationResponse()

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		rospy.loginfo_once('Publishing the following message on topic {}:\n{}'.format(
			PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		rate = rospy.Rate(self.update_rate)
		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			rate.sleep()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = AccelerationPublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
