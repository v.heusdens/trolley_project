#!/usr/bin/env python

"""This script posts test data to the /trolley/actuators/motor_torque topic"""

import rospy

from std_msgs.msg import Float64
from trolley_msgs.srv import SetMotorTorque, SetMotorTorqueResponse

from toolbox.ros import load_config, wait_on_simulation_start

PKG = 'trolley_tests'
NODE_NAME = 'motor_torque_publisher'
PUBLISHER_TOPIC = '/trolley/actuators/motor_torque'
SERVICE_TOPIC = '/trolley/tests/set_motor_torque'


#^ CLASS #######################################
class MotorTorquePublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_tests = load_config('trolley_tests')
		config_controller = load_config('trolley_controller')

		self.update_rate = config_controller['update_rate']
		self.duration = False
		self.wait_on_sim = False

		if rospy.has_param('~duration'):
			self.duration = rospy.get_param('~duration')
			rospy.loginfo('publish duration set to {} s'.format(self.duration), logger_name=NODE_NAME)

		if rospy.has_param('~wait_on_simulation_start'):
			self.wait_on_sim = rospy.get_param('~wait_on_simulation_start')

		if rospy.has_param('~motor_torque'):  #@ Check if input effort is given explicitly
			motor_torque = rospy.get_param('~motor_torque')
			rospy.loginfo('motor torque set to {} Nm from param server'.format(motor_torque), logger_name=NODE_NAME)
		else:
			motor_torque = config_tests['motor_torque']
			rospy.loginfo('motor torque set to default of {} Nm'.format(motor_torque), logger_name=NODE_NAME)

		#^ init msg
		self.msg = Float64()
		self.msg.data = motor_torque

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Service(SERVICE_TOPIC, SetMotorTorque, self._srv_handle)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def _wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.update_rate, NODE_NAME=NODE_NAME)

	#^ #######################################
	def _srv_handle(self, value):
		"""Service call handler"""
		torque = value.torque
		self.msg.data = torque
		rospy.loginfo('SetMotorTorque service called: torque per wheel set to {}'.format(torque), logger_name=NODE_NAME)
		return SetMotorTorqueResponse()

	#^ #######################################
	def _publish(self):
		rospy.loginfo('Publishing the following message on topic {}:\n{}'.format(PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		if self.duration:
			_duration = rospy.get_time() + self.duration

		rate = rospy.Rate(self.update_rate)
		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			rate.sleep()

			if self.duration and rospy.get_time() > _duration:
				return

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		if self.wait_on_sim:
			self._wait_on_simulation_start()
		self._publish()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = MotorTorquePublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
