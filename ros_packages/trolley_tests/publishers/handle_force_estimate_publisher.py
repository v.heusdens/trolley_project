#!/usr/bin/env python

"""This script posts test data to the /trolley/controller/handle_force_estimate topic"""

import rospy

from std_msgs.msg import Float64
from trolley_msgs.srv import SetHandleForce, SetHandleForceResponse

from toolbox.ros import load_config, wait_on_simulation_start

PKG = 'trolley_tests'
NODE_NAME = 'handle_force_estimate_publisher'
PUBLISHER_TOPIC = '/trolley/controller/handle_force_estimate'
SERVICE_TOPIC = '/trolley/tests/set_handle_force_estimate'


#^ CLASS #######################################
class HandleForceEstimatePublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_controller = load_config('trolley_controller')

		self.update_rate = config_controller['update_rate']
		rospy.loginfo('publish rate = {} Hz'.format(self.update_rate), logger_name=NODE_NAME)

		if rospy.has_param('~handle_force_estimate'):
			handle_force_estimate = rospy.get_param('~handle_force_estimate')
			rospy.loginfo('handle force estimate set to {} N from param server'.format(handle_force_estimate),
				logger_name=NODE_NAME)
		else:
			handle_force_estimate = 0
			rospy.loginfo('handle force estimate set to default of {} N'.format(handle_force_estimate),
				logger_name=NODE_NAME)

		#^ init msg
		self.msg = Float64()
		self.msg.data = handle_force_estimate

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Service(SERVICE_TOPIC, SetHandleForce, self.srv_handle)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def srv_handle(self, value):
		"""Service call handler"""
		self.msg.data = value.handle_force
		rospy.loginfo('SetHandleForce service called: handle force estimate set to {}'.format(value),
			logger_name=NODE_NAME)
		return SetHandleForceResponse()

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		rospy.loginfo_once('Publishing the following message on topic {}:\n{}'.format(
			PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		r = rospy.Rate(self.update_rate)
		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			r.sleep()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = HandleForceEstimatePublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
