#!/usr/bin/env python

"""This script posts test data to the /trolley/sensors/velocity topic"""

import rospy

from std_msgs.msg import Float64
from trolley_msgs.srv import SetVelocity, SetVelocityResponse

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'velocity_publisher'
PUBLISHER_TOPIC = '/trolley/sensors/velocity'
SERVICE_TOPIC = '/trolley/tests/set_velocity'


#^ CLASS #######################################
class VelocityPublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_sensors = load_config('trolley_sensors')
		self.update_rate = config_sensors['update_rate']

		if rospy.has_param('~velocity'):
			velocity = rospy.get_param('~velocity')
			rospy.loginfo('velocity set to {} m/s from param server'.format(velocity), logger_name=NODE_NAME)
		else:
			velocity = 0
			rospy.loginfo('velocity set to default of {} m/s'.format(velocity), logger_name=NODE_NAME)

		#^ init msg
		self.msg = Float64()
		self.msg.data = velocity

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Service(SERVICE_TOPIC, SetVelocity, self.srv_handle)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def srv_handle(self, value):
		"""Service call handler"""
		self.msg.data = value.velocity
		rospy.loginfo('SetVelocity service called: velocity set to {}'.format(value), logger_name=NODE_NAME)
		return SetVelocityResponse()

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		rospy.loginfo_once('Publishing the following message on topic {}:\n{}'.format(
			PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		r = rospy.Rate(self.update_rate)
		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			r.sleep()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = VelocityPublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
