#!/usr/bin/env python

"""This script posts test data to the /trolley/controller/disturbance_estimate_filtered topic"""

import rospy

from std_msgs.msg import Float64

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'disturbance_estimate_filtered_publisher'
PUBLISHER_TOPIC = '/trolley/controller/disturbance_estimate_filtered'


#^ CLASS #######################################
class DisturbanceEstimateFilteredPublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_controller = load_config('trolley_controller')

		update_rate = config_controller['update_rate']
		self.rate = rospy.Rate(update_rate)
		rospy.loginfo('publish rate = {} Hz'.format(update_rate), logger_name=NODE_NAME)

		self.msg = Float64()
		self.msg.data = 4

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		rospy.loginfo_once('Publishing the following message on topic {}:\n{}'.format(
			PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			self.rate.sleep()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = DisturbanceEstimateFilteredPublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
