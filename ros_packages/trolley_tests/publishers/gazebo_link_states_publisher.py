#!/usr/bin/env python

"""This script posts test data to the /gazebo/link_states topic"""

import rospy

from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Pose, Twist

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'gazebo_link_states_publisher'
PUBLISHER_TOPIC = '/gazebo/link_states'


#^ CLASS #######################################
class GazeboLinkStatesPublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_sensors = load_config('trolley_sensors')
		config_tests = load_config('trolley_tests')

		update_rate = config_sensors['update_rate']
		self.rate = rospy.Rate(update_rate)
		rospy.loginfo('publish rate = {} Hz'.format(update_rate), logger_name=NODE_NAME)

		#^ init message
		self.msg = LinkStates()
		data = config_tests['gazebo_link_states_publisher_data']
		self.populate_msg(data)

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, LinkStates, queue_size=1)
		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def populate_msg(self, data):
		self.msg.name = data['link_names']

		pose = Pose()
		twist = Twist()

		pose.position.x = data['pose']['position']['x']
		pose.position.y = data['pose']['position']['y']
		pose.position.z = data['pose']['position']['z']

		pose.orientation.x = data['pose']['orientation']['x']
		pose.orientation.y = data['pose']['orientation']['y']
		pose.orientation.z = data['pose']['orientation']['z']
		pose.orientation.w = data['pose']['orientation']['w']

		twist.linear.x = data['twist']['linear']['x']
		twist.linear.y = data['twist']['linear']['y']
		twist.linear.z = data['twist']['linear']['z']

		twist.angular.x = data['twist']['angular']['x']
		twist.angular.y = data['twist']['angular']['y']
		twist.angular.z = data['twist']['angular']['z']

		for _ in range(len(self.msg.name)):
			self.msg.pose.append(pose)
			self.msg.twist.append(twist)

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		rospy.loginfo_once('Publishing the following message on topic {}:\n{}'.format(
			PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			self.rate.sleep()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = GazeboLinkStatesPublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
