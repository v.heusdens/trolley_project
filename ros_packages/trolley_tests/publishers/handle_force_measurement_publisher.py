#!/usr/bin/env python

"""This script posts test data to the /trolley/plugins/handle_force_measurement topic"""

import rospy

from geometry_msgs.msg import Vector3

from toolbox.ros import load_config, wait_on_simulation_start

PKG = 'trolley_tests'
NODE_NAME = 'handle_force_measurement_publisher'
PUBLISHER_TOPIC = '/trolley/plugins/handle_force_measurement'


#^ CLASS #######################################
class HandleForceMeasurementPublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_tests = load_config('trolley_tests')
		config_sensors = load_config('trolley_sensors')

		_input_force = config_tests['input_force']
		self.update_rate = config_sensors['update_rate']
		rospy.loginfo('publish rate = {} Hz'.format(self.update_rate), logger_name=NODE_NAME)

		self.duration = False
		self.wait_on_sim = False

		if rospy.has_param('~duration'):
			self.duration = rospy.get_param('~duration')
			rospy.loginfo('publish duration set to {} s'.format(self.duration), logger_name=NODE_NAME)

		if rospy.has_param('~wait_on_simulation_start'):
			self.wait_on_sim = rospy.get_param('~wait_on_simulation_start')

		#^ init msg
		self.msg = Vector3()
		self.msg.x = _input_force
		self.msg.y = 0
		self.msg.z = 0

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Vector3, queue_size=1)
		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)

		#^ shutdown hook
		rospy.on_shutdown(self.shutdown)

	#^ #######################################
	def _wait_on_simulation_start(self):
		"""Function which terminates as soon as simulation time starts running"""
		wait_on_simulation_start(self.update_rate, NODE_NAME=NODE_NAME)

	#^ #######################################
	def _publish(self):
		rospy.loginfo('Publishing the following message on topic {}:\n{}'.format(PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		if self.duration:
			_duration = rospy.get_time() + self.duration

		rate = rospy.Rate(self.update_rate)
		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			rate.sleep()

			if self.duration and rospy.get_time() > _duration:
				return

	#^ #######################################
	def shutdown(self):
		rospy.loginfo('Shutdown; setting handle force to 0', logger_name=NODE_NAME)
		self.msg.x = 0
		self.msg.y = 0
		self.msg.z = 0
		self.pub.publish(self.msg)

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		if self.wait_on_sim:
			self._wait_on_simulation_start()
		self._publish()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = HandleForceMeasurementPublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
