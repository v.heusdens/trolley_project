#!/usr/bin/env python

"""This script posts test data to the /trolley/controller/disturbance_estimate_unfiltered topic"""

import rospy

from std_msgs.msg import Float64
from trolley_msgs.srv import SetDisturbanceEstimate, SetDisturbanceEstimateResponse

from toolbox.ros import load_config

PKG = 'trolley_tests'
NODE_NAME = 'disturbance_estimate_unfiltered_publisher'
PUBLISHER_TOPIC = '/trolley/controller/disturbance_estimate_unfiltered'
SERVICE_TOPIC = '/trolley/tests/set_disturbance_estimate_unfiltered'


#^ CLASS #######################################
class DisturbanceEstimateUnfilteredPublisher():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		config_controller = load_config('trolley_controller')

		self.update_rate = config_controller['update_rate']
		rospy.loginfo('publish rate = {} Hz'.format(self.update_rate), logger_name=NODE_NAME)

		if rospy.has_param('~disturbance_estimate_unfiltered'):
			disturbance_estimate_unfiltered = rospy.get_param('~disturbance_estimate_unfiltered')
			rospy.loginfo('unfiltered disturbance estimate set to {} N from param server'.format(
				disturbance_estimate_unfiltered),
				logger_name=NODE_NAME)
		else:
			disturbance_estimate_unfiltered = 0
			rospy.loginfo(
				'handle force estimate set to default of {} N'.format(disturbance_estimate_unfiltered),
				logger_name=NODE_NAME)

		#^ init msg
		self.msg = Float64()
		self.msg.data = disturbance_estimate_unfiltered

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(PUBLISHER_TOPIC, Float64, queue_size=1)
		rospy.Service(SERVICE_TOPIC, SetDisturbanceEstimate, self.srv_handle)

		rospy.loginfo('Publisher initiated for {}'.format(PUBLISHER_TOPIC), logger_name=NODE_NAME)
		rospy.loginfo('Service initiated for {}'.format(SERVICE_TOPIC), logger_name=NODE_NAME)

	#^ #######################################
	def srv_handle(self, value):
		"""Service call handler"""
		self.msg.data = value.disturbance_estimate
		rospy.loginfo(
			'SetDisturbanceEstimate service called: unfiltered disturbance estimate set to {}'.format(value),
			logger_name=NODE_NAME)
		return SetDisturbanceEstimateResponse()

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		rospy.loginfo_once('Publishing the following message on topic {}:\n{}'.format(
			PUBLISHER_TOPIC, self.msg),
			logger_name=NODE_NAME)

		r = rospy.Rate(self.update_rate)
		while not rospy.is_shutdown():
			self.pub.publish(self.msg)
			r.sleep()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = DisturbanceEstimateUnfilteredPublisher()
		node.spin()
	except rospy.ROSInterruptException:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
