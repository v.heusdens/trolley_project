#!/usr/bin/env python

"""This scripts obtains (elements of) specified messages and redericts them to a new topic"""

import re
import rospy
import rostopic

from std_msgs.msg import Float64

PKG = 'trolley_tests'
NODE_NAME = 'remap_msg_element'


#^ CLASS #######################################
class RemapMsgElement():

	def __init__(self):
		rospy.init_node(NODE_NAME)

		#^ init params
		try:
			source_topic = rospy.get_param('~source_topic')
			source_msg = rospy.get_param('~source_msg')
			self.target_topic = rospy.get_param('~target_topic')
			pub_queue = rospy.get_param('~pub_queue')
		except KeyError as e:
			print('Incorrect settings provided: {}'.format(e))
			raise

		sub_msg_type, _, _ = rostopic.get_topic_class(source_topic, blocking=True)
		self.source_msg_elem = [x for x in re.split("\.|\[", source_msg) if x not in sub_msg_type._type.split("/")]

		#^ init publishers/subscribers/services
		self.pub = rospy.Publisher(self.target_topic, Float64, queue_size=pub_queue)
		rospy.Subscriber(source_topic, sub_msg_type, self._callback)

		rospy.loginfo('Publisher initiated for {}'.format(self.target_topic), logger_name=NODE_NAME)
		rospy.loginfo('Subscribed to {}'.format(source_topic), logger_name=NODE_NAME)

	#^ #######################################
	def _callback(self, msg):
		rospy.loginfo_once('Message received on topic {}'.format(source_topic), logger_name=NODE_NAME)

		msg_mapped = msg
		for elem in self.source_msg_elem:
			if elem[-1] == "]":
				msg_mapped = msg_mapped[int(elem[:-1])]
			else:
				msg_mapped = getattr(msg_mapped, elem)
		try:
			self.pub.publish(msg_mapped)
		except rospy.ROSException as e:
			if e.message == 'publish() to a closed topic':
				rospy.logwarn(e.message, logger_name=NODE_NAME)
			else:
				raise

	#^ #######################################
	def spin(self):
		rospy.loginfo("{} started".format(NODE_NAME), logger_name=NODE_NAME)
		while not rospy.is_shutdown():
			rospy.spin()


#^ MAIN #######################################
if __name__ == '__main__':
	try:
		node = RemapMsgElement()
		node.spin()
	except rospy.ROSInterruptException as e_ros:
		rospy.loginfo("Interrupted", logger_name=NODE_NAME)
