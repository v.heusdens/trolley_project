#!/usr/bin/env python

import logging
import math
import sympy as sp
import unittest

from toolbox.params import ParamsNumeric
from toolbox.ros import load_config
from toolbox.transform import convert_string_to_sympy


#^ #######################################
class TrolleyTestsNormalForce(unittest.TestCase):

	def setUp(self):
		config_tests = load_config('trolley_tests')
		self.model = config_tests['model_Fn']

		logging.warn('\n' + self._testMethodName + ': COM mid_frame')

	def test_Fn_stationary_for_frame_horizontal_without_friction(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.L_h = n.R_w  #@ -> beta = 0

		n.d_xw = 0
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = 0
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		normal_force = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_Fn_total = g * (2 * m_w + 0.5 * (m_f + m_b))
		expected_Fn_per_wheel = 0.5 * expected_Fn_total

		#^ --------------------------------------
		self.assertAlmostEqual(normal_force, expected_Fn_per_wheel)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected normal force per wheel: {}; normal force from model: {}'.format(
			expected_Fn_per_wheel, normal_force))

	def test_Fn_stationary_for_frame_horizontal_with_friction(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.L_h = n.R_w  #@ -> beta = 0

		n.d_xw = 0
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = 0.5
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		normal_force = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_Fn_total = g * (2 * m_w + 0.5 * (m_f + m_b))
		expected_Fn_per_wheel = 0.5 * expected_Fn_total

		#^ --------------------------------------
		self.assertAlmostEqual(normal_force, expected_Fn_per_wheel)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected normal force per wheel: {}; normal force from model: {}'.format(
			expected_Fn_per_wheel, normal_force))

	def test_Fn_stationary_for_frame_45_degrees_without_friction(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.beta = sp.pi / 4.  #@ -> beta 45*

		n.d_xw = 0
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = 0
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		normal_force = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_Fn_total = g * (2 * m_w + 0.5 * (m_f + m_b))
		expected_Fn_per_wheel = 0.5 * expected_Fn_total

		#^ --------------------------------------
		self.assertAlmostEqual(normal_force, expected_Fn_per_wheel)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected normal force per wheel: {}; normal force from model: {}'.format(
			expected_Fn_per_wheel, normal_force))

	def test_Fn_stationary_for_frame_45_degrees_with_friction(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.beta = sp.pi / 4.  #@ -> beta 45*

		n.d_xw = 0
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = 0.5
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		normal_force = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_Fn_total = g * (2 * m_w + 0.5 * (m_f + m_b))
		expected_Fn_per_wheel = 0.5 * expected_Fn_total

		#^ --------------------------------------
		self.assertAlmostEqual(normal_force, expected_Fn_per_wheel)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected normal force per wheel: {}; normal force from model: {}'.format(
			expected_Fn_per_wheel, normal_force))

	#^ ----------------------------------------------------------------------------
	def test_Fn_accelerating_for_frame_horizontal_with_friction_without_input(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.L_h = n.R_w  #@ -> beta = 0

		n.d_xw = 1.  #@ for friction direction
		n.d_beta = 0
		n.mu = 0.5  #@
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		normal_force = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_Fn_total = g * (2 * m_w + 0.5 * (m_f + m_b))
		expected_Fn_per_wheel = 0.5 * expected_Fn_total

		#^ --------------------------------------
		self.assertAlmostEqual(normal_force, expected_Fn_per_wheel)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected normal force per wheel: {}; normal force from model: {}'.format(
			expected_Fn_per_wheel, normal_force))

	def test_Fn_accelerating_for_frame_horizontal_with_friction_with_Fhx(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		F_hx = 5.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.L_h = n.R_w  #@ -> beta 0

		n.d_xw = 1.  #@ for friction direction
		n.d_beta = 0
		n.mu = 0.5  #@
		n.gamma = 0
		n.T_m = 0
		n.F_hx = F_hx

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		normal_force = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		expected_Fn_per_wheel = (1 / 4. * (m_f + m_b) + m_w) * g

		#^ --------------------------------------
		self.assertAlmostEqual(normal_force, expected_Fn_per_wheel)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected normal force per wheel: {}; normal force from model: {}'.format(
			expected_Fn_per_wheel, normal_force))


#^ #######################################
class TrolleyTestsAcceleration(unittest.TestCase):

	def setUp(self):
		config_tests = load_config('trolley_tests')
		self.model = config_tests['model_ddxw']

	def test_a_stationary_for_frame_45_degrees_no_friction_no_inputs(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.beta = sp.pi / 4.  #@ -> beta 45*

		n.d_xw = 0
		n.d_beta = 0
		n.mu = 0
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		acceleration = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_acceleration = 0

		#^ --------------------------------------
		self.assertAlmostEqual(acceleration, expected_acceleration)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected acceleration: {}; acceleration from model: {}'.format(expected_acceleration, acceleration))

	def test_a_moving_for_frame_45_degrees_no_friction_no_inputs(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.beta = sp.pi / 4.  #@ -> beta 45*

		n.d_xw = 1
		n.d_beta = 0
		n.mu = 0
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		acceleration = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_acceleration = 0

		#^ --------------------------------------
		self.assertAlmostEqual(acceleration, expected_acceleration)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected acceleration: {}; acceleration from model: {}'.format(expected_acceleration, acceleration))

	def test_a_moving_for_frame_45_degrees_no_friction_with_Fhx(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		F_hx = 8.

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.beta = sp.pi / 4.  #@ -> beta 45*

		n.d_xw = 1
		n.d_beta = 0
		n.mu = 0
		n.gamma = 0
		n.F_hx = F_hx
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		acceleration = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_acceleration = F_hx / (m_f + m_b + 3 * m_w)

		#^ --------------------------------------
		self.assertAlmostEqual(acceleration, expected_acceleration)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected acceleration: {}; acceleration from model: {}'.format(expected_acceleration, acceleration))

	def test_a_moving_for_frame_horizontal_with_friction_no_Fhx(self):  #@
		#% Arrange: setup of variables
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		mu = 0.1

		#^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  #@ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  #@ -> middle of the frame
		n.beta = 0  #@ -> beta 0*

		n.d_xw = 1
		n.d_beta = 0
		n.mu = mu
		n.gamma = 0
		n.F_hx = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g

		#% Act: use the function/class and get its result
		acceleration = convert_string_to_sympy(self.model, n)

		#% Assert: check if the result is correct
		#^ Expectation: 1/2 frame weight on wheels, acc should not have effect due to horizontal frame
		#@ 2* wheel mass + 0.5 * mass frame + bag
		expected_normal_force = (m_f / 4 + m_b / 4 + m_w) * g
		expected_rolling_friction_force = -mu * expected_normal_force
		expected_acceleration = 2 * expected_rolling_friction_force / (m_f + m_b + 3 * m_w)

		#^ --------------------------------------
		self.assertAlmostEqual(acceleration, expected_acceleration)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected acceleration: {}; acceleration from model: {}'.format(expected_acceleration, acceleration))


#^ #######################################
class TrolleyTestsHandleForce(unittest.TestCase):

	def setUp(self):
		config_controller = load_config("trolley_controller")
		self.model = config_controller['model_Fhx']

		logging.warn('\n' + self._testMethodName + ': COM mid_frame')

	def test_fhx_stationary_no_input_forces(self):
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		mu = 0.1

		# ^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  # @ -> bag COM in center trolley frame
		n.beta = sp.pi / 4.  # @ -> beta 45*

		n.d_xw = 0
		n.dd_xw = 0
		n.d_beta = 0
		n.gamma = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g
		n.mu = mu

		# % Act: use the function/class and get its result
		handle_force = convert_string_to_sympy(self.model, n)

		# % Assert: check if the result is correct
		# @ 2* wheel mass + 0.5 * mass frame + bag
		expected_handle_force = 0

		# ^ --------------------------------------
		self.assertAlmostEqual(handle_force, expected_handle_force)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected handle force: {}; handleforce from model: {}'.format(expected_handle_force, handle_force))

	def test_fhx_stationary_with_acceleration(self):
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		mu = 0.1
		dd_xw = 1

		# ^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  # @ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  # @ -> middle of the frame
		n.beta = sp.pi / 4.  # @ -> beta 45*

		n.d_xw = 0
		n.d_beta = 0
		n.gamma = 0
		n.T_m = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g
		n.mu = mu
		n.dd_xw = dd_xw

		# % Act: use the function/class and get its result
		handle_force = convert_string_to_sympy(self.model, n)

		# % Assert: check if the result is correct
		# @ 2* wheel mass + 0.5 * mass frame + bag
		expected_handle_force = dd_xw * (m_f + m_b + 3 * m_w)

		# ^ --------------------------------------
		self.assertAlmostEqual(handle_force, expected_handle_force)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected handle force: {}; handleforce from model: {}'.format(expected_handle_force, handle_force))

	def test_fhx_stationary_no_acceleration_with_motor_torque(self):
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		mu = 0.1
		R_w = 0.15
		T_m = 3

		# ^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  # @ -> bag COM in center trolley frame
		n.L_wf = 0.5 * n.L_wh  # @ -> middle of the frame
		n.beta = sp.pi / 4.  # @ -> beta 45*

		n.d_xw = 0
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = mu
		n.gamma = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g
		n.R_w = R_w
		n.T_m = T_m

		# % Act: use the function/class and get its result
		handle_force = convert_string_to_sympy(self.model, n)

		# % Assert: check if the result is correct
		# @ 2* wheel mass + 0.5 * mass frame + bag
		expected_handle_force = -2 * T_m / R_w

		# ^ --------------------------------------
		self.assertAlmostEqual(handle_force, expected_handle_force)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected handle force: {}; handleforce from model: {}'.format(expected_handle_force, handle_force))

	def test_fhx_moving_no_acceleration(self):
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		mu = 0.1
		T_m = 6
		R_w = 0.15
		beta = math.pi / 4.
		L_wh = 0.8

		# ^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  # @ -> bag COM in center trolley frame
		n.beta = beta  # @ -> beta 45*

		n.d_xw = 1
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = mu
		n.gamma = 0

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g
		n.T_m = T_m
		n.R_w = R_w
		n.L_wh = L_wh
		n.L_wf = 0.5 * L_wh  # @ -> middle of the frame

		# % Act: use the function/class and get its result
		handle_force = convert_string_to_sympy(self.model, n)

		# % Assert: check if the result is correct
		friction_force = (m_f / 2 + m_b / 2 + 2 * m_w) * g * mu
		motor_force = -2 * T_m * (1 / R_w - mu / (L_wh * math.cos(beta)))
		expected_handle_force = (friction_force + motor_force) / (1 + mu * math.tan(beta))

		# ^ --------------------------------------
		self.assertAlmostEqual(handle_force, expected_handle_force)

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected handle force: {}; handleforce from model: {}'.format(expected_handle_force, handle_force))

	def test_fhx_moving_no_torque_no_acceleration_with_gamma(self):
		m_w = 1.
		m_f = 2.
		m_b = 3.
		g = 10.
		mu = 0.1
		R_w = 0.15
		beta = math.pi / 4.  # @ -> beta 45*
		L_wh = 0.8
		gamma = math.radians(10)

		# ^ --------------------------------------
		n = ParamsNumeric()
		n.W_wf = 0  # @ -> bag COM in center trolley frame
		n.beta = beta

		n.d_xw = 1
		n.dd_xw = 0
		n.d_beta = 0
		n.mu = mu
		n.gamma = gamma

		n.m_w = m_w
		n.m_f = m_f
		n.m_b = m_b
		n.g = g
		n.T_m = 0
		n.R_w = R_w
		n.L_wh = L_wh
		n.L_wf = 0.5 * L_wh  # @ -> middle of the frame

		# % Act: use the function/class and get its result
		handle_force = convert_string_to_sympy(self.model, n)

		# % Assert: check if the result is correct
		m_ft = m_f + m_b
		m_tot = m_ft + 2 * m_w
		friction_force = g * (-mu * math.cos(beta + gamma) / (2 * math.cos(beta)) * m_ft +
			(math.sin(gamma) + mu * math.cos(gamma)) * m_tot)
		expected_handle_force = friction_force / (1 + mu)

		# ^ --------------------------------------

		logging.warn('\n' + self._testMethodName
			+ ':\nexpected handle force: {}; handleforce from model: {}'.format(expected_handle_force, handle_force))
		self.assertAlmostEqual(handle_force, expected_handle_force)


#^ MAIN #######################################
if __name__ == "__main__":
	#unittest.main()
	import rosunit
	rosunit.unitrun('trolley_tests', "test_config_models", __name__)
