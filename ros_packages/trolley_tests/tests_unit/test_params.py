#!/usr/bin/env python

import unittest

from toolbox.params import ParamsNumeric


class TestParamsNumeric(unittest.TestCase):

	def test_beta(self):
		#% Arrange
		n = ParamsNumeric()
		n.L_wh = 1
		n.R_w = 0.1
		n.L_h = 0.1  # L_h = R_w -> beta = 0

		#% Act
		beta = n.beta

		#% Assert
		self.assertEqual(beta, 0)


if __name__ == "__main__":
	#unittest.main()
	import rosunit
	rosunit.unitrun('trolley_tests', "test_params_numeric", TestParamsNumeric)
