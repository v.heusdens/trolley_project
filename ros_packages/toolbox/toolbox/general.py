"""Submodule of 'Toolbox' with general functions"""
import numpy as np
import os
import sympy as sp
import time
from collections import Iterable

from toolbox.transform import split_strings_in_list


#^ #######################################
def make_list(instance):  #~function make_list
	"""Get 'instance' as a list if it isn't a list or a tuple.
	Args:		instance (-): instance to check
	Returns:	list or tuple: if 'instance' is a list or tuple, return unchanged. Otherwise return as list.
	."""
	if not isinstance(instance, Iterable) or isinstance(instance, str):
		return [instance]
	else:
		return instance


#^ #######################################
def check():  #~function check
	"""Used for checking if package can be imported"""
	print('Succes: toolbox module loaded properly!')
	return


#^ #######################################
def beep(  #~function beep
		cycle_duration=10,
		beep_ratio=0.05,
		abs_beep_duration=None,
		freq=500,
		volume=0.5,
		once=False):
	"""
	cycle_duration: duration of beep cycle in seconds  
	beep_ratio: part of beep cycle that actually beeps (0-1)
	abs_beep_duration: when set, this absolute time in seconds is used for beep
	freq: frequency of beeping sound in Hz
	volume: volume of beeping sound % of system volume
	once: If set to True, only beep once and return immediately
	"""

	if abs_beep_duration is not None:
		dur_beep = abs_beep_duration
	else:
		dur_beep = cycle_duration * beep_ratio

	dur_silence = cycle_duration - dur_beep

	try:
		while True:
			os.system('play -nq -t alsa synth {} sine {} vol {} fade {} 0'.format(
				dur_beep, freq, volume, 0.1 * dur_beep, 0.05 * dur_beep))
			if once:
				return
			time.sleep(dur_silence)
	except KeyboardInterrupt:
		pass


#^ #######################################
def print_list(lst):  #~function print_list
	if isinstance(lst, (list, tuple, set, np.ndarray)):
		for entry in lst:
			print(entry)
	elif isinstance(lst, dict):
		for key, value in lst.items():
			print('{}: {}'.format(key, value))
	else:
		raise TypeError('Error: {} not supported'.format(type(lst)))


#^ #######################################
def setattr_multi(obj, **kwargs):  #~function setattr_multi
	"""
    Sets multiple attributes of obj at once.
    Use:
            setattr_multi(obj,
						  a = 1,
						  b = 2,
						  ...
            )
    """

	for k, v in kwargs.items():
		setattr(obj, k, v)


#^ #######################################
def getattr_multi(object, names):  #~function getattr_multi
	"""
    object =    object to get attributes from
    names =     string of attribute names, separated by a space  
    """
	names = names.split()
	attributes = [getattr(object, name) for name in names]
	return attributes


#^ #######################################
def define_sympy_function_multi(names, dep_var):  #~function define_sympy_function_multi
	"""
    Makes multiple sympy functions depending on the same variable at once. 
    names =     iterable with function name strings  
    dep_var =   symbolic variable or string
    """
	if isinstance(dep_var, str):
		dep_var = sp.symbols(dep_var)

	names = split_strings_in_list(names)

	functions = [sp.Function(name)(dep_var) for name in names]
	return functions


#^ #######################################
def define_sympy_derivative_multi(functions, dep_var, amount=1):  #~function define_sympy_derivative_multi
	"""
    Makes multiple sympy derivatives depending on the same variables at once. 
    functions =     iterable with functions to take derivative from 
    dep_var =   symbolic variable or string
    """
	if isinstance(dep_var, str):
		dep_var = sp.symbols(dep_var)

	derivatives = [sp.Derivative(function, dep_var, amount) for function in functions]
	return derivatives


## MAIN #######################################
if __name__ == "__main__":
	test0 = (1, 2, 3)
	test1 = [1, 2, 3]
	test2 = None
	test3 = 1
	test4 = "test"
	print(make_list(test0))
	print(make_list(test1))
	print(make_list(test2))
	print(make_list(test3))
	print(make_list(test4))
