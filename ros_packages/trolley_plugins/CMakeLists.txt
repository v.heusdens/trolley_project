cmake_minimum_required(VERSION 2.8.3)
project(trolley_plugins)

## Add support for C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++11)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS
  roscpp
  gazebo_ros
  trolley_msgs
)

# Depend on system install of Gazebo
find_package(gazebo REQUIRED)

catkin_package(
  DEPENDS
    roscpp
    gazebo_ros
    trolley_msgs
)

###########
## Build ##
###########

link_directories(${GAZEBO_LIBRARY_DIRS})
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})
list(APPEND CMAKE_CXX_FLAGS "${GAZEBO_CXX_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GAZEBO_CXX_FLAGS}")

# apply_force_plugin
add_library(apply_force_plugin src/apply_force_plugin.cc)
target_link_libraries(apply_force_plugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})

# rolling_friction_plugin
add_library(rolling_friction_plugin src/rolling_friction_plugin.cc)
target_link_libraries(rolling_friction_plugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES} yaml-cpp)

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(
# include
#  ${catkin_INCLUDE_DIRS}
# )

