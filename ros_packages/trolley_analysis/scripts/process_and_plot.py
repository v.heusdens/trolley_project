from trolley_analysis.find import filter_most_recent_per_group, filter_names, get_filenames, get_filenames_unprocessed, get_path_to_sim_data
from trolley_analysis.plot import plot_sim_data
from trolley_analysis.process import process_sim_data

#^ #######################################
if __name__ == "__main__":
	#% Manual settings #######################################
	#^ Names #######################################
	#folder = 'testdata'

	#id = 'handle_plugin_spin'

	##data_file_features = ""
	##situation_ids = ['slope']
	#situation_ids = []
	#exclude = ""

	folder = 'final'
	id = ""
	suffix = ""
	exclude = ""
	data_file_features = ""

	#^ Ranges #######################################
	x_axis_ranges = None
	y_axis_ranges = [None, [-10, 10], [0, 2.5]]

	#^ Save/show #######################################
	save = True
	show = False

	#% #######################################
	if len(id) != 0 and id[-1] != '_':
		id_feat = id + '_'
	else:
		id_feat = id

	if len(id) != 0 and id[0] != '_':
		id_name = '_' + id
	else:
		id_name = id

	data_file_features = [id_feat + 'no_control_no_dist', id_feat + 'no_control_slope', id_feat + 'dob_slope']
	filename = 'sim_data_plot_' + folder + id_name + '.pdf'
	path = get_path_to_sim_data(subfolder=folder)

	#% Processing #######################################

	filenames = filter_names(get_filenames(path=path), include=data_file_features, exclude=exclude)
	filename_nominal = filter_most_recent_per_group(filter_names(filenames, include='no_control_no_dist'))
	filenames_recent = filter_most_recent_per_group(filter_names(filenames,
		exclude='no_control_no_dist'))  #@ most recent unprocessed files per simulation type
	filenames_to_process = filter_names(get_filenames_unprocessed(path=path), include=filename_nominal + filenames_recent)

	if len(filenames_to_process) > 0:
		filenames_to_process.extend(filename_nominal)

	process_sim_data(filenames=filenames_to_process, path=path)

	#% Plotting #######################################
	if folder == 'final':
		plot_sim_data(data_folder=path, filename=filename, save=save, show=show)
	else:
		plot_sim_data(data_folder=path,
			data_file_features=data_file_features,
			situation_ids=situation_ids,
			filename=filename,
			x_axis_ranges=x_axis_ranges,
			y_axis_ranges=y_axis_ranges,
			save=save,
			show=show)
