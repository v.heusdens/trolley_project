"""Generate bode-plot for controller Q-filter
"""
import numpy as np

from toolbox.packages import load_q_filter
from trolley_analysis.plot import bode_plot

num, denom = load_q_filter()
bode_plot(num, denom, cutoff_freq=5 * 2 * np.pi)
