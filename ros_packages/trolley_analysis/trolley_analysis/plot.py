import matplotlib.font_manager as fontmanager
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os
from pandas.plotting import register_matplotlib_converters
from scipy import signal

from toolbox.compute import mean
from trolley_analysis.files import load_dataframes_from_csv
from trolley_analysis.find import filter_dataframe, filter_dict, filter_names, get_filenames_processed_latest, get_path_to_sim_data, get_path_to_sim_plots

register_matplotlib_converters()  #@ Enable plotting of pandas datetime

plt.style.use('seaborn')


## #######################################
def bode_plot(num, denom, cutoff_freq=None, freq_range=None, savename='bode_plot', save=False, show=True):  #~function bode_plot
	"""Generate bode-plot for a continuous filter.
		Args:
			num (list): filter numerator
			denom (list): filter denominator
			cutoff_freq (float, optional): Cutoff frequency in [rad/s]. Defaults to None.
			freq_range (iterable, optional): list containing either the plot frequency limits (list length == 2) or all plot frequencies (list length > 2). 'None' to determine automatically. Defaults to None.
		."""
	def closest(lst, K): 
		lst = np.asarray(lst) 
		idx = (np.abs(lst - K)).argmin() 
		return lst[idx], idx

	#% Calculate
	if freq_range is None:
		freqs = None
	elif len(freq_range) == 2:
		freqs = np.logspace(freq_range[0], freq_range[1], 10 * (freq_range[1] - freq_range[0]) + 1)
	else:
		freqs = freq_range

	w, mag, phase = signal.bode((num, denom), freqs)

	#% Plot
	plot_color = 'tab:blue'
	cutoff_color = 'tab:red'
	hp_color_phase = 'tab:green'
	hp_color_mag = 'tab:green'


	fig, [ax1, ax2] = plt.subplots(2, 1, sharex=True)
	#plt.suptitle('Bode plot')

	#& Magnitude
	ax1.semilogx(w, mag, color=plot_color, label='filter response')
	
	if cutoff_freq is not None:
		ax1.vlines(cutoff_freq,
			ax1.get_ylim()[0],
			ax1.get_ylim()[1],
			color=cutoff_color,
			linestyle='dashed',
			linewidth=1,
			label='cutoff frequency ({:.0f} Hz)'.format(cutoff_freq / (2. * np.pi)))
	
	ax1.hlines(-3, w[0], w[-1], color=hp_color_mag, linewidth=1, linestyle='dashed', label='half-power magnitude (-3 dB)')

	ax1.set_ylabel('Magnitude [dB]')
	ax1.grid(which='minor', linestyle=':')
	ax1.legend(loc='lower left')

	#& Phase
	ax2.semilogx(w, phase, c=plot_color, label='_nolegend_')

	if cutoff_freq is not None:
		ax2.vlines(cutoff_freq,
			ax2.get_ylim()[0],
			ax2.get_ylim()[1],
			color=cutoff_color,
			linestyle='dashed',
			linewidth=1,
			label='_nolegend_'.format(cutoff_freq))

	_, i = closest(w, cutoff_freq)
	ax2.hlines(phase[i], w[0], w[-1], color=hp_color_phase, linewidth=1, linestyle='dashed', label='half-power phase')
	
	ax2.set_ylabel('Phase [deg]')
	ax2.set_xlabel('Frequency [rad/s]')
	ax2.yaxis.set_major_locator(ticker.MultipleLocator(45))
	ax2.grid(which='minor', linestyle=':')
	ax2.legend(loc='lower left')

	if save:
		if not '.' in savename:
			savename += '.pdf'
		path = get_path_to_sim_plots()
		filepath = os.path.join(path, savename)
		fig.savefig(filepath, bbox_inches='tight')
		print('Plot saved to {}'.format(filepath))

	if show:	
		plt.show()


## #######################################
def test_plot():
	_, situation_info_set, _ = get_plot_info()

	data_path = get_path_to_sim_data(subfolder='analysis')
	amount = 3
	situation_features = ['no_control_slope', 'dob_slope']
	situation_prefix = 'slope_sharp_'
	situation_info = situation_info_set['slope']
	#measure_features = [
	#	'imu_measurement.linear_acceleration.x', 'disturbance_estimate_unfiltered', 'disturbance_estimate_filtered'
	#]
	measure_features = ['imu_measurement.linear_acceleration.x', 'performance_error']

	#@ Get filenames
	include = [situation_prefix + '_' + feature for feature in situation_features]
	filenames = get_filenames_processed_latest(path=data_path, include=include, amount=amount)
	sim_set = load_dataframes_from_csv(filenames,
		path=data_path)  #@ get dict with df's belonging to current disturbance situation

	fig, axes = plt.subplots(nrows=3,
		ncols=2,
		squeeze=False,
		sharex='col',
		sharey='row',
		figsize=np.array([np.sqrt(2), 1]) * 10)

	colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
	i = j = 0
	for j in range(axes.shape[1]):
		situation_set = filter_dict(sim_set, include=situation_features[j])

		x_axis_topic = filter_names(situation_set.values()[0].columns,
			include=situation_info.x_axis_feature)  #@ get column name of x-axis data

		for df in situation_set.values():
			df.set_index(x_axis_topic, inplace=True)  #@ set x-axis data as index
			df.index = situation_info.x_axis_formatter(df.index)  #@ Format

		for i in range(axes.shape[0]):
			counter = 0

			print('plot {}:'.format((i, j)))
			for name, df in situation_set.items():
				print('- {} - measure: {}'.format(name, measure_features[i]))

				data = filter_dataframe(df, measure_features[i]).squeeze()
				data.plot(ax=axes[i][j], label=name, color=colors[counter])  #@ plot

				#% format
				axes[i][j].set_xlim([0, 30])
				if i == 0:  #@ titles
					axes[i][j].set_title(situation_features[j])
				elif i == axes.shape[0] - 1:  #@ x-axis labels
					axes[i][j].set_xlabel(situation_info.x_axis_label)
				if j == 0:  #@ y-axis labels
					axes[i][j].set_ylabel(measure_features[i])

				if i in [1, 2]:
					axes[i][j].set_ylim([-30, 30])

				counter += 1

	legend_handles = []
	for i in range(len(measure_features)):
		handle = mlines.Line2D([0], [0], label=i, color=colors[i])
		legend_handles.append(handle)

	fig.legend(title='samples', handles=legend_handles)
	plt.show()
	print()


## #######################################
#region  final plot
#region #& CLASSES
class SampleInfo:  #~class SampleInfo

	def __init__(self, id, label, feature, color, linestyle, alpha=1):
		self.id = id
		self.label = label
		self.feature = feature
		self.color = color
		self.alpha = alpha
		self.linestyle = linestyle


class SituationInfo:

	def __init__(self, id, col_label, feature, x_axis_type):
		self.id = id
		self.feature = feature
		self.col_label = col_label
		self.x_axis_type = x_axis_type
		self.x_axis_feature = None
		self.x_axis_label = None
		self.x_axis_spacing = None  #@ unit distance between major ticks
		self.x_axis_minor = None  #@ how many minor tick areas between major ticks
		self.x_axis_formatter = None

		self._set_x_axis_info()

	def _set_x_axis_info(self):
		if self.x_axis_type == 'time':
			self.x_axis_label = 'time [s]'
			self.x_axis_feature = 'time'
			self.x_axis_spacing = 1
			self.x_axis_minor = 2
			self.x_axis_formatter = self._fmt_time
		elif self.x_axis_type == 'dist':
			self.x_axis_label = 'distance [m]'
			self.x_axis_feature = '/analysis/traveled_distance'
			self.x_axis_spacing = 5
			self.x_axis_minor = 2
			self.x_axis_formatter = self._fmt_dist
		else:
			raise ValueError("Wrong x-axis type specified. Should be 'time' or 'dist'")

	@staticmethod
	def _fmt_time(t):
		return t.second + t.microsecond / 1.e6

	@staticmethod
	def _fmt_dist(dist):
		return dist


class MeasureInfo:

	def __init__(self, id, label, feature, sample_ids, y_axis_spacing, y_axis_minor):
		self.id = id
		self.label = label
		self.feature = feature
		self.sample_ids = sample_ids
		self.y_axis_spacing = y_axis_spacing
		self.y_axis_minor = y_axis_minor


#endregion CLASSES


#^ #######################################
def get_plot_info():
	"""Get information about plot axes and data.
		Returns:
			dict: sample info (lines), situation info (cols), measure info (rows).
	."""
	#region #% Samples
	sample_info_set = {}

	#@ No control
	id = 'no_control'
	sample_info_set[id] = SampleInfo(  #
		id=id,  #
		label='No control',
		feature='no_control',
		color='tab:blue',
		alpha=0.75,
		linestyle='solid')

	#@ DOB control
	id = 'dob'
	sample_info_set[id] = SampleInfo(  #
		id=id,  #
		label='Disturbance observer control',
		feature='dob',
		color='tab:orange',
		alpha=0.75,
		linestyle='solid')

	#@ Handle force
	id = 'handle_force'
	sample_info_set[id] = SampleInfo(  #
		id=id,  #
		label='Handle force',
		feature=None,
		color='tab:green',
		linestyle='solid')

	#@ Motor torque
	id = 'motor_torque'
	sample_info_set[id] = SampleInfo(  #
		id=id,  #
		label='Motor torque',
		feature=None,
		color='tab:red',
		linestyle='solid')

	#@ Nominal behavior
	id = 'nom'
	sample_info_set[id] = SampleInfo(  #
		id=id,  #
		label='Nominal behavior',
		feature=None,
		color='tab:gray',
		linestyle=(0, (1.5, 1.5)))
	#endregion Samples

	#region #% Situations
	situation_info_set = {}

	#@ No disturbance
	id = 'no_dist'
	situation_info_set[id] = SituationInfo(  #
		id=id,  #
		col_label='No disturbance (nominal)',
		feature='no_dist',
		x_axis_type='time')

	#@ Added mass
	id = 'mass'
	situation_info_set[id] = SituationInfo(  #
		id=id,  #
		col_label='Added mass',
		feature='mass',
		x_axis_type='time')

	#@ Slope
	id = 'slope'
	situation_info_set[id] = SituationInfo(  #
		id=id,  #
		col_label='Slope',
		feature='slope',
		x_axis_type='dist')
	#endregion Situations

	#region #% Measures
	measure_info_set = {}

	#@ Input force
	id = 'input_force'
	measure_info_set[id] = MeasureInfo(  #
		id=id,  #
		label='Input force [N]',
		feature='/sensors/handle_force',
		sample_ids=['handle_force'],
		y_axis_spacing=5,
		y_axis_minor=2)

	#@ Motor torque
	id = 'motor_torque'
	measure_info_set[id] = MeasureInfo(  #
		id=id,  #
		label='Motor torque [Nm]',
		feature='/actuators/motor_torque',
		sample_ids=['motor_torque'],
		y_axis_spacing=1,
		y_axis_minor=2)

	#@ Acceleration
	id = 'acc'
	measure_info_set[id] = MeasureInfo(  #
		id=id,  #
		label='Acceleration [$\mathit{ms^{-2}}$]',
		feature='/sensors/acceleration',
		sample_ids=['no_control', 'dob', 'nom'],
		y_axis_spacing=2,
		y_axis_minor=2)

	#@ Performance error
	id = 'error'
	measure_info_set[id] = MeasureInfo(  #
		id=id,  #
		label='Performance error [$\mathit{ms^{-1}}$]',
		feature='error',
		sample_ids=['no_control', 'dob'],
		y_axis_spacing=1,
		y_axis_minor=2)
	#endregion Measures

	return sample_info_set, situation_info_set, measure_info_set


#^ #######################################
def plot_sim_data(  #~function plot_sim_data
		folder=get_path_to_sim_data(subfolder='final'),
		sample_ids=['handle_force', 'motor_torque', 'no_control', 'dob', 'nom'],
		situation_ids=['no_dist', 'mass', 'slope'],
		measure_ids=['input_force', 'motor_torque', 'acc', 'error'],
		data_file_features=[
	'no_control_no_dist', 'no_control_mass', 'no_control_slope', 'dob_no_dist', 'dob_mass', 'dob_slope'
		],
		include=None,
		savename='sim_data_plot.pdf',
		x_axis_ranges=None,
		y_axis_ranges=None,
		save=False,
		show=True):
	"""Function for plotting simulation data in desired format.
		Args:
			path (str, optional): Path to target folder. Defaults to get_path_to_sim_data().
			sample_ids (iterable, optional): List of sample id's in the correct plot order. Defaults to final plot samples.
			situation_ids (iterable, optional): List of situation id's in the correct plot order. Defaults to final plot situations.
			measure_ids (iterable, optional): List of measure id's in the correct plot order. Defaults to final plot measures.
			data_file_features (iterable, optional): List of features used to determine target files. Defaults to final plot features.  
			x_axis_ranges (list, optional): List containing ranges of subplot x-axes. Set to None to determine range automatically. Defaults to None. 
			y_axis_ranges ...
			save (bool, optional): Whether to save the produced graph. Defaults to False.
			show (bool, optional): Whether to show the produced graph. Defaults to True.
		."""

	#region #& INIT #######################################
	#@ Load data
	filenames = get_filenames_processed_latest(path=folder, include=include, include_ext=False)
	filenames = filter_names(filenames, include=data_file_features)

	sim_set = load_dataframes_from_csv(filenames, path=folder)

	sample_info_set, situation_info_set, measure_info_set = get_plot_info()
	#endregion INIT

	#region #& PLOT #######################################

	#region #% Init #######################################
	use_motor_torque = True

	if not use_motor_torque:
		measure_ids.remove('motor_torque')
		sample_ids.remove('motor_torque')
		gridspecs = {'height_ratios': [1, 1, 1]}
	else:
		gridspecs = {'height_ratios': [1, 1, 2, 2]}

	ncols = len(situation_ids)
	nrows = len(measure_ids)

	fig, axes = plt.subplots(nrows=nrows,
		ncols=ncols,
		squeeze=False,
		sharex='col',
		sharey='row',
		gridspec_kw=gridspecs,
		figsize=np.array([np.sqrt(2), 1]) * 10)

	print("Making plots...")
	print("The following files are used:")
	print("In {}:".format(folder))
	for fn in filenames:
		print("    {}".format(fn))
	#endregion Init

	#region #% Plot data #######################################
	#^ COLUMN LOOP #######################################
	for j in range(ncols):
		situation_info = situation_info_set[situation_ids[j]]

		#@ Get relevant dataframes
		nom_set = filter_dict(sim_set, include='no_control_no_dist')  #@ get data beloning to nominal behavior
		situation_set = filter_dict(sim_set,
			include=situation_info.feature)  #@ get dict with df's belonging to current disturbance situation
		x_axis_topic = filter_names(situation_set.values()[0].columns,
			include=situation_info.x_axis_feature)  #@ get column name of x-axis data

		#@ Format x-axis data and set as index
		for df in nom_set.values():
			df.set_index(x_axis_topic, inplace=True)  #@ set x-axis data as index
			df.index = situation_info.x_axis_formatter(df.index)  #@ Format

		for df in situation_set.values():
			df.set_index(x_axis_topic, inplace=True)  #@ set x-axis data as index
			df.index = situation_info.x_axis_formatter(df.index)  #@ Format

		#^ ROW LOOP #######################################
		for i in range(nrows):
			#print('Plotting graph [{}; {}]'.format(i, j))
			measure_info = measure_info_set[measure_ids[i]]
			ax = axes[i, j]

			#^ SAMPLE LOOP #######################################
			for id in sample_ids:

				if id in measure_info.sample_ids:  #@ only use relevant dataframes
					sample_info = sample_info_set[id]

					#@ Check which dataset to use
					if sample_info.id == 'handle_force' or sample_info.id == 'nom':
						sample_set = nom_set
					else:
						sample_set = situation_set

					data_name = filter_names(sample_set.keys(), include=sample_info.feature)[0]  #@ get current dataframe
					sample_data = sample_set[data_name]
					sample_data = filter_dataframe(sample_data,
						include=measure_info.feature).squeeze()  #@ get current column

					sample_data.plot(ax=ax,
						color=sample_info.color,
						linestyle=sample_info.linestyle,
						alpha=sample_info.alpha)  #@ plot

	#endregion Plot data

	#region #% Format #######################################

	#region #% Main #
	#@ Subplot spacing
	plt.subplots_adjust(wspace=0.1, hspace=0.1)

	#@ Define font properties
	fig_axes_fp = fontmanager.FontProperties(size='x-large', weight='roman', style='normal')
	col_titles_fp = fontmanager.FontProperties(size='large', weight='semibold', style='normal')
	subplot_axes_fp = fontmanager.FontProperties(size='medium', weight='roman', style='oblique')
	legend_fp = fontmanager.FontProperties()

	#region #@ Titles and labels
	#fig.suptitle('TESTDATA', fontproperties=title_fp)  #@ figure title
	if use_motor_torque:  #@ scale title position
		y_comp = 2
	else:
		y_comp = 1

	fig_label_x = axes[0, ncols / 2].text(  #@ common x-axis label
		s='DISTURBANCE SCENARIOS',
		x=0.5,
		y=(1 + 0.2 * y_comp),
		rotation='horizontal',
		horizontalalignment='center',
		verticalalignment='bottom',
		transform=axes[0, ncols / 2].transAxes,
		fontproperties=fig_axes_fp)

	fig_label_y = axes[nrows / 2, 0].text(  #@ common y-axis label
		s='MEASURES',
		x=-0.2,
		y=0.5,
		rotation='vertical',
		horizontalalignment='right',
		verticalalignment='center',
		transform=axes[nrows / 2, 0].transAxes,
		fontproperties=fig_axes_fp)

	fig.align_labels()  #@ align subplot x-and-y-axis labels
	#endregion Titles and labels

	#region #% Legend
	legend_handles = []

	for sample_id in sample_ids:
		sample_info = sample_info_set[sample_id]
		handle = mlines.Line2D([0], [0], label=sample_info.label, color=sample_info.color, linestyle=sample_info.linestyle)
		legend_handles.append(handle)

	legend = axes[nrows - 1, ncols / 2].legend(title="",
		handles=legend_handles,
		ncol=len(sample_ids),
		loc='upper center',
		bbox_to_anchor=(0.5, -0.2),
		prop=legend_fp)
	#endregion Legend

	artists = [fig_label_x, fig_label_y, legend]  #@ save text instances for automatic scaling
	#endregion Main

	#region #% Per element #
	for i in range(nrows):
		for j in range(ncols):
			ax = axes[i, j]
			situation_info = situation_info_set[situation_ids[j]]
			measure_info = measure_info_set[measure_ids[i]]

			#@ Rows
			if j == 0:  #@ subplot y-axis format
				#@ Determine range
				if y_axis_ranges is None or y_axis_ranges[i] is None:
					pass
				else:
					ax.set_ylim(y_axis_ranges[i])

				#@ Add labels
				ax.set_ylabel(measure_info.label, fontproperties=subplot_axes_fp)
				ax.yaxis.set_major_locator(ticker.MultipleLocator(measure_info.y_axis_spacing))
				ax.yaxis.set_minor_locator(ticker.AutoMinorLocator(measure_info.y_axis_minor))

			#@ Columns
			if i == 0:  #@ column titles
				ax.set_title(situation_info.col_label, fontproperties=col_titles_fp, verticalalignment='bottom')

			if i == nrows - 1:  #@ subplot x-axis format
				##@ Crop to shortest data range
				if x_axis_ranges is None or x_axis_ranges[j] is None:
					subplot_samples = ax.lines
					crop = None

					for sample in subplot_samples:
						rnge = sample.get_xdata()[-1]
						if rnge < crop or crop is None:
							crop = rnge

					ax.set_xlim(right=crop)
				else:
					ax.set_xlim = x_axis_ranges[j]

				#@ Add labels
				ax.set_xlabel(situation_info.x_axis_label, fontproperties=subplot_axes_fp)
				ax.xaxis.set_major_locator(ticker.MultipleLocator(situation_info.x_axis_spacing))
				ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(situation_info.x_axis_minor))

			#@ Subplots
			ax.grid(which='minor', linestyle=':')

			#@ Indicate slope
			slope_dims = [10, 22]
			slope_edge_curves = 1
			if situation_info.feature == 'slope':
				ax.axvspan(slope_dims[0], slope_dims[1], color="gray", alpha=0.25)
				ax.axvspan(slope_dims[0], slope_dims[0] + slope_edge_curves, color="gray", alpha=0.25)
				ax.axvspan(slope_dims[1] - slope_edge_curves, slope_dims[1], color="gray", alpha=0.25)

				if i == nrows - 1:
					y_arrow = 0

					#@ Plot text
					text = ax.text(s='slope',
						x=mean(slope_dims),
						y=y_arrow,
						horizontalalignment='center',
						verticalalignment='center')

					#@ Get text position info
					bbox = text.get_window_extent(renderer=fig.canvas.get_renderer())
					transf = ax.transData.inverted()
					bbox = bbox.transformed(transf)

					#@ Draw arrows
					ax.annotate("",
						xy=(slope_dims[0], y_arrow),
						xytext=(bbox.x0, y_arrow),
						arrowprops=dict(arrowstyle="->", shrinkA=3, shrinkB=0))
					ax.annotate("",
						xy=(bbox.x1, y_arrow),
						xytext=(slope_dims[1], y_arrow),
						arrowprops=dict(arrowstyle="<-", shrinkA=0, shrinkB=3))

	#endregion Per element
	#endregion Format

	#region #% Save/show #######################################
	if save:
		path = get_path_to_sim_plots()
		filepath = os.path.join(path, savename)
		fig.savefig(filepath, orientation='landscape', bbox_inches='tight', bbox_extra_artists=artists)
		print('Plot saved to {}'.format(filepath))

	if show:
		#@ Maximize shown fig size
		mng = plt.get_current_fig_manager()
		#mng.full_screen_toggle()
		dim = mng.window.maxsize()
		fig.set_figheight(dim[0] / 200)
		fig.set_figwidth(dim[1] / 50)

		plt.tight_layout()
		plt.show()  #@ show
	#endregion Save/show
	#endregion PLOT


#endregion final plot


## #######################################
def detailed_plot(
		folder=get_path_to_sim_data(subfolder='final'), include=None, save=False, show=True, savename=None, savename_suffix=""):

	#@ settings
	save = True
	show = False

	nlabelcol = 3
	savename_suffix = ''
	xlims = None
	xmajor = None
	xminor = None

	#data_file_features = ['dob_mass']; titles = ["Added mass scenario - controlled"]
	#data_file_features = ['no_control_mass']; titles = ["Added mass scenario - uncontrolled"]
	#data_file_features = ['no_control_no_dist']; titles = ["No disturbance scenario - uncontrolled"]; xlims = [(2, 2.3)]
	#data_file_features = ['dob_no_dist']; titles = ["No disturbance scenario - controlled"]
	#data_file_features = ['dob_slope']; titles = ["Slope scenario - controlled"]
	data_file_features = ['no_control_slope', 'dob_slope']; titles = ["Slope scenario - uncontrolled", "Slope scenario - controlled"]
	#xlims = [(4.5, 5), (4.44, 4.94)]
	xlims = [(6.2, 6.7), (6, 6.5)]
	savename_suffix = '_slope_end'

	#xlims = [(4, 4.3)]
	#xlims = [(4.3, 5)]
	#xlims = [(6, 6.5)]
	#xlims = [(2, 6)]
	#xlims = [(4.4, 4.9)]


	xmajor = 1 / 50.
	xminor = 2

	topic_features = [
		'sensors/handle_force', #
		#'plugins/imu_measurement.linear_acceleration.x', #
		'sensors/acceleration', #
		'controller/handle_force_estimate',
		'controller/disturbance_estimate_unfiltered', #
		'controller/disturbance_estimate_filtered', #
		'wheel_joint_effort_controller/command.data.0', #
	]

	#colors = matplotlib.rcParams['axes.prop_cycle'].by_key()['color']
	topic_info = {
		'sensors/handle_force': {
		'label': "Handle force measurement",
		'unit': "N",
		'unit_prefix': "",
		'scale': 1,
		'color': 'tab:green'
		},
		'controller/handle_force_estimate': {
		'label': 'Nom. handle force estimate',
		'unit': "N",
		'unit_prefix': "",
		'scale': 1,
		'color': 'tab:purple'
		},
		'sensors/acceleration': {
		'label': "Acceleration measurement",
		'unit': "ms$^{-2}$",
		'unit_prefix': "$1e^{1}$ ",
		'scale': 10,
		'color': 'tab:orange'
		},
		'plugins/imu_measurement.linear_acceleration.x': {
		'label': "IMU sensor output",
		'unit': "ms$^{-2}$",
		'unit_prefix': "",
		'scale': 1,
		'color': 'tab:grey'
		},
		'wheel_joint_effort_controller/command.data.0': {
		'label': "Motor torque command",
		'unit': "Nm",
		'unit_prefix': "1$e^{1}$ ",
		'scale': 10,
		'color': 'tab:blue'
		},
		'controller/disturbance_estimate_unfiltered': {
		'label': 'Unfiltered disturbance estimate',
		'unit': "N",
		'unit_prefix': "",
		'scale': 1,
		'color': 'tab:olive',
		},
		'controller/disturbance_estimate_filtered': {
		'label': 'Filtered disturbance estimate',
		'unit': "N",
		'unit_prefix': "",
		'scale': 1,
		'color': 'tab:cyan',
		}
	}

	#@ Load data
	tmp = get_filenames_processed_latest(path=folder, include=include, include_ext=False)
	filenames = filter_names(tmp, include=data_file_features, sort=False)
	dfs = load_dataframes_from_csv(filenames, path=folder).values()
	
	print('Loaded files: {}'.format(filenames))

	title_fp = fontmanager.FontProperties(size='large', weight='roman', style='normal')
	axes_fp = fontmanager.FontProperties(size='medium', weight='roman', style='oblique')

	for df in dfs:
		# Scaling
		topics = df.columns.values
		for topic, info in topic_info.items():
			topic_name = filter_names(topics, topic)
			df[topic_name] = df[topic_name] * info['scale']

		#@ Set/format x-axis data
		fmt = lambda t: t.second + t.microsecond / 1.e6
		df.set_index('time', inplace=True)  #@ set time as index
		df.index = fmt(df.index)  #@ format

	#@ Plot
	n = len(data_file_features)
	fig, axes = plt.subplots(n, 1, sharex=False, squeeze=False)
	if xlims is None:
		xlims = [None, None]

	for i in range(n):
		df = dfs[i]
		ax = axes[i][0]
		title = titles[i]
		xlim = xlims[i]
		
		if xlim is None:
			xlim = (0, df.index[-1])

		for top_feature in topic_features:
			topic = filter_names(topics, top_feature)
			data = filter_dataframe(df, include=topic).squeeze()
			info = topic_info[top_feature]
			if i == n-1:
				label = info['label'] + " [{}{}]".format(info['unit_prefix'], info['unit'])
			else:
				label=""

			if info['color'] is not None:
				data.loc[xlim[0]:xlim[1]].plot(ax=ax, label=label, color=info['color']) #@ plot
			else:
				data.loc[xlim[0]:xlim[1]].plot(ax=ax, label=label) #@ plot

		ax.set_title(title, y=1, verticalalignment='bottom', fontproperties=title_fp)
		if xmajor is not None: ax.xaxis.set_major_locator(ticker.MultipleLocator(xmajor))
		if xminor is not None: ax.xaxis.set_minor_locator(ticker.AutoMinorLocator(xminor))
		ax.grid(which='minor', linestyle=':')

		ax.set_xlabel('time [s]', fontproperties=axes_fp)

	size = fig.get_size_inches()
	fig.set_size_inches(size[0] * 1.6, size[1])
	legend = axes[-1][0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.1*n), ncol=nlabelcol, handleheight=2)
	plt.subplots_adjust(hspace=0.45) #@ adjust spacing

	if savename is None:
		savename = 'sim_data_plot_detailed_'
		if len(data_file_features) == 1:
			savename += data_file_features[0] + savename_suffix
		else:
			savename += 'multi' + savename_suffix

	if save:
		path = get_path_to_sim_plots()
		if '.' not in savename:
			savename += '.pdf'
		filepath = os.path.join(path, savename)
		fig.savefig(filepath, orientation='landscape', bbox_inches='tight', bbox_extra_artists=[legend])
		print('Plot saved to {}'.format(filepath))

	if show:
		plt.tight_layout()
		plt.show()


##H MAIN #######################################
if __name__ == "__main__":
	#path = get_path_to_sim_data(subfolder='final'); plot_sim_data(save=False, show=True, savename='sim_data_plot_final.pdf')
	#detailed_plot()
	bode_plot([10*np.pi], [1, 10*np.pi], cutoff_freq=10*np.pi, save=True, show=False)
