from datetime import datetime
from os import remove


#% GENERAL #######################################
def raise_type_error(type_exp, type_given):

	if isinstance(type_exp, (list, tuple)):
		type_exp = [str(type) + ' or' for type in type_exp]
	type_exp = ' '.join(type_exp).rstrip(' or')

	raise TypeError("must be {}, not {}".format(type_exp, type_given))


#% FORMAT #######################################
def get_base(filenames):
	"""Get base of filenames
	Args:
		filenames (list): List of filenames to search in.
	Returns:
		list: List containing name bases.
	."""
	filenames = {remove_timestamp(remove_extension(name)) for name in filenames}

	filenames = list(filenames)
	filenames.sort()
	return filenames


#^ #######################################
def get_timestamp(filename):
	"""Get timestamp part of filename."""
	format = '%Y-%m-%d-%H-%M-%S'
	date = remove_extension(filename).split("_")[-1]
	return datetime.strptime(date, format)


#^ #######################################
def remove_timestamp(filename):
	"""Remove timestamp from filename."""
	name_elems = filename.split("_")[:-1]
	return "_".join(name_elems)


#^ #######################################
def remove_extension(name):
	"""Removes '.ext. from name string."""
	return name.split('.')[0]


## MAIN #######################################
if __name__ == "__main__":
	pass
