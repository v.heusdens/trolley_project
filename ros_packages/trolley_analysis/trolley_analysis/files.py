import os
import pandas as pd
from collections import OrderedDict
from matplotlib.backends.backend_pdf import PdfPages

from trolley_analysis.find import get_filenames_of_type, get_path_to_sim_data, get_path_to_sim_plots
from trolley_analysis.general import remove_extension


#% LOAD/SAVE #######################################
def load_dataframe_from_csv(filename, path=get_path_to_sim_data(), time_to_datetime=True):
	"""Load dataframe from csv file.
	Args:
		filename (str): Name of target file.
		path (str, optional): Path to folder containing target file. Defaults to get_path_to_sim_data().
		time_to_datetime (bool, optional): Whether to convert time values to datetime format. Defaults to True.
	Raises:
		Exception: Name with wrong file extension given.
	Returns:
		pandas Dataframe: Dataframe containing data from target file.
	."""
	#% init
	if '.' in filename and '.csv' not in filename:
		raise TypeError('Error: wrong filetype.')

	filename = remove_extension(filename)

	#% load
	df = pd.read_csv(os.path.join(path, filename + '.csv'), index_col=False, low_memory=False)
	df.name = filename  #@ Add filename as 'name' element to dataframe (extra)

	#% convert time data to timestamps
	if time_to_datetime:
		df['time'] = pd.to_datetime(df['time'])

	#% return
	return df


def load_dataframes_from_csv(filenames, path=get_path_to_sim_data(), time_to_datetime=True):
	result = OrderedDict()
	for fn in filenames:
		df = load_dataframe_from_csv(fn, path=path, time_to_datetime=time_to_datetime)
		result[fn] = df

	return result


#^ #######################################
def save_dataframe_as_csv(df, filepath, index=False):
	"""Save dataframe as csv file to given filepath.
	Args:		
		dataframe (pandas.DataFrame): dataframe to save  
		filepath (str): complete path where file should be saved  
	."""
	filepath = remove_extension(filepath) + '.csv'
	if not df.index.name is None:  #@ make sure all non-standard index columns are included as columns
		df = df.reset_index()
	df.to_csv(filepath, index=False)


#^ #######################################
def save_fig_as_pdf(fig, name, path=get_path_to_sim_plots()):
	filepath = os.path.join(path, name + '.pdf')
	with PdfPages(filepath) as pdf:
		pdf.savefig(fig)

	print("> Figure saved to '{}'".format(filepath))


## MAIN #######################################
if __name__ == "__main__":
	test = get_filenames_of_type(ext='csv', include_intermediate=True)
	print(test)
	pass
