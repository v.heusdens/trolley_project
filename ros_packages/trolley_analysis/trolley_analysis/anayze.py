import numpy as np
import os
from scipy import signal

from toolbox.compute import mean
from trolley_analysis.files import load_dataframes_from_csv
from trolley_analysis.find import filter_dataframe, filter_dict, filter_names, get_filenames_processed_latest, get_path_to_sim_data, get_path_to_sim_plots

def load_most_recent_data():
	path = get_path_to_sim_data(subfolder='final')
	filenames = get_filenames_processed_latest(path=path)
	data = load_dataframes_from_csv(filenames, path=path)
	print('Loaded files: {}'.format(filenames))
	return data

def get_df(data, feature):
	"""Get name and dataframe from dict, set time as index"""
	name, df = filter_dict(data, include=feature).items()[0]

	fmt = lambda t: t.second + t.microsecond / 1.e6
	df.set_index('time', inplace=True)
	df.index = fmt(df.index) 
	return name, df

def calculate_rise_time_from_data():
	data = load_most_recent_data()

	#@ Get nominal dataframe
	name_nom, df_nom = get_df(data, 'no_control_no_dist')
	print("Getting nominal acceleration from '{}'".format(name_nom))

	#@ Get nominal acceleration
	topics = df_nom.columns.values
	acc_topic = filter_names(topics, 'sensors/acceleration')
	acc_nom = df_nom[acc_topic].loc[3].values[0]
	print("Nominal acceleration: {} ms^-2".format(acc_nom))

	#@ Calc rise time
	scenario_feature = 'mass'
	name, df = get_df(data, 'dob_' + scenario_feature)
	print("Checking rise time using file '{}'".format(name))
	df_rise = df.loc[2:4]
	rise_time_elem = lambda df, part: (df[acc_topic] - acc_nom*part).abs().nsmallest(1, acc_topic).index.values[0]
	rise_time_start = rise_time_elem(df_rise, 0.1)
	rise_time_end = rise_time_elem(df_rise, 0.9)
	rise_time = rise_time_end - rise_time_start
	print("0.1 at {} s; 0.9 at {} s; rise time = {} s".format(rise_time_start, rise_time_end, rise_time))
	
def calculate_total_performance_error_from_data():
	data = load_most_recent_data()
	scenario_feature = 'mass'
	print("Calculating total performace errors for the '{}' scenario.".format(scenario_feature))

	#@ Get dataframes
	name_uncontrolled, df_uncontrolled = get_df(data, 'no_control_' + scenario_feature)
	name_controlled, df_controlled = get_df(data, 'dob_' + scenario_feature)
	print("Getting performance error from '{}', '{}'".format(name_uncontrolled, name_controlled))

	#@ Get performance errors
	topics = df_uncontrolled.columns.values
	error_topic = filter_names(topics, 'performance_error')
	error_tot_uncontrolled = df_uncontrolled[error_topic].iloc[-1].values[0]
	error_tot_controlled = df_controlled[error_topic].iloc[-1].values[0]

	#@ Get delta
	error_tot_delta = error_tot_controlled - error_tot_uncontrolled
	error_tot_delta_perc = error_tot_delta / error_tot_uncontrolled * 100 #@ wrt uncontrolled

	print("Total performance errors:")
	print("- Uncontrolled: {} m/s".format(error_tot_uncontrolled))
	print("- Controlled: {} m/s".format(error_tot_controlled))
	print("- Delta: {} m/s = {} %".format(error_tot_delta, error_tot_delta_perc))

def calculate_rise_time_first_order_system():
	calc_tr = lambda fc: 0.35 / fc #@ rise time

	fc = 5 #@ Cutoff frequency in Hz
	tr = calc_tr(fc)
	print("Rise time for a cutoff frequency of {} [Hz]: {} [s]".format(fc, tr))


if __name__ == "__main__":
	#calculate_total_performance_error_from_data()
	#calculate_rise_time_from_data()
	calculate_rise_time_first_order_system()

