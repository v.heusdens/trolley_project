import os
import pandas as pd
import rospkg
from collections import OrderedDict

from toolbox.general import make_list
from trolley_analysis.general import get_timestamp, remove_extension, remove_timestamp, get_base


#% LOCATIONS #######################################
def get_path_to_sim_data(subfolder=""):
	"""Get path to folder containing simulation data."""
	sim_data_folder = os.path.join(rospkg.RosPack().get_path('trolley_analysis'), "sim_data", subfolder)
	return sim_data_folder


def get_path_to_sim_plots(subfolder=""):
	"""Get path to folder containing simulation plots."""
	sim_plot_folder = os.path.join(rospkg.RosPack().get_path('trolley_analysis'), "plots", subfolder)
	return sim_plot_folder


#% GET FILENAMES #######################################
def get_filenames(path=get_path_to_sim_data(), include=None, exclude=None, include_ext=True, include_raw=False):
	"""Get names of files in folder (with extension).
	Args: 		
		path (string, optional): Path to forder containing files. Defaults to get_path_to_sim_data().
		include (list, optional): List of elements (string) that are part of included names. Set to None to disable. Defaults to None.
		exclude (list, optional): List of elements (string) that are part of excluded names. Set to None to disable. Defaults to None.
		include_ext (bool, optional): Whether to include the extension suffix in the returned filenames. If true, remove double names. Defaults to True.
		include_raw (bool, optional): Whether to include 'raw' files. Defaults to False.  
	Returns: 	
		list: List containing filenames
	."""
	#% get target filenames
	filenames = [x for x in os.listdir(path) if os.path.isfile(os.path.join(path, x))]

	if len(filenames) == 0:
		print('Warning: no files found in folder {}'.format(path))
		return filenames

	#% filter
	filenames = filter_names(filenames, include=include, exclude=exclude)

	#% remove 'raw' csv files
	if not include_raw:
		filenames = filter_names(filenames, exclude=['RAW'])

	#% remove extension
	if not include_ext:
		filenames = {remove_extension(name) for name in filenames}
		filenames = list(filenames)

	#% sort and return
	filenames.sort()
	return filenames


#^ #######################################
def get_filenames_of_type(path=get_path_to_sim_data(), ext='.bag', include_ext=True, include_raw=False):
	"""Get all filenames which end with a certain extension from folder.
	Args:		
		path (string, optional): Path to folder containing files. Defaults to get_path_to_sim_data().
		ext (str, optional): [description]. Defaults to .bag.
		include_ext (bool, optional): Whether to include the extension suffix in the returned filenames. If true, remove double names. Defaults to True.
		include_raw (bool, optional): Whether to include 'raw' files. Defaults to False.  
	Returns:	
		list: List of filenames
	."""
	#% init
	if not ext[0] == '.':
		ext = '.' + ext

	#% get filenames
	filenames_all = get_filenames(path=path, include_raw=include_raw, include_ext=include_ext)
	filenames_ext = filter_names(filenames_all, include=ext)

	#% remove extension from filenames
	if not include_ext:
		filenames_ext = {remove_extension(name) for name in filenames_ext}
		filenames_ext = list(filenames_ext)

	#% sort and return
	filenames_ext.sort()
	return filenames_ext


#^ #######################################
def get_filenames_unprocessed(path=get_path_to_sim_data(), include_ext=True):
	"""Get filenames of unprocessed files.
	Args:		
		path (string, optional): Path to folder containing files. Defaults to get_path_to_sim_data().  
		include_ext (bool, optional): Whether to include the extension suffix in the returned filenames. If true, remove double names. Defaults to True.  
	Returns:	
		list: List of filenames
	."""
	filenames = {remove_extension(name) for name in get_filenames(path=path, include_ext=include_ext)}
	filenames_unprocessed = [name for name in filenames if not os.path.isfile(os.path.join(path, name + '.csv'))]

	if include_ext:
		filenames_unprocessed = [name + '.bag' for name in filenames_unprocessed]

	if len(filenames_unprocessed) == 0:
		print("No unprocessed files found in folder {}".format(path))

	filenames_unprocessed.sort()
	return list(filenames_unprocessed)


#^ #######################################
def get_filenames_processed(path=get_path_to_sim_data(), include=None, exclude=None, include_ext=True):
	"""Get filenames of processed files.
	Args:		
		path (string, optional): Path to folder containing files. Defaults to get_path_to_sim_data().  
		include (list, optional): List of elements (string) that are part of included names. Set to None to disable. Defaults to None.
		exclude (list, optional): List of elements (string) that are part of excluded names. Set to None to disable. Defaults to None.
		include_ext (bool, optional): Whether to include the extension suffix in the returned filenames. If true, remove double names. Defaults to True.  
	Returns:	
		list: List of filenames
	."""
	filenames = {
		remove_extension(name)
		for name in get_filenames(path=path, include=include, exclude=exclude, include_ext=include_ext)
	}
	filenames_processed = [name for name in filenames if os.path.isfile(os.path.join(path, name + '.csv'))]

	if include_ext:
		filenames_processed = [name + '.csv' for name in filenames_processed]

	filenames_processed.sort()
	return list(filenames_processed)


#^ #######################################
def get_filenames_processed_latest(path=get_path_to_sim_data(), include=None, exclude=None, include_ext=True, amount=1):
	"""Get filenames of most recent processed files (from datetime description in filenames).
	Args:		
		path (string, optional): Path to folder containing files. Defaults to get_path_to_sim_data().  
		include (list, optional): List of elements (string) that are part of included names. Set to None to disable. Defaults to None.
		exclude (list, optional): List of elements (string) that are part of excluded names. Set to None to disable. Defaults to None.
		include_ext (bool, optional): Whether to include the extension suffix in the returned filenames. If true, remove double names. Defaults to True.  
		amount (int, optional): how many returned. Defaults to 1.  
	Returns:	
		list: List of filenames
	."""
	filenames = get_filenames_processed(path=path, include=include, exclude=exclude, include_ext=include_ext)
	filenames_latest = filter_most_recent_per_group(filenames, amount=amount)

	filenames_latest.sort()
	return filenames_latest


#% FILTER #######################################
def filter_names(names, include=None, exclude=None, sort=True):
	"""Get subset of 'names' with entries determined by 'include' and 'exclude'.
	Args:
		names (list): list of names (string) to search in
		include (list, optional): List of elements (string) that are part of included names. Set to None to disable. Defaults to None.
		exclude (list, optional): List of elements (string) that are part of excluded names. Set to None to disable. Defaults to None.
	Returns:
		list: List of names which comply with given criteria
	."""
	#% Preprocessing
	if include is not None:
		include = make_list(include)
	if exclude is not None:
		exclude = make_list(exclude)

	result = names

	#% Filter
	if include is not None and len(include) != 0:
		result = {name for name in names for elem in include if elem in name}

	if exclude is not None and len(exclude) != 0:
		result = {name for name in result if not any(elem in name for elem in exclude)}

	if len(result) == 0:  #@ No names matching criteria
		print("Warning from function 'filter_names': No names matching criteria include={}; exclude={}".format(
			include, exclude))

	result = list(result)
	if sort: result.sort()
	return result


#^ #######################################
def filter_most_recent(filenames, amount=1):
	"""Return most recent filename (from datetime description in filenames).
	Args:
		filenames (list): list of filenames to search in.
		amount (int, optional): how many returned. Defaults to 1.  
	Returns:
		str: name of most recent file.
	."""
	filenames = sorted(filenames, key=get_timestamp)

	if amount > len(filenames):
		print('requested more names than available. Returning {} names'.format(len(filenames)))
		return filenames
	else:
		return filenames[-amount:]


#^ #######################################
def filter_most_recent_per_group(filenames, amount=1):
	"""Return most recent files per data group (from datetime description in filenames).
	Args:
		filenames (list): List of filenames to search in.
		amount (int, optional): how many returned. Defaults to 1.  
	Returns:
		list: List of names of most recent files.
	."""
	bases = get_base(filenames)
	result = []
	for base in bases:
		names = filter_names(filenames, include=base)
		result.extend(filter_most_recent(names, amount))

	result.sort()
	return result


#^ #######################################
def filter_dict(dictionary, include=None, exclude=None, copy=True):
	"""Get subset of 'dict' with entries determined by 'include' and 'exclude'.  
	Args:
		dictionary (dict): Superset dictionary.  
		include (list, optional): List of elements (string) that are part of included names. Set to None to disable. Defaults to None.  
		exclude (list, optional): List of elements (string) that are part of excluded names. Set to None to disable. Defaults to None.  
		copy (bool, optional): Whether to copy dataframes or use pointers to originals.  
	Returns:
		OrderedDict: Subset dictionary.  
	."""
	#% init
	if not isinstance(dictionary, dict):
		raise Exception("Error: input not of type 'dict'")

	#% main
	keep = filter_names(dictionary.keys(), include, exclude)
	if copy:
		result = {key: dictionary[key].copy() for key in keep}  #@ Get filtered entries
	else:
		result = {key: dictionary[key] for key in keep}  #@ Get filtered entries
	result = OrderedDict(sorted(result.items()))
	return result


#^ #######################################
def filter_dataframe(dataframe, include=None, exclude=None):
	"""Get subset of 'dataframe' with columns determined by 'include' and 'exclude'.
	Args:
		dataframe (pandas.DataFrame): Superset dataframe
		include (bool, optional): List of elements (string) that are part of included names. Set to None to disable. Defaults to None.
		exclude (bool, optional): List of elements (string) that are part of excluded names. Set to None to disable. Defaults to None.
	Raises:
		Exception: dataframe not convertable to correct type
	Returns:
		pandas DataFrame or pandas Series: Resulting subset
	."""
	#% Preprocessing
	if not isinstance(dataframe, pd.DataFrame):
		if len(dataframe) != 1:
			raise Exception('Error: Dataframe is of len {} instead of single entry'.format(len(dataframe)))
		dataframe = dataframe[0]

	#% Get columns to keep
	columns = dataframe.columns.values
	keep = filter_names(columns, include=include, exclude=exclude)
	result = dataframe[keep]

	#% Return
	return result


## MAIN #######################################
if __name__ == "__main__":
	filenames = get_filenames()
	test = filter_most_recent_per_group(filenames)
