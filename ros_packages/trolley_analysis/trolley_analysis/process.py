import numpy as np
import os
import pandas as pd
import rosbag
import rospy
from collections import OrderedDict
from numbers import Number
from scipy import integrate
from tqdm import tqdm

from toolbox.general import make_list
from trolley_analysis.files import load_dataframe_from_csv, save_dataframe_as_csv
from trolley_analysis.find import filter_dataframe, filter_most_recent_per_group, filter_names, get_filenames, get_filenames_of_type, get_filenames_unprocessed, get_path_to_sim_data
from trolley_analysis.general import raise_type_error, remove_extension


## FUNCTIONS #######################################
def _flatten_rosmsg(msg, elements=None, name="", time=None):
	"""Flattens rosmessage to 1-dimensional dict containing all message element names
	Args:		msg: rosmessage obtained from rosbag.bag.Bag file
				time (rospy.Time, optional): timestamp. Defaults to None.
	Returns:	dict: dictionary containing flattened rosmessage element names
	."""
	#% init
	msg_flat = {}
	if time is not None:  #@ Add time element in top function call
		msg_flat["time"] = time.to_sec()

	#% End of tree is reached (leafnode)
	if isinstance(msg, (Number, str)):
		msg_flat[name] = msg
		return msg_flat

	#% msg is list
	if isinstance(msg, (list, tuple)):
		for i, slot in enumerate(msg):
			if not elements == None:
				ext = elements[i]
			else:
				ext = str(i)
			msg_flat.update(_flatten_rosmsg(slot, elements=elements, name='.'.join([name, ext])))
		return msg_flat

	#% msg is class
	else:
		slots = list(msg.__slots__)
		if "name" in slots and isinstance(msg.name, list):
			elements = msg.name
			slots.remove("name")

		for slot in slots:
			msg_flat.update(_flatten_rosmsg(getattr(msg, slot), elements=elements, name='.'.join([name, slot])))

		return msg_flat


def _load_rosbag(filename, path=get_path_to_sim_data(), min_time=None, max_time=None):
	"""Get content of rosbag file as pandas dataframe.
	Args:
		filename (str): Name of target file.
		path (str, optional): Path to folder containing target file. Defaults to get_path_to_sim_data().
		min_time (float, optional): Simulation time from where to process data. Set to None to disable. Defaults to None.  
		max_time (float, optional): Simulation time to where to process data. Set to None to disable. Defaults to None.  
	Returns:
		pandas Dataframe: Resulting dataframe containing bag file data
	."""
	#% load file as bagfile
	fn = remove_extension(filename)

	filepath = os.path.join(path, fn + '.bag')
	bag = rosbag.Bag(filepath)

	#% process msgs
	msgs = []
	for topic, msg, t in tqdm(bag.read_messages(),
		total=bag.get_message_count(),
		desc='>> Converting rosbag file to dataframe...'):
		if (min_time is None or t >= rospy.Time(min_time)) and (max_time is None or t < rospy.Time(max_time)):
			msgs.append(_flatten_rosmsg(msg, name=topic, time=t))

	bag.close()

	#% convert msgs to pandas dataframe
	df = pd.DataFrame.from_dict(msgs)
	df['time'] = pd.to_datetime(df['time'], unit='s')  #@ Transform time index to datetime type

	#% return result
	return df


#% #######################################
def load_file(filename, path=get_path_to_sim_data(), from_bag=False, save_intermediate=True, min_time=None, max_time=None):
	"""Load data from bagfile or intermediate csv, depending on which is available.
	Args:
		filename (str): Name of target file.
		path (string, optional): Path to folder containing target file. Defaults to get_path_to_sim_data().  
		from_bag (bool, optional): Whether to always load data from bagfile. Defaults to False.
		save_intermediate (bool, optional): Whether to save obtained dataframe as csv. Defaults to True.
		min_time (float, optional): Simulation time from where to process data. Set to None to disable. Defaults to None.  
		max_time (float, optional): Simulation time to where to process data. Set to None to disable. Defaults to None.  
	Returns:
		pandas DataFrame: Data from file as dataframe.
	."""
	#% init
	filename = remove_extension(filename)

	#% main
	intermediate = os.path.exists(os.path.join(path, filename + '_RAW.csv'))

	#& Load data from bag file
	if from_bag is True or not intermediate:
		tqdm.write("> Loading {} from bagfile.".format(filename))
		df = _load_rosbag(filename, path, min_time=min_time, max_time=max_time)

		#& save intermediate data to csv
		if save_intermediate:
			filepath = os.path.join(path, filename + '_RAW.csv')
			save_dataframe_as_csv(df, filepath, index=False)
			tqdm.write("> Saved intermediate data to 'raw' csv file.")

	#& Load data from raw csv file
	else:
		tqdm.write("> Loading {} from 'raw' csv.".format(filename))
		filepath = os.path.join(path, filename + '_RAW.csv')
		df = load_dataframe_from_csv(filepath)

	df = df.set_index('time')  #@ use time as index
	return df


def load_files(filenames, path=get_path_to_sim_data(), from_bag=False, save_intermediate=True, min_time=None, max_time=None):
	"""Load data from bagfiles or intermediate csv's, depending on which are available.
	Args:
		filenames (list): List of names of target files.
		path (string, optional): Path to folder containing target files. Defaults to get_path_to_sim_data().  
		from_bag (bool, optional): Whether to always load data from bagfile. Defaults to False.
		save_intermediate (bool, optional): Whether to save obtained dataframe as csv. Defaults to True.  
		min_time (float, optional): Simulation time from where to process data. Set to None to disable. Defaults to None.  
		max_time (float, optional): Simulation time to where to process data. Set to None to disable. Defaults to None.  
	Returns:
		OrderedDict: Ordered dictionary containing loaded dataframes with filenames as keys.
	."""
	#% init
	filenames = make_list(filenames)
	dfs = OrderedDict()

	for filename in tqdm(filenames, desc="Loading files"):
		dfs[filename] = load_file(filename,
			path,
			from_bag=from_bag,
			save_intermediate=save_intermediate,
			min_time=min_time,
			max_time=max_time)

	return dfs


#% #######################################
def edit_dataframe(dataframe, cutoff_time=8):
	"""Edits data in dataframe: 
		- removes irrelevant data columns
		- crops data to relevant time-frame
		- fill missing data 
	Args:
		dataframe (pandas.DataFrame): Dataframe to edit.  
		cutoff_time (float, optional): crop dataframe to this time. Defaults to 8.
	Returns:
		pandas.DataFrame: Edited dataframe.
	."""
	df = dataframe.copy()

	#% get consistent time-steps from t=0
	df = df.groupby(df.index).mean()  #@ Merge rows with the same timestamp into one

	#@ Add time 0 to beginning of df
	df0 = pd.DataFrame([[np.nan] * len(df.columns)], columns=df.columns)
	df0.index = pd.to_datetime(df0.index)
	df0.index.name = df.index.name
	df = pd.concat([df0, df])

	df = df.resample('ms').mean()  #@ Resample to every millisecond

	#% remove columns with irrelevant data
	columns_all = df.columns.values
	include = []
	exclude = [
		'clock', 'ground_plane', 'bag_link', 'support', 'model_states', 'parameter_descriptions', 'parameter_updates',
		'rosout', 'transforms', 'covariance', 'header', 'slope'
	]
	columns_keep = filter_names(columns_all, include, exclude)
	df = df[columns_keep]

	#% fill in missing data
	#& interpolate position data
	topics = df.columns.values
	pos_topics = filter_names(topics, include=['position', 'traveled_distance'])
	df.update(df[pos_topics].interpolate())

	#& apply zero-order hold to remaining data
	df = df.fillna(axis=0, method="ffill")

	#& fill initial datapoints
	orient_topics = filter_names(topics, include=['orientation'])
	df.update(df[orient_topics].fillna(axis=0,
		method="bfill"))  #@ Apply backward zero-order hold to orientation-related topics
	df = df.fillna(0)  #@ replace the remaining initial NaN's with zero's

	#% trim
	df = df[df.index <= pd.to_datetime(cutoff_time, unit='s')]  #@ Trim dataframe at cutoff time

	#% return
	return df


def edit_dataframes(dataframes, cutoff_time=8):
	"""Edit multiple dataframes at once.
	Args:
		dataframes (dict): Dictionary containing dataframes.  
		cutoff_time (float, optional): crop dataframe to this time. Defaults to 8.
	Returns:
		OrderedDict: Dictionary containing edited dataframes.  
	."""
	#% init
	if not isinstance(dataframes, dict):
		raise_type_error(dict, type(dataframes))
	elif not isinstance(dataframes, OrderedDict):
		dataframes = OrderedDict(dataframes.items())

	#% main
	result = OrderedDict()
	for name, df in tqdm(dataframes.items(), desc="Editing dataframes"):
		result[name] = edit_dataframe(df, cutoff_time=cutoff_time)

	return result


#% #######################################
def _compute_performance_error(acc_data, acc_nom):
	"""Computes accumulated acceleration error.  
	Args:  
		acc_data (pd.Series, pd.DataFrame): Acceleration data from target dataframe with time index.  
		acc_nom (pd.Series, pd.Dataframe): Nominal acceleration data with time index.  
	Raises:  
		TypeError: Inputs of wrong type.  
		Exception: Inputs have wrong amount of columns.  
	Returns:  
		pd.Series: Dataseries containing original time indices and accumulated acceleration error.  
	."""
	#% init
	if not isinstance(acc_data, (pd.Series, pd.DataFrame)):
		raise_type_error((pd.Series, pd.DataFrame), type(acc_data))
	if not isinstance(acc_nom, (pd.Series, pd.DataFrame)):
		raise_type_error((pd.Series, pd.DataFrame), type(acc_nom))

	# squeeze input dataframes to series
	acc_data = acc_data.squeeze()
	acc_nom = acc_nom.squeeze()

	if not isinstance(acc_data, pd.Series):
		raise Exception("Too many columns: {} instead of 1".format(acc_data.shape[2]))
	if not isinstance(acc_nom, pd.Series):
		raise Exception("Too many columns: {} instead of 1".format(acc_nom.shape[2]))

	#% calculate error
	error_dt = (acc_data - acc_nom).squeeze().abs()

	index = error_dt.index
	time_vector = index.second + index.microsecond / 1.e6

	error_tot = integrate.cumtrapz(error_dt, time_vector, initial=0)
	error_tot = pd.Series(error_tot, index=index, name='performance_error')

	return error_tot


def add_error_to_dataframes(dataframes):
	#% init
	if not isinstance(dataframes, dict):
		raise_type_error(dict, type(dataframes))
	elif not isinstance(dataframes, OrderedDict):
		dataframes = OrderedDict(dataframes.items())

	feature_file = 'no_control_no_dist'
	feature_topic = '/sensors/acceleration'

	#% get nominal acceleration
	if not any(feature_file in key for key in dataframes.keys()):
		raise IndexError('Dataframe containing nominal behavior not included.')

	name_nom = filter_names(dataframes.keys(), include=feature_file)[0]
	acc_nom = filter_dataframe(dataframes[name_nom], include=feature_topic)

	#% main
	result = OrderedDict()
	for filename, df in tqdm(dataframes.items(), desc="Adding performance error to dataframes"):
		acc_data = filter_dataframe(df, include=feature_topic)  #@ Get acceleration data from dataframe
		acc_error = _compute_performance_error(acc_data, acc_nom)
		df = df.join(acc_error)
		result[filename] = df

	return result


#% #######################################
def save_dataframes(dataframes, path=get_path_to_sim_data()):
	"""Save multiple dataframes at once.
	Args:
		dataframes (dict): Dict containing dataframes as values and filenames as keys.
		path (str, optional): Path to target folder. Defaults to get_path_to_sim_data().
	."""
	#% init
	if not isinstance(dataframes, dict):
		raise_type_error(dict, type(dataframes))

	for filename, df in tqdm(dataframes.items(), desc="Saving dataframes to csv"):
		filename = remove_extension(filename)
		filepath = os.path.join(path, filename + '.csv')
		save_dataframe_as_csv(df, filepath)


#% #######################################
def process_sim_data(filenames=get_filenames_unprocessed(),
	path=get_path_to_sim_data(),
	from_bag=False,
	min_time=None,
	max_time=None,
	cutoff_time=8):
	"""Loads, edits, and saves data from given filenames.  
	Args:  
		filenames (str, list): Filename/list of filenames to process.  
		path (string, optional): Path to folder containing files. Defaults to get_path_to_sim_data().  
		from_bag (bool, optional): Whether to always load data from bagfile. Defaults to False.  
		min_time (float, optional): Simulation time from where to process data. Set to None to disable. Defaults to None.  
		max_time (float, optional): Simulation time to where to process data. Set to None to disable. Defaults to None. 
		cutoff_time (float, optional): crop dataframe to this time. Defaults to 8.
	."""
	#% init
	filenames = make_list(filenames)  #@ make sure 'filenames' is a list

	if len(filenames) == 0:
		tqdm.write('No files given to process.')
		return

	tqdm.write('\nPROCESSING {} FILES:'.format(len(filenames)))
	for name in filenames:
		tqdm.write("- {}".format(name))

	pbar = tqdm(total=4, desc="TOTAL PROGRESS")

	#% load files
	data = load_files(filenames, path, min_time=min_time, max_time=max_time, from_bag=from_bag)
	pbar.update()

	#% edit data
	data = edit_dataframes(data, cutoff_time=cutoff_time)
	pbar.update()

	#% compute and add error column to dataframes
	data = add_error_to_dataframes(data)
	pbar.update()

	#% Save dataframes to csv
	save_dataframes(data, path)
	pbar.update()


## MAIN #######################################
if __name__ == "__main__":
	#df = process_sim_data(['testdata_2020-12-21-15-39-51', 'testdata_no_control_no_dist_2020-12-21-13-57-24'],
	#from_bag=False)
	#df = process_sim_data(get_filenames())
	folder = 'final'
	#folder = 'testdata'
	path = get_path_to_sim_data(subfolder=folder)
	#features = ['repetition']
	features = [""]

	if len(features) == 0:
		features = [""]

	for feature in features:
		filenames_unprocessed = get_filenames_unprocessed(path=path, include_ext=False)
		filenames_unprocessed = filter_names(filenames_unprocessed, include=feature)

		prefixes = [fn.split('_')[0] for fn in filenames_unprocessed]
		prefixes = set(prefixes)

		filenames_nominal = filter_names(get_filenames(path=path, include_ext=False), include='no_control_no_dist')
		filenames_nominal = filter_names(filenames_nominal, include=prefixes)
		filenames_all = set(filenames_unprocessed + filenames_nominal)
		process_sim_data(filenames=filenames_all, path=path)  #@ default
