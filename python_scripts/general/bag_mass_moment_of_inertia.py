import sympy as sp

"""Calculates symbolic expression for mass moment of inertia conforming with the bag coordinate system as specified in the trolley URDF"""

#@ parallel axis theorem: I = Icm + M r^2
#@ Inertia of point with mass m around axis at distance r: Ip = m r^2
#@ Accompanying figure: triangle_mass_moment_of_inertia.jpg


def frac(a, b):
	return sp.Rational(a, b)


def inertia(r, order, v, rho):
	#print('r = {}'.format(r))
	subj = r**2

	for dir in order:
		range = ranges[str(dir)]

		temp = sp.integrate(subj, dir)
		subj = temp.subs(dir, range[1]) - temp.subs(dir, range[0])

		#print('\nAfter {}:'.format(dir))
		#sp.pprint(subj)

	I_ = subj * rho
	I = I_ - M * v**2

	sp.pprint(sp.simplify(I))
	return I


sp.init_printing(use_unicode=False, order='lex')
x, y, z, h, w, d, M = sp.symbols('x y z h w d M')

ranges = {
	'x': [0, h],
	'y': [0, w],
	'z': [0, (1 - x / h) * d]
}

rho = M / (frac(1, 2) * h * w * d)

#% Around COM y-axis
r_y = sp.sqrt(x**2 + z**2)
v_y = r_y.subs([(x, frac(1, 3) * h), (z, frac(1, 3) * d)])
order_y = [z, x, y]

print('\nInertia around y-axis COM:')
I = inertia(r_y, order_y, v_y, rho)

#% Around COM x-axis
r_x = sp.sqrt(y**2 + z**2)
v_x = r_x.subs([(y, frac(1, 2) * w), (z, frac(1, 3) * d)])
order_x = [z, y, x]

print('\nInertia around x-axis COM:')
I = inertia(r_x, order_x, v_x, rho)

#
#region #!
#rho_box = M / (h * w * d)
#r_box = sp.sqrt(y**2 + z**2)
#v_box = r_box.subs([(y, frac(1, 2) * w), (z, frac(1, 2) * d)])
#ranges_box = ranges
#ranges_box['z'] = [0, d]
#I_box = inertia(r_box, [z, y, x], v_box, rho_box)
#sp.pprint(sp.simplify(I))
#endregion
