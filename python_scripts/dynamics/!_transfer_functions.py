import sympy as sp
from math import asin

from toolbox.params import ParamsNumeric, ParamsSymbolic
from toolbox.transform import subs_multi
from toolbox.general import make_list


class TFsAdaptedSymbolic:  #~class TFsAdaptedSymbolic
	""" Contains Symbolic transfer functions and elements + function for explicitly filling in trolley transfer function.

    Inputs:
     - expand:  List of dependent params to expand.
       Options:
        * None = not expand
        * True = expand everything
        * False (def) = expand default: 'T_roll'
        * List with specific params
    - Subs_plant:  Whether to explicitly fill in trolley TF

    Contains:
    - Symbolic transfer functions and elements
    - Functions:
      - subs_plant():  explicitly fill in trolley transfer function in others and save in class (none)
      - expand: Same as input expand
    """

	def __init__(s, expand=False, subs_plant=False):
		s.A, s.A_n, s.A_a = sp.symbols("A A_n A_a", nonnegative=True)
		s.B, s.G, s.B_n, s.G_n, s.B_a, s.G_a = sp.symbols('B G B_n G_n B_a G_a', real=True)
		s.Q = sp.symbols("Q")

		s.sym = ParamsSymbolic(expand)

		s.TF_L = s.G * s.Q / (s.G_n * (1 - s.Q))
		s.TF_S = sp.simplify(1 / (s.TF_L + 1))
		s.TF_T = sp.simplify(s.TF_L / (s.TF_L + 1))

		beta = sp.asin((s.sym.L_h - s.sym.R_w) / s.sym.L_wh)
		I = s.sym.R_w**2 * (s.sym.m_ft + 3. * s.sym.m_w)
		s.TF_G = 2. / I * (1. - (s.sym.mu * s.sym.R_w) / (s.sym.L_wh * sp.cos(beta)))

		if subs_plant:
			s.subs_plant()

	def subs_plant(s):
		s.TF_L = s.TF_L.subs(s.G, s.TF_G)
		s.TF_S = s.TF_S.subs(s.G, s.TF_G)
		s.TF_T = s.TF_T.subs(s.G, s.TF_G)

	def expand(s, params=None):
		s.sym.expand(s, params)


class TFsAdaptedNumeric:  #~class TFsAdaptedNumeric
	# region  description

	"""Contains adapted transfer functions filled with numeric values.

    - Inputs:
      - par_type: which type of uncertain param value to fill in
          Options:
          * None: don't fill
          *'min'\'max'\'avg'\'nom'

      - expand: Whether to expand depentent variables.
          Options:
          * True:     expand all
          * False:     expand default: 'T_roll'
          * None:     don't expand
          * List with specific params to expand

      - subs_plant: Whether to expand G to function

    - Functions:
      - fill_params(function):  fill in parameter values for symbolic parameters.
    """

	# endregion

	def __init__(s, par_type=None, expand=True, subs_plant=True):
		expand = make_list(expand)

		s.TFs = TFsAdaptedSymbolic(expand=expand, subs_plant=subs_plant)
		s.par_num = ParamsNumeric()

		# % Fill param values in TFs
		if par_type is not None:
			s.par_num.set_params_type(par_type)

	# region  ## Properties
	@property
	def G(s):
		return s.fill_params('TF_G')

	@property
	def L(s):
		return s.fill_params('TF_L')

	@property
	def S(s):
		return s.fill_params('TF_S')

	@property
	def T(s):
		return s.fill_params('TF_T')

	# endregion Properties

	# Functions
	def fill_params(s, function):
		""" Return function with params replaced by values from par_num. """
		TF = getattr(s.TFs, function)
		params_sym = TF.free_symbols  # @ Which params are not set yet
		params_sym.discard(s.TFs.Q)  # @ Remove Q from params to be replaced

		replaced = []
		replacements = []
		not_replaced = []
		for param in params_sym:
			if param == s.TFs.G_n:  # @ Get TF value
				replacement = TFsAdaptedNumeric(par_type='nom').G
			else:  # @ Get param value
				replacement = getattr(s.par_num, str(param))

			if replacement is None:
				not_replaced.append(param)
			else:
				replaced.append(param)
				replacements.append(replacement)

		TF = subs_multi(TF, replaced, replacements)

		if len(not_replaced) != 0:
			print("WARNING: Whilst obtaining numeric {}: Params '{}' are not replaced since no value is set.".
				format(function, not_replaced))
		return TF

	def set_params_min(s):
		s.par_num.set_params_min()

	def set_params_max(s):
		s.par_num.set_params_max()

	def set_params_avg(s):
		s.par_num.set_params_avg()

	def set_params_nom(s):
		s.par_num.set_params_nom()
