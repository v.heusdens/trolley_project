## Results from symbolic_model_calculation.py
# yapf: disable

import sympy as sp
from copy import deepcopy

from toolbox.params import ParamsNumeric, ParamsSymbolic
from toolbox.ros import load_config
from toolbox.transform import convert_sympy_to_latex, convert_sympy_to_sympy_printed

class ModelEquations(object): #~class ModelEquations
	def __init__(self, params=ParamsSymbolic(), acc_explicit=False, added_constraint=True, expand_rolling_torque=True, support_mass=False, detailed_com=True):
		"""Get model equations in different configurations

		Args:
			params (object, optional): class containing variable instances. Defaults to ParamsSymbolic().
			acc_explicit (bool, optional): implies whether dd_xw (horizontal acceleration) is explicitly present in the equation. Defaults to False.
			added_constraint (bool, optional): implies whether function includes additional constraint on handle height (only for dd_xw). Defaults to True.
			detailed_com (bool, optional): determines if W_wf is included. Defaults to True.
			support_mass (bool, optional): determines whether additional mass of Gazebo support wheels is taken into account. Defaults to False.
		"""

		## instantiating the 'Inner' class
		self.p = deepcopy(params)
		self.acc_expl = acc_explicit
		self.add_constr = added_constraint
		self.expand_Troll = expand_rolling_torque

		if not detailed_com:
			self.p.W_wf = 0

		if support_mass:
			config = load_config('trolley_simulation')
			support_ratio = config['support_wheel_mass_ratio']
			support_mass = self.p.m_w * support_ratio
			self.p.m_w = self.p.m_w + support_mass

		self.dd_xw = self._DD_XW(self)
		self.F_hx = self._F_HX(self)
		self.T_roll = self._T_ROLL(self)
		self.F_hy = self._F_HY(self)
		self.F_n = self._F_N(self)

	def _getattribute_vars(self, obj, name):
		if name[0] == '_':
			return object.__getattribute__(obj, name)

		if object.__getattribute__(self, 'acc_expl'):
			type = '_explicit_acc'
		else:
			type = '_explicit_Fhx'
		return getattr(object.__getattribute__(obj, type), name)

	def _getattribute_constr(self, obj, name):
		if name[0] == '_':
			return object.__getattribute__(obj, name)

		if object.__getattribute__(self, 'add_constr'):
			type = '_add_constr'
		else:
			type = '_no_add_constr'
		return getattr(object.__getattribute__(obj, type), name)

	def _getattribute_Troll(self, obj, name):
		if name[0] == '_':
			return object.__getattribute__(obj, name)

		if object.__getattribute__(self, 'expand_Troll'):
			type = '_implicit_TRoll'
		else:
			type = '_explicit_TRoll'
		return getattr(object.__getattribute__(obj, type), name)

	#^ #######################################
	class _DD_XW(object): #~class DD_XW
		def __init__(self, root):
			self._root = root
			self._no_add_constr = self._NoAddedConstraint(self._root)
			self._add_constr = self._AddedConstraint(self._root)

		def __getattribute__(self, name):
			func = object.__getattribute__(self, '_root')._getattribute_constr
			return func(self, name)

		class _NoAddedConstraint(object): #~class -NoAddedConstraint

			def __init__(self, root):
				s = root.p

				self.all = (-2.0*s.F_wg*s.R_w*s.mu*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft)*sp.cos(s.gamma)*sp.sign(s.d_xw) - 2.0*s.F_wg*s.R_w*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft)*sp.sin(s.gamma) + 0.5*s.R_w*s.d_beta**2*s.m_ft*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*(2.0*s.I_f*s.mu*sp.sign(s.d_xw) - s.L_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.sin(2.0*s.beta) + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 2.0*s.L_wf*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.sin(2.0*s.beta)) + 0.5*s.R_w*s.d_beta**2*s.m_ft*(s.L_wf*sp.cos(s.beta) - s.W_wf*sp.sin(s.beta))*(2.0*s.I_f + s.L_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.L_wf**2*s.m_ft + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 2.0*s.L_wf*s.W_wf*s.m_ft*sp.sin(2.0*s.beta) - s.W_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft) + s.R_w*s.m_ft*(-s.L_wf*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + s.L_wf*sp.sin(s.beta) + s.W_wf*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.W_wf*sp.cos(s.beta))*(s.F_hx*s.L_wf*sp.sin(s.beta) - s.F_hx*s.L_wh*sp.sin(s.beta) + s.F_hx*s.W_wf*sp.cos(s.beta) - s.F_hy*s.L_wf*sp.cos(s.beta) + s.F_hy*s.L_wh*sp.cos(s.beta) + s.F_hy*s.W_wf*sp.sin(s.beta) + 2.0*s.T_m) - 0.5*s.R_w*(s.F_fg*sp.sin(s.gamma) - s.F_hx)*(2.0*s.I_f + s.L_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.L_wf**2*s.m_ft + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 2.0*s.L_wf*s.W_wf*s.m_ft*sp.sin(2.0*s.beta) - s.W_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft) - 0.5*s.R_w*(s.F_fg*sp.cos(s.gamma) - s.F_hy)*(2.0*s.I_f*s.mu*sp.sign(s.d_xw) - s.L_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.sin(2.0*s.beta) + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 2.0*s.L_wf*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.sin(2.0*s.beta)) + 2.0*s.T_m*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft))/(s.R_w*(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w))

				self.F_hx = (1.0*s.I_f + 1.0*s.L_wf**2*s.m_ft + 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.cos(2.0*s.beta) - 0.5*s.L_wf*s.L_wh*s.m_ft + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wh*s.W_wf*s.m_ft*sp.sin(2.0*s.beta) + 1.0*s.W_wf**2*s.m_ft)/(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w)

				self.F_hy = (1.0*s.I_f*s.mu*sp.sign(s.d_xw) + 1.0*s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + 1.0*s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw))/(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w)

				self.T_m = 2.0*(s.I_f + s.L_wf**2*s.m_ft - s.L_wf*s.R_w*s.m_ft*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) + s.L_wf*s.R_w*s.m_ft*sp.sin(1.0*s.beta) + s.R_w*s.W_wf*s.m_ft*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) + s.R_w*s.W_wf*s.m_ft*sp.cos(1.0*s.beta) + s.W_wf**2*s.m_ft)/(s.R_w*(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w))

				self.F_fg = (-1.0*s.I_f*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - 1.0*s.I_f*sp.sin(s.gamma) - 0.5*s.L_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta + s.gamma)*sp.sign(s.d_xw) - 0.5*s.L_wf**2*s.m_ft*sp.sin(s.gamma) - 0.5*s.L_wf**2*s.m_ft*sp.sin(2.0*s.beta + s.gamma) - 1.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta + s.gamma)*sp.sign(s.d_xw) - 1.0*s.L_wf*s.W_wf*s.m_ft*sp.cos(2.0*s.beta + s.gamma) - 0.5*s.W_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta + s.gamma)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft*sp.sin(s.gamma) + 0.5*s.W_wf**2*s.m_ft*sp.sin(2.0*s.beta + s.gamma))/(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w)

				self.F_wg = -(2.0*s.I_f*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.I_f*sp.sin(s.gamma) + 2.0*s.L_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.L_wf**2*s.m_ft*sp.sin(s.gamma) + 2.0*s.W_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.W_wf**2*s.m_ft*sp.sin(s.gamma))/(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w)

				self.d_beta_squared = s.m_ft*(s.I_f*s.L_wf*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) + s.I_f*s.L_wf*sp.cos(1.0*s.beta) + s.I_f*s.W_wf*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) - s.I_f*s.W_wf*sp.sin(1.0*s.beta) + s.L_wf**3*s.m_ft*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**3*s.m_ft*sp.cos(1.0*s.beta) + s.L_wf**2*s.W_wf*s.m_ft*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) - s.L_wf**2*s.W_wf*s.m_ft*sp.sin(1.0*s.beta) + s.L_wf*s.W_wf**2*s.m_ft*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) + s.L_wf*s.W_wf**2*s.m_ft*sp.cos(1.0*s.beta) + s.W_wf**3*s.m_ft*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) - s.W_wf**3*s.m_ft*sp.sin(1.0*s.beta))/(s.I_f*s.m_ft + 3.0*s.I_f*s.m_w + 0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.m_ft**2 + 3.0*s.L_wf**2*s.m_ft*s.m_w + s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) - 0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.m_ft**2 + 3.0*s.W_wf**2*s.m_ft*s.m_w)

				self.denom = (s.I_f*s.m_ft+3.0*s.I_f*s.m_w+0.5*s.L_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)+0.5*s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta)+0.5*s.L_wf**2*s.m_ft**2+3.0*s.L_wf**2*s.m_ft*s.m_w+s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)-s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta)-0.5*s.W_wf**2*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)-0.5*s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta)+0.5*s.W_wf**2*s.m_ft**2+3.0*s.W_wf**2*s.m_ft*s.m_w)

				self.other = 0

		class _AddedConstraint(object): #~class -AddedConstraint

			def __init__(self, root):
				self._root = root
				self._explicit_TRoll = self._ExplicitTRoll(self._root)
				self._implicit_TRoll = self._ImplicitTRoll(self._root)

			def __getattribute__(self, name):
				func = object.__getattribute__(self, '_root')._getattribute_Troll
				return func(self, name)

			class _ExplicitTRoll(object): #~class --ExplicitTRoll
				def __init__(self, root):
					s = root.p

					self.all = (-1.0*s.F_fg*s.R_w*sp.sin(s.gamma) + 1.0*s.F_hx*s.R_w - 2.0*s.F_wg*s.R_w*sp.sin(s.gamma) + 1.0*s.L_wf*s.R_w*s.d_beta**2*s.m_ft/sp.cos(1.0*s.beta) + 2.0*s.T_m - 2.0*s.T_roll)/(s.R_w*(s.m_ft + 3.0*s.m_w))

					self.F_hx = 1/(s.m_ft + 3.0*s.m_w)

					self.F_hy = 0

					self.T_roll = -2.0/(s.R_w*(s.m_ft + 3.0*s.m_w))

					self.T_m = 2.0/(s.R_w*(s.m_ft + 3.0*s.m_w))

					self.F_fg = -sp.sin(s.gamma)/(s.m_ft + 3.0*s.m_w)

					self.F_wg = -2.0*sp.sin(s.gamma)/(s.m_ft + 3.0*s.m_w)

					self.d_beta_squared = s.L_wf*s.m_ft/((s.m_ft + 3.0*s.m_w)*sp.cos(1.0*s.beta))

					self.denom = 0

			class _ImplicitTRoll(object): #~class --ImplicitTRoll
				def __init__(self, root):
					s = root.p

					self.all = (-s.F_fg*s.L_wf*s.R_w*s.mu*sp.sin(s.beta)*sp.sin(s.beta + s.gamma)*sp.sign(s.d_xw) + s.F_fg*s.L_wf*s.R_w*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - s.F_fg*s.L_wh*s.R_w*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) - s.F_fg*s.L_wh*s.R_w*sp.sin(s.gamma)*sp.cos(s.beta)**2 - s.F_fg*s.R_w*s.W_wf*s.mu*sp.sin(s.beta)*sp.cos(s.beta + s.gamma)*sp.sign(s.d_xw) - s.F_fg*s.R_w*s.W_wf*s.mu*sp.sin(s.gamma)*sp.sign(s.d_xw) + s.F_hx*s.L_wh*s.R_w*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw)/2.0 + s.F_hx*s.L_wh*s.R_w*sp.cos(s.beta)**2 - 2.0*s.F_wg*s.L_wh*s.R_w*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) - 2.0*s.F_wg*s.L_wh*s.R_w*sp.sin(s.gamma)*sp.cos(s.beta)**2 + s.I_f*s.R_w*s.d_beta**2*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.R_w*s.d_beta**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wf*s.L_wh*s.R_w*s.d_beta**2*s.m_ft*sp.cos(s.beta) + s.L_wh*s.R_w*s.W_wf*s.d_beta**2*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + 2.0*s.L_wh*s.T_m*sp.cos(s.beta)**2 - 2.0*s.R_w*s.T_m*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw))/(s.R_w*(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))*sp.cos(s.beta))

					self.F_hx = s.L_wh*(s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + sp.cos(s.beta))/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

					self.F_hy = 0

					self.T_m = 2.0*(s.L_wh*sp.cos(s.beta) - s.R_w*s.mu*sp.sign(s.d_xw))/(s.R_w*(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw)))

					self.F_fg = sp.Rational(2, 3)*(-s.L_wf*s.mu*sp.sin(s.beta)*sp.sin(s.beta + s.gamma)*sp.sign(s.d_xw) + s.L_wf*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - s.L_wh*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) - s.L_wh*sp.sin(s.gamma)*sp.cos(s.beta)**2 - s.W_wf*s.mu*sp.sin(s.beta)*sp.cos(s.beta + s.gamma)*sp.sign(s.d_xw) - s.W_wf*s.mu*sp.sin(s.gamma)*sp.sign(s.d_xw))/(sp.Rational(1, 3)*s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + sp.Rational(1, 3)*s.L_wh*s.m_ft + 1.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 1.0*s.L_wh*s.m_w + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.F_wg = -2.0*s.L_wh*(s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + sp.sin(s.gamma))*sp.cos(s.beta)/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

					self.d_beta_squared = sp.Rational(2, 3)*(s.I_f*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wf*s.L_wh*s.m_ft*sp.cos(s.beta) + s.L_wh*s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw))/(sp.Rational(1, 3)*s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + sp.Rational(1, 3)*s.L_wh*s.m_ft + 1.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 1.0*s.L_wh*s.m_w + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.denom = (s.L_wf*s.m_ft*s.mu*sp.sin(1.0*s.beta)+s.L_wh*s.m_ft*sp.cos(1.0*s.beta)+3.0*s.L_wh*s.m_w*sp.cos(1.0*s.beta)+s.W_wf*s.m_ft*s.mu*sp.cos(1.0*s.beta))

					self.other = 0

	#^ #######################################
	class _F_HX(object): #~class F_HX
		def __init__(self, root):
			s = root.p

			self.all = (s.F_fg*s.L_wf*s.R_w*s.mu*sp.sin(s.beta)*sp.sin(s.beta + s.gamma)*sp.sign(s.d_xw) - s.F_fg*s.L_wf*s.R_w*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.F_fg*s.L_wh*s.R_w*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.F_fg*s.L_wh*s.R_w*sp.sin(s.gamma)*sp.cos(s.beta)**2 + s.F_fg*s.R_w*s.W_wf*s.mu*sp.sin(s.beta)*sp.cos(s.beta + s.gamma)*sp.sign(s.d_xw) + s.F_fg*s.R_w*s.W_wf*s.mu*sp.sin(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.L_wh*s.R_w*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.L_wh*s.R_w*sp.sin(s.gamma)*sp.cos(s.beta)**2 - s.I_f*s.R_w*s.d_beta**2*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) - s.L_wf**2*s.R_w*s.d_beta**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) - s.L_wf*s.L_wh*s.R_w*s.d_beta**2*s.m_ft*sp.cos(s.beta) + s.L_wf*s.R_w*s.dd_xw*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw)/2.0 - s.L_wh*s.R_w*s.W_wf*s.d_beta**2*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.R_w*s.dd_xw*s.m_ft*sp.cos(s.beta)**2 + 3.0*s.L_wh*s.R_w*s.dd_xw*s.m_w*sp.cos(s.beta)**2 - 2.0*s.L_wh*s.T_m*sp.cos(s.beta)**2 + 2.0*s.R_w*s.T_m*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) - s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.R_w*s.W_wf*s.dd_xw*s.m_ft*s.mu*sp.cos(s.beta)**2*sp.sign(s.d_xw))/(s.L_wh*s.R_w*(s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + sp.cos(s.beta))*sp.cos(s.beta))

			self.F_hy = 0

			self.dd_xw = (s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))/(s.L_wh*(s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + sp.cos(s.beta)))

			self.T_m = 2.0*(-s.L_wh*sp.cos(s.beta) + s.R_w*s.mu*sp.sign(s.d_xw))/(s.L_wh*s.R_w*(s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + sp.cos(s.beta)))

			self.F_fg = 2.0*(s.L_wf*s.mu*sp.sin(s.beta)*sp.sin(s.beta + s.gamma)*sp.sign(s.d_xw) - s.L_wf*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.L_wh*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.L_wh*sp.sin(s.gamma)*sp.cos(s.beta)**2 + s.W_wf*s.mu*sp.sin(s.beta)*sp.cos(s.beta + s.gamma)*sp.sign(s.d_xw) + s.W_wf*s.mu*sp.sin(s.gamma)*sp.sign(s.d_xw))/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1))

			self.d_beta_squared = -2.0*(s.I_f*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wf*s.L_wh*s.m_ft*sp.cos(s.beta) + s.L_wh*s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw))/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1))

			self.F_wg = 2.0*(s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + sp.sin(s.gamma))*sp.cos(s.beta)/(s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + sp.cos(s.beta))

			self.other = 0

			self.denom = s.L_wh*(s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) + sp.cos(1.0*s.beta))

	#^ #######################################
	class _T_ROLL(object): #~class T_ROLL

		def __init__(self, root):
			self._root = root
			self._explicit_acc = self._ExplicitAcc(self._root)
			self._explicit_Fhx = self._ExplicitFhx(self._root)

		def __getattribute__(self, name):
			func = object.__getattribute__(self, '_root')._getattribute_vars
			return func(self, name)

		class _ExplicitAcc(object): #~class -ExplicitAcc
			def __init__(self, root):
				s = root.p

				self.all = 0.5*s.mu*(-s.F_fg*s.L_wf*s.R_w*sp.cos(s.gamma) - s.F_fg*s.L_wf*s.R_w*sp.cos(2.0*s.beta + s.gamma) + s.F_fg*s.L_wh*s.R_w*sp.cos(s.gamma) + s.F_fg*s.L_wh*s.R_w*sp.cos(2.0*s.beta + s.gamma) + s.F_fg*s.R_w*s.W_wf*sp.sin(s.gamma) + s.F_fg*s.R_w*s.W_wf*sp.sin(2.0*s.beta + s.gamma) + 2.0*s.F_wg*s.L_wh*s.R_w*sp.cos(s.gamma) + 2.0*s.F_wg*s.L_wh*s.R_w*sp.cos(2.0*s.beta + s.gamma) - 2.0*s.I_f*s.R_w*s.d_beta**2*sp.sin(s.beta) - 2.0*s.L_wf**2*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) + 2.0*s.L_wf*s.L_wh*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) + s.L_wf*s.R_w*s.dd_xw*s.m_ft*sp.sin(2.0*s.beta) - 2.0*s.L_wh*s.R_w*s.W_wf*s.d_beta**2*s.m_ft*sp.cos(s.beta) - s.L_wh*s.R_w*s.dd_xw*s.m_ft*sp.sin(2.0*s.beta) - 3.0*s.L_wh*s.R_w*s.dd_xw*s.m_w*sp.sin(2.0*s.beta) + 2.0*s.L_wh*s.T_m*sp.sin(2.0*s.beta) + 4.0*s.R_w*s.T_m*sp.cos(s.beta) - 2.0*s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft*sp.sin(s.beta) + s.R_w*s.W_wf*s.dd_xw*s.m_ft*sp.cos(2.0*s.beta) + s.R_w*s.W_wf*s.dd_xw*s.m_ft)*sp.sign(s.d_xw)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_hx = 0

				self.F_hy = 0

				self.dd_xw = s.R_w*s.mu*(0.5*s.L_wf*s.m_ft*sp.sin(2.0*s.beta) - 0.5*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) - 1.5*s.L_wh*s.m_w*sp.sin(2.0*s.beta) + 0.5*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.W_wf*s.m_ft)*sp.sign(s.d_xw)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.T_m = s.mu*(1.0*s.L_wh*sp.sin(2.0*s.beta) + 2.0*s.R_w*sp.cos(s.beta))*sp.sign(s.d_xw)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_fg = 0.5*s.R_w*s.mu*(-s.L_wf*sp.cos(s.gamma) - s.L_wf*sp.cos(2.0*s.beta + s.gamma) + s.L_wh*sp.cos(s.gamma) + s.L_wh*sp.cos(2.0*s.beta + s.gamma) + s.W_wf*sp.sin(s.gamma) + s.W_wf*sp.sin(2.0*s.beta + s.gamma))*sp.sign(s.d_xw)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.d_beta_squared = 1.0*s.R_w*s.mu*(-s.I_f*sp.sin(s.beta) - s.L_wf**2*s.m_ft*sp.sin(s.beta) + s.L_wf*s.L_wh*s.m_ft*sp.sin(s.beta) - s.L_wh*s.W_wf*s.m_ft*sp.cos(s.beta) - s.W_wf**2*s.m_ft*sp.sin(s.beta))*sp.sign(s.d_xw)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_wg = 1.0*s.R_w*s.mu*(sp.cos(s.gamma) + sp.cos(2.0*s.beta + s.gamma))*sp.sign(s.d_xw)/(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0)

				self.other = 0

		class _ExplicitFhx(object): #~class -ExplicitFhx
			def __init__(self, root):
				s = root.p

				self.all = 0.5*s.mu*(s.F_fg*s.R_w*(s.m_ft + 3.0*s.m_w)*(-s.L_wf*sp.cos(s.beta) + s.L_wh*sp.cos(s.beta) + s.W_wf*sp.sin(s.beta))*sp.cos(s.beta)*sp.cos(s.gamma) + 2.0*s.F_wg*s.L_wh*s.R_w*(s.m_ft + 3.0*s.m_w)*sp.cos(s.beta)**2*sp.cos(s.gamma) - 2.0*s.F_wg*s.R_w*s.m_ft*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*sp.sin(s.gamma)*sp.cos(s.beta) - 3.0*s.R_w*s.d_beta**2*s.m_ft*s.m_w*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*(s.L_wf*sp.cos(s.beta) - s.W_wf*sp.sin(s.beta))*sp.cos(s.beta) - s.R_w*s.d_beta**2*s.m_ft*(s.m_ft + 3.0*s.m_w)*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*(-s.L_wf*sp.cos(s.beta) + s.L_wh*sp.cos(s.beta) + s.W_wf*sp.sin(s.beta))*sp.cos(s.beta) - 0.5*s.R_w*s.d_beta**2*(2.0*s.I_f*s.m_ft + 6.0*s.I_f*s.m_w + s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + s.L_wf**2*s.m_ft**2 + 6.0*s.L_wf**2*s.m_ft*s.m_w - s.L_wf*s.L_wh*s.m_ft**2*sp.cos(2.0*s.beta) - s.L_wf*s.L_wh*s.m_ft**2 - 3.0*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.cos(2.0*s.beta) - 3.0*s.L_wf*s.L_wh*s.m_ft*s.m_w - 2.0*s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) + s.L_wh*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) + 3.0*s.L_wh*s.W_wf*s.m_ft*s.m_w*sp.sin(2.0*s.beta) - s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft**2 + 6.0*s.W_wf**2*s.m_ft*s.m_w)*sp.sin(s.beta) + 3.0*s.R_w*s.m_w*(s.F_fg*sp.sin(s.gamma) - s.F_hx)*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*sp.cos(s.beta) + s.R_w*(s.m_ft + 3.0*s.m_w)*(s.F_hx*s.L_wf*sp.sin(s.beta) - s.F_hx*s.L_wh*sp.sin(s.beta) + s.F_hx*s.W_wf*sp.cos(s.beta) + 2.0*s.T_m)*sp.cos(s.beta) + s.T_m*s.m_ft*(s.L_wf*sp.sin(2.0*s.beta) + s.W_wf*sp.cos(2.0*s.beta) + s.W_wf))*sp.sign(s.d_xw)/((s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))*sp.cos(s.beta))

				self.F_hx = s.R_w*s.mu*(0.5*s.L_wf*s.m_ft*sp.sin(s.beta) - 0.5*s.L_wh*s.m_ft*sp.sin(s.beta) - 1.5*s.L_wh*s.m_w*sp.sin(s.beta) + 0.5*s.W_wf*s.m_ft*sp.cos(s.beta))*sp.sign(s.d_xw)/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

				self.F_hy = 0

				self.T_m = s.mu*(sp.Rational(1, 3)*s.L_wf*s.m_ft*sp.sin(2.0*s.beta) + sp.Rational(2, 3)*s.R_w*s.m_ft*sp.cos(s.beta) + 2.0*s.R_w*s.m_w*sp.cos(s.beta) + sp.Rational(1, 3)*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + sp.Rational(1, 3)*s.W_wf*s.m_ft)*sp.sign(s.d_xw)/(sp.Rational(1, 3)*s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + sp.Rational(1, 3)*s.L_wh*s.m_ft + 1.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 1.0*s.L_wh*s.m_w + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

				self.F_fg = s.R_w*s.mu*(-0.5*s.L_wf*s.m_ft*sp.cos(s.beta)*sp.cos(s.gamma) - 1.5*s.L_wf*s.m_w*sp.cos(s.beta + s.gamma) + 0.5*s.L_wh*s.m_ft*sp.cos(s.beta)*sp.cos(s.gamma) + 1.5*s.L_wh*s.m_w*sp.cos(s.beta)*sp.cos(s.gamma) + 0.5*s.W_wf*s.m_ft*sp.sin(s.beta)*sp.cos(s.gamma) + 1.5*s.W_wf*s.m_w*sp.sin(s.beta + s.gamma))*sp.sign(s.d_xw)/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

				self.d_beta_squared = s.R_w*s.mu*(-0.5*s.I_f*s.m_ft*sp.sin(s.beta) - 1.5*s.I_f*s.m_w*sp.sin(s.beta) - 0.5*s.L_wf**2*s.m_ft**2*sp.sin(s.beta)**3 - 0.25*s.L_wf**2*s.m_ft**2*sp.sin(s.beta)*sp.cos(2.0*s.beta) + 0.25*s.L_wf**2*s.m_ft**2*sp.sin(s.beta) - 1.5*s.L_wf**2*s.m_ft*s.m_w*sp.sin(s.beta) + 0.5*s.L_wf*s.L_wh*s.m_ft**2*sp.sin(s.beta)**3 + 0.25*s.L_wf*s.L_wh*s.m_ft**2*sp.sin(s.beta)*sp.cos(2.0*s.beta) - 0.25*s.L_wf*s.L_wh*s.m_ft**2*sp.sin(s.beta) + 1.5*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.sin(s.beta)**3 + 0.75*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.sin(s.beta)*sp.cos(2.0*s.beta) - 0.75*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.sin(s.beta) + 0.5*s.L_wf*s.W_wf*s.m_ft**2*sp.sin(s.beta)*sp.sin(2.0*s.beta) + 1.0*s.L_wf*s.W_wf*s.m_ft**2*sp.cos(s.beta)**3 - 0.5*s.L_wf*s.W_wf*s.m_ft**2*sp.cos(s.beta) - 0.25*s.L_wh*s.W_wf*s.m_ft**2*sp.sin(s.beta)*sp.sin(2.0*s.beta) - 0.5*s.L_wh*s.W_wf*s.m_ft**2*sp.cos(s.beta)**3 - 0.75*s.L_wh*s.W_wf*s.m_ft*s.m_w*sp.sin(s.beta)*sp.sin(2.0*s.beta) - 1.5*s.L_wh*s.W_wf*s.m_ft*s.m_w*sp.cos(s.beta)**3 + 0.5*s.W_wf**2*s.m_ft**2*sp.sin(s.beta)**3 + 0.25*s.W_wf**2*s.m_ft**2*sp.sin(s.beta)*sp.cos(2.0*s.beta) - 0.75*s.W_wf**2*s.m_ft**2*sp.sin(s.beta) - 1.5*s.W_wf**2*s.m_ft*s.m_w*sp.sin(s.beta))*sp.sign(s.d_xw)/((s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))*sp.cos(s.beta))

				self.F_wg = -s.R_w*s.mu*(1.0*s.L_wf*s.m_ft*sp.sin(s.beta)*sp.sin(s.gamma) - 1.0*s.L_wh*s.m_ft*sp.cos(s.beta)*sp.cos(s.gamma) - 3.0*s.L_wh*s.m_w*sp.cos(s.beta)*sp.cos(s.gamma) + 1.0*s.W_wf*s.m_ft*sp.sin(s.gamma)*sp.cos(s.beta))*sp.sign(s.d_xw)/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

				self.other = 0

	#^ #######################################
	class _F_HY(object): #~class F_HY
		def __init__(self, root):
			self._root = root
			self._explicit_acc = self._ExplicitAcc(self._root)
			self._explicit_Fhx = self._ExplicitFhx(self._root)

		def __getattribute__(self, name):
			func = object.__getattribute__(self, '_root')._getattribute_vars
			return func(self, name)

		class _ExplicitAcc(object): #~class -ExplicitAcc
			def __init__(self, root):
				s = root.p

				self.all = (1.0*s.F_fg*s.L_wf*s.R_w*sp.cos(s.gamma) + 1.0*s.F_fg*s.L_wf*s.R_w*sp.cos(2.0*s.beta + s.gamma) + 1.0*s.F_fg*s.L_wh*s.R_w*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 1.0*s.F_fg*s.L_wh*s.R_w*sp.sin(2.0*s.beta)*sp.sin(s.gamma) - 1.0*s.F_fg*s.R_w*s.W_wf*sp.sin(s.gamma) - 1.0*s.F_fg*s.R_w*s.W_wf*sp.sin(2.0*s.beta + s.gamma) + 2.0*s.F_wg*s.L_wh*s.R_w*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.L_wh*s.R_w*sp.sin(2.0*s.beta)*sp.sin(s.gamma) + 2.0*s.I_f*s.R_w*s.d_beta**2*sp.sin(s.beta) + 2.0*s.L_wf**2*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) - 2.0*s.L_wf*s.L_wh*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) - 1.0*s.L_wf*s.R_w*s.dd_xw*s.m_ft*sp.sin(2.0*s.beta) - 2.0*s.L_wh*s.R_w*s.W_wf*s.d_beta**2*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + 1.0*s.L_wh*s.R_w*s.dd_xw*s.m_ft*sp.sin(2.0*s.beta) + 3.0*s.L_wh*s.R_w*s.dd_xw*s.m_w*sp.sin(2.0*s.beta) - 2.0*s.L_wh*s.T_m*sp.sin(2.0*s.beta) - 4.0*s.R_w*s.T_m*sp.cos(s.beta) + 2.0*s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft*sp.sin(s.beta) - 1.0*s.R_w*s.W_wf*s.dd_xw*s.m_ft*sp.cos(2.0*s.beta) - 1.0*s.R_w*s.W_wf*s.dd_xw*s.m_ft)/(s.L_wh*s.R_w*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.dd_xw = (-s.L_wf*s.m_ft*sp.sin(2.0*s.beta) + s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 3.0*s.L_wh*s.m_w*sp.sin(2.0*s.beta) - s.W_wf*s.m_ft*sp.cos(2.0*s.beta) - s.W_wf*s.m_ft)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.T_m = -(2.0*s.L_wh*sp.sin(2.0*s.beta) + 4.0*s.R_w*sp.cos(s.beta))/(s.L_wh*s.R_w*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_fg = 1.0*(s.L_wf*sp.cos(s.gamma) + s.L_wf*sp.cos(2.0*s.beta + s.gamma) + s.L_wh*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.L_wh*sp.sin(2.0*s.beta)*sp.sin(s.gamma) - s.W_wf*sp.sin(s.gamma) - s.W_wf*sp.sin(2.0*s.beta + s.gamma))/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.d_beta_squared = 2.0*(s.I_f + s.L_wf**2*s.m_ft - s.L_wf*s.L_wh*s.m_ft - s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft)*sp.sin(s.beta)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_wg = 2.0*(s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + sp.sin(s.gamma))*sp.sin(2.0*s.beta)/(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0)

				self.other = 0

		class _ExplicitFhx(object): #~class -ExplicitFhx

			def __init__(self, root):
				self._root = root
				self._no_add_constr = self._NoAddedConstraint(self._root)
				self._add_constr = self._AddedConstraint(self._root)

			def __getattribute__(self, name):
				func = object.__getattribute__(self, '_root')._getattribute_constr
				return func(self, name)

			class _AddedConstraint(object):
				def __init__(self, root):
					s = root.p

					self.all = (1.0*s.F_fg*s.L_wf*s.R_w*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 1.0*s.F_fg*s.L_wf*s.R_w*s.m_ft*sp.cos(2.0*s.beta)*sp.cos(s.gamma) + 1.0*s.F_fg*s.L_wf*s.R_w*s.m_ft*sp.cos(s.gamma) + 3.0*s.F_fg*s.L_wf*s.R_w*s.m_w*sp.cos(s.gamma) + 3.0*s.F_fg*s.L_wf*s.R_w*s.m_w*sp.cos(2.0*s.beta + s.gamma) + 1.0*s.F_fg*s.R_w*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 1.0*s.F_fg*s.R_w*s.W_wf*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - 1.0*s.F_fg*s.R_w*s.W_wf*s.m_ft*sp.sin(2.0*s.beta)*sp.cos(s.gamma) - 3.0*s.F_fg*s.R_w*s.W_wf*s.m_w*sp.sin(s.gamma) - 3.0*s.F_fg*s.R_w*s.W_wf*s.m_w*sp.sin(2.0*s.beta + s.gamma) - 1.0*s.F_hx*s.L_wf*s.R_w*s.m_ft*sp.sin(2.0*s.beta) + 1.0*s.F_hx*s.L_wh*s.R_w*s.m_ft*sp.sin(2.0*s.beta) + 3.0*s.F_hx*s.L_wh*s.R_w*s.m_w*sp.sin(2.0*s.beta) - 1.0*s.F_hx*s.R_w*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) - 1.0*s.F_hx*s.R_w*s.W_wf*s.m_ft + 2.0*s.F_wg*s.L_wf*s.R_w*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.L_wf*s.R_w*s.m_ft*sp.sin(2.0*s.beta)*sp.sin(s.gamma) + 2.0*s.F_wg*s.R_w*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.R_w*s.W_wf*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.R_w*s.W_wf*s.m_ft*sp.sin(s.gamma)*sp.cos(2.0*s.beta) + 2.0*s.F_wg*s.R_w*s.W_wf*s.m_ft*sp.sin(s.gamma) + 2.0*s.I_f*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) + 6.0*s.I_f*s.R_w*s.d_beta**2*s.m_w*sp.sin(s.beta) + 6.0*s.L_wf**2*s.R_w*s.d_beta**2*s.m_ft*s.m_w*sp.sin(s.beta) - 2.0*s.L_wf*s.R_w*s.W_wf*s.d_beta**2*s.m_ft**2*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) - 2.0*s.L_wf*s.R_w*s.W_wf*s.d_beta**2*s.m_ft**2*sp.cos(s.beta) - 2.0*s.L_wf*s.T_m*s.m_ft*sp.sin(2.0*s.beta) - 4.0*s.R_w*s.T_m*s.m_ft*sp.cos(s.beta) - 12.0*s.R_w*s.T_m*s.m_w*sp.cos(s.beta) - 2.0*s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft**2*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + 2.0*s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft**2*sp.sin(s.beta) + 6.0*s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft*s.m_w*sp.sin(s.beta) - 2.0*s.T_m*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) - 2.0*s.T_m*s.W_wf*s.m_ft)/(s.R_w*(s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + s.L_wh*s.m_ft + 3.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 3.0*s.L_wh*s.m_w + s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw)))

					self.F_hx = (-s.L_wf*s.m_ft*sp.sin(2.0*s.beta) + s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 3.0*s.L_wh*s.m_w*sp.sin(2.0*s.beta) - s.W_wf*s.m_ft*sp.cos(2.0*s.beta) - s.W_wf*s.m_ft)/(s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + s.L_wh*s.m_ft + 3.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 3.0*s.L_wh*s.m_w + s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.T_m = -(2.0*s.L_wf*s.m_ft*sp.sin(2.0*s.beta) + 4.0*s.R_w*s.m_ft*sp.cos(s.beta) + 12.0*s.R_w*s.m_w*sp.cos(s.beta) + 2.0*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 2.0*s.W_wf*s.m_ft)/(s.R_w*(s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + s.L_wh*s.m_ft + 3.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 3.0*s.L_wh*s.m_w + s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw)))

					self.F_fg = (1.0*s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 1.0*s.L_wf*s.m_ft*sp.cos(2.0*s.beta)*sp.cos(s.gamma) + 1.0*s.L_wf*s.m_ft*sp.cos(s.gamma) + 3.0*s.L_wf*s.m_w*sp.cos(s.gamma) + 3.0*s.L_wf*s.m_w*sp.cos(2.0*s.beta + s.gamma) + 1.0*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + 1.0*s.W_wf*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - 1.0*s.W_wf*s.m_ft*sp.sin(2.0*s.beta)*sp.cos(s.gamma) - 3.0*s.W_wf*s.m_w*sp.sin(s.gamma) - 3.0*s.W_wf*s.m_w*sp.sin(2.0*s.beta + s.gamma))/(s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + s.L_wh*s.m_ft + 3.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 3.0*s.L_wh*s.m_w + s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.d_beta_squared = (2.0*s.I_f*s.m_ft*sp.sin(s.beta) + 6.0*s.I_f*s.m_w*sp.sin(s.beta) + 6.0*s.L_wf**2*s.m_ft*s.m_w*sp.sin(s.beta) - 2.0*s.L_wf*s.W_wf*s.m_ft**2*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) - 2.0*s.L_wf*s.W_wf*s.m_ft**2*sp.cos(s.beta) - 2.0*s.W_wf**2*s.m_ft**2*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + 2.0*s.W_wf**2*s.m_ft**2*sp.sin(s.beta) + 6.0*s.W_wf**2*s.m_ft*s.m_w*sp.sin(s.beta))/(s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + s.L_wh*s.m_ft + 3.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 3.0*s.L_wh*s.m_w + s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.F_wg = 2.0*s.m_ft*(s.L_wf*s.mu*sp.sin(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.L_wf*sp.sin(2.0*s.beta)*sp.sin(s.gamma) + s.W_wf*s.mu*sp.cos(2.0*s.beta)*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.W_wf*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.W_wf*sp.sin(s.gamma)*sp.cos(2.0*s.beta) + s.W_wf*sp.sin(s.gamma))/(s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + s.L_wh*s.m_ft + 3.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 3.0*s.L_wh*s.m_w + s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.other = 0

			class _NoAddedConstraint(object):
				def __init__(self, root):
					s = root.p

					self.all = (0.5*s.F_fg*s.R_w*(2.0*s.I_f*s.mu*sp.sign(s.d_xw) - s.L_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.sin(2.0*s.beta) + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 2.0*s.L_wf*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.sin(2.0*s.beta))*sp.cos(s.gamma) + 2.0*s.F_wg*s.R_w*s.mu*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft)*sp.cos(s.gamma)*sp.sign(s.d_xw) - 0.5*s.R_w*s.d_beta**2*s.m_ft*(s.L_wf*sp.sin(1.0*s.beta) + s.W_wf*sp.cos(1.0*s.beta))*(2.0*s.I_f*s.mu*sp.sign(s.d_xw) - s.L_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.sin(2.0*s.beta) + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 2.0*s.L_wf*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.sin(2.0*s.beta)) + s.R_w*s.dd_xw*s.m_w*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft) + 0.5*s.R_w*s.m_ft*(-s.L_wf*s.d_beta**2*sp.cos(1.0*s.beta) + s.W_wf*s.d_beta**2*sp.sin(1.0*s.beta) + s.dd_xw)*(2.0*s.I_f + s.L_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.L_wf**2*s.m_ft + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 2.0*s.L_wf*s.W_wf*s.m_ft*sp.sin(2.0*s.beta) - s.W_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft) - s.R_w*s.m_ft*(s.F_hx*s.L_wf*sp.sin(1.0*s.beta) - s.F_hx*s.L_wh*sp.sin(1.0*s.beta) + s.F_hx*s.W_wf*sp.cos(1.0*s.beta) + 2.0*s.T_m)*(-s.L_wf*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) + s.L_wf*sp.sin(1.0*s.beta) + s.W_wf*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) + s.W_wf*sp.cos(1.0*s.beta)) + 0.5*s.R_w*(s.F_fg*sp.sin(s.gamma) - s.F_hx)*(2.0*s.I_f + s.L_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.L_wf**2*s.m_ft + 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 2.0*s.L_wf*s.W_wf*s.m_ft*sp.sin(2.0*s.beta) - s.W_wf**2*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - s.W_wf**2*s.m_ft*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft) + 2.0*s.R_w*(s.F_wg*sp.sin(s.gamma) + s.dd_xw*s.m_w)*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft) - 2.0*s.T_m*(s.I_f + s.L_wf**2*s.m_ft + s.W_wf**2*s.m_ft))/(s.R_w*(s.I_f*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw)))

					self.F_hx = (-1.0*s.I_f - 1.0*s.L_wf**2*s.m_ft*sp.sin(1.0*s.beta)**2 - 0.5*s.L_wf**2*s.m_ft*sp.cos(2.0*s.beta) - 0.5*s.L_wf**2*s.m_ft - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 1.0*s.L_wf*s.L_wh*s.m_ft*sp.sin(1.0*s.beta)**2 - 2.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(1.0*s.beta)**2*sp.sign(s.d_xw) - 1.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + 1.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw) + 1.0*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(1.0*s.beta)**2*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.sin(2.0*s.beta) + 1.0*s.W_wf**2*s.m_ft*sp.sin(1.0*s.beta)**2 + 0.5*s.W_wf**2*s.m_ft*sp.cos(2.0*s.beta) - 1.5*s.W_wf**2*s.m_ft)/(s.I_f*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.T_m = 2.0*(-s.I_f - s.L_wf**2*s.m_ft + s.L_wf*s.R_w*s.m_ft*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) - s.L_wf*s.R_w*s.m_ft*sp.sin(1.0*s.beta) - s.R_w*s.W_wf*s.m_ft*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) - s.R_w*s.W_wf*s.m_ft*sp.cos(1.0*s.beta) - s.W_wf**2*s.m_ft)/(s.R_w*(s.I_f*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw)))

					self.F_fg = (1.0*s.I_f*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 1.0*s.I_f*sp.sin(s.gamma) + 0.5*s.L_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) - 0.5*s.L_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta + s.gamma)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.m_ft*sp.sin(s.gamma) + 0.5*s.L_wf**2*s.m_ft*sp.sin(2.0*s.beta + s.gamma) + 1.0*s.L_wf*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta + s.gamma)*sp.sign(s.d_xw) + 1.0*s.L_wf*s.W_wf*s.m_ft*sp.cos(2.0*s.beta + s.gamma) + 0.5*s.W_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + 0.5*s.W_wf**2*s.m_ft*s.mu*sp.cos(2.0*s.beta + s.gamma)*sp.sign(s.d_xw) + 0.5*s.W_wf**2*s.m_ft*sp.sin(s.gamma) - 0.5*s.W_wf**2*s.m_ft*sp.sin(2.0*s.beta + s.gamma))/(s.I_f*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.d_beta_squared = 0

					self.F_wg = 2.0*(s.I_f*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.I_f*sp.sin(s.gamma) + s.L_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*sp.sin(s.gamma) + s.W_wf**2*s.m_ft*s.mu*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.W_wf**2*s.m_ft*sp.sin(s.gamma))/(s.I_f*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw))

					self.other = (-1.0*s.I_f*s.L_wf*s.d_beta**2*s.m_ft*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) - 1.0*s.I_f*s.L_wf*s.d_beta**2*s.m_ft*sp.cos(1.0*s.beta) - 1.0*s.I_f*s.W_wf*s.d_beta**2*s.m_ft*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) + 1.0*s.I_f*s.W_wf*s.d_beta**2*s.m_ft*sp.sin(1.0*s.beta) + 1.0*s.I_f*s.dd_xw*s.m_ft + 3.0*s.I_f*s.dd_xw*s.m_w - 1.0*s.L_wf**3*s.d_beta**2*s.m_ft**2*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) - 1.0*s.L_wf**3*s.d_beta**2*s.m_ft**2*sp.cos(1.0*s.beta) - 1.0*s.L_wf**2*s.W_wf*s.d_beta**2*s.m_ft**2*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) + 1.0*s.L_wf**2*s.W_wf*s.d_beta**2*s.m_ft**2*sp.sin(1.0*s.beta) + 0.5*s.L_wf**2*s.dd_xw*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wf**2*s.dd_xw*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.L_wf**2*s.dd_xw*s.m_ft**2 + 3.0*s.L_wf**2*s.dd_xw*s.m_ft*s.m_w - 1.0*s.L_wf*s.W_wf**2*s.d_beta**2*s.m_ft**2*s.mu*sp.sin(1.0*s.beta)*sp.sign(s.d_xw) - 1.0*s.L_wf*s.W_wf**2*s.d_beta**2*s.m_ft**2*sp.cos(1.0*s.beta) + 1.0*s.L_wf*s.W_wf*s.dd_xw*s.m_ft**2*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 1.0*s.L_wf*s.W_wf*s.dd_xw*s.m_ft**2*sp.sin(2.0*s.beta) - 1.0*s.W_wf**3*s.d_beta**2*s.m_ft**2*s.mu*sp.cos(1.0*s.beta)*sp.sign(s.d_xw) + 1.0*s.W_wf**3*s.d_beta**2*s.m_ft**2*sp.sin(1.0*s.beta) - 0.5*s.W_wf**2*s.dd_xw*s.m_ft**2*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.W_wf**2*s.dd_xw*s.m_ft**2*sp.cos(2.0*s.beta) + 0.5*s.W_wf**2*s.dd_xw*s.m_ft**2 + 3.0*s.W_wf**2*s.dd_xw*s.m_ft*s.m_w)/(s.I_f*s.mu*sp.sign(s.d_xw) + s.L_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) - 0.5*s.L_wf*s.L_wh*s.m_ft*s.mu*sp.sign(s.d_xw) + 0.5*s.L_wf*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + 0.5*s.L_wh*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.L_wh*s.W_wf*s.m_ft + s.W_wf**2*s.m_ft*s.mu*sp.sign(s.d_xw)) #! Contains dd_xw!

	#^ #######################################
	class _F_N(object): #~class F_N
		def __init__(self, root):
			self._root = root
			self._explicit_acc = self._ExplicitAcc(self._root)
			self._explicit_Fhx = self._ExplicitFhx(self._root)

		def __getattribute__(self, name):
			func = object.__getattribute__(self, '_root')._getattribute_vars
			return func(self, name)

		class _ExplicitAcc(object): #~class -ExplicitAcc
			def __init__(self, root):
				s = root.p

				self.all = (-0.5*s.F_fg*s.L_wf*s.R_w*sp.cos(s.gamma) - 0.5*s.F_fg*s.L_wf*s.R_w*sp.cos(2.0*s.beta + s.gamma) + 0.5*s.F_fg*s.L_wh*s.R_w*sp.cos(s.gamma) + 0.5*s.F_fg*s.L_wh*s.R_w*sp.cos(2.0*s.beta + s.gamma) + 0.5*s.F_fg*s.R_w*s.W_wf*sp.sin(s.gamma) + 0.5*s.F_fg*s.R_w*s.W_wf*sp.sin(2.0*s.beta + s.gamma) + 1.0*s.F_wg*s.L_wh*s.R_w*sp.cos(s.gamma) + 1.0*s.F_wg*s.L_wh*s.R_w*sp.cos(2.0*s.beta + s.gamma) - 1.0*s.I_f*s.R_w*s.d_beta**2*sp.sin(s.beta) - 1.0*s.L_wf**2*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) + 1.0*s.L_wf*s.L_wh*s.R_w*s.d_beta**2*s.m_ft*sp.sin(s.beta) + 0.5*s.L_wf*s.R_w*s.dd_xw*s.m_ft*sp.sin(2.0*s.beta) - 1.0*s.L_wh*s.R_w*s.W_wf*s.d_beta**2*s.m_ft*sp.cos(s.beta) - 0.5*s.L_wh*s.R_w*s.dd_xw*s.m_ft*sp.sin(2.0*s.beta) - 1.5*s.L_wh*s.R_w*s.dd_xw*s.m_w*sp.sin(2.0*s.beta) + 1.0*s.L_wh*s.T_m*sp.sin(2.0*s.beta) + 2.0*s.R_w*s.T_m*sp.cos(s.beta) - 1.0*s.R_w*s.W_wf**2*s.d_beta**2*s.m_ft*sp.sin(s.beta) + 0.5*s.R_w*s.W_wf*s.dd_xw*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.R_w*s.W_wf*s.dd_xw*s.m_ft)/(s.L_wh*s.R_w*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_hx = 0

				self.F_hy = 0

				self.dd_xw = (0.5*s.L_wf*s.m_ft*sp.sin(2.0*s.beta) - 0.5*s.L_wh*s.m_ft*sp.sin(2.0*s.beta) - 1.5*s.L_wh*s.m_w*sp.sin(2.0*s.beta) + 0.5*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + 0.5*s.W_wf*s.m_ft)/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.T_m = (1.0*s.L_wh*sp.sin(2.0*s.beta) + 2.0*s.R_w*sp.cos(s.beta))/(s.L_wh*s.R_w*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_fg = 0.5*(-s.L_wf*sp.cos(s.gamma) - s.L_wf*sp.cos(2.0*s.beta + s.gamma) + s.L_wh*sp.cos(s.gamma) + s.L_wh*sp.cos(2.0*s.beta + s.gamma) + s.W_wf*sp.sin(s.gamma) + s.W_wf*sp.sin(2.0*s.beta + s.gamma))/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.d_beta_squared = (-s.I_f*sp.sin(s.beta) - s.L_wf**2*s.m_ft*sp.sin(s.beta) + s.L_wf*s.L_wh*s.m_ft*sp.sin(s.beta) - s.L_wh*s.W_wf*s.m_ft*sp.cos(s.beta) - s.W_wf**2*s.m_ft*sp.sin(s.beta))/(s.L_wh*(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0))

				self.F_wg = 1.0*(sp.cos(s.gamma) + sp.cos(2.0*s.beta + s.gamma))/(s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.cos(2.0*s.beta) + 1.0)

		class _ExplicitFhx(object): #~class -ExplicitFhx
			def __init__(self, root):
				s = root.p

				self.all = (0.5*s.F_fg*s.R_w*(s.m_ft + 3.0*s.m_w)*(-s.L_wf*sp.cos(s.beta) + s.L_wh*sp.cos(s.beta) + s.W_wf*sp.sin(s.beta))*sp.cos(s.beta)*sp.cos(s.gamma) + 1.0*s.F_wg*s.L_wh*s.R_w*(s.m_ft + 3.0*s.m_w)*sp.cos(s.beta)**2*sp.cos(s.gamma) - 1.0*s.F_wg*s.R_w*s.m_ft*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*sp.sin(s.gamma)*sp.cos(s.beta) - 1.5*s.R_w*s.d_beta**2*s.m_ft*s.m_w*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*(s.L_wf*sp.cos(s.beta) - s.W_wf*sp.sin(s.beta))*sp.cos(s.beta) - 0.5*s.R_w*s.d_beta**2*s.m_ft*(s.m_ft + 3.0*s.m_w)*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*(-s.L_wf*sp.cos(s.beta) + s.L_wh*sp.cos(s.beta) + s.W_wf*sp.sin(s.beta))*sp.cos(s.beta) - 0.25*s.R_w*s.d_beta**2*(2.0*s.I_f*s.m_ft + 6.0*s.I_f*s.m_w + s.L_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + s.L_wf**2*s.m_ft**2 + 6.0*s.L_wf**2*s.m_ft*s.m_w - s.L_wf*s.L_wh*s.m_ft**2*sp.cos(2.0*s.beta) - s.L_wf*s.L_wh*s.m_ft**2 - 3.0*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.cos(2.0*s.beta) - 3.0*s.L_wf*s.L_wh*s.m_ft*s.m_w - 2.0*s.L_wf*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) + s.L_wh*s.W_wf*s.m_ft**2*sp.sin(2.0*s.beta) + 3.0*s.L_wh*s.W_wf*s.m_ft*s.m_w*sp.sin(2.0*s.beta) - s.W_wf**2*s.m_ft**2*sp.cos(2.0*s.beta) + s.W_wf**2*s.m_ft**2 + 6.0*s.W_wf**2*s.m_ft*s.m_w)*sp.sin(s.beta) + 1.5*s.R_w*s.m_w*(s.F_fg*sp.sin(s.gamma) - s.F_hx)*(s.L_wf*sp.sin(s.beta) + s.W_wf*sp.cos(s.beta))*sp.cos(s.beta) + 0.5*s.R_w*(s.m_ft + 3.0*s.m_w)*(s.F_hx*s.L_wf*sp.sin(s.beta) - s.F_hx*s.L_wh*sp.sin(s.beta) + s.F_hx*s.W_wf*sp.cos(s.beta) + 2.0*s.T_m)*sp.cos(s.beta) + 0.5*s.T_m*s.m_ft*(s.L_wf*sp.sin(2.0*s.beta) + s.W_wf*sp.cos(2.0*s.beta) + s.W_wf))/(s.R_w*(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))*sp.cos(s.beta))

				self.F_hx = (0.5*s.L_wf*s.m_ft*sp.sin(s.beta) - 0.5*s.L_wh*s.m_ft*sp.sin(s.beta) - 1.5*s.L_wh*s.m_w*sp.sin(s.beta) + 0.5*s.W_wf*s.m_ft*sp.cos(s.beta))/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

				self.F_hy = 0

				self.T_m = (sp.Rational(1, 3)*s.L_wf*s.m_ft*sp.sin(2.0*s.beta) + sp.Rational(2, 3)*s.R_w*s.m_ft*sp.cos(s.beta) + 2.0*s.R_w*s.m_w*sp.cos(s.beta) + sp.Rational(1, 3)*s.W_wf*s.m_ft*sp.cos(2.0*s.beta) + sp.Rational(1, 3)*s.W_wf*s.m_ft)/(s.R_w*(sp.Rational(1, 3)*s.L_wf*s.m_ft*s.mu*sp.sin(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.L_wh*s.m_ft*sp.cos(2.0*s.beta) + sp.Rational(1, 3)*s.L_wh*s.m_ft + 1.0*s.L_wh*s.m_w*sp.cos(2.0*s.beta) + 1.0*s.L_wh*s.m_w + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.cos(2.0*s.beta)*sp.sign(s.d_xw) + sp.Rational(1, 3)*s.W_wf*s.m_ft*s.mu*sp.sign(s.d_xw)))

				self.F_fg = (-0.5*s.L_wf*s.m_ft*sp.cos(s.beta)*sp.cos(s.gamma) - 1.5*s.L_wf*s.m_w*sp.cos(s.beta + s.gamma) + 0.5*s.L_wh*s.m_ft*sp.cos(s.beta)*sp.cos(s.gamma) + 1.5*s.L_wh*s.m_w*sp.cos(s.beta)*sp.cos(s.gamma) + 0.5*s.W_wf*s.m_ft*sp.sin(s.beta)*sp.cos(s.gamma) + 1.5*s.W_wf*s.m_w*sp.sin(s.beta + s.gamma))/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))

				self.d_beta_squared = (-0.5*s.I_f*s.m_ft*sp.sin(s.beta) - 1.5*s.I_f*s.m_w*sp.sin(s.beta) - 0.5*s.L_wf**2*s.m_ft**2*sp.sin(s.beta)**3 - 0.25*s.L_wf**2*s.m_ft**2*sp.sin(s.beta)*sp.cos(2.0*s.beta) + 0.25*s.L_wf**2*s.m_ft**2*sp.sin(s.beta) - 1.5*s.L_wf**2*s.m_ft*s.m_w*sp.sin(s.beta) + 0.5*s.L_wf*s.L_wh*s.m_ft**2*sp.sin(s.beta)**3 + 0.25*s.L_wf*s.L_wh*s.m_ft**2*sp.sin(s.beta)*sp.cos(2.0*s.beta) - 0.25*s.L_wf*s.L_wh*s.m_ft**2*sp.sin(s.beta) + 1.5*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.sin(s.beta)**3 + 0.75*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.sin(s.beta)*sp.cos(2.0*s.beta) - 0.75*s.L_wf*s.L_wh*s.m_ft*s.m_w*sp.sin(s.beta) + 0.5*s.L_wf*s.W_wf*s.m_ft**2*sp.sin(s.beta)*sp.sin(2.0*s.beta) + 1.0*s.L_wf*s.W_wf*s.m_ft**2*sp.cos(s.beta)**3 - 0.5*s.L_wf*s.W_wf*s.m_ft**2*sp.cos(s.beta) - 0.25*s.L_wh*s.W_wf*s.m_ft**2*sp.sin(s.beta)*sp.sin(2.0*s.beta) - 0.5*s.L_wh*s.W_wf*s.m_ft**2*sp.cos(s.beta)**3 - 0.75*s.L_wh*s.W_wf*s.m_ft*s.m_w*sp.sin(s.beta)*sp.sin(2.0*s.beta) - 1.5*s.L_wh*s.W_wf*s.m_ft*s.m_w*sp.cos(s.beta)**3 + 0.5*s.W_wf**2*s.m_ft**2*sp.sin(s.beta)**3 + 0.25*s.W_wf**2*s.m_ft**2*sp.sin(s.beta)*sp.cos(2.0*s.beta) - 0.75*s.W_wf**2*s.m_ft**2*sp.sin(s.beta) - 1.5*s.W_wf**2*s.m_ft*s.m_w*sp.sin(s.beta))/((s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))*sp.cos(s.beta))

				self.F_wg = (-1.0*s.L_wf*s.m_ft*sp.sin(s.beta)*sp.sin(s.gamma) + 1.0*s.L_wh*s.m_ft*sp.cos(s.beta)*sp.cos(s.gamma) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta)*sp.cos(s.gamma) - 1.0*s.W_wf*s.m_ft*sp.sin(s.gamma)*sp.cos(s.beta))/(s.L_wf*s.m_ft*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.m_ft*sp.cos(s.beta) + 3.0*s.L_wh*s.m_w*sp.cos(s.beta) + s.W_wf*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))


if __name__ == "__main__":
	import os
	os.system('clear')

	#region #% functions #######################################
	def make_basic(eq, simplify=False):
		eq = eq.subs(s.d_xw, 1)
		if simplify:
			eq = sp.simplify(eq)
		return eq
	#endregion

	s = ParamsSymbolic(); s.beta = 1.0*s.beta #@ Circumvent sympy bug
	n = ParamsNumeric()

	#% Symbolic calculations #######################################
	if True:
		#s.d_xw = 1

		eqs_sym_unconstr = ModelEquations(s, acc_explicit=False, added_constraint=False)
		eqs_sym_constr = ModelEquations(s, acc_explicit=False, added_constraint=True)
		eqs_troll_expl = ModelEquations(s, acc_explicit=False, added_constraint=True, expand_rolling_torque=False)

		target_eqs = eqs_sym_constr
		output_str = "F_hx"
		output_func = getattr(target_eqs, output_str)

		vars = ['all', 'dd_xw', 'F_hx', 'F_hy', 'T_m', 'F_fg', 'F_wg', 'd_beta_squared', 'other', 'denom']
		vars.remove(output_str)

		#@ Print
		if False:
			print(output_str)
			for elem_str in vars:
				elem_func = getattr(output_func, elem_str)
				print('- {}:\n{}\n'.format(elem_str, convert_sympy_to_sympy_printed(elem_func)))

		if True:
			denom = getattr(output_func, 'denom')
			vars.remove('denom')

			print(output_str)
			print('- denom:\n{}\n'.format(convert_sympy_to_latex(denom)))
			for elem_str in vars:
				elem_func = getattr(output_func, elem_str)
				elem_func_ = sp.simplify(elem_func * denom)
				print('- {}:\n({}) / denom\n'.format(elem_str, convert_sympy_to_latex(elem_func_)))


	#% Numueric calculations #######################################
	if False:
		n.set_params_nom()
		n.set_params_symbolic(['T_roll', 'F_hx', 'F_hy', 'T_m', 'dd_xw'])
		n.m_b = n.m_b + 20

		n.d_beta = 0
		n.d_xw = 1  #@

		#^ --------------------------
		eqs_num_acc = ModelEquations(n, acc_explicit=True)
		eqs_num_Fhx = ModelEquations(n, acc_explicit=False)

		#^ Calculate necessary acceleration force when trolley has max weight --------------------------
		config_simulation = load_config('trolley_simulation')
		target_v = config_simulation['target_velocity']
		a_duration = config_simulation['acceleration_duration']
		target_a = target_v/a_duration

		n.m_b += added_mass
		eqs_num_acc_sup = ModelEquations(n, acc_explicit=True, support_mass=True)

		F = eqs_num_acc_sup.F_hx.all
		print(F.subs([(s.T_m, 0), (s.dd_xw, target_a)]))
