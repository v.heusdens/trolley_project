import numpy as np
import platform
import rospkg
import sympy as sp
import yaml
from inspect import currentframe

from python_scripts.dynamics.trolley_dynamic_equations import get_eqs_COM, get_eqs_constraint, get_eqs_motion
from toolbox.general import beep, print_list
from toolbox.params import ParamsGroups, ParamsSymbolic
from toolbox.transform import add_sympy_prefix, expand_to_variables

# region ## SETTINGS
#% calculations
expand_params = []
expand_params.append('I_w')

expand_params.append('T_roll')  #! Replace 'T_roll' by it's equation
T_roll_nonlin = True  #! Make T_roll dependent of direction of travel (sign(d_xw))

s = ParamsSymbolic(expand_params, T_roll_nonlin=T_roll_nonlin)

add_con = True  #! Add additional constraint to model
var_to_ditch = s.F_hy  #! Which variable to eliminate due to additional constraint (if add_con = True)

var_to_solve = s.F_hx  #! Additional implicit variable to eliminate when extra constraint is added
acc_explicit = False  #! set to true if acc isn't var_to_solve and is needed explicitly in the equation instead of Fhx

#% additional
simplify = False  #@ Simplify equation for verification
calc_com = False  #@ Calculate center of mass variables
expand = True  #@ Print solved variable as elements of other variables
disp_type = 'c'  #@ Display type of variables: normal ('n'), class ('c') or report ('r')
beep_when_done = True  #@ Beep when computation is finished (only on linux)

# endregion


# region  ##functions
def print_inverse_model(expr_parts):
	if disp_type == 'r':
		print("({})*{} \\ \n \
		    + ({})*{} \\ \n \
				= {} \\ \n \
				    - ({})*{} \\ \n \
						- ({})*{F_wg}".format(expr_parts[F_hx], F_hx, expr_parts[T_m], T_m, dd_theta, expr_parts[F_fg], F_fg,
			expr_parts[F_wg], F_wg))
	else:
		print("({})*{} \\ \n \
		    + ({})*{} \\ \n \
				- {} \\ \n \
				     + ({})*{} \\ \n \
						 + ({})*{}".format(expr_parts[F_hx], F_hx, expr_parts[T_m], T_m, dd_theta, expr_parts[F_fg], F_fg,
			expr_parts[F_wg], F_wg))


def get_linenumber():
	cf = currentframe()
	return cf.f_back.f_lineno


# endregion functions

#region ## INITIALIZATION ##
if 'linux' not in platform.uname()[0].lower() or "microsoft" in platform.uname()[2].lower():
	beep_when_done = False
	if "microsoft" in platform.uname()[2].lower():
		string = " WSL on windows"
	else:
		string = ""
	print(
		"Warning: beep_when_done only available using Linux drivers. Current os is {}; beep_when_done disabled."
		.format(platform.uname()[0] + string))

print("Settings: \n \
		- calculate center of mass: {} \n \
		- add extra constraint: {} \n \
		- expanded parameters: {} \n \
		- use explicit acceleration: {} \n \
		- simplify equation for verification: {} \n \
		- additionally print model split into elements of forces: {} \n \
		- print output type: {} \n \
		- beep when done: {} \
		".format(calc_com, add_con, expand_params, acc_explicit, simplify, expand, disp_type, beep_when_done))

#% Load parameters
if disp_type == 'r':
	#% Knowns
	F_fg, F_wg, F_hx = sp.symbols('F_{FG} F_{WG} F_{HX}', real=True)
	m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols('m_F m_B m_{FT} m_W I_F I_W', real=True)
	L_wf, L_wh, W_wf, R_w, C = sp.symbols('L_{WF} L_{WH} W_{WF} R_W C', real=True)
	L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf, m_f, m_b = sp.symbols(
		"L_{FC} L_B L_{BC} W_F W_B W_{BC} W_{WF} m_ft' M_B", real=True)
	g, mu = sp.symbols('g, \mu', real=True)

	#% Unknowns
	F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols('F_WX F_WY T_M F_N F_HY F_STAT', real=True)
	beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols(
		'\\beta \dot{\\beta} \ddot{\\beta} \ddot{\\theta} \gamma', real=True)
	dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols('\ddot{x}_F \ddot{y}_F \ddot{x}_W \ddot{y}_W', real=True)
	C = sp.var('c', real=True)
	#
elif disp_type == 'c':
	prefix = 'sym.'
	#% Knowns
	F_fg, F_wg, F_hx = sp.symbols('{0}F_fg {0}F_wg {0}F_hx'.format(prefix), real=True)
	m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols("{0}m_f {0}m_b {0}m_ft {0}m_w {0}I_f {0}I_w".format(prefix),
		real=True)
	L_wf, L_wh, W_wf, R_w = sp.symbols("{0}L_wf {0}L_wh {0}W_wf {0}R_w".format(prefix), real=True)
	L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf, m_f, m_b = sp.symbols(
		"{0}L_fc {0}L_b {0}L_bc {0}W_f {0}W_b {0}W_bc {0}W_wf {0}m_f, {0}m_b".format(prefix), real=True)
	g, mu = sp.symbols("{0}g {0}mu".format(prefix), real=True)

	#% Unknowns
	F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols(
		"{0}F_wx {0}F_wy {0}T_m {0}F_n {0}F_hy {0}F_stat".format(prefix), real=True)
	beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols(
		"{0}beta {0}d_beta {0}dd_beta {0}dd_theta {0}gamma".format(prefix), real=True)
	dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols("{0}dd_xf {0}dd_yf {0}dd_xw {0}dd_yw".format(prefix), real=True)
	C = sp.var('{0}C'.format(prefix), real=True)
	#
elif disp_type == 'n':
	#% Knowns
	F_fg, F_wg, F_hx = sp.symbols('F_fg F_wg F_hx', real=True)
	m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols('m_f m_b m_ft m_w I_f I_w', real=True)
	L_wf, L_wh, W_wf, R_w, C = sp.symbols('L_wf L_wh W_wf R_w C', real=True)
	L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf, m_f, m_b = sp.symbols("L_fc L_b L_bc W_f W_b W_bc W_wf m_f, m_b",
		real=True)
	g, mu = sp.symbols("g mu", real=True)

	#% Unknowns
	F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols('F_wx F_wy T_m F_n F_hy F_stat', real=True)
	beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols('beta d_beta dd_beta dd_theta gamma', real=True)
	dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols('dd_xf dd_yf dd_xw dd_yw', real=True)
	C = sp.var('C', real=True)
	#
else:
	raise Exception("wrong display type. Choose either 'n', 'c' or 'r'.")

add_T_roll = False

t = sp.Symbol("t")
theta_t = sp.Function("theta")(t)  #^ pylint: disable=not-callable
beta_t = sp.Function("beta")(t)  #^ pylint: disable=not-callable
xw_t = sp.Function("xw")(t)  #^ pylint: disable=not-callable
yw_t = sp.Function("yw")(t)  #^ pylint: disable=not-callable
xf_t = sp.Function("xf")(t)  #^ pylint: disable=not-callable
yf_t = sp.Function("yf")(t)  #^ pylint: disable=not-callable
# endregion INITIALIZATION

## MAIN ##
if __name__ == "__main__":
	if beep_when_done:
		beep(abs_beep_duration=0.5, once=True)
	try:
		if disp_type == 'c':
			for name, key in s.__dict__.items():
				if key.is_Atom:
					key.name = 's.' + key.name
					setattr(s, name, key)

		#% Variable selection
		vars_eom = s.get_symbolic_params(ParamsGroups().params_acceleration)
		vars_constr = s.get_symbolic_params(ParamsGroups().params_implicit)
		vars = vars_eom + vars_constr

		if add_con:
			vars.append(var_to_ditch)

		if var_to_solve == s.T_roll:  #@ T_roll
			vars.append(s.T_roll)
			add_T_roll = True

		if var_to_solve not in vars:  #@ other variable which is not in vars
			vars.remove(s.dd_xw)
			vars.append(var_to_solve)

		elif acc_explicit and not var_to_solve == s.dd_xw:  #@ in vars but not dd_xw + dd_xw explicit
			vars.remove(s.dd_xw)
			vars.append(s.F_hx)

		#% Calculate center of mass coordinate equations
		if calc_com:
			print("\nCalculating center of mass coordinates...")

			eqs_com = get_eqs_COM(s)
			impl_vars_com = [s.L_fc, s.L_bc, s.L_wf, s.W_bc, s.W_wf]

			sol_com = sp.solve(eqs_com, impl_vars_com)
			L_wf = sol_com[s.L_wf]
			W_wf = sol_com[s.W_wf]

			print("L_wf: {}".format(L_wf))
			print("W_wf: {}".format(W_wf))

		#% Calculate model
		print("\nCalculating model for {}; explicit acc = {}, added constraint = {}, ditched variable = {}".
			format(var_to_solve, acc_explicit, add_con, var_to_ditch))

		eqs_motion = get_eqs_motion(s, add_T_roll=add_T_roll, T_roll_nonlin=T_roll_nonlin)
		eqs_con = get_eqs_constraint(s, add_con=add_con)

		#% Solving
		sys = eqs_motion + eqs_con

		print('# vars = {}; # eqs = {}'.format(len(vars), len(sys)))
		print('\nvars: {}'.format(vars))
		sol = sp.solve(sys, vars, dict=True)
		print_list(sol)
		model = sol[0][var_to_solve]

		#% Simplification
		#@ Simplify if requested
		if simplify:
			model = model.subs([(gamma, 0), (T_m, 0), (mu, 0)])

		#region #% PRINTING
		model = model.subs(s.beta, 1.0 * s.beta)
		if disp_type == 'r':
			print("\nmodel:\n {} = {}\n".format(var_to_solve, sp.simplify(model)))
		#@ add sp. before sin, cos and tan
		else:
			print("\nmodel:\n {} = {}\n".format(var_to_solve, add_sympy_prefix(sp.simplify(model))))

		#@ Expand model equation wrt variables
		if expand:
			expand_to = list(ParamsGroups().params_forces)
			expand_to.append('d_beta')
			if acc_explicit:
				expand_to.append('dd_xw')
			expand_to = [
				x for x in expand_to if x not in (expand_params + [var_to_solve.name.split('.')[-1]])
			]
			expand_to_ = s.get_symbolic_params(expand_to)
			for i, x in enumerate(expand_to_):
				if x == s.d_beta:
					expand_to_[i] = x**2

			expr_parts = expand_to_variables(model, expand_to_)

			print("\nSubdivided into expand_to:")
			for key, value in expr_parts.items():
				value = value.subs(s.beta, 1.0 * s.beta)  #@ Avoid sympy symplify to crash

				if disp_type == 'r':
					try:
						print("{}: {}\n".format(key, sp.simplify(value)))
					except KeyError as e:
						print("{}: {}\n".format(key, value))
						print("(could not be simplified due to the following KeyError: {})".format(e.message))
				else:
					try:
						print("{}: {}\n".format(key, add_sympy_prefix(sp.simplify(value))))
					except KeyError as e:
						print("{}: {}\n".format(key, add_sympy_prefix(value)))
						print("(could not be simplified due to the following KeyError: {})".format(e.message))
	except KeyboardInterrupt:
		print("Manually interrupted")
	except:
		if beep_when_done:
			beep(cycle_duration=10, abs_beep_duration=0.5)
		raise

	if beep_when_done:
		beep(cycle_duration=10, abs_beep_duration=0.5)
	#endregion
