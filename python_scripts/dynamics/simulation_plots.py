import matplotlib
import matplotlib.lines as mlines
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os
from fractions import Fraction
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.legend_handler import HandlerPatch

from python_scripts.dynamics.simulation import *
from toolbox.general import make_list
from toolbox.params import ParamsNumeric, ParamsSymbolic
from toolbox.ros import find_workspace_root


#! Not plotting correctly!
##------------------------------------------------------------
class HandlerEllipse(HandlerPatch):

	def create_artists(self, legend, orig_handle, xdescent, ydescent, width, height, fontsize, trans):
		center = 0.5 * width - 0.5 * xdescent, 0.5 * height - 0.5 * ydescent
		p = mpatches.Ellipse(xy=center,
			width=(width + height) / 2 + xdescent,
			height=(width + height) / 2 + ydescent)
		self.update_prop(p, orig_handle, legend)
		p.set_transform(trans)
		return [p]


##------------------------------------------------------------
def format_ticks(denom_):

	def fmt(input, _):
		sign = ''
		mult = input / np.pi  #@ Amount of pi in input
		n = int(mult)  #@ Whole times pi in amount
		rem = mult - n  #@ Remainder of amount

		#% Check signs
		if n == 0 and rem == 0: return '0'
		if n < 0 or rem < 0: sign = '-'
		n = abs(n)
		rem = abs(rem)

		#% Fraction
		if rem == 0:
			frac = ''
		else:
			i = rem * denom_
			# if denom_ % i == 0: #@ Fraction can be simplified
			#     num = 1
			#     denom = int(denom_/i)
			# else: #@ Fraction cannot be simplified
			#     num = int(i)
			#     denom = int(denom_)
			frac_ = Fraction(int(i), int(denom_))
			num = frac_.numerator
			denom = frac_.denominator
			frac = r'\frac{{%d}}{{%d}}' % (num, denom)

		#% Integer
		if n == 0 or (n == 1 and rem == 0):
			whole = ''
		else:
			whole = str(n)

		result = r'$ %s %s %s \pi $' % (sign, whole, frac)
		return result

	return fmt


def save_plot(n, fig, name, outside=[], init_cond=np.zeros(6)):
	path = find_workspace_root()
	path = os.path.join(path, '~images', 'trolley_dynamics_evaluation')

	outside = make_list(outside)
	with PdfPages(os.path.join(path, name + '.pdf')) as pdf:
		pdf.attach_note(
			"F_hx: {} \nF_hy: {} \nT_m: {} \nW_wf: {} \nm_b: {} \nmu: {} \ngamma: {} \ntheta-init: {} \nbeta-init: {}"
			.format(n.F_hx, n.F_hy, n.T_m, n.W_wf, n.m_b, n.mu, n.gamma, init_cond['theta'],
			init_cond['beta']))
		pdf.savefig(fig, bbox_extra_artists=outside, bbox_inches='tight')

		print("Plot saved to {}".format(os.path.join(path, name + '.pdf')))


##------------------------------------------------------------
def plot_motion(n, data, xlim=None, ylim=None, show=False, savename=None):
	print("\nPlotting motion...")

	## Config
	plt.rc('xtick', labelsize='small')
	plt.rc('ytick', labelsize='small')
	plt.rc('legend', fontsize='small')

	frame_color = 'g'
	wheel_color = 'b'
	road_color = 'tab:gray'
	# handle_color = 'm'
	init_color = 'r'
	init_linestyle = '--'

	## Plot motion
	fig, ax = plt.subplots()

	for t in data.index[1:]:
		#% Set plot color
		if t == 0.:
			color_frame = init_color
			color_wheel = init_color
		else:
			color_frame = frame_color
			color_wheel = wheel_color

		#% Plot trolley configuration
		posx = get_pos_x(n, data.loc[t, 'xw'], data.loc[t, 'beta'])
		posy = get_pos_y(n, data.loc[t, 'xw'], data.loc[t, 'beta'])

		legend_frame, = ax.plot(posx, posy, color_frame, marker='o', label='frame')
		# legend_handle, = ax.plot(posx[1], posy[1], handle_color, marker='o', label='handle (H)')
		legend_wheel = ax.add_artist(
			plt.Circle([posx[0], posy[0]], n.R_w, color=color_wheel, fill=False, label='wheel'))

	## Plot initial configuration
	posx = get_pos_x(n, data.loc[0, 'xw'], data.loc[0, 'beta'])
	posy = get_pos_y(n, data.loc[0, 'xw'], data.loc[0, 'beta'])
	legend_init, = ax.plot(posx,
		posy,
		init_color,
		marker='o',
		linestyle=init_linestyle,
		label='initial configuration')
	ax.add_artist(
		plt.Circle([posx[0], posy[0]], n.R_w, color=init_color, fill=False, linestyle=init_linestyle))

	## axes limits
	#% x
	if xlim is not None:
		ax.set_xlim(*xlim)
	elif abs(ax.get_xlim()[1] - ax.get_xlim()[0]) < 1:
		ax.set_xlim(-1, 1)
	#% y
	if ylim is not None:
		ax.set_ylim(*ylim)
	else:
		ax.set_ylim(ax.get_ylim()[0] - n.R_w, ax.get_ylim()[1] + 2 * n.R_w)

	## Plot road
	road_x = ax.get_xlim()
	road_y_l = sp.tan(n.gamma) * road_x[0] - n.R_w
	road_y_r = sp.tan(n.gamma) * road_x[1] - n.R_w
	legend_road, = ax.plot(road_x, [road_y_l, road_y_r], 'k', color=road_color, label="road")

	## Formatting
	ax.set_xlabel('horizontal position [m]')
	ax.set_ylabel('vertical position [m]')
	ax.set_aspect('equal')

	#@ Legend
	lgd = ax.legend(handles=[legend_road, legend_wheel, legend_frame, legend_init],
		handler_map={mpatches.Circle: HandlerEllipse()},
		loc='upper center',
		bbox_to_anchor=(0.5, 1.1),
		ncol=4)

	if savename is not None:
		save_plot(n, fig, savename, lgd, init_cond=data.iloc[0][['theta', 'beta']])

	if show:
		plt.show()

	plt.close()
	print("Done plotting motion.")


def plot_states(n, data, states, shape, show=False, savename=None):
	"""
    data :    callable object containing simulation data  
    states :    states to plot (first hor, then vert)
    shape :    positioning of plots
    """
	print("\nPlotting states {}...".format(states))

	## Config
	# plt.rc('text', usetex=True)
	plt.rc('xtick', labelsize='small')
	plt.rc('ytick', labelsize='small')
	plt.rc('legend', fontsize='small')
	# plt.rc('axes', titleweight='heavy', titlesize="medium")

	## Plot
	fig, ax = plt.subplots(shape[0], shape[1], sharex=True, squeeze=False)

	combis = zip(states, ax.flat)

	for state, ax_ in combis:
		set_angular_ticks = False
		ax_.plot(data.index, getattr(data, state))

		if state == 'theta':
			set_angular_ticks = True
			ax_.set_title(r"$\theta$")
			ax_.set_ylabel("angle [rad]")
		elif state == 'beta':
			set_angular_ticks = True
			ax_.set_title(r"$\beta$")
			ax_.set_ylabel("angle [rad]")
			ylim = ax_.get_ylim()
			ylim_ = abs(ylim[1] - ylim[0]) * 0.25
			ylim = [ylim[0] - ylim_, ylim[1] + ylim_]
			# for val in [x*np.pi for x in np.arange(-2, 2, 0.5)]: #@ Set lines on angles colinear to road
			#     if val >= ylim[0] and val <= ylim[1]:
			#         ax_.hlines([val], 0, data.index[-1], colors='black', linestyles='dotted', label='road angle')

		elif state == 'd_theta':
			set_angular_ticks = True
			ax_.set_title(r"$\dot{\theta}$")
			ax_.set_ylabel("angular velocity [rad/s]")
		elif state == 'd_beta':
			set_angular_ticks = True
			ax_.set_title(r"$\dot{\beta}$")
			ax_.set_ylabel("angle [rad/s]")

		elif state == 'dd_theta':
			set_angular_ticks = True
			ax_.set_title(r"$\ddot{\theta}$")
			ax_.set_ylabel("angular acceleration [rad/s^2]")
		elif state == 'dd_beta':
			set_angular_ticks = True
			ax_.set_title(r"$\ddot{\beta}$")
			ax_.set_ylabel("acceleration [rad/s^2]")

		elif state == 'xw':
			ax_.set_title(r"$x_W$")
			ax_.set_ylabel("position [m]")
		elif state == 'xf':
			ax_.set_title(r"$x_F$")
			ax_.set_ylabel("position [m]")
		elif state == 'yf':
			ax_.set_title(r"$x_F$")
			ax_.set_ylabel("position [m]")

		elif state == 'd_xw':
			ax_.set_title(r"$\dot{x}_W$")
			ax_.set_ylabel("velocity [m/s]")
		elif state == 'd_xf':
			ax_.set_title(r"$\dot{x}_F$")
			ax_.set_ylabel("velocity [m/s]")
		elif state == 'd_yf':
			ax_.set_title(r"$\dot{y}_F$")
			ax_.set_ylabel("velocity [m/s]")

		elif state == 'dd_xw':
			ax_.set_title(r"$\ddot{x}_W$")
			ax_.set_ylabel("acceleration [m/s^2]")
		elif state == 'dd_xf':
			ax_.set_title(r"$\ddot{x}_F$")
			ax_.set_ylabel("acceleration [m/s^2]")
		elif state == 'dd_yf':
			ax_.set_title(r"$\dot{y}_F$")
			ax_.set_ylabel("acceleration [m/s^2]")

		#% General stuff
		ax_.grid(True)

		lim = ax_.get_ylim()
		if abs(lim[1] - lim[0]) < 0.01:  #@ Increase y-range when small
			avg = lim[0] + (lim[1] - lim[0]) / 2.
			if set_angular_ticks:
				ax_.set_ylim(avg - np.pi / 2, avg + np.pi / 2)
			else:
				ax_.set_ylim(avg - 1, avg + 1)

		if set_angular_ticks:  #@ Set ticks to be in pi radians
			min_number = 4
			ylim = ax_.get_ylim()
			rnge = abs(ylim[1] - ylim[0])
			if rnge / (np.pi) >= min_number:
				denominator = 1
			else:
				x_ = min_number * np.pi / rnge
				if x_ > 1:
					denominator = np.floor(x_)
					if denominator % 2 != 0:
						denominator = denominator + 1
				else:
					denominator = np.ceil(x_)

			base = np.pi / denominator
			locator = ticker.MultipleLocator(base)

			formatter = ticker.FuncFormatter(format_ticks(denominator))
			ax_.yaxis.set_major_locator(locator)
			ax_.yaxis.set_major_formatter(formatter)

	for ax_ in ax[-1, :]:
		ax_.set_xlabel('time [s]')

	## Save and show
	plt.tight_layout()

	if savename is not None:
		save_plot(n, fig, savename, init_cond=data.iloc[0][['theta', 'beta']])

	if show:
		plt.show()

	plt.close()
	print("Done plotting states.")


##------------------------------------------------------------
def plot_steady_equilibrium(show=True, save=False):
	print("\n~ Plot steady equilibrium ~\n")
	sym = ParamsSymbolic()
	num = ParamsNumeric()

	## Set variables and initial conditions
	num.set_params_to_type('m_b', 'nom')
	num.mu = num.par_unc.mu_nom
	num.T_m = 0
	num.F_hx = 0
	num.F_hy = 0
	num.gamma = 0
	num.W_wf = 0
	beta_init = -np.pi / 2. - num.gamma
	theta_init = 0
	init_cond = [theta_init, beta_init]

	pos_init = np.zeros(6)
	pos_init[2] = theta_init
	pos_init[-1] = beta_init
	duration = 4

	## Get data
	data = model_simulation(sym, num, pos_init, duration=duration)

	## Plot data
	name_plot_motion = None
	name_plot_states = None
	if save:
		name_plot_motion = "equilibrium_motion"
		name_plot_states = "equilibrium_states"

	plot_motion(num, data[(data.index * 1000) % (0.5 * 1000) == 0], show=show, savename=name_plot_motion)
	plot_states(num, data, ['xw', 'dd_xw', 'beta', 'dd_beta'], [2, 2], show=show, savename=name_plot_states)


#%------------------------------------------------------------
def plot_beta(show=True, save=False):  #@ beta
	print("\n~ Plot beta ~\n")
	sym = ParamsSymbolic()
	num = ParamsNumeric()
	num.set_params_to_type('m_b', 'nom')
	num.mu = 0
	num.T_m = 0
	num.F_hx = 0
	num.F_hy = 0
	num.W_wf = 0
	num.gamma = 0
	theta_init = 0
	beta_init = 0

	## Set initial conditions
	pos_init = np.zeros(6)
	pos_init[2] = theta_init
	pos_init[-1] = beta_init
	duration = 5

	## Get data
	data = model_simulation(sym, num, pos_init, duration=duration)

	## Plot data
	name_plot_motion = None
	name_plot_states = None
	if save:
		name_plot_motion = "beta_motion"
		name_plot_states = "beta_states"

	plot_motion(num, data[(data.index * 1000) % (0.1 * 1000) == 0], show=show, savename=name_plot_motion)
	plot_states(num, data, ['xw', 'dd_xw', 'beta', 'dd_beta'], [2, 2], show=show, savename=name_plot_states)


#%------------------------------------------------------------
def plot_slope(show=True, save=False):  #@ gamma
	print("\n~ Plot slope ~\n")
	sym = ParamsSymbolic()
	num = ParamsNumeric()

	## Set variables and initial conditions

	num.set_params_to_type('m_b', 'nom')
	num.mu = 0
	num.T_m = 0
	num.F_hx = 0
	num.F_hy = 0
	num.W_wf = 0
	num.gamma = math.tan(1 / 10.)
	beta_init = -np.pi / 2. - num.gamma
	theta_init = 0
	init_cond = [theta_init, beta_init]

	pos_init = np.zeros(6)
	pos_init[-1] = beta_init

	## Get data
	data = model_simulation(sym, num, pos_init)

	## Plot data
	name_plot_motion = None
	name_plot_states = None
	if save:
		name_plot_motion = "slope_motion"
		name_plot_states = "slope_states"

	plot_motion(num, data[(data.index * 1000) % (0.20 * 1000) == 0], show=show, savename=name_plot_motion)
	plot_states(num, data, ['xw', 'dd_xw', 'beta', 'dd_beta'], [2, 2], show=show, savename=name_plot_states)


#%------------------------------------------------------------
def plot_input_force(show=True, save=False):
	print("\n~ Plot input force ~\n")
	sym = ParamsSymbolic()
	num = ParamsNumeric()

	## Set variables and initial conditions

	num.set_params_to_type('m_b', 'nom')
	num.mu = 0
	num.T_m = 0
	num.F_hx = 10
	num.F_hy = 0
	num.W_wf = 0
	num.gamma = 0
	beta_init = 0
	theta_init = 0
	init_cond = [theta_init, beta_init]

	pos_init = np.zeros(6)
	pos_init[-1] = beta_init
	duration = 4

	## Get data
	data = model_simulation(sym, num, pos_init, duration=duration)

	## Plot data
	name_plot_motion = None
	name_plot_states = None
	if save:
		name_plot_motion = "force_motion"
		name_plot_states = "force_states"

	plot_motion(num, data[(data.index * 1000) % (0.1 * 1000) == 0], show=show, savename=name_plot_motion)
	plot_states(num, data, ['xw', 'dd_xw', 'beta', 'dd_beta'], [2, 2], show=show, savename=name_plot_states)


#%------------------------------------------------------------
def plot_input_torque(show=True, save=False):
	print("\n~ Plot input torque ~\n")
	sym = ParamsSymbolic()
	num = ParamsNumeric()

	## Set variables and initial conditions

	num.set_params_to_type('m_b', 'nom')
	num.mu = 0
	num.T_m = 1
	num.F_hx = 0
	num.F_hy = 0
	num.W_wf = 0
	num.gamma = 0
	beta_init = -np.pi / 2. - num.gamma
	theta_init = 0
	init_cond = [theta_init, beta_init]

	pos_init = np.zeros(6)
	pos_init[-1] = beta_init
	duration = 2

	## Get data
	data = model_simulation(sym, num, pos_init, duration=duration)

	## Plot data
	name_plot_motion = None
	name_plot_states = None
	if save:
		name_plot_motion = "torque_motion"
		name_plot_states = "torque_states"

	plot_motion(num, data[(data.index * 1000) % (0.1 * 1000) == 0], show=show, savename=name_plot_motion)
	plot_states(num, data, ['xw', 'dd_xw', 'beta', 'dd_beta'], [2, 2], show=show, savename=name_plot_states)


##------------------------------------------------------------
def plot_input_torque_mass(show=True, save=False):
	print("\n~ Plot input torque mass ~\n")
	sym = ParamsSymbolic()
	num = ParamsNumeric()

	## Set variables and initial conditions
	num.set_params_to_type('m_b', 'max')
	num.mu = 0
	num.T_m = 3
	num.F_hx = 0
	num.F_hy = 0
	num.W_wf = 0
	num.gamma = 0
	beta_init = -np.pi / 2. - num.gamma
	theta_init = 0
	init_cond = [theta_init, beta_init]

	pos_init = np.zeros(6)
	pos_init[-1] = beta_init
	duration = 2

	## Get data
	data = model_simulation(sym, num, pos_init, duration=duration)

	## Plot data
	name_plot_motion = None
	name_plot_states = None
	if save:
		name_plot_motion = "torque_mass_motion"
		name_plot_states = "torque_mass_states"

	plot_motion(num, data[(data.index * 1000) % (0.1 * 1000) == 0], show=show, savename=name_plot_motion)
	plot_states(num, data, ['xw', 'dd_xw', 'beta', 'dd_beta'], [2, 2], show=show, savename=name_plot_states)


##------------------------------------------------------------
if __name__ == "__main__":
	show = True
	save = False

	plot_beta(show=show, save=save)
	plot_slope(show=show, save=save)
	plot_input_torque(show=show, save=save)
