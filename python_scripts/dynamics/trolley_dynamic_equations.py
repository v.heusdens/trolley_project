import sympy as sp

from toolbox.general import getattr_multi, print_list
from toolbox.params import ParamsNumeric, ParamsSymbolic
from toolbox.ros import load_config
from toolbox.transform import expand_to_variables, subs_multi


def get_eqs_COM(s=None):  #~function get_eqs_COM
	"""Returns list of equations for calculating trolley center of mass position.
	s = class with symbolic variables """
	if s is None:
		s = ParamsSymbolic(expand=['m_ft'])

	eqs = [
		sp.Eq(s.L_fc, 1. / 2. * s.L_wh),
		sp.Eq(s.L_bc, 1. / 3. * s.L_b),
		sp.Eq(s.L_wf, s.m_f / (s.m_ft) * s.L_fc + s.m_b / (s.m_ft) * s.L_bc),
		sp.Eq(s.W_bc, 1. / 3. * s.W_b + 1. / 2. * s.W_f),
		sp.Eq(s.W_wf, s.m_b / (s.m_ft) * s.W_bc)
	]
	return eqs


def get_eqs_motion(  #~function get_eqs_motion
		s=None, replace_derivatives=True, add_T_roll=False, T_roll_nonlin=False):
	""" Returns list of equations describing motion.
    s = class with symbolic variables 
    add_T_roll = Bool, whether or not to add equation for T_roll
	T_roll_nonlin = Bool, whether or not to use nonlinear equation for T_roll
    """
	if s is None:
		s = ParamsSymbolic()

	# % Get correct variables
	if replace_derivatives:
		subfix = ''
	else:
		subfix = '_t'

	names = 'dd_xw{0} dd_yw{0} dd_theta{0} dd_xf{0} dd_yf{0} dd_beta{0} beta{0} '.format(subfix)
	dd_xw, dd_yw, dd_theta, dd_xf, dd_yf, dd_beta, beta = getattr_multi(s, names)

	# % Formulate equations
	L_wh_ = sp.sin(s.beta) * s.L_wh
	W_wh_ = sp.cos(s.beta) * s.L_wh

	L_wf_x = sp.cos(s.beta) * s.L_wf
	L_wf_y = sp.sin(s.beta) * s.L_wf
	W_wf_x = sp.sin(s.beta) * s.W_wf
	W_wf_y = sp.cos(s.beta) * s.W_wf

	L_wf_ = L_wf_y + W_wf_y
	W_wf_ = L_wf_x - W_wf_x

	eqs = [
		#@ Wheel:
		sp.Eq(s.m_w * dd_xw, s.F_wx + s.F_stat - sp.sin(s.gamma) * s.F_wg),  #@ Fx = 0
		sp.Eq(s.m_w * dd_yw, s.F_n - s.F_wy - sp.cos(s.gamma) * s.F_wg),  #@ Fy = 0
		sp.Eq(s.I_w * dd_theta, s.T_m - s.T_roll - s.R_w * s.F_stat),  #@ M = 0

		#@ Frame:
		sp.Eq(s.m_ft * dd_xf, s.F_hx - 2 * s.F_wx - sp.sin(s.gamma) * s.F_fg),  #@ Fx = 0
		sp.Eq(s.m_ft * dd_yf, s.F_hy + 2 * s.F_wy - sp.cos(s.gamma) * s.F_fg),  #@ Fy = 0
		sp.Eq(s.I_f * dd_beta, -2 * s.F_wx * L_wf_ - 2 * s.F_wy * W_wf_ - s.F_hx * (L_wh_ - L_wf_) + s.F_hy *
		(W_wh_ - W_wf_) + 2 * s.T_m)  #@ M = 0
	]

	if add_T_roll:
		if T_roll_nonlin:
			eqs.append(sp.Eq(s.T_roll, s.T_roll_nonlin_dep))
		else:
			eqs.append(sp.Eq(s.T_roll, s.T_roll_dep))

	return eqs


def get_eqs_constraint(  #~function get_eqs_constraint
		s=None, add_con=False, replace_derivatives=True):
	""" Returns list of equations describing constraints.  
    s =     class with symbolic variables
    add_con =     bool - indicator if additional constraint needs to be added
    replace_derivatives =    bool - indicater for replacing derivatives with regular variables
    """
	if s is None:
		s = ParamsSymbolic()

	xw = s.R_w * s.theta_t
	yw = 0.
	xf = s.xw_t + sp.cos(-s.beta_t) * s.L_wf - sp.sin(s.beta_t) * s.W_wf
	yf = s.yw_t + sp.sin(s.beta_t) * s.L_wf + sp.cos(s.beta_t) * s.W_wf

	eqs = [
		sp.Eq(s.dd_xw, xw.diff(s.t, 2)),
		sp.Eq(s.dd_yw, 0),
		sp.Eq(s.dd_xf, xf.diff(s.t, 2)),
		sp.Eq(s.dd_yf, yf.diff(s.t, 2))
	]

	if add_con:
		C = s.yw_t + sp.sin(s.beta_t) * s.L_wh  #@ Handle height constant
		eqs.append(sp.Eq(0, C.diff(s.t, 2)))

	if replace_derivatives:
		old = [s.dd_beta_t, s.d_beta_t, s.beta_t, s.dd_theta_t, s.dd_xw_t, s.dd_yw_t]
		new = [s.dd_beta, s.d_beta, s.beta, s.dd_theta, s.dd_xw, s.dd_yw]
		eqs = subs_multi(eqs, old, new)

	return eqs


def get_model_odes(s=None):  #~function get_model_odes
	"""Calculate solution of model ode's (eqs_motion + eqs_constraint)"""

	print("Obtaining model ode's...")

	if s is None:
		s = ParamsSymbolic()

	x_sym = [s.xw, s.yw, s.theta, s.xf, s.yf, s.beta]
	dd_x_sym = [s.dd_xw, s.dd_yw, s.dd_theta, s.dd_xf, s.dd_yf, s.dd_beta]
	vars_implicit = [s.F_wx, s.F_wy, s.F_stat, s.F_n]
	dd_vars = dd_x_sym + vars_implicit

	eqs_motion = get_eqs_motion(s)
	eqs_constr = get_eqs_constraint(s)
	eqs = eqs_motion + eqs_constr
	sol = sp.solve(eqs, dd_vars)

	print(sol)
	expanded = {key: expand_to_variables(value, x_sym) for key, value in sol.items()}
	print_list(sol)

	return sol, expanded


if __name__ == "__main__":
	s = ParamsSymbolic()
	eqm = get_eqs_motion()
	eqc = get_eqs_constraint(add_con=True)
	sols1 = sp.solve(eqc, [s.dd_theta, s.dd_yw, s.dd_xf, s.dd_yf, s.dd_beta])
	print(sols1)

	eqm = [subs_multi(x, sols1.keys(), sols1.values()) for x in eqm]

	sols2 = sp.solve(eqm, [s.F_wx, s.F_wy, s.dd_theta, s.F_n, s.F_stat, s.F_hy])
	print(sols2)
	print()
