from __future__ import print_function

import numpy as np
import sympy as sp
from scipy.optimize import minimize
from sympy.utilities.lambdify import lambdify

from toolbox.compute import avg, jacobian
from toolbox.transform import subs_multi
from toolbox.params import ParamsSymbolic, ParamsUncertain, TransferFunctions

#from matplotlib import pyplot as plt
#import time
import itertools as iter

#from code_general.toolbox import *
#from code_general.load_model_elements import *

#^ #######################################
def map_var(var_unconstr, var_min, var_max): #~ Map variables for constraint elimination
	"""Map variables for constraint elimination.
		Args:
			var_unconstr (iterable): list of symbolic variables to use as new inputs for mappings
			var_min (float): lower-bound of constraint on parameter
			var_max (float): upper-bound of constraint on parameter
		Returns:
			list: list of mappings to be used for constrained variables
	."""
	var_avg = avg(var_min, var_max)
	var_mapped = var_avg + sp.sin(var_unconstr) * (var_max - var_avg) #@ mapping function
	return var_mapped


#^ #######################################
def objective_function(u, G, Ga, f, params_unc): #~ Calculate symbolic optimization function
	"""Calculates simplified, unconstrained objective function f
		Args:
			u (class): class containing uncertain parameter value options
			G (symbolic expression): transfer function
			Ga (float): average tf value
			f (symbolic expression): expression to use for obtimization
			unc_params (iterable): list with symbolic uncertain parameters 
		Returns:
			symbolic expression: simplified, unconstrained objective function f
			list: list of new symbolic arguments of f
	."""

	#% mapping
	params_free = [sp.symbols(str(param) + '_', real=True) for param in params_unc]
	mappings = [map_var(params_free[i], *u.get_lim(str(params_unc[i]))) for i in range(len(params_unc))]

	#% Implementing in model equation
	substitutes = zip(params_unc, mappings)
	f_mapped = f.subs(substitutes)

	return f_mapped, params_free


#^ #######################################
def calc_parameter_uncertainty_radius(s, u, TFs, r, r_, params_unc): #~ Performs optimalization algorithm to determine uncertainty radius
	"""Performs optimalization algorithm to determine uncertainty radius.
		Args:
			s (class): class containing symbolic params
			u (class): class containing uncertain parameter value options
			TFs (slass): class containing plant transfer function
			r (symbolic expression): expression to calculate uncertainty radius
			r_ (symbolic expression): expression to use for obtimization
			unc_params (iterable): list with symbolic uncertain parameters 
		Returns:
			float: calculated uncertainty radius
			list: corresponding values for uncertain parameters
	."""
	print("\nCALCULATING PARAMETER UNCERTAINTY RADIUS...") 
	for x in params_unc:
		print("{}: {}".format(x, u.get_avg(x)))
	G = TFs.get_tf_num('G')
	Ga = TFs.get_tf_avg('G')
	
	f_, params_free = objective_function(u, G, Ga, r_, params_unc)
	j_ = jacobian(f_, params_free)
	f = lambdify(params_free, f_)
	j = lambdify(params_free, j_)

	#% Solve optimization problem
	params_unc0 = [-0.25*np.pi]*3 #@ Initial guess (for mapped params)

	print("Running optimization...")
	sol = minimize(lambda x: f(*x), x0=params_unc0, method='newton-CG', jac=lambda x: np.array(j(*x)), options={'disp': True}) #@ Do not give hessian due to being unable to calculate - due to abs in objective_function function
	params_unc_opt = [map_var(sol.x[i], *u.get_lim(params_unc[i])) for i in range(len(params_unc))] #@ Map unconstrained values back to real parameter values

	print("")
	print("Initial guess: m_b = {:.3f}    L_h = {:.3f}    mu = {:.3f}".format(*params_unc0))
	print("unbound solution: m_b = {:.3f}    L_h = {:.3f}    mu = {:.3f}".format(*sol.x))
	print("Bound solution: m_b = {:.3f}    L_h = {:.3f}    mu = {:.3f}".format(*params_unc_opt))
	print("")

	#% Calculate parameter uncertainty radius
	r = subs_multi(r, params_unc, params_unc_opt)

	print("radius r: {:.3f}".format(r))
	return r, params_unc_opt


#^ #######################################
def verify_optimization_results(s, u, r, params_unc, params_opt_calc): #~ Calculate plant value for various param combis and check if max value is obtained for calculated optimal param values
	"""Calculate plant value for various param combis and check if max value is obtained for calculated optimal param values
		Args:
			s (class): class containing symbolic params
			u (class): class containing uncertain parameter value options
			r (symbolic expression): expression to calculate uncertainty radius
			unc_params (iterable): list with symbolic uncertain parameters 
			params_opt_calc (iterable): list with expected optimal uncertain parameter values
		Returns:
			float: max uncertainty radius from tests
			list: corresponding values for uncertain parameters
	."""
	print("\nVERIFYING OPTIMIZATION RESULTS...")
	
	nr_of_points = 10
	r_opt_calc = subs_multi(r, params_unc, params_opt_calc)

	#% Generate param value combinations
	params_lim = [u.get_lim(param) for param in params_unc]
	param_values = [np.linspace(lim[0], lim[1], nr_of_points) for lim in params_lim]
	combis = list(iter.product(*param_values))
	
	#% Calculate resulting values of r
	r_test = [subs_multi(r, params_unc, combi) for combi in combis]
	r_opt_test = max(r_test)
	params_opt_test = combis[r_test.index(r_opt_test)]

	print('Checked {} possible parameter combinations.'.format(len(combis)))
	print('Max value of r calculated optimal params: {}'.format(r_opt_calc))
	print('Max value of r from list: {}'.format(r_opt_test))
	if round(r_opt_calc, 7) >= round(r_opt_test, 7):
		print("Calculated optimum is highest. Calculated optimum verified!")
	else:
		print("Calculated optimum is NOT highest;")
		print("calculated max: {} = {}".format(params_unc, params_opt_calc))
		print("tested max: {} = {}".format(params_unc, params_opt_test))
		print("Something's wrong!")

	return r_opt_test, params_opt_test
	

#^ #######################################
def calc_max_added_weight(s, u, r): #~ Calculated max added m_b to let w <= 1 
	"""Calculate for which mass m_b uncertainty radius r is exactly 1, given that the other uncertain parameters are set to their optimal value as calculated by the other functions.
		Args:
			s (class): class containing symbolic params
			u (class): class containing uncertain parameter value options
			r (symbolic expression): expression to calculate uncertainty radius
	."""
	print("\nCALCULATE MAXIMUM WEIGHT")
	m_b_max = sp.symbols('m_b_max_sym', nonnegative=True, real=True)

	r = r.subs([(s.mu, u.mu_min), (s.L_h, u.L_h_min), (s.m_b, m_b_max)])
	eq = sp.Eq(r, 1)
	sol = sp.solve(eq, m_b_max)[0] #@ Solve for r = 1

	print('max total bag mass: {}'.format(sol))
	print('max added bag mass: {}'.format(sol - u.m_b_min))


#^ #######################################
def calc_qfilter_params(s, TFs):
	from sympy import I
	"""qfilter = a / (s + b)"""
	w = 1.406
	Ga = TFs.get_tf_avg("G")
	Gn = TFs.get_tf_nom("G")

	b_a = 1/w - 2*Ga/Gn
	a_b = 1/b_a

	print("Gn = {}".format(Gn))
	print("Ga = {}".format(Ga))
	print("w = {}".format(w))
	print("1/w = {}".format(1 / w))
	print("Ga/Gn = {}".format(Ga / Gn))
	print("b/a < {}".format(b_a))
	print("{} > a/b or a/b > {}".format(w, a_b))


	#print("Works!")


## MAIN #######################################
if __name__ == "__main__":
	s = ParamsSymbolic()
	TFs = TransferFunctions()
	G = TFs.get_tf_num('G')
	Ga = TFs.get_tf_avg('G')

	if False: #& optimization 
		u = ParamsUncertain()
		params_unc = [s.m_b, s.L_h, s.mu]
		r = abs((G - Ga)/Ga)
		f = - abs(G - Ga)

		r_opt_calc, params_opt_calc = calc_parameter_uncertainty_radius(s, u, TFs, r, f, params_unc)
		r_opt_test, params_opt_test = verify_optimization_results(s, u, r, params_unc, params_opt_calc)
		calc_max_added_weight(s, u, r)

	if True: #& determine Q-filter parameters
		calc_qfilter_params(s, TFs)

