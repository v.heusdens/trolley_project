import sympy as sp

from toolbox.params import ParamsNumeric, ParamsSymbolic, ParamsUncertain
from toolbox.ros import load_config
from toolbox.transform import convert_string_to_sympy

s = ParamsSymbolic()
n = ParamsNumeric()
n.set_params_nom()
n.F_hx = s.F_hx
n.d_xw = 1  #@ Only used in determining the direction of T_roll
n.d_beta = 0
n.T_m = s.T_m
n.gamma = s.gamma
n.dd_xw = s.dd_xw

#! Something's wrong wth T_roll model
#% Check what difference the value of F_hx makes in the value of T_roll
config_tests = load_config('trolley_tests')
config_controller = load_config('trolley_controller')
config_simulation = load_config('trolley_simulation')
model = convert_string_to_sympy(config_controller['model_Fhx'], n)

target_acceleration = config_simulation['target_velocity'] / config_simulation['acceleration_duration']
F_hx_acc = model.subs(s.dd_xw, target_acceleration)
F_hx_ss = model.subs(s.dd_xw, 0)

#% Value of T_roll (from symbolic_model_calculation with added constraint)
T_roll = -0.5 * n.F_fg * n.R_w * sp.sin(s.gamma) + 0.5 * s.F_hx * n.R_w - n.F_wg * n.R_w * sp.sin(
	s.gamma) + 0.5 * n.L_wf * n.R_w * n.d_beta**2 * n.m_ft / sp.cos(
	n.beta) - 0.5 * n.R_w * s.dd_xw * n.m_ft - 1.5 * n.R_w * s.dd_xw * n.m_w + s.T_m

#% Check what difference the value of gamma makes in the value of T_roll
T_roll_flat = T_roll.subs([(s.F_hx, F_hx_ss), (s.T_m, 0), (s.dd_xw, 0), (s.gamma, 0)])
T_roll_slope = T_roll.subs([(s.F_hx, F_hx_ss), (s.T_m, 0), (s.dd_xw, 0),
	(s.gamma, ParamsUncertain().gamma_max)])

T_roll_acc = T_roll.subs([(s.F_hx, F_hx_acc), (s.T_m, 0), (s.dd_xw, target_acceleration), (s.gamma, 0)])

diff_T_roll_gamma = T_roll_flat - T_roll_slope
diff_T_roll_Fhx = T_roll_acc - T_roll_flat

T_roll_sim = T_roll.subs([(s.F_hx, 0), (s.T_m, 0), (s.dd_xw, 0), (s.gamma, 0)])
print('T_roll for T_m = gamma = F_hx = dd_xw = 0: {}'.format(T_roll_sim))

print('Difference in T_roll due to change in gamma: {} = {}% of flat'.format(diff_T_roll_gamma,
	diff_T_roll_gamma / T_roll_flat))
print('Difference in T_roll due to change in F_hx: {} = {}% of steady state'.format(
	diff_T_roll_Fhx, diff_T_roll_Fhx / T_roll_flat))
