"""Calculations belonging to appendix for calculating the error in beta due to assumptions"""
import numpy as np
import os
import sympy as sp

from python_scripts.dynamics.saved_symbolic_model_equations import ModelEquations
from toolbox.params import ParamsNumeric, ParamsSymbolic

os.system('clear')

## define variables
s = ParamsSymbolic()
n = ParamsNumeric()
n.d_xw = 1
n.set_params_to_type(['L_h', 'm_b', 'mu'], 'nom')
n.set_params_symbolic(['F_hx', 'F_hy', 'T_m', 'dd_xw', 'beta', 'd_beta', 'dd_xw', 'T_roll'])
n.set_params_to_type('gamma', 'max')  #^ pylint: disable=assignment-from-no-return

if True:  ## Human body orientation
	#% symbolic
	beta1 = sp.asin(s.L_h_acc / s.L_wh)
	L1 = s.L_h - s.R_w / sp.cos(s.gamma)
	L2 = sp.cos(s.gamma) * L1
	beta2 = sp.asin(L2 / s.L_wh)
	delta_beta_orientation = abs(sp.simplify(beta2 - beta1))

	#% numeric
	replacements = [(s.L_h, n.L_h), (s.L_wh, n.L_wh), (s.R_w, n.R_w), (s.gamma, n.gamma)]

	beta1_ = float(beta1.subs(replacements))
	beta2_ = float(beta2.subs(replacements))
	delta_beta_orientation_ = float(delta_beta_orientation.subs(replacements))

	#% print
	print("\nBeta due to human body orientation:")
	print("  beta 1: {:>.3g} rad".format(beta1_))
	print("  beta 2: {:>.3g} rad ".format(beta2_))

	print("delta beta:")
	print("  {:>.3g} rad".format(delta_beta_orientation_))
	print("  {:>.3g} deg".format(np.rad2deg(delta_beta_orientation_)))
	print("  {:>.3g} %".format(delta_beta_orientation_ / beta2_ * 100))

if True:  ## Max error in dd_xw due to constant gamma
	#% calc beta
	beta3_ = float(beta1_ + n.gamma)

	print("\nBeta due to varying gamma:")
	print("  beta 3: {:>.3g}".format(beta3_))

	#dd_xw = 1 / (m_ft + 3*m_w)* 2 * (1 / n.R_w - n.mu / (n.L_wh * sp.cos(s.beta))) * s.T_m
	dd_xw_Tm = ModelEquations(n, acc_explicit=False, added_constraint=True).dd_xw.T_m
	val_ddxw_beta1_ = dd_xw_Tm.subs(s.beta, beta1_)
	val_ddxw_beta3_ = dd_xw_Tm.subs(s.beta, beta3_)
	delta_ddxw_ = val_ddxw_beta3_ - val_ddxw_beta1_

	print("\ndd_xw depending on beta:")
	print("  dd_xw using beta 1: {:>.5g}".format(val_ddxw_beta1_))
	print("  dd_xw using beta 3: {:>.5g}".format(val_ddxw_beta3_))

	print("\nchange in dd_xw due to beta 3 wrt beta 1:")
	print("  {:>.3g} [m/s^2]".format(delta_ddxw_))
	print("  {:>.2g} %".format(delta_ddxw_ / beta1_ * 100))

	## Time-span of being affected by changing gamma
	x_ = np.sqrt(n.L_wh**2 - n.L_h_acc**2)  #% distance
	vel_min_ = (127.2 - 21.1) / 100
	vel_min_ = 1
	time_ = x_ / vel_min_
	print('\nTime-span:')
	print('distance: {}'.format(x_))
	print('min. velocity: {}'.format(vel_min_))
	print('time: {}'.format(time_))
