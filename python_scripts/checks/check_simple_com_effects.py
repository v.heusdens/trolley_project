"""Checks the difference between results of simple and detailed COM implementation using steady state, nominal and max (gamma) values"""

import os
import sympy as sp

from python_scripts.dynamics.saved_symbolic_model_equations import ModelEquations
from toolbox.params import ParamsNumeric, ParamsSymbolic
from toolbox.transform import convert_string_to_sympy


def calc_Fhx(params):
	Fhx = ModelEquations(params, detailed_com=True).F_hx.all
	return convert_string_to_sympy(Fhx, params)


os.system('clear')

#% set params
s = ParamsSymbolic()
n = ParamsNumeric()
n.set_params_unspecified_symbolic()

n.d_beta = 0
n.d_xw = 1
n.set_params_to_type(['L_h', 'm_b', 'mu'], 'nom')
n.dd_xw = 0
#n.T_m = 0
#n.gamma = 0
#n.F_hx = calc_Fhx(n)

#% calc and print
decimals = 3

string = ('gamma = 0:')

print('Fhx: {}'.format(n.F_hx))
for _ in range(2):
	acc_simple = ModelEquations(n, detailed_com=False).dd_xw.all
	acc_elab = ModelEquations(n, detailed_com=True).dd_xw.all
	acc_diff = acc_simple - acc_elab
	Fhy_simple = ModelEquations(n, acc_explicit=False, detailed_com=False).F_hy.all
	Fhy_elab = ModelEquations(n, acc_explicit=False, detailed_com=True).F_hy.all
	Fhy_diff = Fhy_simple - Fhy_elab

	acc = ['ACCELERATION:', acc_simple, acc_elab, acc_diff]
	Fhy = ['VERTICAL HANDLE FORCE:', Fhy_simple, Fhy_elab, Fhy_diff]

	print('')
	print(string)

	print(acc_diff)
	print(Fhy_diff)

	#for item in [acc, Fhy]:
	#	print(
	#		'{text0:<30}{text1:<15}{simple:<20.{dec}g}{text2:<15}{elab:<20.{dec}g}{text3:<15}{diff:<20.{dec}g}'
	#		.format(text0=item[0],
	#		text1='simple COM:',
	#		simple=item[1],
	#		text2='elaborate COM:',
	#		elab=item[2],
	#		text3='Difference:',
	#		diff=sp.simplify(item[3]),
	#		dec=decimals))

	#n.set_params_to_type('gamma', 'max')
	#string = 'gamma = max:'
