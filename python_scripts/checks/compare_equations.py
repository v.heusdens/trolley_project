import os
import sympy as sp

from toolbox.params import ParamsSymbolic
from python_scripts.dynamics.saved_symbolic_model_equations import ModelEquations

os.system('clear')
print('start')
s = ParamsSymbolic()

#@ Config
eq1 = (-s.F_fg*s.L_wf*s.R_w*s.mu*sp.cos(s.beta)*sp.cos(s.beta + s.gamma)*sp.sign(s.d_xw) + s.F_fg*s.L_wh*s.R_w*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) + s.F_fg*s.L_wh*s.R_w*sp.sin(s.gamma)*sp.cos(s.beta)**2 + s.F_fg*s.R_w*s.W_wf*s.mu*sp.sin(s.beta + s.gamma)*sp.cos(s.beta)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.L_wh*s.R_w*s.mu*sp.cos(s.beta)**2*sp.cos(s.gamma)*sp.sign(s.d_xw) + 2.0*s.F_wg*s.L_wh*s.R_w*sp.sin(s.gamma)*sp.cos(s.beta)**2 - s.I_f*s.R_w*s.d_beta**2*s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) - s.L_wf*s.L_wh*s.R_w*s.d_beta**2*s.m_ft*sp.cos(s.beta) - s.L_wh*s.R_w*s.W_wf*s.d_beta**2*s.m_ft*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw) + s.L_wh*s.R_w*s.dd_xw*s.m_ft*sp.cos(s.beta)**2 + 3.0*s.L_wh*s.R_w*s.dd_xw*s.m_w*sp.cos(s.beta)**2 - 2.0*s.L_wh*s.T_m*sp.cos(s.beta)**2 + 2.0*s.R_w*s.T_m*s.mu*sp.cos(s.beta)*sp.sign(s.d_xw))/(s.L_wh*s.R_w*(s.mu*sp.sin(s.beta)*sp.sign(s.d_xw) + sp.cos(s.beta))*sp.cos(s.beta))

#@ Symbolic model equation calculation
eq3 = (s.F_fg * s.L_wf * s.R_w * s.mu * sp.sin(s.beta) * sp.sin(s.beta + s.gamma) * sp.sign(s.d_xw)
	- s.F_fg * s.L_wf * s.R_w * s.mu * sp.cos(s.gamma) * sp.sign(s.d_xw)
	+ s.F_fg * s.L_wh * s.R_w * s.mu * sp.cos(s.beta)**2 * sp.cos(s.gamma) * sp.sign(s.d_xw)
	+ s.F_fg * s.L_wh * s.R_w * sp.sin(s.gamma) * sp.cos(s.beta)**2
	+ s.F_fg * s.R_w * s.W_wf * s.mu * sp.sin(s.beta) * sp.cos(s.beta + s.gamma) * sp.sign(s.d_xw)
	+ s.F_fg * s.R_w * s.W_wf * s.mu * sp.sin(s.gamma) * sp.sign(s.d_xw)
	+ 2.0 * s.F_wg * s.L_wh * s.R_w * s.mu * sp.cos(s.beta)**2 * sp.cos(s.gamma) * sp.sign(s.d_xw)
	+ 2.0 * s.F_wg * s.L_wh * s.R_w * sp.sin(s.gamma) * sp.cos(s.beta)**2
	- s.I_f * s.R_w * s.d_beta**2 * s.mu * sp.sin(s.beta) * sp.sign(s.d_xw)
	- s.L_wf**2 * s.R_w * s.d_beta**2 * s.m_ft * s.mu * sp.sin(s.beta) * sp.sign(s.d_xw)
	- s.L_wf * s.L_wh * s.R_w * s.d_beta**2 * s.m_ft * sp.cos(s.beta)
	+ s.L_wf * s.R_w * s.dd_xw * s.m_ft * s.mu * sp.sin(2.0 * s.beta) * sp.sign(s.d_xw) / 2.0
	- s.L_wh * s.R_w * s.W_wf * s.d_beta**2 * s.m_ft * s.mu * sp.cos(s.beta) * sp.sign(s.d_xw)
	+ s.L_wh * s.R_w * s.dd_xw * s.m_ft * sp.cos(s.beta)**2
	+ 3.0 * s.L_wh * s.R_w * s.dd_xw * s.m_w * sp.cos(s.beta)**2 - 2.0 * s.L_wh * s.T_m * sp.cos(s.beta)**2
	+ 2.0 * s.R_w * s.T_m * s.mu * sp.cos(s.beta) * sp.sign(s.d_xw)
	- s.R_w * s.W_wf**2 * s.d_beta**2 * s.m_ft * s.mu * sp.sin(s.beta) * sp.sign(s.d_xw)
	+ s.R_w * s.W_wf * s.dd_xw * s.m_ft * s.mu * sp.cos(s.beta)**2 * sp.sign(s.d_xw)) / (s.L_wh * s.R_w *
	(s.mu * sp.sin(s.beta) * sp.sign(s.d_xw) + sp.cos(s.beta)) * sp.cos(s.beta))

#@ ModelEquations class
eq2 = (s.F_fg * s.L_wf * s.R_w * s.mu * sp.sin(s.beta) * sp.sin(s.beta + s.gamma) * sp.sign(s.d_xw)
	- s.F_fg * s.L_wf * s.R_w * s.mu * sp.cos(s.gamma) * sp.sign(s.d_xw)
	+ s.F_fg * s.L_wh * s.R_w * s.mu * sp.cos(s.beta)**2 * sp.cos(s.gamma) * sp.sign(s.d_xw)
	+ s.F_fg * s.L_wh * s.R_w * sp.sin(s.gamma) * sp.cos(s.beta)**2
	+ s.F_fg * s.R_w * s.W_wf * s.mu * sp.sin(s.beta) * sp.cos(s.beta + s.gamma) * sp.sign(s.d_xw)
	+ s.F_fg * s.R_w * s.W_wf * s.mu * sp.sin(s.gamma) * sp.sign(s.d_xw)
	+ 2.0 * s.F_wg * s.L_wh * s.R_w * s.mu * sp.cos(s.beta)**2 * sp.cos(s.gamma) * sp.sign(s.d_xw)
	+ 2.0 * s.F_wg * s.L_wh * s.R_w * sp.sin(s.gamma) * sp.cos(s.beta)**2
	- s.I_f * s.R_w * s.d_beta**2 * s.mu * sp.sin(s.beta) * sp.sign(s.d_xw)
	- s.L_wf**2 * s.R_w * s.d_beta**2 * s.m_ft * s.mu * sp.sin(s.beta) * sp.sign(s.d_xw)
	- s.L_wf * s.L_wh * s.R_w * s.d_beta**2 * s.m_ft * sp.cos(s.beta)
	+ s.L_wf * s.R_w * s.dd_xw * s.m_ft * s.mu * sp.sin(2.0 * s.beta) * sp.sign(s.d_xw) / 2.0
	- s.L_wh * s.R_w * s.W_wf * s.d_beta**2 * s.m_ft * s.mu * sp.cos(s.beta) * sp.sign(s.d_xw)
	+ s.L_wh * s.R_w * s.dd_xw * s.m_ft * sp.cos(s.beta)**2
	+ 3.0 * s.L_wh * s.R_w * s.dd_xw * s.m_w * sp.cos(s.beta)**2 - 2.0 * s.L_wh * s.T_m * sp.cos(s.beta)**2
	+ 2.0 * s.R_w * s.T_m * s.mu * sp.cos(s.beta) * sp.sign(s.d_xw)
	- s.R_w * s.W_wf**2 * s.d_beta**2 * s.m_ft * s.mu * sp.sin(s.beta) * sp.sign(s.d_xw)
	+ s.R_w * s.W_wf * s.dd_xw * s.m_ft * s.mu * sp.cos(s.beta)**2 * sp.sign(s.d_xw)) / (s.L_wh * s.R_w *
	(s.mu * sp.sin(s.beta) * sp.sign(s.d_xw) + sp.cos(s.beta)) * sp.cos(s.beta))

print(sp.simplify(eq2 - eq1))
