"""
Converting the continuous-
y/u =

  0.095
----------
z - 0.904

=

  0.095 z^-1
---------------
1 - 0.904 z^-1


Using X*z^-1 in discrete frequency domain -> x[n-1] in discrete time domain:
-> y[n] - 0.904 y[n-1] = 0.095 u[n-1]
-> y[n] = 0.904 y[n-1] + 0.095 u[n-1]

"""
import control
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as scs

if __name__ == '__main__':
	# Input signal
	dt = 0.02
	t = np.arange(0, 6, dt)
	u = np.zeros_like(t)
	u[(0.5 < t) & (t < 2)] = 1  # Input is 1 between t=0.5 and t=2, 0 otherwise

	# System definition
	var = 5 * 2 * np.pi  # cutoff frequency
	num_continuous = [var]
	denom_continuous = [1, var]

	# Continuous-time simulation
	tf_continuous = control.TransferFunction(num_continuous, denom_continuous)
	t, y, _ = control.forced_response(tf_continuous, t, u)
	print("Continuous-time transfer function:")
	print(tf_continuous)

	# Discrete-time simulation
	num_discrete, den_discrete, _ = scs.cont2discrete((num_continuous, denom_continuous), dt)
	num_discrete = num_discrete.flatten()
	den_discrete = den_discrete.flatten()
	print("Discrete numerator", num_discrete)
	print("Discrete denominator", den_discrete)

	y_discrete = np.zeros_like(t)
	initial_condition = scs.lfilter_zi(num_discrete.flatten(), den_discrete)
	for i in range(1, len(y_discrete)):  # Start at the second time-step
		# Using scipy.signal.lfilter; this works for 1-order filter but didn't work well for more
		# y_discrete[i] = scs.lfilter(num_discrete.flatten(), den_discrete, [u[i-1], 0], zi=initial_condition*y_discrete[i-1])[0][1]

		# Hard-coded values, this is equivalent with
		# y_discrete[i] = -0.90483742 * y_discrete[i-1] + 0.09516258 * u[i-1]

		# Same as above but with values taken from the numerator and denominator
		y_discrete[i] = -den_discrete[1] * y_discrete[i - 1] + num_discrete[1] * u[i - 1]

	# Plotting
	fig, axes = plt.subplots(2, sharex=True)
	axes[0].plot(t, u, '.', label="input")
	axes[0].grid()
	axes[0].legend()
	axes[1].plot(t, y, label="Continuous-time simulation")
	axes[1].plot(t, y_discrete, 'o', label="Discrete-time using for-loop")
	axes[1].grid()
	axes[1].legend()
	plt.show()
