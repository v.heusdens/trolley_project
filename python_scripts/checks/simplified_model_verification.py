import os
import sympy as sp

from toolbox.params import ParamsSymbolic
from toolbox.transform import convert_sympy_to_latex

os.system('clear')

s = ParamsSymbolic(expand=None)
s.d_xw = 1

eq = -s.R_w * (s.F_fg * s.R_w * sp.sin(s.gamma) - s.F_hx * s.R_w + 2 * s.F_wg * s.R_w * sp.sin(s.gamma)
	- s.L_wf * s.R_w * s.d_beta**2 * s.m_ft / sp.cos(1.0 * s.beta) - 2 * s.T_m + 2 * s.T_roll) / (2 * s.I_w
	+ s.R_w**2 * s.m_ft + 2 * s.R_w**2 * s.m_w)

print(convert_sympy_to_latex(sp.simplify(eq)))

# s.dd_xw = -s.R_w*(s.F_fg*s.R_w*sp.sin(s.gamma) - s.F_hx*s.R_w + 2*s.F_wg*s.R_w*sp.sin(s.gamma) - s.L_wf*s.R_w*s.d_beta**2*s.m_ft/sp.cos(1.0*s.beta) - 2*s.T_m + 2*s.T_roll)/(2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)

#Subdivided into expand_to:
#s.F_hx: s.R_w**2/(2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)

#s.F_hy: 0

#s.T_roll: -2*s.R_w/(2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)

#s.T_m: 2*s.R_w/(2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)

#s.F_fg: -s.R_w**2*sp.sin(s.gamma)/(2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)

#rest: 0

#s.d_beta**2: s.L_wf*s.R_w**2*s.m_ft/((2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)*sp.cos(1.0*s.beta))

#s.F_wg: -2*s.R_w**2*sp.sin(s.gamma)/(2*s.I_w + s.R_w**2*s.m_ft + 2*s.R_w**2*s.m_w)
