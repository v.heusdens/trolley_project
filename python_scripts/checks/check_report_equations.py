#
#^ pylint: disable=invalid-name
#^ pylint: disable=anomalous-backslash-in-string

import os
import sympy as sp

from python_scripts.dynamics.saved_symbolic_model_equations import ModelEquations
from toolbox.params import ParamsSymbolic
from toolbox.transform import convert_latex_to_sympy, convert_sympy_to_latex, convert_sympy_to_sympy_printed, expand_to_variables

os.system('clear')

s = ParamsSymbolic()

eq_report = "\Biggl[	 & -2 \biggl(\
		L_{WH} \cos(\beta) R_{W}^{-1} - \sign(\dot{x}_W) \mu\
		\biggr) T_M\
		+ \biggl(\
		L_{WH} \sin(\gamma) \cos(\beta)\
	+ \sign(\dot{x}_W) \mu \\\
	 & \qquad \Bigl(\
		L_{WH} \cos(\beta) \cos(\gamma) + W_{WF} \sin(\beta + \gamma) - L_{WF} \cos(\beta + \gamma)\
		\Bigr)\
	\biggr) F_{FG} \nonumber \\\
	 & \quad + \biggl(2 L_{WH} \bigl(\sign(\dot{x}_W) \mu \cos(\gamma) + \sin(\gamma) \bigr) \cos(\beta)\biggr) F_{WG} \\\
	 & \quad +\
	\biggl(\sign(\dot{x}_W) \mu m_{FT} \bigl(L_{WF} \sin(\beta) + W_{WF} \cos(\beta)\bigr) \\\
	 & \qquad + L_{WH} (m_{FT} + 3 m_{W}) \cos(\beta) \biggr) \ddot{x}_W\
		\Biggr]\
	\Bigl(\
	L_{WH} \bigl(\sign(\dot{x}_W) \mu \sin(\beta) + \cos(\beta) \bigr)\
	\Bigr)^{-1}"

eq_report = convert_latex_to_sympy(eq_report, s)

eq_check = ModelEquations(s, added_constraint=True).F_hx.all
eq_check = eq_check.subs(s.d_beta, 0)

#print("diff:")
#print(sp.simplify(eq_report - eq_check))
diff = eq_report - eq_check
diff = diff.subs(s.beta, 1.0 * s.beta)
diff = sp.expand(diff)
#diff = sp.simplify(diff)
#print("\nDiff simplified:")
#print(diff)

print("\nDiff per element:")
expanded = expand_to_variables(diff, [s.T_m, s.F_fg, s.F_wg, s.dd_xw])
for key, value in expanded.items():
	print('-{}: {}'.format(key, value))
