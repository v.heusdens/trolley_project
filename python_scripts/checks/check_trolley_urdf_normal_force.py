import sympy as sp

from toolbox.params import ParamsNumeric, ParamsSymbolic, ParamsUncertain

s = ParamsSymbolic()
n = ParamsNumeric()
n.set_params_nom()
n.F_hx = 0
n.d_beta = 0
n.T_m = 0

F_n = -0.5 * (s.F_fg * s.L_wf * sp.cos(s.beta + s.gamma) / sp.cos(s.beta) - s.F_fg * s.L_wh * sp.cos(s.gamma)
	- s.F_fg * s.W_wf * sp.sin(s.beta + s.gamma) / sp.cos(s.beta) + s.F_hx * s.L_wh * sp.tan(s.beta)
	- 2.0 * s.F_wg * s.L_wh * sp.cos(s.gamma) + s.I_f * s.d_beta**2 * sp.sin(s.beta) / sp.cos(s.beta)**2
	+ s.L_wh * s.W_wf * s.d_beta**2 * s.m_ft / sp.cos(s.beta) - 2.0 * s.T_m / sp.cos(s.beta)) / s.L_wh

F_n = F_n.subs([(s.T_m, 0), (s.gamma, 0), (s.F_hx, 0), (s.d_beta, 0)])
print('F_n for trolley URDF: {}'.format(F_n))

#@ Implemented in URDF:
#@ F_n =
#@ 	0.5/L_wh*
#@ 	(
#@ 		F_fg*
#@ 		(
#@ 			  L_wh
#@ 			- L_wf
#@ 			+ W_wf* tan(beta)
#@ 		)
#@ 		- F_hx*L_wh*tan(beta)
#@ 		+ 2.0*F_wg*L_wh
#@ 	)
