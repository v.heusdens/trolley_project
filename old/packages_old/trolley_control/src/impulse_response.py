#!/usr/bin/env python
# Libraries that we make use of
import scipy as sp              # SciPy library (used all over)
import numpy as np              # NumPy library
from scipy.signal.ltisys import _default_response_times
import warnings
from control.lti import LTI     # base class of StateSpace, TransferFunction
from control.statesp import _convertToStateSpace, _mimo2simo, _mimo2siso
from control.lti import isdtime, isctime
from control.timeresp import forced_response, _get_ss_simo, _check_convert_array
from sys import version_info

#CUSTOM VERSION OF IMPULSE_RESPONSE FUNCTION FROM CONTROL PACKAGE#
def impulse_response(sys, T=None, X0=0., input=0, output=None,
                transpose=False, return_x=False, squeeze=True):
    # pylint: disable=W0622
    """Impulse response of a linear system

    If the system has multiple inputs or outputs (MIMO), one input has
    to be selected for the simulation. Optionally, one output may be
    selected. The parameters `input` and `output` do this. All other
    inputs are set to 0, all other outputs are ignored.

    For information on the **shape** of parameters `T`, `X0` and
    return values `T`, `yout`, see :ref:`time-series-convention`.

    Parameters
    ----------
    sys: StateSpace, TransferFunction
        LTI system to simulate

    T: array-like object, optional
        Time vector (argument is autocomputed if not given)

    X0: array-like object or number, optional
        Initial condition (default = 0)

        Numbers are converted to constant arrays with the correct shape.

    input: int
        Index of the input that will be used in this simulation.

    output: int
        Index of the output that will be used in this simulation. Set to None
        to not trim outputs

    transpose: bool
        If True, transpose all input and output arrays (for backward
        compatibility with MATLAB and scipy.signal.lsim)

    return_x: bool
        If True, return the state vector (default = False).

    squeeze: bool, optional (default=True)
        If True, remove single-dimensional entries from the shape of
        the output.  For single output systems, this converts the
        output response to a 1D array.

    Returns
    -------
    T: array
        Time values of the output
    yout: array
        Response of the system
    xout: array
        Individual response of each x variable

    See Also
    --------
    forced_response, initial_response, step_response

    Notes
    -----
    This function uses the `forced_response` function to compute the time
    response. For continuous time systems, the initial condition is altered to
    account for the initial impulse.

    Examples
    --------
    >>> T, yout = impulse_response(sys, T, X0)

    """
    sys = _get_ss_simo(sys, input, output)

    # System has direct feedthrough, can't simulate impulse response
    # numerically
    if np.any(sys.D != 0) and isctime(sys):
        warnings.warn("System has direct feedthrough: ``D != 0``. The "
                    "infinite impulse at ``t=0`` does not appear in the "
                    "output.\n"
                    "Results may be meaningless!")

    # create X0 if not given, test if X0 has correct shape.
    # Must be done here because it is used for computations here.
    n_states = sys.A.shape[0]
    X0 = _check_convert_array(X0, [(n_states,), (n_states, 1)],
                            'Parameter ``X0``: \n', squeeze=True)

    # Compute T and U, no checks necessary, they will be checked in lsim
    if T is None:
        if isctime(sys):
            T = _default_response_times(sys.A, 100)
            U = np.zeros_like(T)
        else:
            # For discrete time, use integers
            tvec = _default_response_times(sys.A, 100)*2
            T = range(int(np.ceil(max(tvec))))
            U = np.zeros_like(T)
            #CUSTOM: Assures correct time-step is taken into account for forced_response function
            if version_info.major == 3:
                T = np.arange(0, T.stop*sys.dt, sys.dt) 
            else:
                T = [x*sys.dt for x in T]
            

    
    # Compute new X0 that contains the impulse
    # We can't put the impulse into U because there is no numerical
    # representation for it (infinitesimally short, infinitely high).
    # See also: http://www.mathworks.com/support/tech-notes/1900/1901.html
    if isctime(sys):
        B = np.asarray(sys.B).squeeze()
        new_X0 = B + X0
    else:
        new_X0 = X0
        U[0] = 1.

    T, yout, _xout = forced_response(sys, T, U, new_X0, transpose=transpose,
                                    squeeze=squeeze)

    if return_x:
        return T, yout, _xout

    return T, yout