#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray


# node initialization #
rospy.init_node("keyboard_effort_transformer")


# definitions of variables #
effort_msg = Float64MultiArray()
multiplier = 3

# definitions of functions #
def callback(msg):
    """Publish relevant info on effort controller topic"""
    effort_msg.data = [multiplier*msg.linear.x, multiplier*msg.linear.x]
    pub.publish(effort_msg)


# definition of publisher/subscriber and services #
rospy.Subscriber('/cmd_vel', Twist, callback)
pub = rospy.Publisher('/trolley/wheel_joint_effort_controller/command', Float64MultiArray, queue_size=1)


# MAIN
while not rospy.is_shutdown():
    rospy.spin()