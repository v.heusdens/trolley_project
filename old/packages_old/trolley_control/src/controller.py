#!/usr/bin/env python
#^ pylint: skip-file

#region ## IMPORTS
import copy
import statistics
import warnings
from sys import version_info

import control
import matplotlib.pyplot as plt
import numpy as np
import rospy
from control import TransferFunction as tf
from control import matlab
from geometry_msgs.msg import Vector3
from scipy import signal
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64, Float64MultiArray
from code_general.data_analysis.data_plot_functions import plot_filtered_data

import impulse_response
from ros_packages.trolley_description.config.load_model_params import *
#endregion

rospy.init_node("controller_node")

#region ## PARAM DEFINITIONS
motor_cmd = Float64MultiArray()

#% Load parameter files
config_path = rospkg.RosPack().get_path('trolley_control')
config = yaml.safe_load(
    open(config_path + "/config/controller_settings.yaml"))

#% CONTROLLER FREQUENCIES #
freq = config['control_loop_frequency']  #@ Control-loop frequency in Hz
tw_filt_q = config['time_window_q']  #@ Filtering time-window

#% SENSOR DATA FILTERS #
imu_data_filter = []  #@ Memory for sensor filtering
tw_filt_sensor = config['time_window_sensor']

#% Q-FILTER #
# #@ z-transform of 0.5*(x[n] + x[n-1])
# num = np.array([1, 1])
# den = np.array([2, 0])

# #@ z-transform of 0.2*(x[n] + x[n-1] + x[n-2] + x[n-3] + x[n-4])
# num = np.array([1, 1, 1, 1, 1])
# den = np.array([5, 0, 0, 0, 0])

#@ z-transform of a*x[n] + (1 - a)*y[n-1]
alpha = config['alpha']
num = np.array([alpha, 0])
den = np.array([1, alpha - 1])

dw = []  #@ Array to save data per time-window (data-window)
res = np.zeros((tw_filt_q), dtype=complex) #@ Array for saving data for next time-window

#% INITIAL CONDITIONS
ddtheta = 0
Fhx = 0
Tm = 0

#% OTHER
torque_limit = config['torque_limit']
#endregion

#region ## FUNCTIONS

#% CALLBACKS #
def update_angular_acceleration_sensor(msg):
    """
    Obtain linear trolley acceleration from Gazebo Imu plugin
    and transform to angular wheel acceleration
    """
    global ddtheta

    #@ Obtain acceleration
    ddx = msg.linear_acceleration.x
    ddx_filt = IMU_data_filter(ddx)
    ddtheta = ddx_filt/R_w

    #@ Analysis
    puba_acc_lin_raw.publish(ddx)
    puba_acc_lin.publish(ddx_filt)
    puba_acc_ang.publish(ddtheta)
    # print("Angular acceleration updated to {}".format(ddtheta))


def update_force_sensor(msg):
    """
    Obtain horizontal force commands from input force topic
    """
    global Fhx

    Fhx = msg.x

    #@ Analysis
    puba_force.publish(Fhx)
    # print("input force: {}".format(Fhx))


#% CONTROLLER #
def estimate_force_input(): #! UPDATE MODEL!
    """Calculate estimated input using inverse model and velocity info"""
    Fin_est = (2*I_w + R_w**2*(m_ft + 2*m_w))*ddtheta + \
        2*mu*R_w*F_wg - (L_wf/L_wh - 1)*mu*R_w*F_fg

    Fhx_est = (Fin_est - (1 - mu*R_w/(L_wh*np.cos(beta)))*2*Tm)/((mu*np.tan(beta) + 1)*R_w)

    #@ Analysis
    # print("Estimated acceleration: {}".format(ddtheta))
    # print("Estimated input-force: {}".format(Fin_est))
    # print("Estimated human-force: {}\n".format(Fhx_est))
    puba_force_est.publish(Fhx_est)

    return Fhx_est #@ = (mu*tan(beta) + 1)*R_w*Fhx + (1 - mu*R_w/(L_wh*cos(beta)))*2*Tm


def estimate_disturbance(Fhx_est):
    """
    Estimates disturbance by comparing estimated force input by measured force input.
    Positive when user needs to pull harder then expected.

    """
    d_est = Fhx - Fhx_est

    #@ Analysis
    puba_dist_est.publish(d_est)

    return d_est


def apply_qfilter(dw):
    """Process data using q-filter"""

    # print("processing data-window using q-filter...")

    DW_pad = np.fft.fft(dw, tw_filt_q + pad)
    DW_pad_ = DW_pad*Q_IMPULSE_pad
    dw_pad_ = np.fft.ifft(DW_pad_)

    # print("finished processing data-window.")
    return dw_pad_

#% ACTUATOR #
def actuate_motors(forces, threshold=0):
    global Tm, motor_cmd

    if not isinstance(forces, list) and abs(statistics.mean(forces)) > threshold:
        Tm = forces[-1]*R_w
        if Tm > torque_limit:
            Tm = torque_limit
    else:
        Tm = 0

    motor_cmd.data = [-Tm, -Tm]
    pub_control.publish(motor_cmd)

    #@ Analysis
    # print("MOTOR COMMAND APPLIED: {} Nm".format(Tm))
    # print("mean force: {}".format(statistics.mean(forces)))
    puba_motor_cmd.publish(forces[-1])

    return

#% OTHER #
def shutdownhook():
    """Actions to perform on shutdown of node"""
    actuate_motors(0)


def get_impulse_response(filter):
    #@ Obtain impulse response
    T_q_impulse, q_impulse = impulse_response.impulse_response(q_filter)

    #@ Cutoff at 1% of first value
    cutoff = 0.01
    q_impulse = [x for x in q_impulse if x > cutoff*max(q_impulse)]
    T_q_impulse = T_q_impulse[:len(q_impulse)]
    return T_q_impulse, q_impulse


def IMU_data_filter(ddx):
    global imu_data_filter

    #@ Update filter memory
    imu_data_filter.append(ddx)

    if len(imu_data_filter) > tw_filt_sensor:
        imu_data_filter.pop(0)

    #@ Apply filter
    filter_result = np.median(imu_data_filter)
    return filter_result
#endregion


#region ## PUBLISHERS/SUBSCRIBER DEFINITIONS
#% Publishers
pub_control = rospy.Publisher('/trolley/wheel_joint_effort_controller/command', Float64MultiArray, queue_size=1)

# puba_gen = rospy.Publisher('trolley/analysis/general', Float64, queue_size=1)
puba_acc_lin_raw = rospy.Publisher(
    'trolley/analysis/lin_acc_raw', Float64, queue_size=1)
puba_acc_lin = rospy.Publisher('trolley/analysis/lin_acc', Float64, queue_size=1)
puba_acc_ang = rospy.Publisher('trolley/analysis/ang_acc', Float64, queue_size=1)
puba_force = rospy.Publisher('trolley/analysis/force', Float64, queue_size=1)
puba_force_est = rospy.Publisher('trolley/analysis/force_est', Float64, queue_size=1)
puba_dist_est = rospy.Publisher('trolley/analysis/dist_est', Float64, queue_size=1)
puba_motor_cmd = rospy.Publisher('trolley/analysis/motor_cmd', Float64, queue_size=1)

#% Subscribers
rospy.Subscriber("trolley/input_force", Vector3, update_force_sensor)
rospy.Subscriber("trolley/imu", Imu, update_angular_acceleration_sensor)
#endregion


#region ## INITIALIZATION
print("START TROLLEY CONTROLLER\n")

#@ Determine if motor command must be given
motors_active = rospy.get_param('/motors_active', True)
if motors_active:
    print("Motors set to active")
else:
    print("Motors set to inactive")

#@ FILTERING
q_filter = tf(num, den, 1./freq)
T_q_impulse, q_impulse = get_impulse_response(q_filter)
pad = len(q_impulse)
if pad > tw_filt_q:
    raise Exception("padding length longer than time-window: {} vs {}. Data from different data-windows will not be concatenated correctly. Please expand time-window.".format(pad, tw_filt_q))
Q_IMPULSE_pad = np.fft.fft(q_impulse, tw_filt_q + pad)

#@ ANALYSIS
print("Q-filter: {}".format(q_filter))
print("Length of impulse response: {}\n".format(pad))
print("Processing loop takes {} seconds.".format(float(tw_filt_q)/freq))
# plot_filtered_data(q_impulse, q_impulse, freq, title="Q-filter impulse response")
# test = [] #@ Array to save data-windows for testing
# test_ = [] #@ Array to save data-windows for testing
#endregion


#region ## MAIN
#% TEST CODE #
# apply_motor_command(0.1)
# while not rospy.is_shutdown():
#     apply_motor_command(0.1)
#     r.sleep()

#% MAIN CODE #
# print("Start collecting new data-window...")
r = rospy.Rate(freq)
while not rospy.is_shutdown():
    #@ Collect data until data-window is filled
    Fhx_est = estimate_force_input()
    d_est = estimate_disturbance(Fhx_est)
    dw.append(d_est)
    # print("data-window: {}".format(dw))

    if len(dw) == tw_filt_q: #@ When data-window is filled; process data using q-filter and empty window
        # print("data-window complete.")
        dw_pad_ = apply_qfilter(dw)
        dw_ = dw_pad_[:-pad] + res
        res[:pad] = dw_pad_[-pad:]

        #@ ACTUATE MOTORS
        if motors_active:
            actuate_motors(dw_.real, threshold=0.)
        else:
            actuate_motors([0])

        #@ ANALYSIS
        # if dtheta*R_w > 1:
        #     plot_filtered_data(dw, dw_.real, freq, title="data-window")
        # test.extend(dw)
        # test_.extend(dw_.real)
        # if len(test) == 4*tw_filt_q:
        #     break
        dw = [] #@ Reset data-window

    if motors_active:
        rospy.on_shutdown(shutdownhook) #@ Make sure last command to motors is 0

    r.sleep()

# plot_data(test, test_, title="Set of 4 data-windows put behind eachother")
rospy.signal_shutdown("END")
#endregion
