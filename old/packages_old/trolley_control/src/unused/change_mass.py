#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32
from geometry_msgs.msg import Vector3

# node initialization #
rospy.init_node("pub")
start = False

# definition of variables #
new_mass = 20

# definitions of functions #


# definition of publisher/subscriber and services #
pub = rospy.Publisher("/trolley/inertia", Float32, queue_size=1)


##################### main program #####################################
while rospy.Time.now() == rospy.Time(0): # Wait to receive first message
    continue

pub.publish(new_mass)
print("Mass changed to {}".format(new_mass))