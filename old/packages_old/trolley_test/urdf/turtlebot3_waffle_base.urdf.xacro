<?xml version="1.0" encoding="UTF-8"?>
<robot name="turtlebot3_waffle" 
  xmlns:xacro="http://ros.org/wiki/xacro">

  <!-- common constants -->
  <xacro:include filename="$(find trolley_test)/urdf/common_properties.xacro"/>

  <!-- Turtlebot3 platform include files -->
  <xacro:include filename="$(find trolley_test)/urdf/caster.urdf.xacro"/>
  <xacro:include filename="$(find trolley_test)/urdf/human_wheel.urdf.xacro"/>
  <xacro:include filename="$(find toolbox)/urdf/inertias.urdf.xacro"/>

  <!-- Constants for robot dimensions -->
  <xacro:property name="shoulder_height" value="1.402"/>

  <xacro:property name="upper_arm_circ" value="0.35"/>
  <xacro:property name="upper_arm_radius" value="${upper_arm_circ/(2*PI)}"/>
  <xacro:property name="upper_arm_length" value="0.357"/>
  <xacro:property name="upper_arm_mass" value="1"/>

  <xacro:property name="hand_length" value="0.186"/>
  <xacro:property name="hand_grip_length" value="${hand_length - 0.074}"/>
  <xacro:property name="hand_width" value="0.103"/>
  <xacro:property name="hand_thickness" value="0.026"/>
  <xacro:property name="hand_mass" value="1"/>

  <xacro:property name="lower_arm_circ" value="0.24"/>
  <xacro:property name="lower_arm_radius" value="${lower_arm_circ/(2*PI)}"/>
  <xacro:property name="lower_arm_length" value="${0.466 - hand_length}"/>
  <xacro:property name="lower_arm_mass" value="1"/>

  <xacro:property name="arm_length" value="${upper_arm_length + lower_arm_length + hand_grip_length}"/>

  <xacro:property name="body_width" value="0.441"/>
  <xacro:property name="body_depth" value="0.308"/>
  <xacro:property name="body_height" value="${shoulder_height+upper_arm_radius}"/>
  <xacro:property name="body_mass" value="80"/>

  <xacro:property name="body_wheel_radius" value="${body_depth/5}"/>
  <xacro:property name="body_wheel_thickness" value="${body_width/10}"/>
  <xacro:property name="body_wheel_mass" value="1"/>
  <xacro:property name="body_wheel_elev" value="0.01"/>


  <xacro:property name="joint_damping" value="0"/>
  <xacro:property name="alpha" value="0.697752948218834"/>
  <xacro:property name="pitch" value="${-alpha}"/>

  <!-- Base -->
  <link name="base_footprint"/>

  <joint name="base_joint" type="fixed">
    <parent link="base_footprint"/>
    <child link="human/body"/>
    <origin xyz="0 0 ${body_height/2 + body_wheel_elev}" rpy="0 0 0"/>
  </joint>

  <link name="human/body">
    <visual>
      <geometry>
        <box size="${body_depth} ${body_width} ${body_height}"/>
      </geometry>
      <material name="dark"/>
    </visual>
    <collision>
      <geometry>
        <box size="${body_depth} ${body_width} ${body_height}"/>
      </geometry>
    </collision>
    <xacro:box_inertia m="${body_mass - 4*body_wheel_mass}" w="${body_width}" h="${body_height}" d="${body_depth}" z="-${(body_height)/2}"/>
  </link>

  <gazebo reference="base_link">
    <material>Gazebo/Grey</material>
  </gazebo>

  <!-- ARM -->
        <!-- Shoulder -->

        <link name="human/shoulder">
            <visual>
                <geometry>
                    <sphere radius="${upper_arm_radius}"/>
                </geometry>
            </visual>
            <xacro:small_inertia/>
        </link>

         <joint name="shoulder" type="fixed">
            <parent link="human/body"/>
            <child link="human/shoulder"/>
            <origin xyz="0 -${body_width/2} ${(body_height)/2 - upper_arm_radius}"/>
        </joint>

        <link name="human/shoulder_revolute">
            <visual>
                <geometry>
                    <sphere radius="${upper_arm_radius/2}"/>
                </geometry>
            </visual>
            <xacro:small_inertia/>
        </link>

        <joint name="human/shoulder_joint" type="revolute">
            <parent link="human/shoulder"/>
            <child link="human/shoulder_revolute"/>
            <axis xyz="0 1 0"/>
            <origin rpy="0 ${pitch} 0"/>
            <limit lower="${-PI/2 - pitch}" upper="${PI - pitch}" effort="1" velocity="10"/>
            <dynamics damping="${joint_damping}"/>
        </joint>

        <!-- Upper arm -->

        <link name="human/upper_arm">
            <visual>
                <geometry>
                    <cylinder radius="${upper_arm_radius}" length="${upper_arm_length}"/>
                </geometry>
            </visual>
            <xacro:cylinder_inertia m="${upper_arm_mass}" r="${upper_arm_radius}" t="${upper_arm_length}"/>
        </link>

        <joint name="upper_arm" type="fixed">
            <parent link="human/shoulder_revolute"/>
            <child link="human/upper_arm"/>
            <origin xyz="0 0 ${-upper_arm_length/2}"/>
        </joint>

        <!-- Elbow -->

        <link name="human/elbow">
            <visual>
                <geometry>
                    <sphere radius="${upper_arm_radius}"/>
                </geometry>
            </visual>
            <xacro:small_inertia/>
        </link>

        <joint name="elbow" type="fixed">
            <parent link="human/upper_arm"/>
            <child link="human/elbow"/>
            <origin xyz="0 0 ${-upper_arm_length/2}"/>
        </joint>

        <link name="human/elbow_revolute">
            <visual>
                <geometry>
                    <sphere radius="${upper_arm_radius/2}"/>
                </geometry>
            </visual>
            <xacro:small_inertia/>
        </link>

        <joint name="human/elbow_joint" type="revolute">
            <parent link="human/elbow"/>
            <child link="human/elbow_revolute"/>
            <axis xyz="0 1 0"/>
            <limit lower="0" upper="${5/6*PI}" effort="1" velocity="10"/>
            <dynamics damping="${joint_damping}"/>
        </joint>

        <!-- Lower arm -->

        <link name="human/lower_arm">
            <visual>
                <geometry>
                    <cylinder radius="${lower_arm_radius}" length="${lower_arm_length}"/>
                </geometry>
            </visual>
            <xacro:cylinder_inertia m="${lower_arm_mass}" r="${lower_arm_radius}" t="${lower_arm_length}"/>
        </link>

        <joint name="lower_arm" type="fixed">
            <parent link="human/elbow_revolute"/>
            <child link="human/lower_arm"/>
            <origin xyz="0 0 ${-lower_arm_length/2}"/>
        </joint>

        <!-- Wrist -->

        <link name="human/wrist">
            <visual>
                <geometry>
                    <sphere radius="${lower_arm_radius}"/>
                </geometry>
            </visual>
            <xacro:small_inertia/>
        </link>

        <joint name="wrist" type="fixed">
            <parent link="human/lower_arm"/>
            <child link="human/wrist"/>
            <origin xyz="0 0 ${-lower_arm_length/2}"/>
        </joint>

        <link name="human/wrist_revolute">
            <visual>
                <geometry>
                    <sphere radius="${lower_arm_radius/2}"/>
                </geometry>
            </visual>
            <xacro:small_inertia/>
        </link>

        <joint name="human/wrist_joint" type="revolute">
            <parent link="human/wrist"/>
            <child link="human/wrist_revolute"/>
            <axis xyz="0 1 0"/>
            <limit lower="${-PI/2}" upper="${PI/2}" effort="1" velocity="10"/>
            <dynamics damping="${joint_damping}"/>
        </joint>

        <!-- Hand -->
        <link name="human/hand">
            <visual>
                <origin xyz="0 0 ${hand_grip_length/2}"/>
                <geometry>
                    <box size="${hand_thickness*2} ${hand_width} ${hand_grip_length}"/>
                </geometry>
            </visual>
            <xacro:box_inertia m="${hand_mass}" w="${hand_width}" h="${hand_grip_length}" d="${hand_thickness*2}" />
        </link>

        <joint name="human/hand" type="fixed">
            <parent link="human/wrist_revolute"/>
            <child link="human/hand"/>
            <origin xyz="0 0 ${-hand_grip_length}" rpy="0 0 0"/>
        </joint>

  <!-- <link name="human/upper_arm">
        <visual>
            <origin xyz="0 0 ${-upper_arm_length/2}"/>
            <geometry>
                <cylinder radius="${upper_arm_radius}" length="${upper_arm_length}"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 ${-upper_arm_length/2}"/>
            <geometry>
                <cylinder radius="${upper_arm_radius}" length="${upper_arm_length}"/>
            </geometry>
        </collision>
        <xacro:cylinder_inertia m="${upper_arm_mass}" r="${upper_arm_radius}" t="${upper_arm_length}" yaw="${PI/2}"/>
    </link>

    <joint name="coupling_joint" type="revolute">
        <parent link="human/body"/>
        <child link="human/upper_arm"/>
        <axis xyz="0 1 0"/>
        <origin xyz="0 ${-body_width/2 - upper_arm_radius} ${body_height/2 - body_wheel_elev}"/>
        <limit lower="${-PI/2}" upper="${PI}" effort="1" velocity="10"/>
        <dynamics damping="${joint_damping}"/>
    </joint> -->


  <!-- Turtlebot3 wheel macros -->
  <xacro:turtlebot3_wheel alignment_w="left" alignment_d="front">
    <origin xyz="${body_depth/2} ${(body_width + body_wheel_thickness)/2} -${(body_height)/2 - body_wheel_radius + body_wheel_elev}" rpy="${-PI*0.5} 0 0"/>
  </xacro:turtlebot3_wheel>
  <xacro:turtlebot3_wheel alignment_w="right" alignment_d="front">
    <origin xyz="${body_depth/2} -${(body_width + body_wheel_thickness)/2} -${(body_height)/2 - body_wheel_radius + body_wheel_elev}" rpy="${-PI*0.5} 0 0"/>
  </xacro:turtlebot3_wheel>

  <xacro:turtlebot3_wheel alignment_w="left" alignment_d="back">
    <origin xyz="${-body_depth/2} ${(body_width + body_wheel_thickness)/2} -${(body_height)/2 - body_wheel_radius + body_wheel_elev}" rpy="${-PI*0.5} 0 0"/>
  </xacro:turtlebot3_wheel>
  <xacro:turtlebot3_wheel alignment_w="right" alignment_d="back">
    <origin xyz="${-body_depth/2} -${(body_width + body_wheel_thickness)/2} -${(body_height)/2 - body_wheel_radius + body_wheel_elev}" rpy="${-PI*0.5} 0 0"/>
  </xacro:turtlebot3_wheel>

  <!-- Gazebo simulation plugins -->
  <xacro:include filename="$(find
trolley_test)/urdf/turtlebot3_waffle_base.gazebo.xacro"/>

  <!-- sensors -->


</robot>