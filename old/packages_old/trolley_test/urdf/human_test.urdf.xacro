<?xml version="1.5"?>
<!-- Human model -->
<robot name="human" 
    xmlns:xacro="http://www.ros.org/wiki/xacro">

    <xacro:property name="PI" value="3.1415926535897931"/>

    <!-- Constants for robot dimensions -->
    <xacro:property name="shoulder_height" value="1.402"/>

    <xacro:property name="upper_arm_circ" value="0.35"/>
    <xacro:property name="upper_arm_radius" value="${upper_arm_circ/(2*PI)}"/>
    <xacro:property name="upper_arm_length" value="0.357"/>
    <xacro:property name="upper_arm_mass" value="1"/>

    <xacro:property name="hand_length" value="0.186"/>
    <xacro:property name="hand_grip_length" value="${hand_length - 0.074}"/>
    <xacro:property name="hand_width" value="0.103"/>
    <xacro:property name="hand_thickness" value="0.026"/>
    <xacro:property name="hand_mass" value="1"/>

    <xacro:property name="lower_arm_circ" value="0.24"/>
    <xacro:property name="lower_arm_radius" value="${lower_arm_circ/(2*PI)}"/>
    <xacro:property name="lower_arm_length" value="${0.466 - hand_length}"/>
    <xacro:property name="lower_arm_mass" value="1"/>

    <xacro:property name="arm_length" value="${upper_arm_length + lower_arm_length + hand_grip_length}"/>

    <xacro:property name="body_width" value="0.441"/>
    <xacro:property name="body_depth" value="0.308"/>
    <xacro:property name="body_height" value="${shoulder_height+upper_arm_radius}"/>
    <xacro:property name="body_mass" value="1"/>

    <xacro:property name="body_wheel_radius" value="${body_depth/5}"/>
    <xacro:property name="body_wheel_thickness" value="${body_width/10}"/>
    <xacro:property name="body_wheel_mass" value="1"/>
    <xacro:property name="body_wheel_elev" value="0.01"/>


    <xacro:property name="joint_damping" value="0"/>

    <!-- BASE -->
    <link name="base_footprint"/>

    <!-- BODY -->
    <link name="human/body">
        <visual>
            <geometry>
                <box size="${body_depth} ${body_width} ${body_height}"/>
            </geometry>
        </visual>
        <collision>
            <geometry>
                <box size="${body_depth} ${body_width} ${body_height}"/>
            </geometry>
        </collision>
        <xacro:box_inertia m="${body_mass}" w="${body_width}" h="${body_height}" d="${body_depth}" z="${-(body_height)/2}"/>
    </link>

    <joint name="body_to_world" type="fixed">
        <parent link="base_footprint"/>
        <child link="human/body"/>
        <origin xyz="0 ${-body_depth/2} ${(body_height)/2 + body_wheel_elev}" rpy="0 0 ${PI/2}"/>
    </joint>

    <!-- <link name="wheel_front_left_link">
            <visual>
                <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
                <geometry>
                    <cylinder length="${body_wheel_thickness}" radius="${body_wheel_radius}"/>
                </geometry>
                <material name="orange"/>
            </visual>
            <xacro:cylinder_inertia m="${body_wheel_mass}" r="${body_wheel_radius}" t="${body_wheel_thickness}" />
            <collision>
                <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
                <geometry>
                    <cylinder length="${body_wheel_thickness}" radius="${body_wheel_radius}"/>
                </geometry>
            </collision>
        </link>

        <joint name="wheel_front_left_joint" type="continuous">
            <parent link="human/body"/>
            <child link="wheel_front_left_link"/>
            <origin xyz="${body_depth/2} ${(body_width + body_wheel_thickness)/2} ${-(body_height)/2 + body_wheel_radius - body_wheel_elev}" rpy="0 0 0"/>
            <axis xyz="0 1 0"/>
        </joint> -->

        <!-- <link name="wheel_front_right_link">
            <visual>
                <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
                <geometry>
                    <cylinder length="${body_wheel_thickness}" radius="${body_wheel_radius}"/>
                </geometry>
                <material name="orange"/>
            </visual>
            <xacro:cylinder_inertia m="${body_wheel_mass}" r="${body_wheel_radius}" t="${body_wheel_thickness}" />
            <collision>
                <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
                <geometry>
                    <cylinder length="${body_wheel_thickness}" radius="${body_wheel_radius}"/>
                </geometry>
            </collision>
        </link>

        <joint name="wheel_front_right_joint" type="continuous">
            <parent link="human/body"/>
            <child link="wheel_front_right_link"/>
            <origin xyz="${body_depth/2} -${(body_width + body_wheel_thickness)/2} ${-(body_height)/2 + body_wheel_radius - body_wheel_elev}" rpy="0 0 0"/>
            <axis xyz="0 1 0"/>
        </joint> -->

    <!-- Wheel macro -->
    <xacro:macro name="human_wheel" params="alignment_w alignment_d *origin">
        <link name="wheel_${alignment_d}_${alignment_w}_link">
            <visual>
                <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
                <geometry>
                    <cylinder length="${body_wheel_thickness}" radius="${body_wheel_radius}"/>
                </geometry>
                <material name="orange"/>
            </visual>
            <xacro:cylinder_inertia m="${body_wheel_mass}" r="${body_wheel_radius}" t="${body_wheel_thickness}" />
            <collision>
                <origin xyz="0 0 0" rpy="${PI/2} 0 0"/>
                <geometry>
                    <cylinder length="${body_wheel_thickness}" radius="${body_wheel_radius}"/>
                </geometry>
            </collision>
        </link>

        <joint name="wheel_${alignment_d}_${alignment_w}_joint" type="continuous">
            <parent link="human/body"/>
            <child link="wheel_${alignment_d}_${alignment_w}_link"/>
            <xacro:insert_block name="origin"/>
            <axis xyz="0 1 0"/>
        </joint>

        <gazebo reference="wheel_${alignment_d}_${alignment_w}_link">
            <mu1 value="10.0"/>
            <mu2 value="10.0"/>
            <kp value="500000.0" />
            <kd value="10.0" />
            <minDepth>0.001</minDepth>
            <maxVel>0.1</maxVel>
            <fdir1>1 0 0</fdir1>
            <material>Gazebo/Orange</material>
        </gazebo>
    </xacro:macro>

    <xacro:human_wheel alignment_w="left" alignment_d="front">
        <origin xyz="${body_depth/2} ${(body_width + body_wheel_thickness)/2} ${-(body_height)/2 + body_wheel_radius - body_wheel_elev}" rpy="0 0 0"/>
    </xacro:human_wheel>

    <xacro:human_wheel alignment_w="right" alignment_d="front">
        <origin xyz="${body_depth/2} -${(body_width + body_wheel_thickness)/2} ${-(body_height)/2 + body_wheel_radius - body_wheel_elev}" rpy="0 0 0"/>
    </xacro:human_wheel>

    <xacro:human_wheel alignment_w="left" alignment_d="back">
        <origin xyz="-${body_depth/2} ${(body_width + body_wheel_thickness)/2} ${-(body_height)/2 + body_wheel_radius - body_wheel_elev}" rpy="0 0 0"/>
    </xacro:human_wheel>

    <xacro:human_wheel alignment_w="right" alignment_d="back">
        <origin xyz="-${body_depth/2} -${(body_width + body_wheel_thickness)/2} ${-(body_height)/2 + body_wheel_radius - body_wheel_elev}" rpy="0 0 0"/>
    </xacro:human_wheel> -->

    <!-- <link name="human/upper_arm">
        <visual>
            <origin xyz="0 0 ${-upper_arm_length/2}"/>
            <geometry>
                <cylinder radius="${upper_arm_radius}" length="${upper_arm_length}"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 ${-upper_arm_length/2}"/>
            <geometry>
                <cylinder radius="${upper_arm_radius}" length="${upper_arm_length}"/>
            </geometry>
        </collision>
        <xacro:cylinder_inertia m="${upper_arm_mass}" r="${upper_arm_radius}" t="${upper_arm_length}" yaw="${PI/2}"/>
    </link>

    <joint name="upper_arm" type="revolute">
        <parent link="human/body"/>
        <child link="human/upper_arm"/>
        <axis xyz="0 1 0"/>
        <origin xyz="0 ${-body_width/2 - upper_arm_radius} ${body_height/2} - body_wheel_elev"/>
        <limit lower="${-PI/2}" upper="${PI}" effort="1" velocity="10"/>
        <dynamics damping="${joint_damping}"/>
    </joint> -->

</robot>