#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from gazebo_msgs.msg import LinkStates
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3

# node initialization #
rospy.init_node("test_remap_node")

def callback_imu(msg):
    ddx = msg.linear_acceleration.x
    pub_imu.publish(ddx)

def callback_vel(msg):
    i = msg.name.index('trolley_IMU::base_footprint')
    j = msg.name.index('trolley_IMU::front_left_wheel_link')
    vel = msg.twist[i].linear.x
    pub_vel.publish(vel)

    pos_x = msg.pose[j].position.x
    pos_z = msg.pose[j].position.z
    pub_posx.publish(pos_x)
    pub_posz.publish(pos_z)

def callback_force(msg):
    pub_force.publish(msg.x)

pub_imu = rospy.Publisher('/test/imu', Float64, queue_size=1)
pub_vel = rospy.Publisher('/test/vel', Float64, queue_size=1)
pub_posx = rospy.Publisher('/test/posx', Float64, queue_size=1)
pub_posz = rospy.Publisher('/test/posz', Float64, queue_size=1)
pub_force = rospy.Publisher('/test/force', Float64, queue_size=1)

rospy.Subscriber("imu", Imu, callback_imu)
rospy.Subscriber("gazebo/link_states", LinkStates, callback_vel)
rospy.Subscriber("trolley/input_force", Vector3, callback_force)

while not rospy.is_shutdown():
    rospy.spin()
