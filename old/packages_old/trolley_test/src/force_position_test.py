#!/usr/bin/env python

import rospy
from gazebo_msgs.srv import ApplyBodyWrench, BodyRequest, GetLinkProperties
from geometry_msgs.msg import Wrench, Point
from std_msgs.msg import Float64
import math
import time

# node initialization #
rospy.init_node("pub")

# definition of variables #
link = "block_launch::base_footprint"
ref_frame = link
# ref_frame = ""

force_x = 2
force_y = 0
force_z = 0
dur = 3

wrench = Wrench()

ref_point = Point()  # Reference point from local frame (?)
ref_point.x = 0
ref_point.y = 0
ref_point.z = 0


def apply_force(force_x=0, force_y=0, force_z=0):

    rospy.wait_for_service('/gazebo/apply_body_wrench')
    apply_wrench = rospy.ServiceProxy('/gazebo/apply_body_wrench',ApplyBodyWrench)

    # Compose wrench
    wrench.force.x = force_x
    wrench.force.y = force_y
    wrench.force.z = force_z
    wrench.torque.x = 0
    wrench.torque.y = 0
    wrench.torque.z = 0

    # Apply wrench
    # print(wrench)
    return_msg = apply_wrench(body_name=link,
                wrench=wrench,
                # reference_frame=ref_frame,
                # reference_point=ref_point,
                duration=rospy.Duration(dur))

    # When using force plugin
    # pub_force.publish(wrench)
    # return_msg = ""

    # Publish applied force
    net_force = math.sqrt(wrench.force.y**2 + wrench.force.z**2)
    pub.publish(net_force)

    return return_msg


def clear_forces():
    clear_forces = rospy.ServiceProxy('/gazebo/clear_body_wrenches',
                                      BodyRequest)
    return_msg = clear_forces(body_name=link)
    return return_msg


# definition of publisher/subscriber and services #
pub = rospy.Publisher('trolley/handle_force_measurements',
                      Float64,
                      queue_size=1)
# pub_force = rospy.Publisher('trolley/apply_force', Wrench, queue_size=1)

##################### main program #####################################

print("Applying forward force...")
return_msg = apply_force(force_x, force_y, force_z)
print(return_msg)

# start = time.time()
# while time.time() < start + 2:
#     return_msg = apply_force(force_x, force_y, force_z)
#     print(return_msg)

# print("Applying neutral force...")
# return_msg = apply_force()
# print(return_msg)

# clear_forces()
# print(return_msg)
print("Force exertion completed")
