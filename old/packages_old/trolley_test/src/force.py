#!/usr/bin/env python

import math

import rospkg
import rospy
import yaml
from geometry_msgs.msg import Vector3
from std_msgs.msg import Float64
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

#@ node initialization #
rospy.init_node("force_input")
freq = 50

#@ definition of variables #
force_ss = 1
force_acc = 4
print("Acceleration force: {}".format(force_acc))
print("Steady state force: {}".format(force_ss))

dur_wait = 2
dur_acc = 2
dur_ss = 10

#@ Build force profile
force_wait_ = np.zeros(freq * dur_wait)
force_acc_ = np.ones(freq * dur_acc)*force_acc
force_ss_ = np.ones(freq * dur_ss)*force_ss

force_total_ = list(np.concatenate([force_wait_, force_acc_, force_ss_]))
time = np.linspace(0, float(len(force_total_))/freq,
                   float(len(force_total_)), endpoint=False)

butter = signal.butter(3, 5, fs=50, output='sos')
force_total_filt = list(signal.sosfilt(butter, force_total_))
# plt.plot(time, force_total_, label="unfiltered")
# plt.plot(time, force_total_filt, label="filtered")
# plt.legend()
# plt.show()

dur_wait = rospy.Duration.from_sec(dur_wait)
dur_acc = rospy.Duration.from_sec(dur_acc)
dur_ss = rospy.Duration.from_sec(dur_ss)

#@ Set initial force
force = Vector3()
force.x = 0
force.y = 0
force.z = 0

#@ definition of functions #


def update_force2():
    global force, force_total_filt

    if len(force_total_filt) > 1:
        force.x = force_total_filt[0]
        force_total_filt.pop(0)
    else:
        force.x = force_total_filt[0]


def update_force():
    global force

    cur_time = rospy.Time.now()  #@ Get current time in Gazebo
    # print("Start time: {}. Current time: {}. Duration: {}.".format(start_time.to_time(), cur_time.to_time(), dur.to_sec()))
    if cur_time < start_time + dur_wait:
        print("Waiting...")
        force.x = 0

    elif cur_time < start_time + dur_wait + dur_acc:
        print("Accelerating using a force of {} N".format(force_acc))
        force.x = force_acc

    elif dur_ss is not False and cur_time < start_time + dur_wait + dur_acc + dur_ss:
        print("Applying a steady-state force of {} N".format(force_ss))
        force.x = force_ss

    # elif dur_ss is not False:
    #         # print("Not applying any force")
    #         force.x = 0
    #         force.y = 0
    #         force.z = 0

    else:
        print("Applying a steady-state force of {} N".format(force_ss))
        force.x = force_ss


#@ definition of publisher/subscriber and services #
pub = rospy.Publisher('trolley/input_force', Vector3, queue_size=1)
# puba = rospy.Publisher('trolley/analysis/input_force', Float64, queue_size=1)


#@#################### main program ####################################
while rospy.Time.now() == rospy.Time(0):  #@ Wait to receive first message
    continue

start_time = rospy.Time.now()  #@ Save start time of node

r = rospy.Rate(freq)
while not rospy.is_shutdown():
    update_force()
    pub.publish(force)
    # puba.publish(force.x)
    r.sleep()
