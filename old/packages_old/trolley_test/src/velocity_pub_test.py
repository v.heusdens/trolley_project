#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist

rospy.init_node("velocity_pub_test")
vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

cmd = Twist()
vel_pub.publish(cmd) # Make sure initial velocity is set to 0

cmd.linear.y = 1

while rospy.get_time() == 0 and not rospy.is_shutdown():
    continue

while not rospy.is_shutdown():
    vel_pub.publish(cmd)