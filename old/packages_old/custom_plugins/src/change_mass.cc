#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

#include <ros/ros.h>
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

#include "std_msgs/Float32.h"

namespace gazebo
{
  class ChangeMass : public ModelPlugin
  {
    ////////////////////////////////////////////////////////
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) 
    {
      ROS_WARN("Loading ChangeMass plugin...");
      // Store the pointer to the model
      this->model = _parent;
      ROS_WARN("parent is: %s", this->model->GetName().c_str());

      // Check and store link on which plugin applies
      if (!_sdf->HasElement("link")) {
          ROS_ERROR("No link element present. ChangeMassPlugin could not be loaded.");
          return;
      }
      this->link_name = _sdf->GetElement("link")->Get<std::string>();

      // Get pointer to link
      this->link = this->model->GetLink(this->link_name); 
      if (this->link) {
          ROS_WARN("ChangeMassPlugin loaded on link: %s", link_name.c_str());
      }
      else
          ROS_ERROR_STREAM("Link '" << link_name << "' not found! ChangeMassPlugin could not be loaded.");


      // Listen to the update event. This event is broadcast every simulation iteration.
      // this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
      //     std::bind(&ChangeMass::OnUpdate, this));
      
      // Save initial inertial properties
      this->GetInertia();

      // ROS
      // Initialize ros, if it has not already bee initialized.
      if (!ros::isInitialized())
      {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "change_mass_node",
            ros::init_options::NoSigintHandler);
            ROS_INFO_STREAM("ROS initialized");
      }

      // Create ROS node
      this->rosNode.reset(new ros::NodeHandle("change_mass_node"));

      // Determine subscriber topic
        if (!_sdf->HasElement("topic")) {
          this->topic = "/trolley/inertia";
          ROS_WARN("No topic specified. Using default topic: %s", this->topic.c_str());
          }
        else {
          this->topic = _sdf->GetElement("topic")->Get<std::string>();
          ROS_WARN("Set subscriber topic to...%s", this->topic.c_str());
        }

      // Initialize subscriber
      ros::SubscribeOptions so =
        ros::SubscribeOptions::create<std_msgs::Float32>(
            this->topic,
            1,
            boost::bind(&ChangeMass::OnRosMsg, this, _1),
            ros::VoidPtr(), &this->rosQueue);
      this->rosSub = this->rosNode->subscribe(so);
      
      // Spin up the queue helper thread.
      this->rosQueueThread =
        std::thread(std::bind(&ChangeMass::QueueThread, this));

      
      ROS_WARN("ChangeMass plugin loaded!");
    }


    ////////////////////////////////////////////////////////
    // Called by the world update start event
    // public: void OnUpdate() 
    // {
      // this->link->SetInertial(this->inertia);
    // }

    public: void GetInertia()
    {
      this->inertia = this->link->GetInertial();
      ROS_WARN("Initial link mass: %f", this->inertia->Mass());
    }

    ////////////////////////////////////////////////////////
    // public: void SetForce(const geometry_msgs::Vector3ConstPtr &_force)
    public: void SetInertia(const std_msgs::Float32::ConstPtr &_mass)
    {
      this->inertia->SetMass(_mass->data);
      this->link->SetInertial(this->inertia);
      ROS_WARN("New link mass set: %f", _mass->data);
      // ROS_WARN("New link mass set: %f", this->link->GetInertial()->Mass());
    }

    ////////////////////////////////////////////////////////
    public: void OnRosMsg(const std_msgs::Float32::ConstPtr &_msg)
    {
      this->SetInertia(_msg);
    }

    ////////////////////////////////////////////////////////
    /// ROS helper function that processes messages
    private: void QueueThread()
    {
      static const double timeout = 0.01;
      while (this->rosNode->ok())
      {
        this->rosQueue.callAvailable(ros::WallDuration(timeout));
      }
    }

    ////////////////////////////////////////////////////////
    // Parameters
    private: std::string link_name;
    private: std::string topic;

    private: double mass;
    private: gazebo::physics::InertialPtr inertia;

    // Pointers to the joints
    private: physics::LinkPtr link;

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection_;

    /// A node used for ROS transport
    private: std::unique_ptr<ros::NodeHandle> rosNode;

    /// A ROS subscriber
    private: ros::Subscriber rosSub;
    /// A ROS callbackqueue that helps process messages
    private: ros::CallbackQueue rosQueue;
    /// A thread the keeps running the rosQueue
    private: std::thread rosQueueThread;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ChangeMass)
}