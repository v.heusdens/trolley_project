#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

#include "std_msgs/Float32.h"
#include "geometry_msgs/Vector3.h"

#include <ignition/math/Vector3.hh>

#include <ros/ros.h>
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

namespace gazebo
{
  class ApplyForce : public ModelPlugin
  {
    ////////////////////////////////////////////////////////
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) 
    {
      ROS_WARN("Loading ApplyInputForce plugin...");
      // Store the pointer to the model
      this->model_ = _parent;
      ROS_WARN("parent is: %s", this->model_->GetName().c_str());

      // Check and store link on which force should be applied
      if (!_sdf->HasElement("link")) {
          ROS_ERROR("No link element present. ApplyForcePlugin could not be loaded.");
          return;
      }
      this->link_name_ = _sdf->GetElement("link")->Get<std::string>();

      // Get pointer to link
      this->link_ = this->model_->GetLink(this->link_name_); 
      if (this->link_) {
          ROS_WARN("ApplyForcePlugin loaded on link: %s", link_name_.c_str());
      }
      else
          ROS_ERROR_STREAM("Link '" << link_name_ << "' not found! ApplyForcePlugin could not be loaded.");

      // Check and store position at which force should be applied (wrt center of mass)
      if (!_sdf->HasElement("x") or !_sdf->HasElement("y") or !_sdf->HasElement("z")) {
          ROS_WARN("No application point specified. Using center of mass (?)");
        }
      else {
          std::string temp_x = _sdf->GetElement("x")->Get<std::string>();
          std::string temp_y = _sdf->GetElement("y")->Get<std::string>();
          std::string temp_z = _sdf->GetElement("z")->Get<std::string>();
          float temp_x2 = strtof((temp_x).c_str(),0);
          float temp_y2 = strtof((temp_y).c_str(),0);
          float temp_z2 = strtof((temp_z).c_str(),0);
          this->pos = ignition::math::Vector3d(temp_x2, temp_y2, temp_z2);
          ROS_WARN("Set application point to...x = %f, y = %f, z = %f", temp_x2, temp_y2, temp_z2);
      }


      // Listen to the update event. This event is broadcast every simulation iteration.
      this->updateConnection_ = event::Events::ConnectWorldUpdateBegin(
          std::bind(&ApplyForce::OnUpdate, this));
      

      // ROS
      // Initialize ros, if it has not already bee initialized.
      if (!ros::isInitialized())
      {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "input_force_node",
            ros::init_options::NoSigintHandler);
            ROS_INFO_STREAM("ROS initialized");
      }

      // Create ROS node
      this->rosNode.reset(new ros::NodeHandle("input_force_node"));

      // Determine subscriber topic
        if (!_sdf->HasElement("topic")) {
          this->topic = "/trolley/input_force";
          ROS_WARN("No topic specified. Using default topic: %s", this->topic.c_str());
          }
        else {
          this->topic = _sdf->GetElement("topic")->Get<std::string>();
          ROS_WARN("Set subscriber topic to...%s", this->topic.c_str());
        }

      // Initialize subscriber
      ros::SubscribeOptions so =
        ros::SubscribeOptions::create<geometry_msgs::Vector3>(
            this->topic,
            1,
            boost::bind(&ApplyForce::OnRosMsg, this, _1),
            ros::VoidPtr(), &this->rosQueue);
      this->rosSub = this->rosNode->subscribe(so);
      
      // Spin up the queue helper thread.
      this->rosQueueThread =
        std::thread(std::bind(&ApplyForce::QueueThread, this));

      
      ROS_WARN("ApplyInputForce plugin loaded!");
    }


    ////////////////////////////////////////////////////////
    // Called by the world update start event
    public: void OnUpdate() 
    {
      // Apply a force to the model in its own reference frame
      this->link_->AddLinkForce(this->force, this->pos);
    }

    ////////////////////////////////////////////////////////
    public: void SetForce(const geometry_msgs::Vector3ConstPtr &_force)
    {

      this->force = ignition::math::Vector3d(_force->x, _force->y, _force->z);
      // ROS_WARN("force set >> %f %f %f", this->force.X(), this->force.Y(), this->force.Z());
    }

    ////////////////////////////////////////////////////////
    public: void OnRosMsg(const geometry_msgs::Vector3ConstPtr &_msg)
    {
      this->SetForce(_msg);
    }

    ////////////////////////////////////////////////////////
    /// ROS helper function that processes messages
    private: void QueueThread()
    {
      static const double timeout = 0.01;
      while (this->rosNode->ok())
      {
        this->rosQueue.callAvailable(ros::WallDuration(timeout));
      }
    }

    ////////////////////////////////////////////////////////
    // Parameters
    private: std::string link_name_;
    private: std::string topic;

    private: ignition::math::Vector3d force;
    private: ignition::math::Vector3d pos;

    // Pointers to the joints
    private: physics::LinkPtr link_;

    // Pointer to the model
    private: physics::ModelPtr model_;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection_;

    /// A node used for ROS transport
    private: std::unique_ptr<ros::NodeHandle> rosNode;

    /// A ROS subscriber
    private: ros::Subscriber rosSub;
    /// A ROS callbackqueue that helps process messages
    private: ros::CallbackQueue rosQueue;
    /// A thread the keeps running the rosQueue
    private: std::thread rosQueueThread;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ApplyForce)
}