#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64

print("Initializing fake input node...")


# Initialize node
rospy.init_node("fake_input_node")


# definitions of variables #
force = Float64()
vel = Float64()

switch_time = 5


# Functions
def publish_force():
    # if cur_time < switch_time:
    #     force = 1
    # else:
    #     force = 2

    force = 1.0
    
    pub_force.publish(force)
    return


def publish_vel():
    vel = 1.0

    pub_vel.publish(vel)
    return


# definition of publisher/subscriber and services #
pub_force = rospy.Publisher('/sensors/forces', Float64, queue_size=1) #Fake force output
pub_vel = rospy.Publisher('/sensors/velocities', Float64, queue_size=1) #Fake velocity output


# main program #
print("Running main program...")

r = rospy.Rate(5) # Set to sensor publisher rate
rospy.set_param("use_sim_time", False) #Turn off use of Gazebo time
start_time = rospy.get_time()

while not rospy.is_shutdown():
    cur_time = rospy.get_time() - start_time    #Set current time wrt start of node
    # print("Time passed: {} seconds".format(cur_time))

    publish_force()
    publish_vel()
    r.sleep()

print("Fake input publisher is shut down.")