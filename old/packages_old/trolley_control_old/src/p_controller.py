#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from trolley_msgs.msg import DataStamped

# node initialization #
rospy.init_node("p-controller")


# definitions of variables #
control_signal = Twist()

# definitions of functions #
def callback(force):
    global control_signal, pub
    
    # P-action
    control_torque = force.force.length
    # control_torque = 500
    
    # Publish control signal
    control_signal.linear.x = control_torque
    pub.publish(control_signal)
    
    
# definition of publisher/subscriber and services #
rospy.Subscriber('sim/forces', DataStamped, callback)
pub = rospy.Publisher('/trolley/controller/cmd_vel', Twist, queue_size=1)


# main program #
r = rospy.Rate(5) #5 Hz

while not rospy.is_shutdown():
    r.sleep()