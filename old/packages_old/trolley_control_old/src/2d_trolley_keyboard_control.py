#!/usr/bin/env python

################### import ###########################
import rospy
from geometry_msgs.msg import Twist

############# node initialization ####################
rospy.init_node('teleop_2D', anonymous=True)  # init_node

############# definition of variables ####################
info = Twist()


############ definitions of functions ##################
def callback(data):
    global info
    # rospy.loginfo(info)

    if data.linear.x != 0.0:
        info = data
        info.linear.y = 0
        info.linear.z = 0
        info.angular.x = 0
        info.angular.y = 0
        info.angular.z = 0
        pub.publish(info)
        print(info)


#### definition of publisher/subscriber and services ###
rospy.Subscriber("/cmd_vel", Twist, callback)
pub = rospy.Publisher('/trolley/controller/cmd_vel', Twist, queue_size=10)

############# main program #############################
rate = rospy.Rate(10)

#--------------endless loop till shut down -------------#
while not rospy.is_shutdown():

    # pub.publish(info)
    rate.sleep()
