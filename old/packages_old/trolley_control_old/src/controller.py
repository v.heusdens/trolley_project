#!/usr/bin/env python

import rospy
import math
import sympy
from scipy import signal
from std_msgs.msg import Float64

print("Initializing controller...")

# Initialize node
rospy.init_node("controller_node")

# Definition of variables
force_alive = False
vel_alive = False

force = 0
vel = 0
motor_input = Float64() #TO DO: check what type of signal this must be (check motor plugins gazebo)

control_sig = 0
feedback = 0

wheel_radius = 0.105 #[m]
m = 0.7+3.9*2+1.2+0.1 #mass [kg]
c = 1 #damping [kg/s]


# Functions
def callback_forces(msg):
    """This function filters and updates the input force for use in the controller"""
    global force, force_alive
    force_alive = True #Bool to determine if force data is being recieved
    msg = msg.data

    "PUT DATA FILTERING STUFF HERE (not needed for 'dry' tests)"

    force = msg
    return


def callback_velocities(msg):
    """This function filters and updates the linear wheel velocity 
        for use in the controller"""
    global vel, vel_alive
    vel_alive = True #Bool to determine if force data is being recieved
    msg = msg.data
    
    "PUT DATA FILTERING STUFF HERE (not needed for 'dry' tests)"
    
    # vl = wheel_radius*msg # [rad/s] -> [m/s]
    # vel = vl
    vel = msg
    return


def prop_control():
    """Proportional velocity control function"""
    k = 1
    
    return k*force


def DOB():
    """This function implements the disturbance observer.
        Inputs are control signal and measured velocity."""

    # MODEL
    t = sympy.symbols('t', real=True)
    v = sympy.Function('v')(t)
    dv = sympy.Derivative(v, t)
    inv_model = m*dv + c*v # 1st ode with inertia and damping (v -> F)
    print("inverse model: {}".format(inv_model))
        
    # Q-FILTER
    q = 1

    dist_est = inv_model*vel - control_sig
    return q*dist_est


def publish_control_signal():
    """This function transforms the control signal into the correct signal 
        for the motor input and publishes it on topic '/trolley/motor_input'"""
    global motor_input

    motor_input.data = control_sig

    pub.publish(motor_input)
    return


# definition of publisher/subscriber and services #
rospy.Subscriber('/sensors/forces', Float64, callback_forces) #Force sensor measurements
rospy.Subscriber('/sensors/velocities', Float64, callback_velocities) #Velocity sensor measurements
pub = rospy.Publisher('/control/motor_input', Float64, queue_size=1) #Motor control signal

# MAIN CODE #
print("Waiting for input data...")

# Wait to fake_input_node to publish
while not (force_alive or vel_alive) and not rospy.is_shutdown():
    continue

"""Main control loop"""
print("Running main program...")
print("\n")
r = rospy.Rate(1)

while not rospy.is_shutdown():
    print("measured force: {}".format(force))
    print("measured velocity: {}".format(vel))
    feedback = DOB()
    print("feedback: {}".format(feedback))
    control_sig = prop_control()
    print("control_sig before feedback: {}".format(control_sig))
    control_sig -= feedback
    print("control_sig after feedback: {}".format(control_sig))
    
    publish_control_signal()
    print("control_signal: {}".format(motor_input))
    print("\n")
    r.sleep()

print("Controller is shut down.")