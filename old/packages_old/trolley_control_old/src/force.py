#!/usr/bin/env python

# import #
import rospy
from gazebo_msgs.srv import ApplyBodyWrench, BodyRequest, GetLinkProperties
from geometry_msgs.msg import Wrench, Point
from std_msgs.msg import String
from time import sleep

# node initialization #
rospy.init_node("pub")

# definition of publisher/subscriber and services #
def get_link_properties(link):
    rospy.wait_for_service('/gazebo/get_link_properties')

    try:
        get_prop = rospy.ServiceProxy('/gazebo/get_link_properties',GetLinkProperties)
        link_prop = get_prop(link_name = "trolley::{}".format(link))
        return link_prop

    except rospy.ServiceException:
        print("{} is not a valid link!".format(link))


def apply_force(link, force_x=0, force_y=0, force_z=0, torque_x=0, torque_y=0, torque_z=0):
    rospy.wait_for_service('/gazebo/apply_body_wrench')

    try:
        apply_force = rospy.ServiceProxy('/gazebo/apply_body_wrench',ApplyBodyWrench)
        wrench = Wrench()

        wrench.force.x = force_x
        wrench.force.y = force_y
        wrench.force.z = force_z
        wrench.torque.x = torque_x
        wrench.torque.y = torque_y
        wrench.torque.z = torque_z

        if link == "wheels":
            apply_force(body_name = "trolley::trolley/left_wheel_link", 
            wrench = wrench, 
            reference_frame = ref_frame, 
            duration = rospy.Duration(-1))
            apply_force(body_name="trolley::trolley/right_wheel_link",
                        wrench=wrench,
                        reference_frame = ref_frame, 
                        duration=rospy.Duration(-1))

        elif link == "handle":

            apply_force(body_name="trolley::trolley/support_link",
                        wrench=wrench,
                        reference_frame=ref_frame,
                        duration=rospy.Duration(-1))

        else:
            print("ERROR: {} is not a valid position to apply a wrench".format(link))

    except rospy.ServiceException:
        print("Service call to /gazebo/apply_body_wrench failed")


def clear_forces(link):
    try:
        clear_forces = rospy.ServiceProxy('/gazebo/clear_body_wrenches',BodyRequest)

        if link=="wheels":
            clear_forces(body_name = "trolley::trolley/left_wheel_link")
            clear_forces(body_name = "trolley::trolley/right_wheel_link")

        elif link == "handle":
            clear_forces(body_name = "trolley::trolley/support_link")

        else:
            print("ERROR: {} is not a valid position to clear wrenches".format(link))

    except rospy.ServiceException:
        print("Service call to /gazebo/apply_body_wrench failed")


##################### main program #####################################
# definitions of variables #
x = 5


# Execution code #
# Wait untill start of simulation
# while rospy.get_time() == 0:
#     continue
# ref_frame = "trolley::support_link" # Set reference frame - comment out to use inertial frame

apply_force(link = "handle", force_x = x)
sleep(5)
clear_forces(link = "handle")
print("Force exertion completed")


# r = rospy.Rate(2) #5 Hz
# while not rospy.is_shutdown():

#     # write a string to the ROS message field
#     apply_force(torque_x=-2*x)
#     sleep(2)
#     apply_force(torque_x=2*x)
#     sleep(2)

#     r.sleep()