#!/usr/bin/env python

# import #
import rospy
from std_msgs.msg import String

text = rospy.get_param('test/text')

# node initialization #
rospy.init_node("pub")

# definitions of variables #
msg_string = String()

# definitions of functions #

# definition of publisher/subscriber and services #
pub = rospy.Publisher('test_topic', String, queue_size=1)

# main program #
r = rospy.Rate(5) #5 Hz
while not rospy.is_shutdown():
    
    # write a string to the ROS message field
    msg_string.data = text
    pub.publish(msg_string)
    r.sleep()





# def my_print(string):    
#     print("print in function: "+string)
    

# if __name__=="__main__":
#     # myargv = rospy.myargv(argv=sys.argv)
#     myargv = sys.argv
#     if len(myargv) < 1:
#         print("Expect 1 input, got {}.".format(len(myargv)))
#     else:
#         print("input: {}".format(myargv))
#         # my_print(myargv[0])