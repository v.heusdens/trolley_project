import yaml
import rospkg
from math import asin

_description_path = rospkg.RosPack().get_path('trolley_description')
_model_params = yaml.safe_load(
    open(_description_path + "/config/model_params.yaml"))

## Parameters ##
L_wh = _model_params['frame_height']
W_f = _model_params['frame_depth']
L_b = _model_params['bag_height']
W_b = _model_params['bag_depth']
L_h = _model_params['handle_height_nom']
R_w = _model_params['wheel_radius']

m_f = _model_params['frame_mass']
m_b = _model_params['bag_mass']
m_w = _model_params['wheel_mass']
m_wm = _model_params['wheel_motor_mass']

g = _model_params['gravity']
mu = _model_params['friction']
damping = _model_params['damping']

## Equations ##
m_ft = m_f + m_b
F_fg = m_ft*g
F_wg = m_w*g
L_wf = 1./m_ft*(m_f/2.*L_wh + m_b/3.*L_b)
W_wf = m_b/m_ft*(W_f/2. + W_b/3.)
L_h_acc = L_h - R_w
beta = asin(L_h_acc/L_wh)
I_w = 0.5*m_w*R_w**2
I = R_w**2*(m_ft + 3*m_w)

## Print statements ##
# print('')
# print("Running load_model_params.py:")
# print(" * Loading model parameters from {}".format(_description_path))
# print(" * Loaded the following parameter values: \n\
#         L_wh, W_f, L_b, W_b, L_h, R_w, m_f, m_b, m_w, m_wm, g, mu, damping")
# print(' * Loaded the following relating parameter values: \n\
#         m_ft, F_fg, F_wg, L_wf, W_wf, beta, I_w, I')
# print('')
