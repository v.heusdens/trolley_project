import sympy as sp

## Parameters ##
F_fg, F_wg, F_hx = sp.symbols\
    ('F_fg F_wg F_hx', real=True)
L_wf, L_wh, L_h, W_wf, R_w = sp.symbols\
    ('L_wf L_wh L_h W_wf R_w', real=True)
L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf = sp.symbols\
    ("L_fc L_b L_bc W_f W_b W_bc W_wf", real=True)
m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols\
    ('m_f m_b m_ft m_w I_f I_w', real=True)
mu, g = sp.symbols\
    ("mu g", real=True)

F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols\
    ('F_wx F_wy T_m F_n F_hy F_stat', real=True)
beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols\
    ('beta d_beta dd_beta dd_theta gamma', real=True)
dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols\
    ('dd_xf dd_yf dd_xw dd_yw', real=True)
C = sp.var('C', real=True)

## Equations ##
T_roll = mu*F_n*R_w

## Print statements ##
# print('')
# print("Running load_model_params_symbolic.py: Loading symbolic model parameters")
# print(" * Loaded the following parameter values: \n\
#         F_fg, F_wg, F_hx, F_wx, F_wy, T_m, F_n, F_hy, F_stat\n \
#         L_wf, L_wh, L_h, W_wf, R_w, L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf \n \
#         m_f, m_b, m_ft, m_w, I_f, I_w, mu, g \n \
#         beta, d_beta, dd_beta, dd_theta, gamma, dd_xf, dd_yf, dd_xw, dd_yw")
# print(' * Loaded the following relating parameter values: \n \
#         T_roll')
# print('')