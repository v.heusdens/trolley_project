<?xml version="1.5"?>
<!-- Trolley model -->
<robot name="trolley"
    xmlns:xacro="http://www.ros.org/wiki/xacro">

    <!-- Load parameters from yaml file -->
    <xacro:property name="yaml_file" value="$(find trolley_description)/config/model_params.yaml" />
    <xacro:property name="params" value="${load_yaml(yaml_file)}" />

    <!-- Constants for robot dimensions -->
    <xacro:property name="frame_height" value="${params['frame_height']}"/>
    <xacro:property name="frame_width" value="${params['frame_width']}"/>
    <xacro:property name="frame_depth" value="${params['frame_depth']}"/>

    <xacro:property name="frame_mass" value="${0.99*params['frame_mass']}"/>
    <xacro:property name="support_mass" value="${0.01*params['frame_mass']}"/>
    <!-- <xacro:property name="handle_mass" value="${0.19*params['frame_mass']}"/> -->

    <xacro:property name="bag_height" value="${params['bag_height']}"/>
    <xacro:property name="bag_width" value="${params['bag_width']}"/>
    <xacro:property name="bag_depth" value="${params['bag_depth']}"/>
    <xacro:property name="bag_mass" value="${params['bag_mass']}"/>
    <xacro:arg name="bag_mass_added" default="0"/>
    <xacro:property name="bag_mass_total" value="${bag_mass + $(arg bag_mass_added)}"/>

    <xacro:property name="wheel_radius" value="${params['wheel_radius']}"/>
    <xacro:property name="wheel_thickness" value="${params['wheel_thickness']}"/>
    <xacro:property name="wheel_separation" value="${frame_width + wheel_thickness}"/>
    <xacro:property name="wheel_mass" value="${0.99*params['wheel_mass_nom']}"/>
    <xacro:property name="support_wheel_mass" value="${0.01*params['wheel_mass_nom']}"/>


    <xacro:property name="handle_height" value="${params['handle_height_nom']}"/>
    <xacro:property name="trolley_angle" value="${acos((handle_height - wheel_radius)/frame_height)}"/>

    <xacro:property name="support_angle" value="${0}"/>
    <xacro:property name="support_radius" value="${wheel_radius}"/>
    <xacro:property name="support_height" value="${(handle_height-support_radius)/cos(support_angle)}"/>


    <!-- Additional properties -->
    <xacro:property name="friction_coefficient" value="${params['friction_nom']}"/>
    <xacro:property name="damping_coefficient" value="${params['damping']}"/>

    <xacro:property name="wheel_friction" value="${friction_coefficient*wheel_radius*9.81*(0.5*(params['frame_mass']+bag_mass_total) + params['wheel_mass_nom'])}"/>
    <xacro:property name="wheel_damping" value="${damping_coefficient*wheel_radius*9.81*(0.5*(params['frame_mass']+bag_mass_total) + params['wheel_mass_nom'])}"/>


    <!-- Import other files -->
    <xacro:include filename="$(find trolley_description)/urdf/basics.urdf.xacro" />
    <xacro:include filename="$(find trolley_description)/urdf/trolley.gazebo" />

    <!-- ####################################################################################### -->
    <!-- Frame -->
    <link name="frame_link">
        <collision>
            <origin xyz="-${frame_height/2} 0 0" rpy="0 0 0"/>
            <geometry>
                <box size="${frame_depth} ${frame_width} ${frame_height}"/>
            </geometry>
        </collision>
        <visual>
            <origin xyz="-${frame_height/2} 0 0" rpy="0 0 0"/>
            <geometry>
                <box size="${frame_height} ${frame_width} ${frame_depth}"/>
            </geometry>
            <material name="silver"/>
        </visual>
        <xacro:box_inertia m="${frame_mass}" w="${frame_width}" d="${frame_height}" h="${frame_depth}" x="-${frame_height/2}"/>
    </link>

    <joint name="frame_joint" type="fixed">
        <parent link="base_footprint"/>
        <child link="frame_link"/>
        <origin xyz="0 0 ${handle_height}" rpy="0 -${PI/2 - trolley_angle} 0"/>
    </joint>

    <!-- Handle -->
    <!-- <link name="handle_link">
        <collision>
            <origin rpy="${PI/2} 0 0"/>
            <geometry>
                <cylinder radius="${frame_depth/2}" length="${frame_width}"/>
            </geometry>
        </collision>
        <visual>
            <origin rpy="${PI/2} 0 0"/>
            <geometry>
                <cylinder radius="${frame_depth/2}" length="${frame_width}"/>
            </geometry>
            <material name="red"/>
        </visual>
        <xacro:cylinder_inertia m="${handle_mass}" r="${frame_depth/2}" t="${frame_width}" roll="${PI/2}"/>
    </link>

    <joint name="handle_joint" type="fixed">
        <parent link="frame_link"/>
        <child link="handle_link"/>
        <origin xyz="0 0 0" rpy="0 ${PI/2 - trolley_angle} 0"/>
    </joint> -->


    <!-- Wheels -->
    <xacro:macro name="wheel" params="prefix reflect">
        <link name="${prefix}_wheel_link">
            <visual>
                <origin rpy="${-PI*0.5} 0 0"/>
                <geometry>
                    <cylinder radius="${wheel_radius}" length="${wheel_thickness}"/>
                </geometry>
                <material name="blue"/>
            </visual>
            <collision>
                <origin rpy="${-PI*0.5} 0 0"/>
                <geometry>
                    <cylinder radius="${wheel_radius}" length="${wheel_thickness}"/>
                </geometry>
            </collision>
            <xacro:cylinder_inertia m="${wheel_mass}" r="${wheel_radius}" t="${wheel_thickness}" roll="${-PI*0.5}"/>
        </link>

        <joint name="${prefix}_wheel_joint" type="continuous">
            <parent link="frame_link"/>
            <child link="${prefix}_wheel_link"/>
            <origin xyz="${-frame_height} ${(frame_width + wheel_thickness)/2*reflect} 0"/>
            <axis xyz="0 1 0"/>
            <dynamics damping="${wheel_damping}" friction="${wheel_friction}"/>
        </joint>

        <transmission name="${prefix}_wheel_trans">
            <type>transmission_interface/SimpleTransmission</type>
            <joint name="${prefix}_wheel_joint">
                <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
            </joint>
            <actuator name="${prefix}_wheel_motor">
                <hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>
            </actuator>
        </transmission>
    </xacro:macro>

    <xacro:wheel prefix="left" reflect="1"/>
    <xacro:wheel prefix="right" reflect="-1"/>


    <!-- Bag -->
    <link name="bag_link">
        <collision>
            <geometry>
                <box size="${bag_height} ${bag_width} ${bag_depth}"/>
            </geometry>
        </collision>
        <visual>
            <geometry>
                <box size="${bag_height} ${bag_width} ${bag_depth}"/>
            </geometry>
            <material name="red"/>
        </visual>
        <xacro:box_inertia m="${bag_mass_total}" w="${bag_width}" d="${2/3*bag_depth}" h="${2/3*bag_height}"/>
    </link>

    <joint name="bag_joint" type="fixed">
        <parent link="frame_link"/>
        <child link="bag_link"/>
        <origin xyz="${-frame_height + bag_height*0.5 + wheel_radius + 0.02} 0 ${(frame_depth + bag_depth)*0.5}"/>
    </joint>


    <!-- SUPPORT -->
    <link name="support_link">
        <collision>
            <origin xyz="0 0 -${support_height/2}" rpy="0 0 0"/>
            <geometry>
                <box size="${frame_depth} ${frame_width} ${support_height}"/>
            </geometry>
        </collision>
        <visual>
            <origin xyz="0 0 -${support_height/2}" rpy="0 0 0"/>
            <geometry>
                <box size="${frame_depth} ${frame_width} ${support_height}"/>
            </geometry>
            <material name="dark"/>
        </visual>
        <xacro:box_inertia m="${support_mass}" w="${frame_width}" d="${frame_depth}" h="${support_height}" z="-${support_height/2}"/>
    </link>

    <joint name="support_joint" type="fixed">
        <parent link="frame_link"/>
        <child link="support_link"/>
        <origin xyz="0 0 0" rpy="0 ${PI/2 - trolley_angle - support_angle} 0"/>
    </joint>


    <!-- SUPPORT WHEELS-->
    <xacro:macro name="support_wheel" params="prefix reflect">
        <link name="${prefix}_support_wheel_link">
            <visual>
                <origin rpy="${-PI*0.5} 0 0"/>
                <geometry>
                    <cylinder radius="${support_radius}" length="${wheel_thickness}"/>
                </geometry>
                <material name="dark"/>
            </visual>
            <collision>
                <origin rpy="${-PI*0.5} 0 0"/>
                <geometry>
                    <cylinder radius="${support_radius}" length="${wheel_thickness}"/>
                </geometry>
            </collision>
            <xacro:cylinder_inertia m="${support_wheel_mass}" r="${support_radius}" t="${wheel_thickness}" roll="${-PI*0.5}"/>
        </link>

        <joint name="${prefix}_support_wheel_joint" type="continuous">
            <parent link="support_link"/>
            <child link="${prefix}_support_wheel_link"/>
            <origin xyz="0 ${(frame_width + wheel_thickness)/2*reflect} -${support_height}"/>
            <axis xyz="0 1 0"/>
        </joint>
    </xacro:macro>

    <xacro:support_wheel prefix="left" reflect="1"/>
    <xacro:support_wheel prefix="right" reflect="-1"/>
</robot>