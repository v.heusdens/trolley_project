#!/usr/bin/env python

# import #
import rospy
from std_msgs.msg import Float64
from geometry_msgs.msg import Vector3, Pose2D, WrenchStamped
from gazebo_msgs.msg import LinkStates
from sensor_msgs.msg import Imu

# node initialization #
rospy.init_node("analysis_remap_msgs_node")

# definitions of variables #
rw = 0.11

pos = Pose2D()
obj="trolley::base_footprint"

"""definitions of functions"""
def callback_force_sensor(msg):
    pub_force_sensor.publish(msg.wrench.force)

def callback_pos(msg):
    global pos
    i = msg.name.index(obj)

    pos.x = msg.pose[i].position.x
    pos.y = msg.pose[i].position.z
    pub_pos.publish(pos)

# def callback_imuros(msg):
#     pub_acc.publish(msg.linear_acceleration.x)

# def callback_vel(msg):
#     global msg_vel
#     i = msg.name.index("trolley::left_wheel_link")

#     msg_vel = msg.twist[i].angular.y*rw
#     pub_vel.publish(msg_vel)


# definition of publisher/subscriber and services #
pub_force_sensor = rospy.Publisher('/trolley/analysis/force_sensor', Vector3, queue_size=1)
pub_pos = rospy.Publisher('/trolley/analysis/pos', Pose2D, queue_size=1)
# pub_force = rospy.Publisher('/trolley/analysis/force', Float64, queue_size=1)
# pub_vel = rospy.Publisher('/trolley/analysis/ang_vel', Float64, queue_size=1)
# pub_acc = rospy.Publisher('/trolley/analysis/lin_acc', Float64, queue_size=1)

rospy.Subscriber("trolley/force_sensor", WrenchStamped, callback_force_sensor)
rospy.Subscriber("gazebo/link_states", LinkStates, callback_pos)
# rospy.Subscriber("gazebo/link_states", LinkStates, callback_vel)
# rospy.Subscriber("gazebo/link_states", LinkStates, callback_vel2)
# rospy.Subscriber("/trolley/imu", Imu, callback_imuros)



while not rospy.is_shutdown():
    rospy.spin()