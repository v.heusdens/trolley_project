#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
from gazebo_msgs.msg import LinkStates

rospy.init_node("track_velocity_node")
Rw = 0.11  # [m]

def callback(msg):
    """
    Obtain angular wheel velocity from Gazebo node
    """

    # Obtain index of target link
    i_l = msg.name.index('trolley::left_wheel_link')
    i_r = msg.name.index('trolley::right_wheel_link')

    # Update velocity measurement
    dtheta_l = msg.twist[i_l].angular.y
    dtheta_r = msg.twist[i_r].angular.y
    dtheta = (dtheta_l + dtheta_r)/2

    # Publish
    pub_ang.publish(dtheta)
    pub_lin.publish(dtheta*Rw)

    return

pub_ang = rospy.Publisher('trolley/analysis/ang_vel', Float64, queue_size=1)
pub_lin = rospy.Publisher('trolley/analysis/lin_vel', Float64, queue_size=1)
rospy.Subscriber("gazebo/link_states", LinkStates, callback)


while not rospy.is_shutdown():
    rospy.spin()