#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
from gazebo_msgs.msg import LinkStates
from math import sqrt

rospy.init_node("track_distance_node")

obj="trolley::base_footprint"

distance = 0
pos_x = 0
pos_z = 0

test = LinkStates()

def callback(msg):
    global distance, pos_x, pos_z
    
    i = msg.name.index(obj)
    pos_x_new = msg.pose[i].position.x
    pos_z_new = msg.pose[i].position.z
    
    delta_x = pos_x_new - pos_x
    delta_z = pos_z_new - pos_z
    distance += sqrt(delta_x**2 + delta_z**2)
    
    pos_x = pos_x_new
    pos_z = pos_z_new

    pub.publish(distance)


pub = rospy.Publisher('trolley/analysis/traveled_dist', Float64, queue_size=1)
rospy.Subscriber("gazebo/link_states", LinkStates, callback)


while not rospy.is_shutdown():
    rospy.spin()