#!/usr/bin/env python

import rospy
import numpy as np
from geometry_msgs.msg import WrenchStamped
from trolley_msgs.msg import Vector2

rospy.init_node("filter_force_sensor_data_node")

f_filt = Vector2()
sensor_data_filter_x = []  # * Memory for sensor filtering
sensor_data_filter_z = []  # * Memory for sensor filtering
tw_filt_sensor = 5


def callback(msg):
    global sensor_data_filter_x, sensor_data_filter_z, f_filt
    
    #* Update filter memory
    sensor_data_filter_x.append(msg.wrench.force.x)
    sensor_data_filter_z.append(msg.wrench.force.z)

    if len(sensor_data_filter_x) > tw_filt_sensor:
        sensor_data_filter_x.pop(0)
        sensor_data_filter_z.pop(0)

    #* Apply filter
    fx_filt = np.median(sensor_data_filter_x)
    fz_filt = np.median(sensor_data_filter_z)
    
    #* Publish
    f_filt.x = fx_filt
    f_filt.z = fz_filt

    pub.publish(f_filt)


pub = rospy.Publisher('trolley/analysis/force_sensor_filtered', Vector2, queue_size=1)
rospy.Subscriber("trolley/force_sensor", WrenchStamped, callback)


while not rospy.is_shutdown():
    rospy.spin()