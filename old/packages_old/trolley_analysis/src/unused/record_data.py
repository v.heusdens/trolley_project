#!/usr/bin/env python

import rospy
import rosbag
import rospkg
import yaml
from datetime import datetime

from std_msgs.msg import Float64


"""INITIALIZATION"""
print("Initializing 'record_data'...")

# Try to get filename from parameter server, otherwise set default
sim_scen = rospy.get_param('/sim_scen', 'undefined')
print("simulation scenario: {}".format(sim_scen))

# Settings
rospath = rospkg.RosPack().get_path('trolley_analysis')
config = yaml.safe_load(open(rospath + "/config/general.yml"))

# Obtain all topic names
topics = rospy.get_published_topics()


# Open file to write to
now = datetime.now()
now_ = now.strftime("%Y-%m-%d-%H-%M-%S")
bag = rosbag.Bag(rospath + '/../../sim_data/raw/' + sim_scen + "_" + now_ + '.bag', 'w')


try:
    f = Float64()
    f.data = 1
    bag.write('testdata', f)
finally:
    bag.close
