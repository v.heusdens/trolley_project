#!/usr/bin/env python

import rospy
from gazebo_msgs.msg import LinkStates
from std_msgs.msg import Float64

# node initialization #
rospy.init_node("map")

# definitions of variables #
mapped = Float64()

test = LinkStates()

# definitions of functions #
def callback(msg):
    i = msg.name.index(obj)
    
    mapped.data = msg.twist[i].angular.x
    # mapped.data = msg.twist[i].linear.y
    
    pub.publish(mapped)

    # print("velocity of {}: {}".format(obj, mapped.data))

# definition of publisher/subscriber and services #
rospy.Subscriber('gazebo/link_states', LinkStates, callback)
pub = rospy.Publisher('trolley/debug', Float64, queue_size=1)

# main program #
obj = 'trolley::left_wheel_link'

while not rospy.is_shutdown():
    rospy.spin()