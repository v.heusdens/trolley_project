#!/usr/bin/env python
from math import pi
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

# Make data.
X = np.arange(0.1, 30, 0.1) #m
Y = np.arange(0.1, 10, 0.1) #c
X, Y = np.meshgrid(X, Y)
Z = 1 - np.arctan(X/Y)
Z = Z / (2 * pi) * 360

# PLOT #
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# surf = ax.plot_surface(X,
#                        Y,
#                        Z,
#                        cmap=cm.coolwarm,
#                        linewidth=0,
#                        antialiased=False)

fig2 = plt.figure()
ax2 = fig2.add_subplot(111, projection='3d')
surf2 = ax2.plot_wireframe(X,
                         Y,
                         Z)

plt.xlabel('m')
plt.ylabel('c')

plt.show()