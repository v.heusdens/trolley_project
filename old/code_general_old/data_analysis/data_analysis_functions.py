#!/usr/bin/env python

import rospkg
import rosbag
import yaml
import os
import tqdm
import pandas as pd
from datetime import datetime
import copy
from numbers import Number

#@ GENERAL

def get_simdata_filepath():
    filepath = rospkg.RosPack().get_path('trolley_analysis') + "/sim_data/"
    return filepath


def get_topics(columns, include=None, exclude=None):

    if include is not None and len(include) != 0:
        columns_conc = []
        for incl in include:
            for column in columns:
                if incl in column and column not in columns_conc:
                    columns_conc.append(column)

        columns = columns_conc

    if exclude is not None and len(exclude) != 0:
        columns_conc = list(columns)
        for excl in exclude:
            for column in columns:
                if excl in column and column in columns_conc:
                    columns_conc.remove(column)

        columns = columns_conc

    return columns


def get_bagfilenames(filepath, amount):
    """
    Obtains specified filenames of bagfiles without extension from given location.
    filepath:   Path of folder in which to look
    amount:     Last 'amount' of saved datafiles are used. set 'None' to use all files.
    """
    #@ Get filenames
    bagfiles = [x for x in os.listdir(filepath) if x[-3:] == "bag"]
    bagfiles = sorted(bagfiles, key=get_timestamp)

    if amount == "" or amount is None or amount == 0:  #@ Get all available filenames
        filenames = bagfiles
    else:  #@ Get last 'amount' of bagfiles
        filenames = bagfiles[-amount:]

    for i in range(len(filenames)):  #@ Remove extension from names
        filenames[i] = filenames[i].split(".")[0]

    return filenames


def get_sim_types(filenames):
    """Get simulation-type part of filename"""
    if not isinstance(filenames, list):
        filenames = [filenames]

    sim_types = []
    for filename in filenames:
        parts = filename.split("_")
        parts.pop(-1)
        temp = "_"
        sim_types.append(temp.join(parts))

    return sim_types


def get_timestamp(filename):
    """Get timestamp part of filename"""
    format = '%Y-%m-%d-%H-%M-%S'
    date = filename.split("_")[-1].split('.')[0]
    return datetime.strptime(date, format)


def concat_name(name, ext):
    name += "." + ext
    return name


def check_existance(fn, path):
    for loc in ["raw/", "processed/"]:
        for ext in [".bag", ".csv"]:
            if os.path.isfile(path + loc + fn + ext):
                return True
    return False


#@ IMPORT AND SAVE

def rosbag2pandas(fn, path):
    """
    Takes rosbag, transform it to pandas Dataframe
    """
    #@ Extract info from bag file
    print("Extracting bag file...")
    bag = rosbag.Bag(path + "raw/" + fn + ".bag")
    msg_amount = bag.get_message_count()

    msgs = []
    for topic, msg, t in tqdm.tqdm(bag.read_messages(), total=msg_amount):
        msgs.append(flatten_rosmsg(msg, name=topic, time=t))

    bag.close()
    print("Done extracting bag file.")

    #@ Transform msgs to DataFrame
    print("Transforming to DataFrame...")
    df = pd.DataFrame.from_dict(msgs)
    print("Done transforming to DataFrame.")
    return df


def load_pandas(fn, path, loc="raw"):
    """
    Loads a pandas datafile from a csv file in the spefied location
    """

    total_path = path + loc + "/" + fn + ".csv"
    df = pd.read_csv(total_path)
    print("file loaded from {}".format(total_path))
    return df


def save_pandas(df, fn, path, loc="raw"):
    """
    Saves a pandas dataframe to a csv file in the specified location
    loc can be 'raw' or 'processed'
    """

    #@ Check if correct location is given
    if loc != "raw" and loc != "processed":
        print("ERROR: wrong location is given")
        return

    #@ Save
    total_path = path + loc + "/" + fn + ".csv"
    df.to_csv(total_path, index=False)
    print("file saved as {} in {}".format(fn + ".csv", path + loc + "/"))


def load(fn, path, loc="raw"):
    """
    Loads file given by 'fn' from location specified by 'loc'.
    If no CSV of the bagfile is present, it will be made
    and saved in the 'raw' folder. If it does exist this will be loaded.

    """
    print("Looking for csv file in '{}' folder:".format(loc))
    
    #@ No csv has been made yet --> Make one
    if loc == "raw" and not os.path.isfile(path + "raw/" + fn + ".csv"):
        print("No csv file has been found in 'raw' folder. Making one.")
        df = rosbag2pandas(fn, path)
        save_pandas(df, fn, path)
    else:  #@ A csv file already exists --> Load existing file
        print("csv file has been found in 'raw' folder. Loading.")
        df = load_pandas(fn, path, loc)

    return df


def load_trolley_data(fns, path, include=None, exclude=None, trim=None, fillna=True, new=False, test=False):
    """
    Loads and processes trolley data. 
    Automatically makes raw and processed csv if needed and loads most relevant one.
    fns:        Filenames (without file extension) to load
    path:       Path to data (without "raw" or "processed")
    include:    (parts of) Topics to include in df when data needs to be processed
    exclude:    (parts of) Topics to exclude in df when data needs to be processed 
    trim:       Time from which df will be trimmed when data needs to be processed 
    new:        Specify if dataframe needs to be processed by default (normally this step is skipped when a processed file is found)
    test:       Specifies if the used topics are stated and confirmation is asked before proceeding when data needs to be processed
    """
    print("\n-- LOAD TROLLEY DATA --\n")
    
    #@ Set basic excludes
    if exclude is None:
        exclude = ["clock", "ground_plane", "parameter_descriptions", "parameter_updates",
                "rosout", "header", "covariance", "support", "transforms", "orientation"]

    #@ Make list of fn if not already
    if not isinstance(fns, list):
        fns = [fns]

    #@ Load data
    print("Files to load:")
    for i, fn in enumerate(fns):
        print("  {} - '{}'".format(i, fn))

    dfs = []
    for i, fn in enumerate(fns):
        print("\nLoading {} - '{}':".format(i, fn))
        if not check_existance(fn, path):
            print("File '{}' does not exists. Skipping.".format(fn))
            continue
        if not fillna: #@ Set prefix to use df's without removed nans
            fn_ = "-nonan_" + fn
        else: fn_ = fn
        #@ A processed file is already made and no need to make a new one
        if os.path.isfile(path + "processed/" + fn_ + ".csv") and new is False:
            print("Processed file has been found.")
            df = load(fn_, path, loc="processed")
        else:  #@ New processed file has to be made
            if new is True:
                print("Make new file command given.")
            else:
                print("No processed file has been found.")

            df = load(fn, path, loc="raw")

            if test:  #@ Check which topics would be need to be taken into account
                topics = get_topics(df.columns, include, exclude)
                print("Topics are:")
                for topic in topics:
                    print(" - {}".format(topic))

                trigger = False
                while not trigger:
                    answer = raw_input(
                        "Would you like continue to process and save the data using these topics? (y/n)")
                    if answer == "y":
                        trigger = True
                    elif answer == "n":
                        print("Data not processed. Abort.")
                        return []
                    else:
                        print("Please answer 'y' or 'n'.")

            #@ Process and save data
            df = process_data(df, trim=trim,
                              include=include, exclude=exclude, fillna=fillna)

            save_pandas(df, fn=fn_, path=path, loc="processed")

        dfs.append(df)

    print("\n-- LOAD TROLLEY DATA COMPLETE --\n")
    return dfs


#@ PROCESS

def flatten_rosmsg(msg, elements=None, name="", time=None):
    """
    Flattens rosmsg to 1-layer dict
    """
    msg_flat = {}

    if time is not None:  #@ Add time element
        msg_flat["time"] = time.to_sec()

    if isinstance(msg, (int, str, Number, bool)):  #@ End of tree is reached (leafnode)
        msg_flat[name] = msg

        return msg_flat

    if isinstance(msg, (list, tuple)):  #@ msg is list
        for i, slot in enumerate(msg):
            if not elements == None:
                ext = elements[i]
            else:
                ext = str(i)
            msg_flat.update(
                flatten_rosmsg(slot, elements=elements,
                               name=concat_name(name, ext))
            )
        return msg_flat

    else:  #@ msg is class
        slots = list(msg.__slots__)

        #@ Check element names (if exist)
        if "name" in slots and isinstance(msg.name, list):
            elements = msg.name
            slots.remove("name")

        for slot in slots:
            msg_flat.update(
                flatten_rosmsg(
                    getattr(msg, slot), elements=elements, name=concat_name(name, slot)
                )
            )

        return msg_flat


def process_data(df, trim=None, include=None, exclude=None, fillna=True):
    """
    Obtains relevant columns, clean up and trim data.
    df = dataframes to process
    trim = time untill which data is kept, rest is trimmed off
    include = list of (parts of) column names to include
    exclude = list of (parts of) column names to exclude
    """
    print("\nProcessing data...")

    #@ Make sure 'include'/'exclude' is a list
    if isinstance(include, str):
        include = [include]
    if isinstance(exclude, str):
        exclude = [exclude]

    #@ Make list with columns to keep
    columns = df.columns.values.tolist()
    columns = (get_topics(columns, include, exclude))

    #@ Obtain DataFrame with appropriate columns
    df = df[columns]
    # print("DataFrame made with columns: \n{}".format(columns))

    if trim is not None:
        #@ Remove rows after trim time
        df = df[df.time <= trim]

    #@ Remove rows without data
    remove = df.drop(columns="time").isna().all(axis=1)
    df = df[~remove]

    #@ Fill NaN values
    if fillna:
        topic_int = []
        if "/trolley/analysis/traveled_dist.data" in df.columns:
            topic_int.append("/trolley/analysis/traveled_dist.data")
        if "/gazebo/link_states.pose.trolley::base_footprint.position.x" in df.columns:
            topic_int.append(
                "/gazebo/link_states.pose.trolley::base_footprint.position.x")

        for k in topic_int:
            #@ Interpolate position data
            df[k].interpolate(axis=0, method='linear', inplace=True)

        #@ Fill using zero-order-hold
        df.fillna(axis="index", method="ffill", inplace=True)
        df.fillna(0, inplace=True)

    return df
