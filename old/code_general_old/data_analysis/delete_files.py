import os
from pathlib import Path

import rospkg

import data_analysis_functions as daf

folder = Path(daf.get_simdata_filepath())
y
######## FILL IN ########
# include = daf.get_bagfilenames(folder + "raw", 2)
include = ["test_force_sensor_simple_2020-08-12-10-03-59"]
exclude = ""
#########################


## Checks
if len(include) == 0:
    raise Exception("No input given. Abort mission!")

if not isinstance(include, list) and not isinstance(include, tuple):
    include = [include]

## Program
print("\nSTART DELETE FILES PROGRAM\n")


filepaths = []

for filename in include: #@ Check which files to remove
    temp = [path for path in folder.glob('**/*'+filename+'*') if ('~' not in path.stem and (exclude == "" or exclude not in path.stem))]
    if len(temp) == 0:
        print("No removable files found containing {}.".format(filename))
    else:
        filepaths.extend(temp)

if len(filepaths) == 0:
    print("No files to delete!")
else:
    print("Are you sure you want to delete the following files?")
    for filepath in filepaths:
        print(filepath)

    while True:
        answer = raw_input("y/n")
        if answer == "y":
            for file in filepaths:
                os.remove(str(file))
                print("removed {} from {}".format(file.name, file.parent))
            break
        elif answer == "n":
            print("No files removed.")
            break
        else:
            print("Wrong input. Please enter 'y' or 'n'.")

print("\nEND DELETE FILES PROGRAM\n")
