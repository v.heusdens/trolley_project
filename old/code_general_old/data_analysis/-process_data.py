import data_analysis_functions as daf
import data_plot_functions as dpf

#@ LOAD DATAFRAMES
#@ Select which dataframes to load
filepath = daf.get_simdata_filepath()
folder = "raw"

# filenames = daf.get_bagfilenames(filepath + folder, 1)
filenames = ["~test_IMU_filters_2020-07-20-23-12-50"]
# filenames.append("test_weight_2020-07-17-17-50-14")

#@ Select settings to load them with
fillna = True
include = None
exclude = None
dfs = daf.load_trolley_data(
    filenames, filepath, include, exclude, new=False, test=False, fillna=fillna)
