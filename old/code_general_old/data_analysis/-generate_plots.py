import data_analysis_functions as daf
import data_plot_functions as dpf

#@ LOAD DATAFRAMES
#@ Select which dataframes to load
filepath = daf.get_simdata_filepath()
folder = "raw"

# filenames = daf.get_bagfilenames(filepath + folder, 1)
filenames = ["~test_force_sensor_raw_2020-08-12-21-29-41"]
# filenames.append("test_kp_2020-07-20-23-10-28")
# filenames.append("test_imu_baseline_2020-07-20-22-18-09")
#@ Select settings to load them with
fillna = True
include = None
exclude = None

dfs = daf.load_trolley_data(
    filenames, filepath, include, exclude, fillna=fillna, new=False, test=False)

#@ PLOT FREQUENCY INFO
# df = dfs[0]
# dpf.plot_freq_stem(df, "/trolley/analysis/lin_acc_raw.data")


#@ PLOT 
title = ""
x_axis = "time"
x_label = "Time [s]"
# labels = ["Input force - x", "Force measurement - x", "Force measurement - z"]
labels=None
cutoff = 0
include = []
# include = ["input_force.x", "sensor.wrench.force", "lin_acc", "lin_vel"]
include = ["input_force.x", "sensor.wrench.force"]
exclude = ".y"
subtitles = ""
# subtitles="Force-measurements on handle of simplified trolley"
slope = False
fn = ""
# fn = "Force-measurements on handle of simplified trolley"
ext= [".pdf"]

#@ PLOT DATAFRAMES
# title = daf.get_sim_types(filenames)[0]
# title = "Effect of different simulation adjustments on IMU-data noise"
# subtitles = filenames
# x_axis = "time"
# # include = ["force.x", "test"]
# include = ["lin_acc"]
# exclude = None
# topics = None
# labels = None
# cutoff =0
# fn = ""
# # fn = daf.get_sim_types(filenames)[0]
# # fn = "Simulation_adjustments_for_imu_noise_reduction"
# ext= [".pdf"]

dpf.plot_data_per_df(dfs, include, exclude, labels=labels, subtitles=subtitles, x_axis=x_axis, title=title, cutoff=cutoff, fn=fn, ext=ext, slope=slope)


#@ PLOT TOPICS
# title = "No disturbance - no control vs control"
# x_axis = "time"
# x_label = "Time [s]"
# labels = ["no control", "control"]
# slope = False

# title = "Control - no disturbance vs slope"
# x_axis = ""
# x_label = "Distance [m]"
# labels = ["no disturbance", "slope"]
# slope = True

# title = "Control - no disturbance vs added mass"
# x_axis = "time"
# x_label = "Time [s]"
# labels = ["no disturbance", "20 kg added mass"]
# slope = False

# cutoff = 0
# include = ["analysis/force.", "motor_cmd", "lin_acc.", "lin_vel"]
# subtitles=["Input force [N]", "Motor torque [Nm]", "Acceleration [m/s^2]", "Velocity [m/s]"]
# fn = title
# ext= [".png", ".pdf"]

# dpf.plot_data_per_topic(dfs, include, exclude, labels=labels, subtitles=subtitles,
#                         x_axis=x_axis, x_label=x_label, title=title, slope=slope, cutoff=cutoff, fn=fn, ext=ext)
