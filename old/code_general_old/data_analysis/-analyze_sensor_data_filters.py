import data_analysis_functions as daf
import data_plot_functions as dpf
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
import pandas as pd
from scipy import signal
import rospkg

#@ LOAD DATAFRAMES #
#@ Select which dataframes to load
filepath = daf.get_simdata_filepath()
folder = "raw"

# filenames = daf.get_bagfilenames(filepath + folder, 1)
# filenames = "~test_IMU_filter_controller_2020-07-21-17-06-51"
# filenames = "~test_IMU_noise_smooth_2020-07-21-12-39-58"
filenames = "~test_IMU_noise_2020-07-21-12-37-41"

#@ Select settings to load them with
fillna = False
include = None
exclude = None
dfs = daf.load_trolley_data(
    filenames, filepath, include, exclude, new=False, test=False, fillna=fillna)
df = dfs[0]


#@ SETTINGS #
f_topic = "/trolley/analysis/lin_acc_raw.data"
window_size = 5

# #@ Raw data
data = df[["time", f_topic]].dropna()
d_raw = data[f_topic]
d_raw.name = "Raw IMU data"
time = data["time"]
dt = time.diff().mean()
freq = 1/dt

#@ FILTER AND PLOT DATA #
fig, ax = plt.subplots()
ax.plot(time, d_raw, "--")

# #@ Mean filter
d_mean = d_raw.rolling(window_size).mean()
d_mean.name = "IMU mean"

# ax.plot(time, d_mean)

# #@ Median filter
for window_size in [5]:
    d_median = d_raw.rolling(window_size).median()
    d_median.name = "IMU median, window-size: {}".format(window_size)
    ax.plot(time, d_median)

#@ Butterworth filter
# bw = signal.butter(window_size, freq_cutoff, fs=freq, output='sos')
# d_butter = pd.Series(signal.sosfilt(bw, d_raw))

# d_butter.name="IMU Butterworth, cutoff frequency: {}".format(freq_cutoff)

for order in [3]:
    for cf in [5]:
        bw = signal.butter(order, cf, fs=freq, output='sos')
        d_butter = pd.Series(signal.sosfilt(bw, d_raw))
        d_butter.name = "IMU Butterworth, order: {}, cutoff frequency: {}".format(order, cf)
        ax.plot(time, d_butter)


#@ Plt formatting
ax.set_title("Uncontrolled trolley encountering slope\nIMU data raw vs filtered")

ax.set_xlim(1, 8)
ax.set_xlabel("time [s]")
ax.set_ylabel("acceleration [m/s^2]")

ax.xaxis.set_major_locator(plticker.MultipleLocator(0.5))
ax.xaxis.set_minor_locator(plticker.MultipleLocator(0.1))
plt.grid(which="both", axis="x", linestyle="--")
plt.grid(which="major", axis="y", linestyle="--")
plt.legend()

mng = plt.get_current_fig_manager()
dim = mng.window.maxsize()
scale = 0.5
fig.set_figheight(dim[0]/100*scale)
fig.set_figwidth(dim[1]/100)


#@ Save plot
fn = "IMU_data_filters"
fmt = [".pdf"]
path = rospkg.RosPack().get_path('trolley_analysis') + "/sim_graphs/"
for format in fmt:
    plt.savefig(path + fn + format)
    print("Plot saved to /sim_graphs/{}".format(fn + format))

plt.show()
    
# #@ BODE PLOTS
# b, a = signal.butter(window_size, freq_cutoff, fs=freq, output='ba')
# b_a, a_a = signal.butter(window_size, freq_cutoff, analog=True, output='ba')
# w2, h2 = signal.sosfreqz(bw, fs=freq)

# w, mag, phase = signal.dbode((b, a, dt))
# w_a, mag_a, phase_a = signal.bode((b_a, a_a))

# fig, [ax1, ax2] = plt.subplots(2,1, sharex=True)

# ax1.set_title("Magnitude")
# ax1.semilogx(w2, 20 * np.log10(abs(h2)), 'g', label="digital verified")
# ax1.semilogx(w/(2*np.pi), mag, 'b', label="digital")
# ax1.semilogx(w_a, mag_a, 'r', label="analog")
# ax1.set_ylabel('Amplitude [dB]', color='b')
# ax1.grid(which="both")
# ax1.legend()

# ax2.set_title("Phase")
# ax2.semilogx(w/(2*np.pi), phase, 'b')
# ax2.semilogx(w_a, phase_a, 'r')
# ax2.set_ylabel('Phase [deg]', color='b')
# ax2.set_xlabel('Frequency [Hz]')
# ax2.grid(which="both")
# ax2.yaxis.set_major_locator(plticker.MultipleLocator(45))

# plt.suptitle('Digital filter frequency response')


# plt.show()
