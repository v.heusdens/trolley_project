#!/usr/bin/env python
import os
from copy import copy
from math import ceil, floor
from matplotlib.backends.backend_pdf import PdfPages

import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
import pandas as pd
import rospkg
from scipy import signal
import sympy as sp

from data_analysis_functions import get_topics

slope_begin = 4 #@ [m] Can be set from sim.launch or any launch file that uses it. Default = 10 m
slope_end = 12 + slope_begin  # [m] Slope is ... m long

def save_plot(fig, name, path=None):
    if path is None:
        path = '../../images/plots'
    with PdfPages(os.path.join(path, name + '.pdf')) as pdf:
        pdf.savefig(fig)


def extract_topicname(topic):
    name = topic.split('/')[-1]
    name = name.replace(".data", "")
    return name


def plot_data_per_topic(dfs, include=None, exclude=None, x_axis=None, x_label=None, y_labels=None, cutoff=None, title="", subtitles=[], labels=None, show=True, fn=None, ext=None, slope=False):
    """
    plots data from dataframe. 
    Plots every topic in a subplot, uses same x-axis.

    df:         Dataframe to be plotted
    include:    (List of) (keywords of) topics from dataframe that will be plotted.
    exclude:    (List of) (keywords of) topics from dataframe that will not be plotted.
    x_axis:     Which data to take as measure for x-axis
    x_label:    Label for x-axis
    cutoff:     Sets a range to x-axis data
    title:      Title of complete plot
    subtitles:  Subtitles for subplots
    labels:     List of custom labels belonging to plotted topics
    show:       Whether plot is shown or not
    fn:         Filename for plot to be saved to. None means not save
    ext:        Give (one or multiple) extensions for save files
    """
    print("\n-- PLOTTING DATA --\n")


    #@ Make sure settings are list
    if len(dfs) == 0:
        print("0 dataframes received. Shutting down.")
        return
    if not isinstance(dfs, list):
        dfs = [dfs]
    if y_labels is not None and not isinstance(y_labels, list):
        y_labels = [y_labels]
    if labels is not None and not isinstance(labels, list):
        labels = [labels]

    print("{} Dataframes received.".format(len(dfs)))
    
    if include is not None and len(include) != 0 and not isinstance(include, list):
        include = [include]
    if exclude is not None and len(exclude) != 0 and not isinstance(exclude, list):
        exclude = [exclude]


    #@ Determine x-axis data
    if x_axis is None or x_axis == "":  # Set standard x-axis data if none given
        x_axis = "/trolley/analysis/traveled_dist.data"
        print("No x-axis topic specified. Default to {}".format(x_axis))

    if x_label is None: # Check if label needs to be made
        x_label = extract_topicname(x_axis)


    #@ Set xlim to cutoff value
    x_len = 0
    for df in dfs:
        x_max = df[x_axis].max()
        if x_max > x_len:
            x_len = x_max

    if cutoff is not None and cutoff is not 0 and cutoff < x_len:
        x_len = cutoff
        i_cutoff = dfs[0][dfs[0][x_axis] > cutoff].index[0]
    else: 
        i_cutoff = -1


    #@ Determine topics to use
    topics = []
    for df in dfs[::-1]:
        topics_ = get_topics(df.columns, include, exclude)
        topics_ = [t for t in topics_ if not t in topics]
        topics.extend(topics_)

    if x_axis in topics:
        topics.remove(x_axis)


    #@ Determine subplot titles
    if subtitles is None or len(subtitles) == 0:  #@ Make from topics
        subtitles = [extract_topicname(topic) for topic in topics]
    elif not isinstance(subtitles, list):  #@ Make list of subtitles if not already
        subtitles = [subtitles]

    if len(subtitles) < len(topics):  #@ Make subtitles list match with amount of plots
        for i in range(len(topics) - len(subtitles)):
            subtitles.append("")


    #@ Obtain additional data for slope plots
    x_begin = None
    x_end = None

    if slope or "slope" in subtitles: 
        #@ Find index of start-and end of slope for plotting
        # pos_topic = "/gazebo/link_states.pose.trolley::left_wheel_link.position.x"
        pos_topic = "/gazebo/link_states.pose.trolley::base_footprint.position.x"
        
        if x_len > slope_begin:
            i_begin = dfs[0][dfs[0][pos_topic] > slope_begin].index[0]
            x_begin = dfs[0][x_axis][i_begin]
        if x_len > slope_end:
            i_end = dfs[0][dfs[0][pos_topic] > slope_end].index[0]
            x_end = dfs[0][x_axis][i_end]


    #@ Start plotting
    fig, axes = plt.subplots(nrows=len(topics), sharex=True, squeeze=False)
    
    if title is not None: # Set title
        fig.suptitle(title, fontsize='x-large')

    for i, topic in enumerate(topics):  #@ Plot every topic separately
        print("Making plot for topic {}...".format(topic))

        for j, df in enumerate(dfs):
            if topic in df.columns:
                if labels is None or len(labels) == 0: #@ No labels
                    df.plot(ax=axes[i, 0], x=x_axis, y=topic)
                elif topic == "/trolley/analysis/force.data":
                    df.plot(ax=axes[i, 0], x=x_axis, y=topic, color='g', legend=False)
                else:
                    df.plot(ax=axes[i, 0], x=x_axis, y=topic, label=labels[j])

        if x_begin is not None:
            axes[i, 0].axvline(x_begin, ls=":", c='r', label="start of slope")
        if x_end is not None:
            axes[i, 0].axvline(x_end, ls=":", c='b', label="end of slope")

        axes[i, 0].set_title(subtitles[i])
        axes[i, 0].grid(which="major")
        axes[i, 0].grid(which="minor", linestyle=":")
        axes[i, 0].minorticks_on()
        if not topic == "/trolley/analysis/force.data":
            axes[i, 0].legend(loc='upper right')
        if y_labels is not None and len(y_labels) > 0:
            axes[i, 0].set_ylabel(y_labels[i])


    #@ Set limits
    axes[0, 0].set_xlim(0, x_len)
    axes[0, 0].set_xlabel(x_label)


    #@ Maximize plot
    mng = plt.get_current_fig_manager()
    dim = mng.window.maxsize()
    scale = 0.5
    fig.set_figheight(dim[0]/100*scale)
    fig.set_figwidth(dim[1]/100*scale)


    #@ Save plot
    if fn is not None and fn != "":  #@ Save plot if filename is given
        if ext is None:
            fmt = ".pdf"
        else:
            fmt = ext
        if not isinstance(fmt, list):
            fmt = [fmt]
        path = rospkg.RosPack().get_path('trolley_analysis') + "/sim_graphs/"

        for format in fmt:
            plt.savefig(path + fn + format)
            print("Plot saved to /sim_graphs/{}".format(fn + format))


    #@ Show plot
    if show is True:
        plt.show()

    print("\n-- PLOTTING DATA COMPLETE --\n")


def plot_data_per_df(dfs, include=None, exclude=None, x_axis=None, x_label=None, cutoff=None, title="", subtitles=[], labels=None, show=True, fn=None, ext=None, slope=False):
    """
    plots data from dataframe. 
    Plots every dataframe in a subplot, uses same x-axis.

    dfs:        (List of) dataframes to be plotted
    include:    (List of) (keywords of) topics from dataframes that will be plotted. Topics do not have to be included in all dataframes.
    exclude:    (List of) (keywords of) topics from dataframes that will not be plotted. Topics do not have to be included in all dataframes.
    x_axis:     Which data to take as measure for x-axis
    x_label:    Label for x-axis
    cutoff:     Sets a range to x-axis data
    title:      Title of complete plot
    subtitles:  Subtitles for subplots
    labels:     List of custom labels belonging to plotted topics
    show:       Whether plot is shown or not
    fn:         Filename for plot to be saved to. None means not save
    ext:        Give (one or multiple) extensions for save files
    """
    print("\n-- PLOTTING DATA --\n")

    #@ Prep
    if len(dfs) == 0:
        print("0 dataframes received. Shutting down.")
        return
    if not isinstance(dfs, list):  #@ Make list of dfs if not already
        dfs = [dfs]

    print("{} Dataframes received.".format(len(dfs)))

    if include is None or len(include) == 0:  #@ Use all topics available
        print("No topics specified. Using all topics available.")
    elif not isinstance(include, list):
        include = [include]

    if exclude is not None and len(exclude) != 0 and not isinstance(exclude, list):
        exclude = [exclude]

    #@ Determine x-axis data
    if x_axis is None or x_axis == "":  #@ Set standard x-axis data if none given
        x_axis = "/trolley/analysis/traveled_dist.data"
        x_label = "traveled distance [m]"
        print("No x-axis topic specified. Default to {}".format(x_axis))

    if x_label is None:
        x_label = extract_topicname(x_axis)

    #@ Determine limits
    if cutoff is not None and cutoff is not 0:
        x_len = cutoff
    else:  #@ Set xlim to fit all data
        x_len = 0
        for df in dfs:
            max_val = df[x_axis].max()
            if max_val > x_len:
                x_len = max_val

    #@ Determine titles for subplots
    if not isinstance(subtitles, list):  #@ Make list of subtitles if not already
        subtitles = [subtitles]

    if len(subtitles) < len(dfs):  #@ Make subtitles list match with amount of plots
        for i in range(len(dfs) - len(subtitles)):
            subtitles.append("")

    max_val_y = []
    min_val_y = []

    #@ Start plotting
    fig, axes = plt.subplots(nrows=len(dfs), sharex=True,
                             sharey=True, squeeze=False)
    fig.suptitle(title)

    for i, df in enumerate(dfs):  #@ Plot every dataframe separately
        print("\nMaking plot for dataframe {}...".format(i))

        #@ Obtain topics and corresponding labels
        topics = get_topics(df.columns, include, exclude)
        if x_axis in topics:
            topics.remove(x_axis)

        if len(topics) == 0:  #@ Warn if no topics are left to be plotted
            print("ERROR: no topics left to print for dataframe {}. Abort.".format(i))
            return
        
        if labels is None:
            labels_ = [extract_topicname(topic) for topic in topics]
        else:
            labels_ = copy(labels)
            if len(topics) > len(labels):  #@ Ensure passed label list is long enough
                for j in range(len(labels), len(topics)):
                    labels_.append("")

        df.plot(ax=axes[i, 0], x=x_axis, y=topics)

        #@ Get min and max y-values
        if cutoff is not None and cutoff is not 0 and (df[x_axis] > cutoff).any():
            i_cutoff = df[df[x_axis] > cutoff].index[0]
        else:
            i_cutoff = -1

        for column in topics:
            if df[column].dtypes == np.float64 or df[column].dtypes == np.int64:
                max_val_y.append(df[column][:i_cutoff].max())
                min_val_y.append(df[column][:i_cutoff].min())

        # #@ Draw vertical lines when slope is encountered in slope-scenario
        # pos_topic = "/gazebo/link_states.pose.trolley::left_wheel_link.position.x"
        pos_topic = "/gazebo/link_states.pose.trolley::base_footprint.position.x"

        #@ Find start-and end indices of slope encounter in x-axis units
        if pos_topic in df.columns and slope:
            if any(df[pos_topic][:i_cutoff] > slope_begin):
                i_begin = df[df[pos_topic] > slope_begin].index[0]
                x_begin = df[x_axis][i_begin]
                axes[i, 0].axvline(x_begin, ls=":", c='r')
                labels_.append("start of slope")
            if any(df[pos_topic][:i_cutoff] > slope_end):
                i_end = df[df[pos_topic] > slope_end].index[0]
                x_end = df[x_axis][i_end]
                axes[i, 0].axvline(x_end, ls=":", c='b')
                labels_.append("end of slope")

        axes[i, 0].legend(labels_, loc='upper right')
        axes[i, 0].set_title(subtitles[i])
        axes[i, 0].grid(which="major")
        axes[i, 0].grid(which="minor", linestyle=":")
        axes[i, 0].minorticks_on()

    #@ Set limits
    axes[0, 0].set_xlim(0, x_len)
    axes[0, 0].set_ylim(floor(min(min_val_y)), ceil(max(max_val_y)))
    axes[0, 0].set_xlabel(x_label)

    #@ Maximize plot
    mng = plt.get_current_fig_manager()
    dim = mng.window.maxsize()
    scale = 0.5
    fig.set_figheight(dim[0]/100*scale)
    fig.set_figwidth(dim[1]/100*scale)

    #@ Save plot
    if fn is not None and fn != "":  #@ Save plot if filename is given
        if ext is None:
            fmt = ".pdf"
        else:
            fmt = ext
        if not isinstance(fmt, list):
            fmt = [fmt]
        path = rospkg.RosPack().get_path('trolley_analysis') + "/sim_graphs/"

        for format in fmt:
            plt.savefig(path + fn + format)
            print("Plot saved to /sim_graphs/{}".format(fn + format))

    #@ Show plot
    if show is True:
        plt.show()

    print("\n-- PLOTTING DATA COMPLETE --\n")


def plot_dt(df, column):
    df_acc = df[["time", column]].dropna()

    df_acc.time.round(3).diff().hist(bins=31)
    plt.xlabel(r"$\Delta t$")
    plt.show()


def plot_freq(df, column):
    #@ Read dataframe
    data = df[["time", column]].dropna()
    x = data.time
    y = data[column]

    # mean_sample_time = 0.02  # df_acc.time.diff().mean()
    mean_sample_time = data.time.diff().mean()
    print("mean dt: {}".format(mean_sample_time))

    #@ Uncomment to test for a 9 Hz sine wave
    # x = np.arange(0, 3, mean_sample_time)
    # y = np.sin(x * 2 * np.pi * 9)

    # Compute fft and corresponding frequencies in Hz
    fft = np.fft.rfft(y)
    fft_freq = np.fft.rfftfreq(len(y), mean_sample_time)

    fig, (ax1, ax2) = plt.subplots(nrows=2)
    ax1.plot(x, y)
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel(extract_topicname(column))
    ax1.set_title("signal")
    ax1.grid()

    #@ Normalize for number of samples s.t. amplitude is correct in plot
    ax2.loglog(fft_freq, np.abs(fft) * 2 / len(x))
    ax2.set_xlabel("freq [Hz]")
    ax2.set_ylabel("magnitude")
    ax2.set_title("frequencies")
    ax2.grid()
    plt.show()


def plot_freq_stem(df, column):

    #@ Read dataframe
    data = df[["time", column]].dropna()
    x = data.time
    y = data[column]

    # mean_sample_time = 0.02  # df_acc.time.diff().mean()
    mean_sample_time = data.time.diff().mean()
    print("mean dt: {}".format(mean_sample_time))
    f_s = 1/mean_sample_time

    #@ Uncomment to test for a 9 Hz sine wave
    # x = np.arange(0, 3, mean_sample_time)
    # y = np.sin(x * 2 * np.pi * 9)

    # Compute fft and corresponding frequencies in Hz
    fft = np.fft.rfft(y)
    fft_freq = np.fft.rfftfreq(len(y), mean_sample_time)

    fig, (ax1, ax2) = plt.subplots(nrows=2)
    ax1.plot(x, y)
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel(extract_topicname(column))
    ax1.set_title("signal")
    ax1.grid()

    #@ Normalize for number of samples s.t. amplitude is correct in plot
    ax2.stem(fft_freq, np.abs(fft))
    ax2.set_xlabel("freq [Hz]")
    ax2.set_ylabel("magnitude")
    ax2.set_xlim(-0.5, f_s / 2)
    ax2.set_title("frequencies")
    
    ax2.grid()
    plt.show()


def bode_plot(num, den, w_cutoff=None, freq_range=None, name=None, path=None):
    """
    Custom bode plot function.

    Inputs:
    - num: numerator of transfer function
    - den: denominator of transfer function
    - w_cutoff (optional):  
    - freq_range (optional): iterable with exponent of logarithm of min and max frequency
    - name (optional): set name to save plot
      * None: Don't save
      * True: Automatically name plot to match plotted transfer function
      * Custom name string
    - path (optional): set path to save folder
      * None: Set to default (trolley_project/images/plots)
      * Custom path string
    """
    def expand_to_polynomial_of_s(expr):
        s = sp.symbols('s')
        expr_ = 0
        if not isinstance(expr, (list, tuple)): 
            expr = [expr]
        expr = expr[::-1]

        for i in range(len(expr)):
                expr_ = expr_ + expr[i] * s**i

        return expr_

    num_ = expand_to_polynomial_of_s(num)
    den_ = expand_to_polynomial_of_s(den)

    H = num_/den_
    print("H = {}".format(H))
    print("numerator = {}".format(num_))
    print("denominator = {}".format(den_))

    if freq_range is not None:
        freqs = np.logspace(freq_range[0], freq_range[1], 10*(freq_range[1] - freq_range[0]) + 1)
    else:
        freqs = None
    w, mag, phase = signal.bode((num, den), freqs)

    #% PLOT ##
    fig, [ax1, ax2] = plt.subplots(2,1, sharex=True)
    plt.suptitle('Bode plot for {}'.format(H))

    # ax1.set_title("Magnitude")
    ax1.set_ylabel('Magnitude [dB]')
    ax1.semilogx(w, mag, label='filter response')
    ax1.hlines(-3, ax1.get_xlim()[0], ax1.get_xlim()[1], 'r', 'dashed', label='half-power magnitude (-3 dB)')
    ax1.vlines(w_cutoff, ax1.get_ylim()[0], ax1.get_ylim()[1], 'g', 'dashed', label='cutoff frequency ({} Hz)'.format(w_cutoff))
    ax1.grid(which="both")
    ax1.legend()

    # ax2.set_title("Phase")
    ax2.set_ylabel('Phase [deg]')
    ax2.set_xlabel('Frequency [Hz]')
    ax2.semilogx(w, phase, label='filter response')
    ax2.vlines(w_cutoff, ax2.get_ylim()[0], ax2.get_ylim()[1], 'g', 'dashed', label='cutoff frequency ({} Hz)'.format(w_cutoff))
    
    ax2.yaxis.set_major_locator(plticker.MultipleLocator(45))
    
    ax2.grid(which="both")
    # ax2.legend()

    if name is not None:
        if name is True:
            name = "Bode plot for {}".format(H).replace('/', '-div-').replace(' ', '_')
        save_plot(fig, name, path)
    plt.show()

    print()


def plot_filtered_data(data_pre, data_post, freq, title=None):
    freq_ = float(freq)
    t_pre = np.linspace(0, len(data_pre)/freq_, len(data_pre), endpoint=False)
    t_post = np.linspace(0, len(data_post)/freq_,
                         len(data_post), endpoint=False)

    # if version_info.major == 3:
    #     fig, ax = plt.subplots(1, 1, sharex=True, squeeze=True)
    #     ax.stem(t_pre, data_pre, use_line_collection=True,
    #             label="before processing")
    #     ax.stem(t_post, data_post, use_line_collection=True,
    #             label="after processing", linefmt="C1--", markerfmt="C1o")
    # else:
    fig, ax = plt.subplots(1, 1, sharex=True, squeeze=True)
    ax.stem(t_pre, data_pre, label="before processing")
    ax.stem(t_post, data_post, label="after processing",
            linefmt="C1--", markerfmt="C1o")

    ax.set_ylabel("Magnitude")
    if title is not None:
        plt.title(title)
    ax.legend()
    plt.show()
    return
