from scipy import signal
import matplotlib.pyplot as plt
import numpy as np

# Create a butterworth filter. 3 is the order, 10 is the cutoff frequency in hertz
# analog=False for digital filter. output='sos' if you want to use the filter for actual filtering
fs_hertz = 50
sos = signal.butter(3, 10, 'lowpass', fs=fs_hertz, output='sos', analog=False)

# Create a bode plot. freqz is for digital filters. Set the same sampling frequency as above
# Zoom in onto 10 hz to see the cutoff frequency (-3dB)
w, h = signal.sosfreqz(sos, fs=fs_hertz)
fig, ax1 = plt.subplots()
ax1.set_title('Digital filter frequency response')
ax1.plot(w, 20 * np.log10(abs(h)), 'b')
ax1.set_ylabel('Amplitude [dB]', color='b')
ax1.set_xlabel('Frequency [Hz]')
plt.grid()
plt.show()

# To demonstrate that the filter actually filters above 10 hertz, here's an example where
# the sine of 15 Hz is filtered but 5Hz is kept
t = np.arange(0, 2, 1 / fs_hertz)
sig1 = np.sin(2*np.pi*5*t)
sig2 = np.sin(2*np.pi*15*t)
sig = sig1 + sig2  # 5 and 15 Hz sines
filtered = signal.sosfilt(sos, sig)

# Plot original and filtered signal
fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)

ax1.plot(t, sig)
ax1.set_title('5 Hz and 15 Hz sinusoids')digital
ax1.axis([0, t[-1], -2, 2])

ax2.plot(t, filtered, label="filtered")
ax2.plot(t, sig1, label="Original 5Hz")
ax2.set_title('After 10 Hz high-pass filter')
ax2.axis([0, t[-1], -2, 2])
ax2.set_xlabel('Time [seconds]')
plt.legend()

plt.tight_layout()
plt.show()



