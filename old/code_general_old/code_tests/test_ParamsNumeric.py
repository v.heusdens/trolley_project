from code_general.load_model_elements import *

test = ParamsNumeric()

#% Test Get_defined_params and related
t1 = test.get_defined_params
t2 = test.get_defined_param_names
t3 = test.get_defined_param_values

for x in [t1, t2, t3]:
    print(x)