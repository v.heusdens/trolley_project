from code_general.load_model_elements import ParamGroups, TFsAdaptedNumeric
from code_general.toolbox import *
import numbers

TFs = TFsAdaptedNumeric()
TFs_avg = TFsAdaptedNumeric(par_type='avg')
TFs_nom = TFsAdaptedNumeric(par_type='nom')
TFs_min = TFsAdaptedNumeric(par_type='min')
TFs_max = TFsAdaptedNumeric(par_type='max')
group = ParamGroups()

G = TFs.G
G_avg = TFs_avg.G
G_nom = TFs_nom.G
G_min = TFs_min.G
G_max = TFs_max.G
L = TFs.L
L_avg = TFs_avg.L
L_nom = TFs_nom.L
L_min = TFs_min.L
L_max = TFs_max.L
S = TFs.S
S_avg = TFs_avg.S
S_nom = TFs_nom.S
S_min = TFs_min.S
S_max = TFs_max.S
T = TFs.T
T_avg = TFs_avg.T
T_nom = TFs_nom.T
T_min = TFs_min.T
T_max = TFs_max.T

loc = list(locals())
# for name in loc:
#     if name[0:2] in 'G_L_S_T_':
#         print("{}: {}".format(name, locals()[name]))

#% Check if filled TF's are correct
params = group.params_uncertain
toggle = False
for TF in ['G', 'L', 'S', 'T']:
    for par_type in ['avg', 'nom', 'min', 'max']:
        replacements = getattr(TFs.par_num.par_unc, 'get_params_{}'.format(par_type))()
        
        val_manual = subs_multi(locals()[TF], TFs.TFs.sym.get_symbolic_params(params), replacements)
        val_auto = locals()["{}_{}".format(TF, par_type)]

        if isinstance(val_auto, numbers.Number):
            val_auto = round(val_auto, 4)
        if isinstance(val_manual, numbers.Number):
            val_manual = round(val_manual, 4)
        
        if val_manual == val_auto:
            print("Values for {}_{} match.".format(TF, par_type))
        else:
            toggle = True
            print("ERROR: values of {}_{} don't match.".format(TF, par_type))
            print("Value from TFsAdaptedNumeric: {}".format(val_auto))
            print("Value manual calculation: {}".format(val_manual))

if not toggle:
    print("Test succeeded.")
else:
    print("Test failed.")


