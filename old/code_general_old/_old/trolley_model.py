#!/usr/bin/env python
import sympy as sp
from math import sin, cos

# Initiation of symbolic variables
F_hx, F_hy = sp.symbols('F_hx F_hy' ,real=True)
F_wx, F_wy, T_m, F_n = sp.symbols('F_wx F_wy T_m F_n' ,real=True)

t = sp.Symbol('t')
xw = sp.Function('xw')(t)
yw = sp.Function('yw')(t)
rw = sp.Function('rw')(t)
xf = sp.Function('xf')(t)
yf = sp.Function('yf')(t)
rf = sp.Function('rf')(t)
alpha = sp.Function('alpha')(t)
beta = sp.Function('beta')(t)

dd_xw = sp.diff(xf, t, 2)
dd_yw = sp.diff(yf, t, 2)
dd_rw = sp.diff(rf, t, 2)
dd_xf = sp.diff(xf, t, 2)
dd_yf = sp.diff(yf, t, 2)
dd_rf = sp.diff(rf, t, 2)
dd_alpha = sp.diff(alpha, t, 2)
dd_beta = sp.diff(beta, t, 2)

# Set variables
l_wh = 0.95
l_wf = 1/3*l_wh
m_f = 0.7
r_w = 0.105
m_w = 3.9
g = 9.81

F_fg = m_f*g
F_wg = m_w*g

I_f = 1/3*m_f*l_wh**3 # Infinitely thin rod
I_w = 1/2*m_w*r_w**2 # Massive cylinder

mu_bear_ball = 0.0015
mu_bear_plain = 0.075

T = 0
T_b = 0
T_m = T - T_b

F_roll = 0
F_stat = 0

# T_b = F_wy*(mu_bear_ball + mu_bear_plain)
# F_stat = (2*T_m - 2*T_b)*r_w + F_roll # ?????????



"Newton-Euler equations of motion"
# Wheel
eq_wx = sp.Eq(m_w*dd_xw, F_wx + F_stat - F_roll) # Fx = 0
eq_wy = sp.Eq(m_w*dd_yw, F_n - F_wy - F_wg) # Fy = 0
eq_wr = sp.Eq(I_w*dd_rw, (F_stat - F_roll)*r_w - T_m) # M = 0

# Frame
eq_fx = sp.Eq(m_f*dd_xf, F_hx - 2*F_wx) # Fx = 0
eq_fy = sp.Eq(m_f*dd_yf, 2*F_wy + F_hy - F_fg) # Fy = 0
eq_fr = sp.Eq(I_f*dd_rf, 2*T_m - sp.cos(alpha)*(F_fg*l_wf - F_hy*l_wh) - sp.sin(alpha)*F_hx*l_wh) #- F_hx*l_f*sin(alpha) + F_hy*l_f*cos(alpha) # M = 0

eqs = [eq_wx, eq_wy, eq_wr, eq_fx, eq_fy, eq_fr]

"Constraint equations"
# Position
con_xw = sp.Eq(xw, - r_w*beta)
con_yw = sp.Eq(yw, 0)
con_xf = sp.Eq(xf, xw + l_wf*sp.cos(alpha))
con_yf = sp.Eq(yf, yw + l_wf*sp.sin(alpha))

cons = [con_xw, con_yw, con_xf, con_yf]

# Acceleration
dd_cons = [sp.diff(x,t,2) for x in cons]
print(dd_cons)

# Solve
# sol = sp.solve([eq_wx, eq_wy], F_wx)
# sol = sp.solve(eqs + cons, alpha)
# print(sol)
