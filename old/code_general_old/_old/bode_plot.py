#!/usr/bin/env python

import numpy as np
from math import pi, log, isinf
import control
import matplotlib.pyplot as plt
import matplotlib.axes as axes


class Bode:
    def __init__(self, name, color, mag, phase, omega, gm, pm, gmf, pmf):
        self.name = name
        self.color = color

        self.phase = phase
        self.mag = mag
        self.omega = omega

        self.gm = gm
        self.pm = pm
        self.gmf = gmf
        self. pmf = pmf


def compare_bode(bodes, omega):
    "Plot bode info contained in bode classes using different colors"
    plt.figure()

    if len(bodes) == 1:
        plt.suptitle('Gm = {} dB (at {} Hz), Pm = {} deg (at {} Hz)'.format(bodes[0].gm, bodes[0].gmf, bodes[0].pm, bodes[0].pmf))
    else:
        plt.suptitle('Bode plots of multiple systems')

    # Magnitude
    plt.subplot(211)
    plt.ylabel('Magnitude (dB)')
    plt.grid(True, 'both', 'both')

    for i in range(len(bodes)):
        plt.semilogx(bodes[i].omega, bodes[i].mag, color=bodes[i].color, label=bodes[i].name)

    a = np.zeros(len(omega))
    plt.semilogx(omega, a, 'r--')

    plt.legend()

    # Gain
    plt.subplot(212)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Phase (deg)')
    plt.grid(True, 'both', 'both')

    for i in range(len(bodes)):
        plt.semilogx(bodes[i].omega, bodes[i].phase, color=bodes[i].color)

    b = np.ones(len(omega))*-180
    plt.semilogx(omega, b, 'r--')

    plt.show()

def bode():
    """Calculate frequency response of transfer function, plot results"""
    bodes = []

    "Set custom frequency range"
    f_min = 0.1 # Lower frequency bound in Hz
    f_max = 5 # Upper frequency bound in Hz
    o_min = log(f_min, 10)
    o_max = log(f_max, 10)
    omega = np.logspace(o_min, o_max) # Set desired frequency analyis range in rad/s
    # mag, phase, omega = control.bode(sys, omega, dB=True, Hz=True, deg=True, margins=True, Plot=True)

    "Add relevant info"
    i = 0
    for key, val in sys.items():
        # Calculate system bode properties
        mag, phase, omg = control.bode(val, omega, dB=True, Hz=True, deg=True, margins=True, Plot=False)
        phase = rad2deg(phase)
        mag = control.mag2db(mag)
        gm, pm, gmf, pmf = control.margin(val) # Calculate margins [-], [deg], [Hz], [Hz]

        # Save in array of classes containing these properties
        bodes.append(Bode(key, colors[i], mag, phase, omg, gm, pm, gmf, pmf))

        i+=1
    
    return bodes, omega

def rl():
    roots, gains = control.root_locus(sys)
    plt.suptitle('Root locus')
    plt.show()

def rad2deg(x):
    result =  x / 2 / pi * 360
    return result

def deg2rad(x):
    result = x / 360 * 2 * pi
    return result

###################################################################
m = 0.7 + 1*2 + 0.1 + 0.1
c = 0

sys1 = control.tf([1], [m, c])
sys2 = control.tf([1], [m*4, c])
sys3 = control.tf([1], [m, c*0.25])
sys4 = control.tf([1], [0.1*m, 0.1*c])

# sys = {'regular':sys1, '4*m':sys2, '0.25*c':sys3}
# sys = {'regular':sys1, 'extra gain':sys4}
sys = {'open-loop trolley behavior': sys1}
colors = ['b', 'g', 'm', 'c', 'y', 'k']
print(sys)



bodes, omega = bode()
compare_bode(bodes, omega)
# rl()
