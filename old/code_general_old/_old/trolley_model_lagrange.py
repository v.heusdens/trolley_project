#!/usr/bin/env python
import sympy as sp

# Initiation of symbolic variables
# Knowns
F_hx, F_roll, F_fg, F_wg = sp.var('F_HX F_ROLL F_FG F_WG')
L_wf, L_wh, R_w, H_y = sp.var('L_WF L_WH R_W H_y')

# Unknowns
F_wx, F_wy, F_n, F_stat, F_hy = sp.var('F_WX F_WY F_N F_STAT F_HY')

t = sp.var('t')
xf = sp.Function('xf')(t)
yf = sp.Function('yf')(t)
rf = sp.Function('rf')(t)
xw = sp.Function('xw')(t)
yw = sp.Function('yw')(t)
rw = sp.Function('rw')(t)
alpha = sp.Function('alpha')(t)
beta = sp.Function('beta')(t)

d_xf = sp.diff(xf, t)
d_yf = sp.diff(yf, t)
d_rf = sp.diff(rf, t)
d_xw = sp.diff(xw, t)
d_yw = sp.diff(yw, t)
d_rw = sp.diff(rw, t)
d_alpha = sp.diff(alpha, t)
d_beta = sp.diff(beta, t)

dd_xf = sp.diff(xf, t, 2)
dd_yf = sp.diff(yf, t, 2)
dd_rf = sp.diff(rf, t, 2)
dd_xw = sp.diff(xw, t, 2)
dd_yw = sp.diff(yw, t, 2)
dd_rw = sp.diff(rw, t, 2)
dd_alpha = sp.diff(alpha, t, 2)
dd_beta = sp.diff(beta, t, 2)

x = [xf, yf, alpha, xw, yw, beta]
d_x = [d_xf, d_yf, d_alpha, d_xw, d_yw, d_beta]
dd_x = [dd_xf, dd_yf, dd_alpha, dd_xw, dd_yw, dd_beta]


# Equations of motion
F = [
    F_hx,
    F_hy - F_fg,
    0,
    F_stat - F_roll,
    F_n - F_wg,
    0
]

# Constraint equations
C = [
    xw - R_w * beta,
    yw,
    xf - xw - sp.cos(alpha) * L_wf,
    yf - yw - sp.sin(alpha) * L_wf,
    yf + sp.sin(alpha) * (L_wh - L_wf) - H_y
]
C = sp.Matrix(C)

C_j = C.jacobian(x)
d_C = C_j * sp.Matrix(d_x)

C_jj_dx = d_C.jacobian(d_x)
C_jj_dx_dx = C_jj_dx * sp.Matrix(d_x)
