#!/usr/bin/env python
import sympy as sp
import math

# Initiation of symbolic variables
F_hx, F_hy = sp.symbols('F_hx F_hy' ,real=True)
F_wx, F_wy, T_m, F_n, F_fg, F_wg, F_stat, F_roll = sp.symbols('F_wx F_wy T_m F_n F_fg F_wg F_stat F_roll' ,real=True)

m_f, m_w, I_f, I_w = sp.symbols('m_f m_w I_f I_w', real=True)
L_wf, L_wh, R_w = sp.symbols('L_wf L_wh R_w', real=True)

t = sp.Symbol('t')
xw = sp.Function('xw')(t)
yw = sp.Function('yw')(t)
rw = sp.Function('rw')(t)
xf = sp.Function('xf')(t)
yf = sp.Function('yf')(t)
rf = sp.Function('rf')(t)
alpha = sp.Function('alpha')(t)
beta = sp.Function('beta')(t)

d_alpha = sp.diff(alpha, t)
d_beta = sp.diff(beta, t)

dd_xw = sp.diff(xf, t, 2)
dd_yw = sp.diff(yf, t, 2)
dd_rw = sp.diff(rf, t, 2)
dd_xf = sp.diff(xf, t, 2)
dd_yf = sp.diff(yf, t, 2)
dd_rf = sp.diff(rf, t, 2)
dd_alpha = sp.diff(alpha, t, 2)
dd_beta = sp.diff(beta, t, 2)

# Equations
eq_fx = sp.Eq(F_hx - 2*F_wx, m_w*dd_xf) # Fx = 0
eq_fy = sp.Eq(2*F_wy + F_hy - F_fg, m_f*dd_yf) # Fy = 0
eq_fm = sp.Eq(2*T_m - sp.cos(alpha)*(F_fg*L_wf - F_hy*L_wh) - sp.sin(alpha)*F_hx*L_wh, I_f*dd_alpha) # M = 0

eq_wx = sp.Eq(F_wx + F_stat - F_roll, m_w*dd_xw) # Fx = 0
eq_wy = sp.Eq(F_n - F_wy - F_wg, m_w*dd_yw) # Fy = 0
eq_wm = sp.Eq(R_w*(F_stat - F_roll) - T_m, I_w*dd_beta) # M = 0

eqs = [eq_fx, eq_fy, eq_fm, eq_wx, eq_wy, eq_wm]

con_xf = sp.Eq(dd_xf, dd_xw - L_wf*(sp.cos(alpha)*d_alpha** + sp.sin(alpha)*dd_alpha))
con_yf = sp.Eq(dd_yf, dd_yw - L_wf*(sp.sin(alpha)*d_alpha** - sp.cos(alpha)*dd_alpha))
con_xw = sp.Eq(dd_xw, -R_w*dd_beta)
con_yw = sp.Eq(dd_yw, 0)
cons = [con_xf, con_yf, con_xw, con_yw]

# sol = sp.nonlinsolve(eqs + cons, [F_hy])
# print(sol)