import sympy as sp

t = sp.Symbol('t')
C = sp.Symbol('C')
alpha = sp.Function('alpha')(t)
d_alpha = sp.diff(alpha, t)
dd_alpha = sp.diff(alpha, t, 2)

fun = alpha + d_alpha + dd_alpha + C

ex = sp.solve(fun.subs(alpha, 6), C)
print(ex)