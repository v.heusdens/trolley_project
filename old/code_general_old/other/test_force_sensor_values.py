
import sympy as sp
import rospkg
import yaml

description_path = rospkg.RosPack().get_path('trolley_description')
model_params = yaml.safe_load(
    open(description_path + "/config/model_params.yaml"))

# Knowns
F_fg, F_wg, F_roll, T_roll, F_hx = sp.symbols(
    'F_fg F_wg F_roll T_roll F_hx', real=True)
m_f, m_w, I_f, I_w = sp.symbols('m_f m_w I_f I_w', real=True)
L_wf, L_wh, R_w, C = sp.symbols('L_wf L_wh R_w C', real=True)
g, mu = sp.symbols("g mu", real=True)

# Unknowns
F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols(
    'F_wx F_wy T_m F_n F_hy F_stat', real=True)
beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols(
    'beta d_beta dd_beta dd_theta gamma', real=True)
dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols(
    'dd_xf dd_yf dd_xw dd_yw', real=True)
C = sp.var('C', real=True)

# From trolley_model_symbolic_calculation.py:
F_hy = (F_fg*L_wf*sp.cbetos(beta)**2 + F_hx*L_wh*sp.sin(2*beta)/2)/(L_wh*sp.cos(beta)**2)

m_ = model_params['frame_mass'] + model_params['bag_mass']
g_ = model_params['gravity']
F_fg_ = m_*g_

L_wh_ = model_params['frame_height']
L_wf_ = 0.5*L_wh_

L_h_ = model_params['handle_height']
R_w_ = model_params['wheel_radius']
beta_ = (L_h_ - R_w_)/L_wh_
F_hx_acc = 3.71721
F_hx_steady = 0.40221

F_hy_ = F_hy.subs([(L_wf, L_wf_), (beta, beta_), (L_wh, L_wh_)])
F_hy_acc = F_hy.subs([(F_fg, F_fg_), (L_wf, L_wf_), (beta, beta_), (L_wh, L_wh_), (F_hx, F_hx_acc)])
F_hy_steady = F_hy.subs([(F_fg, F_fg_), (L_wf, L_wf_), (beta, beta_), (L_wh, L_wh_), (F_hx, F_hx_steady)])

print("F_hy general: {} [N]".format(F_hy_))
print("F_hy during acceleration: {} [N]".format(F_hy_acc))
print("F_hy during steady-state: {} [N]".format(F_hy_steady))