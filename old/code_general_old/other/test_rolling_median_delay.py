import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

window = 5

data = np.concatenate((np.zeros(50), np.ones(50)))
data = pd.Series(data)

median = data.rolling(window).median()

data.plot()
median.plot()
plt.grid(which="both")
plt.xticks(range(0, len(data), 1))
plt.show()

idata = data.ne(0).idxmax()
imedian = median.fillna(0).ne(0).idxmax()

print("delay: {}".format(imedian - idata))
