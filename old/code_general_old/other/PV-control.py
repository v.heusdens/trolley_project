import scipy
import sympy as sp
import numpy as np
from matplotlib import pyplot as plt
from sympy.utilities.lambdify import lambdify
from code_general.toolbox import *

dt = 0.01
dur = 15


e, x, v, a, F_h, F_slope = sp.symbols('e x v a F_h F_slope')
v_ref = 2*F_h
e = v_ref - v
F_roll = sp.sign(v)
fe = lambdify([v, F_h], e)
T_m = 0.75*e
T_m = 0
fa = lambdify([v, F_h, F_slope], F_h + T_m - F_roll - F_slope)
fv = lambdify([v, a], v + dt*a)
fx = lambdify([x, v], x + dt*v)

F_h = np.zeros(int(dur/dt))
F_h[int(1/dt):] = 3
F_h[int(3/dt):] = 1
F_slope = np.zeros(int(dur/dt))
F_slope[int(8/dt):int(12/dt)] = 0.5

for i in range(int(dur/dt)):
    if i == 0:
        x_val = [0]
        v_val = [0]
        e_val = [0]
        a_val = [0]
    else:
        x_val.append(fv(x_val[-1], v_val[-1]))
        v_val.append(fv(v_val[-1], a_val[-1]))
        e_val.append(fe(v_val[-1], F_h[i]))
        a_val.append(fa(v_val[-1], F_h[i], F_slope[i]))

t = np.linspace(0, dur, dur/dt, endpoint=False)
dist_time = [0 if j.is_integer() else None for j in t ]



fig, [ax1, ax2] = plt.subplots(2,1)
ax1.plot(t, F_h, label='input force')
ax1.plot(t, v_val, label='vel')
ax1.plot(t, a_val, label='acc')
ax1.plot(t, e_val, label='error/torque')
# ax1.legend()
ax1.minorticks_on()
ax1.grid(True, which='major')
ax1.grid(True, which='minor', alpha=0.2)


ax2.plot(x_val, F_h, label='input force')
ax2.plot(x_val, v_val, label='vel')
ax2.plot(x_val, a_val, label='acc')
ax2.plot(x_val, e_val, label='error/torque')
ax2.plot(x_val, dist_time, '*', label='seconds')
ax2.legend(prop={'size': 6})
ax2.minorticks_on()
ax2.grid(True, which='major')
ax2.grid(True, which='minor', alpha=0.2)

plt.show()


print()
