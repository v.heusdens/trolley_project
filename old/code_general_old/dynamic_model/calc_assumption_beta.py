
"""Calculations belonging to appendix for calculating the error in beta due to assumptions"""
##%%## Imports
import sympy as sp
import numpy as np
from code_general.load_model_elements import ParamsSymbolic, ParamsNumeric

#%%## define variables
s = ParamsSymbolic()

n = ParamsNumeric()
n.set_params_avg()
n.gamma = np.arctan(1./10)


#%%## Human body orientation
#% beta1
beta1 = sp.asin(s.L_h_acc/s.L_wh)

#% beta2
L1 = s.L_h - s.R_w/sp.cos(s.gamma)
L2 = sp.cos(s.gamma)*L1
beta2 = sp.asin(L2/s.L_wh)

#% diff between beta1 & beta2
delta_beta1 = abs(sp.simplify(beta1 - beta2))

# Fill in values
subss = [(s.L_h, n.L_h), (s.L_wh, n.L_wh), (s.R_w, n.R_w), (s.gamma, n.gamma)]

beta1_ = float(beta1.subs(subss))
beta2_ = float(beta2.subs(subss))
delta_beta1_ = float(delta_beta1.subs(subss))

#% print
print("\n Beta due to human body orientation:")
print("beta 1: {}".format(beta1_))
print("beta 2: {}".format(beta2_))

print("delta beta:")
print("{} rad".format(delta_beta1_))
print("{} deg".format(np.rad2deg(float(delta_beta1_))))
print("{} %".format(delta_beta1_/beta1_*100))


#%%## Fixed vs variable gamma
#% calc beta
beta3_ = float(beta1_ + n.gamma)
delta_beta2_ = n.gamma

#% influence of beta on sin and cos
sbeta_nom = np.sin(beta1_)
sbeta_max = np.sin(beta3_)
cbeta_nom = np.cos(beta1_)
cbeta_max = np.cos(beta3_)

sbeta_delta = abs(sbeta_max - sbeta_nom)
cbeta_delta = abs(cbeta_max - cbeta_nom)

#% print
print("\n Beta due to varying gamma:")
print("beta 3: {}".format(beta3_))
print("delta beta:")
print("{} rad".format(delta_beta2_))
print("{} deg".format(np.rad2deg(float(delta_beta2_))))
print("{} %".format(delta_beta2_/beta3_*100))
#print("delta sin(beta):")
#print("{} rad".format(sbeta_delta))
#print("{} deg".format(np.rad2deg(float(sbeta_delta))))
#print("{} %".format(sbeta_delta/sbeta_nom*100))
#print("delta cos(beta):")
#print("{} rad".format(cbeta_delta))
#print("{} deg".format(np.rad2deg(float(cbeta_delta))))
#print("{} %".format(cbeta_delta/cbeta_nom*100))


#%%## Max error in dd_xw due to constant gamma
dd_xw = 1 / n.m_acc * 2 * (1 / n.R_w - n.mu / (n.L_wh * sp.cos(s.beta))) * s.T_m

res_real = dd_xw.subs([(s.T_m, 1), (s.beta, beta3_)])
res_const_perp = dd_xw.subs([(s.T_m, 1), (s.beta, beta1_)])
res_const_vert = dd_xw.subs([(s.T_m, 1), (s.beta, beta2_)])
diff_real_const_perp = res_real - res_const_perp
diff_real_const_vert = res_real - res_const_vert
diff_real_const_perp_perc = (diff_real_const_perp / res_real * 100)
diff_real_const_vert_perc = (diff_real_const_vert / res_real * 100)

print('\nValues for dd_xw due to different values of beta:')
print('    dd_xw for max variable beta: {}'.format(res_real))
print('    dd_xw for const beta perp: {}'.format(res_const_perp))
print('    dd_xw for const beta vert: {}'.format(res_const_vert))

print('\nDifferences in dd_xw due to assumptions affecting beta:')

print('    \nVarying vs constant gamma: delta dd_xw = ')
print('    {} m/s^2'.format(diff_real_const_perp))
print('    {} % w.r.t. real value'.format(diff_real_const_perp_perc))

print("    \nVarying vs constant gamma in 'real' beta situation: delta dd_xw = ")
print('    {} m/s^2'.format(diff_real_const_vert))
print('    {} % w.r.t. real value'.format(diff_real_const_vert_perc))


#%%## Time-span of being affected by changing gamma
#x_ = np.sqrt(n.L_wh**2 - n.L_h_acc**2) #% distance
#vel_min_ = (127.2 - 21.1)/100
#vel_min_ = 1
#time_ = x_ / vel_min_
#print('\n Time-span:')
#print('distance: {}'.format(x_))
#print('min. velocity: {}'.format(vel_min_))
#print('time: {}'.format(time_))













