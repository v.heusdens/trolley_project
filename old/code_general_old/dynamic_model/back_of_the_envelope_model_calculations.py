import sympy as sp
import rospkg
import yaml 
# from ros_packages.trolley_description.config.load_model_params import *
from ros_packages.trolley_description.config.load_model_params_symbolic import *
from code_general.load_model_elements import *

## Model parameters ##
gamma = 0
T_m = 0
mu = 0
d_beta = 0

## Simple model for checking
F_fric = mu*(m_ft + 2*m_w)*g
F_tot = F_hx + 2*T_m/R_w - F_fric
dd_theta_check = F_tot/(R_w*(m_ft + 3.*m_w))

## Model with added constraint from symbolic calculation
ms = ModelElementsSymbolic()
dd_theta_model = ms.model_constrained
dd_theta_model = dd_theta_model.subs([(ms.gamma, gamma), (ms.T_m, T_m), (ms.mu, mu), (ms.d_beta, d_beta)])

print("dd_theta_check: {}".format(dd_theta_check))
print("dd_theta_model: {}".format(dd_theta_model))



