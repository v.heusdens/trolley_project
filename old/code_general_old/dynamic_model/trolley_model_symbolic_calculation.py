from inspect import currentframe
import numpy as np
import rospkg
import sympy as sp
import yaml
from code_general.toolbox import *
from code_general.load_model_elements import *

#region  ##functions
def print_inverse_model(expr_parts):
    if disp_type == 'r':
        print("({})*{} \\ \n \
            + ({})*{} \\ \n \
                = {} \\ \n \
                    - ({})*{} \\ \n \
                        - ({})*{F_wg}".format(expr_parts[F_hx], F_hx, expr_parts[T_m], T_m, dd_theta, expr_parts[F_fg], F_fg, expr_parts[F_wg], F_wg))
    else:
        print("({})*{} \\ \n \
            + ({})*{} \\ \n \
                - {} \\ \n \
                     + ({})*{} \\ \n \
                         + ({})*{}".format(expr_parts[F_hx], F_hx, expr_parts[T_m], T_m, dd_theta, expr_parts[F_fg], F_fg, expr_parts[F_wg], F_wg))

def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno
#endregion functions

#region ## SETTINGS
calc_com = False #@ Calculate center of mass variables
calc_model = True  #@ Calculate model. Otherwise, use model set below.
add_con = True  #@ Add additional constraint to model
expand_params = ['I_w', 'T_roll']
inv_model = False #@ Calculate inverse model for in controller

disp_type = 'c'  #@ Display type of variables: normal ('n'), class ('c') or report ('r')
simplify = False  #@ Simplify equation for verification
expand = True  #@ Print solved variable as elements of forces
#endregion

#region ## INITIALIZATION ##
print("Settings: \n \
        - calculate center of mass: {} \n \
        - add extra constraint: {} \n \
        - calculate model: {} \n \
        - expanded parameters: {} \n \
        - simplify equation for verification: {} \n \
        - additionally print model split into elements of forces: {} \n \
        - calculate inverse model: {} \n \
        - print output type: {} \n \
        ".format(calc_com, add_con, calc_model, expand_params, simplify, expand, inv_model, disp_type))

#% Load parameters
if disp_type == 'r':
    #% Knowns
    F_fg, F_wg, F_hx = sp.symbols(
        'F_{FG} F_{WG} F_{HX}', real=True)
    m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols('m_F m_B m_{FT} m_W I_F I_W', real=True)
    L_wf, L_wh, W_wf, R_w, C = sp.symbols('L_{WF} L_{WH} W_{WF} R_W C', real=True)
    L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf, m_f, m_b = sp.symbols("L_{FC} L_B L_{BC} W_F W_B W_{BC} W_{WF} m_ft' M_B", real=True)
    g, mu = sp.symbols('g, \mu', real=True)

    #% Unknowns
    F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols(
        'F_WX F_WY T_M F_N F_HY F_STAT', real=True)
    beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols(
        '\\beta \dot{\\beta} \ddot{\\beta} \ddot{\\theta} \gamma', real=True)
    dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols(
        '\ddot{x}_F \ddot{y}_F \ddot{x}_W \ddot{y}_W', real=True)
    C = sp.var('c', real=True)
    #
elif disp_type == 'c':
    prefix = 'sym.'
    #% Knowns
    F_fg, F_wg, F_hx = sp.symbols(
        '{0}F_fg {0}F_wg {0}F_hx'.format(prefix), real=True)
    m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols("{0}m_f {0}m_b {0}m_ft {0}m_w {0}I_f {0}I_w".format(prefix), real=True)
    L_wf, L_wh, W_wf, R_w = sp.symbols("{0}L_wf {0}L_wh {0}W_wf {0}R_w".format(prefix), real=True)
    L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf, m_f, m_b = sp.symbols("{0}L_fc {0}L_b {0}L_bc {0}W_f {0}W_b {0}W_bc {0}W_wf {0}m_f, {0}m_b".format(prefix), real=True)
    g, mu = sp.symbols("{0}g {0}mu".format(prefix), real=True)

    #% Unknowns
    F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols(
        "{0}F_wx {0}F_wy {0}T_m {0}F_n {0}F_hy {0}F_stat".format(prefix), real=True)
    beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols(
        "{0}beta {0}d_beta {0}dd_beta {0}dd_theta {0}gamma".format(prefix), real=True)
    dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols(
        "{0}dd_xf {0}dd_yf {0}dd_xw {0}dd_yw".format(prefix), real=True)
    C = sp.var('{0}C'.format(prefix), real=True)
    #
elif disp_type == 'n':
    #% Knowns
    F_fg, F_wg, F_hx = sp.symbols(
        'F_fg F_wg F_hx', real=True)
    m_f, m_b, m_ft, m_w, I_f, I_w = sp.symbols('m_f m_b m_ft m_w I_f I_w', real=True)
    L_wf, L_wh, W_wf, R_w, C = sp.symbols('L_wf L_wh W_wf R_w C', real=True)
    L_fc, L_b, L_bc, W_f, W_b, W_bc, W_wf, m_f, m_b = sp.symbols("L_fc L_b L_bc W_f W_b W_bc W_wf m_f, m_b", real=True)
    g, mu = sp.symbols("g mu", real=True)

    #% Unknowns
    F_wx, F_wy, T_m, F_n, F_hy, F_stat = sp.symbols(
        'F_wx F_wy T_m F_n F_hy F_stat', real=True)
    beta, d_beta, dd_beta, dd_theta, gamma = sp.symbols(
        'beta d_beta dd_beta dd_theta gamma', real=True)
    dd_xf, dd_yf, dd_xw, dd_yw = sp.symbols(
        'dd_xf dd_yf dd_xw dd_yw', real=True)
    C = sp.var('C', real=True)
    #
else:
    raise Exception("wrong display type. Choose either 'n', 'c' or 'r'.")

t = sp.Symbol("t")
theta_t = sp.Function("theta")(t)
beta_t = sp.Function("beta")(t)
xw_t = sp.Function("xw")(t)
yw_t = sp.Function("yw")(t)
xf_t = sp.Function("xf")(t)
yf_t = sp.Function("yf")(t)
#endregion INITIALIZATION

## MAIN ##
if __name__ == "__main__":
    s = ParamsSymbolic(expand_params)
    if disp_type == 'c':
        for name, key in s.__dict__.items():
            if key.is_Atom:
                key.name = 's.' + key.name
                setattr(s, name, key)

    #% Variable selection
    var_to_solve = s.dd_xw
    add_var_constr = s.F_hy #@ Additional implicit variable to eliminate when extra constraint is added
    vars_eom = s.get_symbolic_params(ParamGroups().params_acceleration)
    vars_constr = s.get_symbolic_params(ParamGroups().params_implicit)
    vars = vars_eom + vars_constr + [var_to_solve]

    #% Calculate center of mass coordinate equations
    if calc_com:
        print("\nCalculating center of mass coordinates...")

        eqs_com = get_eqs_COM(s)
        impl_vars_com = [s.L_fc, s.L_bc, s.L_wf, s.W_bc, s.W_wf]

        sol_com = sp.solve(eqs_com, impl_vars_com)
        L_wf = sol_com[s.L_wf]
        W_wf = sol_com[s.W_wf]

        print("L_wf: {}".format(L_wf))
        print("W_wf: {}".format(W_wf))

    #% Calculate/Load appropriate model
    #@ Additional equations
    if not calc_model: #@ Load previously saved model
        if not add_con: #@ No extra constraint
            if 'T_roll' in expand_params:
                model = TrolleyModelTime(s).unconstrained_T_roll_implicit
            else:
                model = TrolleyModelTime(s).unconstrained_T_roll_explicit

        else: #@ Extra constraint
            if 'T_roll' in expand_params:
                model = TrolleyModelTime(s).constrained_T_roll_implicit
            else:
                model = TrolleyModelTime(s).constrained_T_roll_explicit

    if calc_model: #@ Calculate new model
        print("\nCalculating model... added constraint = {}, simplify = {}".format(add_con, simplify))

        eqs_motion = get_eqs_motion(s)
        eqs_con = get_eqs_constraint(s, add_con)

        #% Solving
        sys = eqs_motion + eqs_con
        if add_con:
            vars.append(add_var_constr)

        sol = sp.solve(sys, vars, dict=True)[0]
        model = sol[var_to_solve]
        # if add_con:
        #     model = model.subs(s.d_beta, 0)

    #% Simplification
    if simplify: #@ Simplify if requested
        model = model.subs(
                [(gamma, 0), (T_m, 0), (mu, 0)])


    #% PRINTING
    if disp_type == 'r':
        print("model:\n {} = {}\n".format(var_to_solve, model))
    else: #@ add sp. before sin, cos and tan
        print("model:\n {} = {}\n".format(var_to_solve, make_eq_copyable(model)))

    if expand: #@ Expand model equation wrt variables
        forces = ParamGroups().params_forces
        forces = [force for force in forces if force not in expand_params]
        expr_parts = expand_to_variables(model, s.get_symbolic_params(forces))

        print("\nSubdivided into forces:")
        for key, value in expr_parts.items():
            if disp_type == 'r':
                print("{}: {}\n".format(key, value))
            else:
                print("{}: {}\n".format(key, make_eq_copyable(value)))


    ## INVERSE MODEL ##
    if inv_model: #@ Calculate inverse model
        expr_parts = eq_coeff(model, var_to_solve)
        print("Inverse model:")
        print_inverse_model(expr_parts)

        #% Fill in known variables
        model_simplified = model.subs([(gamma, 0)])
        expr_parts_simplified = eq_coeff(model_simplified, var_to_solve)
        print("Inverse model with gamma = 0:")
        print_inverse_model(expr_parts_simplified)
