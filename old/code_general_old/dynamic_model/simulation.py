import math
import os
from pprint import pprint
import numpy as np
import sympy as sp
from sympy.utilities.lambdify import lambdify
from code_general.load_model_elements import *
from code_general.toolbox import subs_multi
import pandas as pd
from scipy import integrate

def model_simulation(s, n, pos_init=np.zeros(6), vel_init=np.zeros(6), duration=4, dt=0.001):
    """
    Dynamic model equation simulation.  

    x =         [xw, yw, theta, xf, yf, beta]  
    s =         symbolic class  
    n =           numeric class with desired variables  
    pos_init =  list with initial positions for x (optional)  
    vel_init =  list with initial velocities for x (optional)  

    Returns pandas data array
    """
    ## Initialization
    print("Initialization...")
    print(" F_hx = {};   F_hy = {};  T_m = {};   gamma = {};     mu = {};    m_b = {};   m_w = {};   W_wf = {}".format(n.F_hx, n.F_hy, n.T_m, n.gamma, n.mu, n.m_b, n.m_w, n.W_wf))
    rnd = len(str(dt).split('.')[-1])

    #% Expand gravitational forces
    s.F_fg = s.F_fg_dep
    s.F_wg = s.F_wg_dep
    
    #% Relax T_roll
    threshold_ = 0.1
    threshold =  sp.Piecewise( ( 1, sp.GreaterThan( sp.Abs(s.d_theta), threshold_ ) ), ( 0, True ) )
    s.T_roll = s.T_roll*sp.sign(s.d_theta)*threshold

    #% parameters
    x_sym = [s.xw, s.yw, s.theta, s.xf, s.yf, s.beta]
    d_x_sym = [s.d_xw, s.d_yw, s.d_theta, s.d_xf, s.d_yf, s.d_beta]
    dd_x_sym = [s.dd_xw, s.dd_yw, s.dd_theta, s.dd_xf, s.dd_yf, s.dd_beta]
    
    vars_implicit = [s.F_wx, s.F_wy, s.F_stat, s.F_n]
    vars = dd_x_sym + vars_implicit

    #% Parameter values
    old_fixed = [s.L_wf,    s.W_wf,     s.L_wh,     s.R_w,      s.m_f,      s.g]
    new_fixed = [n.L_wf,    n.W_wf,     n.L_wh,     n.R_w,      n.m_f,      n.g]

    old_variable = [s.m_w,  s.m_b,  s.mu,   s.gamma,    s.T_m,  s.F_hx,     s.F_hy]
    new_variable = [n.m_w,  n.m_b,  n.mu,   n.gamma,    n.T_m,  n.F_hx,     n.F_hy]

    # #% Initialize data arrays
    # pos = [pos_init]
    # vel = [vel_init]
    # acc = [np.zeros(6)]
    # reaction_forces = [np.zeros(5)]

    ## Model equations
    print("Loading model equations...")
    eqs_motion = get_eqs_motion(s)
    eqs_constr = get_eqs_constraint(s)
    eqs = eqs_motion + eqs_constr

    #% Substitutions
    I_f = 1/3. * s.m_ft * s.L_wh**2
    eqs = subs_multi(eqs, [s.I_f, s.I_w, s.m_ft], [I_f, s.I_w_dep, s.m_ft_dep])

    old = old_fixed + old_variable
    new = new_fixed + new_variable
    eqs = subs_multi(eqs, old, new)
    T_roll = subs_multi(s.T_roll, old, new)

    #% Get fast symbolic function for numeric iterations
    sol = sp.solve(eqs, vars)
    eq_acc_ = [sol[var] for var in dd_x_sym]
    eq_forces_ = [sol[var] for var in vars_implicit]
    
    ss = lambdify([x_sym + d_x_sym], d_x_sym + eq_acc_)
    ss_scipy = lambda t, x: np.array(ss(x));    ss_scipy.func_name = "state-space-lambda"
    eq_acc = lambda x: ss(x)[6:];    
    eq_acc.func_name = "acceleration-lambda"
    eq_forces = lambdify([x_sym + d_x_sym], eq_forces_); 
    eq_forces.func_name = "forces-lambda"
    eq_T_roll = lambdify([d_x_sym, vars_implicit, s.R_w, s.mu], T_roll);     
    eq_T_roll.func_name = "T_roll-lambda"

    ## Simulation
    print('Running simulation...')
    print(' duration = {} sec, dt = {} sec.'.format(duration, dt))
    time_steps = np.around(np.linspace(0, duration, duration/dt + 1), rnd)

    sol_ode = integrate.solve_ivp(ss_scipy, (0, duration), np.concatenate((pos_init, vel_init)), t_eval=time_steps) #@ Computes solutions for pos and vel for all timesteps in one go
    sol_ode.y = sol_ode.y.T
    sol_pos = sol_ode.y[:, :6]
    sol_vel = sol_ode.y[:, 6:]
    sol_acc = np.array([eq_acc(x) for x in sol_ode.y])
    sol_forces = np.array([eq_forces(x) for x in sol_ode.y])
    zipped = zip(sol_vel, sol_forces)
    sol_T_roll = np.array([eq_T_roll(dx, F, n.R_w, n.mu) for dx, F in zipped])


    #% transform data to dataframe
    x_str = [str(x_) for x_ in x_sym]
    d_x_str = [str(x_) for x_ in d_x_sym]
    dd_x_str = [str(x_) for x_ in dd_x_sym]
    force_str = [str(x_) for x_ in vars_implicit]
    force_str.append('T_roll')
    
    pos = pd.DataFrame(sol_pos, index=time_steps, columns=x_str)
    vel = pd.DataFrame(sol_vel, index=time_steps, columns=d_x_str)
    acc = pd.DataFrame(sol_acc, index=time_steps, columns=dd_x_str)
    forces = pd.DataFrame(np.column_stack((sol_forces, sol_T_roll)), index=time_steps, columns=force_str)

    data = pd.concat([pos, vel, acc, forces], 1)

    print("Simulation finished.")
    return data


def get_pos_x(n, xw, beta):
    w_hor = sp.cos(n.gamma)*xw
    h_hor = w_hor + sp.cos(n.gamma + beta)*n.L_wh
    return [w_hor, h_hor]

def get_pos_y(n, xw, beta):
    w_vert = sp.sin(n.gamma)*xw
    h_vert = w_vert + sp.sin(n.gamma + beta)*n.L_wh
    return [w_vert, h_vert]


if __name__ == "__main__":
    m_w = 'm_w_tot';    m_b = 'm_b_nom';    mu = 0
    T_m = 0;            F_hx = 0;            F_hy = 0
    W_wf = 0;            gamma = 0;            L_h = 'L_h_nom'
    theta_init = 0; beta_init = 0

    sym = ParamsSymbolic()
    num = ParamsNumeric()

    data = model_simulation(sym, num)