import sympy as sp 

t, L_wf, L_wh, R_w, C = sp.symbols("t L_wf L_wh R_w C", real=True)
# x_w, y_w = sp.symbols("x_w y_w", real=True)

theta = sp.Function("theta")(t)
beta = sp.Function("beta")(t)
xw = sp.Function("xw")(t)
yw = sp.Function("yw")(t)
xf = sp.Function("xf")(t)
yf = sp.Function("yf")(t)

C = yf + sp.sin(beta) * (L_wh - L_wf)
xf = xw + sp.cos(beta)*L_wf
yf = yw + sp.sin(beta)*L_wf
xw = R_w*theta
yw = 0

dd_C = C.diff(t, 2)
dd_xf = xf.diff(t, 2)
dd_yf = yf.diff(t, 2)
dd_xw = xw.diff(t, 2)
dd_yw = 0

print("dd_xw = {}".format(dd_xw))
print("dd_yw = {}".format(dd_yw))
print("dd_xf = {}".format(dd_xf))
print("dd_yf = {}".format(dd_yf))
print("dd_C = {}".format(dd_C))

print("")