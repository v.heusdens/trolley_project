import sympy as sp
from sympy.matrices import Matrix, diag, zeros
from code_general.load_model_elements import ParamsSymbolic, TrolleyModelTime, ParamGroups
from code_general.toolbox import *
from pprint import pprint

def dynamic_equation():
    #% Initialization
    s = ParamsSymbolic(expand=['I_w'])

    ## SETTINGS
    T_roll_explicit = False
    constrained = True
    compare = None
    separate = False

    ## EQUATIONS
    m_ = (s.m_ft + 3*s.m_w)
    L_= (s.L_wf * sp.sin(s.beta) + s.W_wf * sp.cos(s.beta))
    L__ = (s.L_wf * (sp.sin(s.beta) - s.mu * sp.cos(s.beta)) + s.W_wf * (s.mu * sp.sin(s.beta) + sp.cos(s.beta)))

    report_T_roll_explicit_unconstrained = 1 / (m_ ) * (
        (1 - s.L_wh * s.m_ft * L_ / s.I_f * sp.sin(s.beta)) * s.F_hx
        +
        (s.L_wh * s.m_ft * L_ / s.I_f * sp.cos(s.beta)) * s.F_hy
        +
        (1 / s.R_w + s.m_ft * L_ / s.I_f) * 2 * s.T_m
        -
        sp.sin(s.gamma) * 2 * s.F_wg
        -
        2 / s.R_w * s.T_roll
        + 
        (s.m_ft * L_ / s.I_f * (s.W_wf * sp.sin(s.beta + s.gamma) - s.L_wf * sp.cos(s.beta + s.gamma)) - sp.sin(s.gamma)) * s.F_fg 
        + 
        (s.m_ft * (s.L_wf * sp.cos(s.beta) - s.W_wf * sp.sin(s.beta))) * s.d_beta**2
    )

    report_T_roll_implicit_unconstrained = 1 / (m_ ) * (
        (1 - s.L_wh * s.m_ft / s.I_f * sp.sin(s.beta) * L__) * s.F_hx
        +
        (s.mu + s.L_wh * s.m_ft / s.I_f * sp.cos(s.beta) * L__) * s.F_hy
        +
        (1 / s.R_w + s.m_ft * (L__ / s.I_f)) * 2 * s.T_m
        -
        (sp.sin(s.gamma) + s.mu * sp.cos(s.gamma)) * 2 * s.F_wg
        + 
        (s.m_ft / s.I_f * L__ * (s.W_wf * sp.sin(s.beta + s.gamma) - s.L_wf * sp.cos(s.beta + s.gamma)) - (sp.sin(s.gamma) + s.mu * sp.cos(s.gamma))) * s.F_fg 
        + 
        (s.m_ft * (s.L_wf * (s.mu * sp.sin(s.beta) + sp.cos(s.beta)) - s.W_wf * (sp.sin(s.beta) - s.mu * sp.cos(s.beta)))) * s.d_beta**2
    )

    report_T_roll_explicit_constrained = 0

    report_T_roll_implicit_constrained = 1 / m_ * \
        (
            (s.mu * sp.tan(s.beta) + 1) * s.F_hx
            #
            + 2 * (1 / s.R_w - s.mu / (s.L_wh * sp.cos(s.beta))) * s.T_m
            #
            - ( s.mu / (s.L_wh * sp.cos(s.beta)) * (s.W_wf * sp.sin(s.beta + s.gamma) - s.L_wf * sp.cos(s.beta + s.gamma)) + (sp.sin(s.gamma) + s.mu * sp.cos(s.gamma))) * s.F_fg
            #
            - 2 * (sp.sin(s.gamma) + s.mu * sp.cos(s.gamma)) * s.F_wg
        )



    #% Print difference as elements of forces
    forces = s.get_symbolic_params(ParamGroups().params_forces)
    if constrained:
        if T_roll_explicit:
            model_calc = TrolleyModelTime(s).constrained_T_roll_explicit
            model_report = report_T_roll_explicit_constrained
        else:
            forces.remove(s.T_roll)
            model_calc = TrolleyModelTime(s).constrained_T_roll_implicit
            model_report = report_T_roll_implicit_constrained
    else:
        if T_roll_explicit:
            model_calc = TrolleyModelTime(s).unconstrained_T_roll_explicit
            model_report = report_T_roll_explicit_unconstrained
        else:
            forces.remove(s.T_roll)
            model_calc = TrolleyModelTime(s).unconstrained_T_roll_implicit
            model_report = report_T_roll_implicit_unconstrained

    forces_report = expand_to_variables(model_calc, forces)
    forces_calc = expand_to_variables(model_report, forces)

    if compare is not None:
        if isinstance(compare[0], str):
            compare = s.get_symbolic_params(compare)
        forces = [force for force in forces if force in compare]

    print("")
    print("Diff between calc and report:")
    for force in forces:
        force = str(force)
        print('\n{}:'.format(force))
        if separate:
            print(' - Report:\n{}').format(forces_report[force])
            print(' - Calc:\n{}').format(forces_calc[force])
        print(' - DIFFERENCE: {}'.format(sp.simplify(forces_calc[force] - forces_report[force])))

def system_of_equations():
    s = ParamsSymbolic()
    dd_x = Matrix([s.dd_xw, s.dd_yw, s.dd_theta, s.dd_xf, s.dd_yf, s.dd_beta])
    F_c = Matrix([s.F_wx, s.F_wy, s.F_stat, s.F_n])
    F_ext = Matrix([
        -sp.sin(s.gamma) * s.F_wg,
        -sp.cos(s.gamma) * s.F_wg,
        s.T_m,
        s.F_hx - sp.sin(s.gamma) * s.F_fg,
        s.F_hy - sp.cos(s.gamma) * s.F_fg,
        -sp.sin(s.beta) * s.L_wh * s.F_hx + sp.cos(s.beta) * s.L_wh * s.F_hy + 2 * s.T_m - (sp.cos(s.beta + s.gamma) * s.L_wf - sp.sin(s.beta + s.gamma) * s.W_wf) * s.F_fg
    ])
    a = Matrix([
        0,
        0,
        (s.W_wf * sp.sin(s.beta) - s.L_wf * sp.cos(s.beta)) * s.d_beta**2,
        -(s.W_wf * sp.cos(s.beta) + s.L_wf * sp.sin(s.beta)) * s.d_beta**2
    ])
    M = diag(s.m_w, s.m_w, s.I_w, s.m_f, s.m_f, s.I_f)
    A = Matrix((
        [-1, 0, -1, 0],
        [0, 1, 0, -1,],
        [0, 0, s.R_w, s.mu * s.R_w],
        [2, 0, 0, 0],
        [0, -2, 0, 0],
        [0, 0, 0, 0]
    ))
    B = Matrix((
        [1, 0, -s.R_w, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [-1, 0, 0, 1, 0, (s.L_wf * sp.sin(s.beta) + s.W_wf * sp.cos(s.beta))],
        [0, -1, 0, 0, 1, (-s.L_wf * sp.cos(s.beta) + s.W_wf * sp.sin(s.beta))]
    ))

    block1 = Matrix(([M, A], [B, zeros(4, 4)]))
    variables = Matrix([dd_x, F_c])
    block2 = Matrix([F_ext, a])

    system = block1 * variables - block2
    pprint(system)


if __name__ == "__main__":
    system_of_equations()