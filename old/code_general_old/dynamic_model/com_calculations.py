import sympy as sp
from numpy import rad2deg
import rospkg
import yaml
import copy

## Solve SYMBOLIC
#@ Make SYMBOLIC variables
L_wh, L_b, L_fc, L_bc, L_wf = sp.symbols("L_wh L_b L_fc L_bc L_wf")
W_f, W_b, W_fc, W_bc, W_wf = sp.symbols("W_f W_b W_fc W_bc W_wf")
m_f, m_b = sp.symbols("m_f m_b")
L_h, R_w, beta = sp.symbols("L_h R_w beta")

#@ Make systems of equations
eqs = [
        sp.Eq(L_fc, sp.Rational(1,2)*L_wh),
        sp.Eq(L_bc, sp.Rational(1,3)*L_b),
        sp.Eq(L_wf, m_f/(m_f+m_b)*L_fc + m_b/(m_f+m_b)*L_bc),
        sp.Eq(W_bc, sp.Rational(1,3)*W_b + sp.Rational(1,2)*W_f),
        sp.Eq(W_wf, m_b/(m_f+m_b)*W_bc)
    ]

impl_vars = [L_fc, L_bc, L_wf, W_bc, W_wf]

#@ Solve
sol_sym = sp.solve(eqs, impl_vars)

#@ Group 
fc_loc_sym = sp.Matrix([sol_sym[L_fc], 0]) #@ Check
bc_loc_sym = sp.Matrix([sol_sym[L_bc], sol_sym[W_bc]]) #@ Check
tc_loc_sym = sp.Matrix([sol_sym[L_wf], sol_sym[W_wf]]) #@ Check
loc_sym = [fc_loc_sym, bc_loc_sym, tc_loc_sym]

# #@ Transform to global
T = sp.Matrix([[sp.cos(beta), -sp.sin(beta)], [sp.sin(beta), sp.cos(beta)]])

fc_glob_sym = T*fc_loc_sym
bc_glob_sym = T*bc_loc_sym
tc_glob_sym = T*tc_loc_sym
glob_sym = [fc_glob_sym, bc_glob_sym, tc_glob_sym]


## Solve numeric
#@ Import/set parameter values
description_path = rospkg.RosPack().get_path('trolley_description')
model_params = yaml.safe_load(
    open(description_path + "/config/model_params.yaml"))

L_f_ = model_params['frame_height']
W_f_ = model_params['frame_depth']
L_b_ = model_params['bag_height']
W_b_ = model_params['bag_depth']
L_h_ = model_params['handle_height']
R_w_ = model_params['wheel_radius']
m_f_ = 0.9
m_b_ = 1.2

beta_ = sp.asin((L_h_ - R_w_)/L_f_)

#@ Solve local
fc_loc = fc_loc_sym.subs([(L_wh, L_f_), (L_b, L_b_), (W_f, W_f_), (W_b, W_b_), (m_f, m_f_), (m_b, m_b_)])
bc_loc = bc_loc_sym.subs([(L_wh, L_f_), (L_b, L_b_), (W_f, W_f_), (W_b, W_b_), (m_f, m_f_), (m_b, m_b_)])
tc_loc = tc_loc_sym.subs([(L_wh, L_f_), (L_b, L_b_), (W_f, W_f_), (W_b, W_b_), (m_f, m_f_), (m_b, m_b_)])
loc = [fc_loc, bc_loc, tc_loc]

#@ Solve global
fc_glob = fc_glob_sym.subs([(L_wh, L_f_), (L_b, L_b_), (W_f, W_f_), (W_b, W_b_), (m_f, m_f_), (m_b, m_b_), (beta, beta_)])
bc_glob = bc_glob_sym.subs([(L_wh, L_f_), (L_b, L_b_), (W_f, W_f_), (W_b, W_b_), (m_f, m_f_), (m_b, m_b_), (beta, beta_)])
tc_glob = tc_glob_sym.subs([(L_wh, L_f_), (L_b, L_b_), (W_f, W_f_), (W_b, W_b_), (m_f, m_f_), (m_b, m_b_), (beta, beta_)])
glob =  [fc_glob, bc_glob, tc_glob]


## Change of T wrt F in global coordinate system:
#@ SYMBOLIC
diff_FT_sym = fc_glob_sym - tc_glob_sym
f_glob_sym = T[:,0]*L_wh
diff_FT_ratio_sym = [diff_FT_sym[0]/f_glob_sym[0], diff_FT_sym[1]/f_glob_sym[1]]

#@ Numeric
diff_FT = fc_glob - tc_glob
f_glob = T[:,0].subs(beta, beta_)*L_f_
diff_FT_ratio = [diff_FT[0]/f_glob[0], diff_FT[1]/f_glob[1]]

## Print
def print_resuL_ts(title, rows, columns, data):
    rows = [columns[0]] + rows
    data = [columns[1:]] + data

    print(title)
    for i in range(len(rows)):
        print("{:<5} {:<90} {:<90}".format(rows[i], data[i][0], data[i][1]))
        if i == 0:
            print("-" * 165)
    print("")

columns_loc = ["", "L", "W"]
columns_glob = ["", "x", "y"]
rows = ["FC" , "BC", "WF"]

print("\n")
print_resuL_ts("LOCAL SYMBOLIC VARIABLES:", rows, columns_loc, loc_sym)
print_resuL_ts("LOCAL VARIABLES [m]:", rows, columns_loc, loc)
print_resuL_ts("GLOBAL SYMBOLIC VARIABLES:", rows, columns_glob, glob_sym)
print_resuL_ts("GLOBAL VARIABLES [m]:", rows, columns_glob, glob)
print_resuL_ts("PARAMETER VALUES [m]:", ["F", "B"], columns_loc, [[L_f_, W_f_], [L_b_, W_b_]])
print_resuL_ts("GLOBAL DIFFERENCE IN POSITION OF F AND T [m]:", ["T"], columns_glob, [diff_FT])
print_resuL_ts("GLOBAL DIFFERENCE RATIO IN POSITION OF F AND T:", ["T"], columns_glob, [diff_FT_ratio])
print("BETA [deg]: \n{}\n".format(rad2deg(float(beta_))))
