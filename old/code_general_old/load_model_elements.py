import sympy as sp
import yaml
import rospkg
from math import asin
from code_general.toolbox import *
from pprint import pprint
from copy import deepcopy
from numbers import Number


## EQUATIONS -------------------------------------------------------------------------------------
def get_eqs_COM(s=None):  #~ List of equations for calculating trolley center of mass position
	""" s = class with symbolic variables """
	if s is None: s = ParamsSymbolic(expand=['m_ft'])

	eqs = [
		sp.Eq(s.L_fc, 1. / 2. * s.L_wh),
		sp.Eq(s.L_bc, 1. / 3. * s.L_b),
		sp.Eq(s.L_wf, s.m_f / (s.m_ft) * s.L_fc + s.m_b / (s.m_ft) * s.L_bc),
		sp.Eq(s.W_bc, 1. / 3. * s.W_b + 1. / 2. * s.W_f),
		sp.Eq(s.W_wf, s.m_b / (s.m_ft) * s.W_bc)
	]

	# eqs = subs_multi(eqs, [s.m_ft], [s.eq_m_ft])
	return eqs


def get_eqs_motion(s=None, replace_derivatives=True):  #~ List of equations describing motion
	""" s = class with symbolic variables """
	if s is None: s = ParamsSymbolic()

	#% Get correct variables
	if replace_derivatives: subfix = ''
	else: subfix = '_t'

	names = 'dd_xw{0} dd_yw{0} dd_theta{0} dd_xf{0} dd_yf{0} dd_beta{0} beta{0} '.format(subfix)
	dd_xw, dd_yw, dd_theta, dd_xf, dd_yf, dd_beta, beta = getattr_multi(s, names)

	#% Formulate equations
	eqs = [
		#@ Wheel:
		sp.Eq(s.m_w * dd_xw, s.F_wx + s.F_stat - sp.sin(s.gamma) * s.F_wg),  #@ Fx = 0
		sp.Eq(s.m_w * dd_yw, s.F_n - s.F_wy - sp.cos(s.gamma) * s.F_wg),  #@ Fy = 0
		sp.Eq(s.I_w * dd_theta, s.T_m - s.T_roll - s.R_w * s.F_stat),
		#@ Frame:
		sp.Eq(s.m_ft * dd_xf, s.F_hx - 2 * s.F_wx - sp.sin(s.gamma) * s.F_fg),  #@ Fx = 0
		sp.Eq(s.m_ft * dd_yf, s.F_hy + 2 * s.F_wy - sp.cos(s.gamma) * s.F_fg),  #@ Fy = 0
		sp.Eq(s.I_f * dd_beta,
		-sp.sin(beta) * s.L_wh * s.F_hx + sp.cos(beta) * s.L_wh * s.F_hy + 2 * s.T_m -
		(sp.cos(beta + s.gamma) * s.L_wf - sp.sin(beta + s.gamma) * s.W_wf)
		* s.F_fg)  #@ M = 0 elaborate
	]

	# eqs = subs_multi(eqs, (s.m_ft, s.F_fg, s.F_wg, s.I_w), (s.m_ft_dep, s.F_fg_dep, s.F_wg_dep, s.I_w_dep))
	return eqs


def get_eqs_constraint(s=None, add=False,
	replace_derivatives=True):  #~ List of equations describing constraints
	"""
    s =     class with symbolic variables
    add =     bool - indicator if additional constraint needs to be added
    replace_derivatives =    bool - indicater for replacing derivatives with regular variables
    """
	if s is None: s = ParamsSymbolic()

	xw = s.R_w * s.theta_t
	yw = 0.
	xf = s.xw_t + sp.cos(-s.beta_t) * s.L_wf - sp.sin(s.beta_t) * s.W_wf
	yf = s.yw_t + sp.sin(s.beta_t) * s.L_wf + sp.cos(s.beta_t) * s.W_wf

	eqs = [
		sp.Eq(s.dd_xw, xw.diff(s.t, 2)),
		sp.Eq(s.dd_yw, 0),
		sp.Eq(s.dd_xf, xf.diff(s.t, 2)),
		sp.Eq(s.dd_yf, yf.diff(s.t, 2))
	]

	if add:
		C = s.yw_t + sp.sin(s.beta_t) * s.L_wh  #@ Handle height constant
		eqs.append(sp.Eq(0, C.diff(s.t, 2)))

	if replace_derivatives:
		old = [s.dd_beta_t, s.d_beta_t, s.beta_t, s.dd_theta_t, s.dd_xw_t, s.dd_yw_t]
		new = [s.dd_beta, s.d_beta, s.beta, s.dd_theta, s.dd_xw, s.dd_yw]
		eqs = subs_multi(eqs, old, new)

	return eqs


def get_model_odes(s=None):
	print("Obtaining model ode's...")
	if s is None: s = ParamsSymbolic()

	x_sym = [s.xw, s.yw, s.theta, s.xf, s.yf, s.beta]
	dd_x_sym = [s.dd_xw, s.dd_yw, s.dd_theta, s.dd_xf, s.dd_yf, s.dd_beta]
	vars_implicit = [s.F_wx, s.F_wy, s.F_stat, s.F_n]
	dd_vars = dd_x_sym + vars_implicit

	eqs_motion = get_eqs_motion(s)
	eqs_constr = get_eqs_constraint(s)
	eqs = eqs_motion + eqs_constr
	sol = sp.solve(eqs, dd_vars)
	print(sol)
	expanded = {key: expand_to_variables(value, x_sym) for key, value in sol.items()}
	print_list(sol)

	return sol, expanded


## CLASSES --------------------------------------------------------------------------------------
class ParamGroups:  #~ Contains groups referencing parameters
	"""
    Inputs:  None
    Contains:  Groups referencing parameter names
    """

	def __init__(s):
		s.params_uncertain = ('mu', 'm_b', 'L_h')
		s.param_certain = ('L_wh', 'L_b', 'W_f', 'W_b', 'R_w', 'm_f', 'g')
		s.params_dependent = ('m_ft', 'F_fg', 'F_wg', 'L_wf', 'W_wf', 'beta', 'I_w')

		s.params_inputs = ('F_hx', 'F_hy', 'T_m')
		s.params_forces = s.params_inputs + ('F_fg', 'F_wg', 'T_roll')
		s.params_position = ('xw', 'yw', 'theta', 'xf', 'yf', 'beta')
		s.params_velocity = ('d_xw', 'd_yw', 'd_theta', 'd_xf', 'd_yf', 'd_beta')
		s.params_acceleration = ('dd_xw', 'dd_yw', 'dd_theta', 'dd_xf', 'dd_yf', 'dd_beta')
		s.params_implicit = ('F_wx', 'F_wy', 'F_stat', 'F_n')


#%---------------------------------------------------------------------------
class ParamsUncertain:  #~ Contains information about uncertain parameters + functions to obtain avg, lim, etc.
	"""
    Inputs:      None
    Contains:
    - values for uncertain parameters
    - functions:
      - get_lin(param):  returns min and max (tuple)
      - get_avg(param):  returns avg value (float)
      - get_params_lim(): returns limits of all uncertain params (list of tuples)
      - get_params_avg():  returns averages of all uncertain params (list)
      - get_params_min:  returns minima of all uncertain params (list)
      - get_params_nom():  returns nominal values of all uncertain params (list)
    """

	def __init__(s):
		_description_path = rospkg.RosPack().get_path('trolley_description')
		_model_params = yaml.safe_load(open(_description_path + "/config/model_params.yaml"))

		#% Wheel mass m_w
		s.m_w_tot = _model_params['wheel_mass_total']
		s.m_w_nom = _model_params['wheel_mass_nom']

		#% Friction coefficient mu
		s.mu_min = _model_params['friction_min']
		s.mu_max = _model_params['friction_max']
		s.mu_nom = _model_params['friction_nom']

		#% Bag mass m_b
		s.m_b_bag = _model_params['bag_mass']
		s.m_b_min = s.m_b_bag + _model_params['bag_mass_added_min']
		s.m_b_max = s.m_b_bag + _model_params['bag_mass_added_max']
		s.m_b_nom = s.m_b_bag + _model_params['bag_mass_added_nom']

		#% Handle height L_h
		s.L_h_min = _model_params['handle_height_min']
		s.L_h_max = _model_params['handle_height_max']
		s.L_h_nom = _model_params['handle_height_nom']

	@property
	def mu_avg(s):
		return s.get_avg('mu')

	@property
	def m_b_avg(s):
		return s.get_avg('m_b')

	@property
	def L_h_avg(s):
		return s.get_avg('L_h')

	def get_lim(s, name):
		"""return list of limit values of specified uncertain parameter"""
		return (getattr(s, name + '_min'), getattr(s, name + '_max'))

	def get_avg(s, name):
		"""return nominal value of specified uncertain parameter"""
		return avg(*s.get_lim(name))

	def get_params_lim(s):
		"""return list of list of limits of uncertain parameter values"""
		return [s.get_lim(name) for name in ParamGroups().params_uncertain]

	def get_params_avg(s):
		"""return list of average uncertain parameter values"""
		return [s.get_avg(name) for name in ParamGroups().params_uncertain]

	def get_params_min(s):
		"""return list of minimal uncertain parameter values"""
		return [getattr(s, name + '_min') for name in ParamGroups().params_uncertain]

	def get_params_max(s):
		"""return list of maximal uncertain parameter values"""
		return [getattr(s, name + '_max') for name in ParamGroups().params_uncertain]

	def get_params_nom(s):
		"""return list of nominal uncertain parameter values"""
		return [getattr(s, name + '_nom') for name in ParamGroups().params_uncertain]


#%---------------------------------------------------------------------------
class ParamsNumeric:  #~ Contains parameter values, yet undefined set to 'None', + function to obtain all defined parameters.
	""" Contains parameter values, undefined set to 'None'.

    - Inputs:
      - m_w, m_b, L_h, mu, F_hx, F_hy, T_m and gamma:  Which uncertain param value to set as default value. Default for m_w is m_w_tot, others are 'None'.

    - Functions:
      - params_defined: obtain all defined parameters (dict)
      - params_defined_names: obtain all defined parameter names (list)
      - params_defined_values: obtain all defined parameter values (list)
      - set_params_type(par_type): set param to 'par_type' (str)
      - set_params_min(): set_params_type('min') for all uncertain params
      - set_params_max(): set_params_type('max') for all uncertain params
      - set_params_avg(): set_params_type('avg') for all uncertain params
      - set_params_nom(): set_params_type('nog') for all uncertain params
    """

	def __init__(
			s,
			m_w='m_w_tot',
			m_b=None,
			L_h=None,
			mu=None,
			F_hx=None,
			F_hy=None,
			T_m=None,
			gamma=None):
		inputs = deepcopy(locals())
		del inputs['s']

		_description_path = rospkg.RosPack().get_path('trolley_description')
		_model_params = yaml.safe_load(open(_description_path + "/config/model_params.yaml"))

		#region ## Parameters
		s.par_unc = ParamsUncertain()

		#region #% Independent params
		s.L_wh = _model_params['frame_height']
		s.W_f = _model_params['frame_depth']
		s.L_b = _model_params['bag_height']
		s.W_b = _model_params['bag_depth']
		s.R_w = _model_params['wheel_radius']
		s.m_f = _model_params['frame_mass']
		s.g = _model_params['gravity']
		s.d = _model_params['damping']

		s.m_w = None
		s.m_b = None
		s.L_h = None
		s.mu = None

		s.F_hx = None
		s.F_hy = None
		s.T_m = None
		s.gamma = None
		#endregion Inputs
		s.__set_uncertain_params(inputs)

	#region #% Dependent params
	@property
	def m_ft(s):
		try:
			return s.m_f + s.m_b
		except:
			return None

	@property
	def m_acc(s):
		try:
			return s.m_ft + 3 * s.m_w
		except:
			return None

	@property
	def F_fg(s):
		try:
			return s.m_ft * s.g
		except:
			return None

	@property
	def F_wg(s):
		try:
			return s.m_w * s.g
		except:
			return None

	@property
	def L_wf(s):
		try:
			return 1. / s.m_ft * (s.m_f / 2. * s.L_wh + s.m_b / 3. * s.L_b)
		except:
			return None

	@property
	def W_wf(s):
		try:
			return s.m_b / s.m_ft * (s.W_f / 2. + s.W_b / 3.)
		except:
			return None

	@property
	def L_h_acc(s):
		try:
			return s.L_h - s.R_w
		except:
			return None

	@property
	def beta(s):
		try:
			return sp.asin((s.L_h - s.R_w) / s.L_wh)
		except:
			return None

	@property
	def I_w(s):
		try:
			return 0.5 * s.m_w * s.R_w**2
		except:
			return None

	@property
	def I(s):
		try:
			return s.R_w**2 * (s.m_ft + 3. * s.m_w)
		except:
			return None

	#endregion
	#endregion

	#region  ## Functions
	def __set_uncertain_params(s, inputs):  #~ Set params as specified in class input
		for name, value in inputs.items():
			if isinstance(value, str):  #@ Get value from uncertain parameter class
				setattr(s, name, getattr(s.par_unc, value))
			else:  #@ Set value directly
				setattr(s, name, value)

	def __set_params_type(s, par_type):  #~ Set uncertain params to given type (min, max, etc.)
		for param in ParamGroups().params_uncertain:
			s.set_unc_params_to_type(param, par_type)

	@property
	def params_defined(s):
		"""Returns dict with defined params and their values"""
		return {name: value for name, value in s.__dict__.items() if isinstance(value, Number)}

	@property
	def params_defined_names(s):
		"""Returns list of strings with names"""
		defined_params = s.params_defined
		return defined_params.keys()

	@property
	def params_defined_values(s):
		defined_params = s.params_defined
		return defined_params.values()

	def set_params_min(s):
		s.__set_params_type('min')

	def set_params_max(s):
		s.__set_params_type('max')

	def set_params_avg(s):
		s.__set_params_type('avg')

	def set_params_nom(s):
		s.__set_params_type('nom')

	def set_unc_params_to_type(s, param, par_type):
		setattr(s, param, getattr(s.par_unc, param + '_' + par_type))

	#endregion Functions


#%---------------------------------------------------------------------------
class ParamsSymbolic:  #~ Contains symbolic variable definitions and dependencies + function to expand dependent variables
	""" Symbolic variable definitions and dependencies.
    - Inputs:
      - expand - Whether to expand depentent variables.
          Options:
          * False (def) = expand default: 'T_roll'
        * True = expand everything
        * None = not expand
        * List with specific params

    - Functions:
      - expand(params):  Expand dependent params (None).
        Same options as input
      - get_symbolic_params(params):  Returns symbolic params from string (list)
    """

	def __init__(s, expand=False, prefix=None):
		if expand is False:
			expand = ['T_roll']  #@ Default

		## Parameters
		#% Dimensions
		s.L_wh, s.W_f, s.L_b, s.W_b, s.L_h, s.R_w = \
            sp.symbols("L_wh W_f L_b W_b L_h R_w", real=True, nonnegative=True)
		s.L_bc, s.W_bc, s.L_fc, s.L_wf, s.W_wf = \
            sp.symbols('L_bc W_bc L_fc L_wf W_wf', real=True, nonnegative=True)

		#% Angles
		s.gamma = \
            sp.symbols('gamma', real=True)

		#% Masses and inertial
		s.m_f, s.m_b, s.m_w, s.m_ft, s.I_f, s.I_w, m = \
            sp.symbols('m_f m_b m_w m_ft I_f I_w m', real=True, nonnegative=True)

		#% Forces
		s.F_hx, s.F_hy, s.T_m, s.F_fg, s.F_wg, s.F_wx, s.F_wy, s.F_n, s.F_stat, s.T_roll = \
            sp.symbols('F_hx F_hy T_m F_fg F_wg F_wx F_wy F_n F_stat T_roll', real=True)

		#% Other
		s.mu, s.g, s.C, s.d = \
            sp.symbols('mu g C d', real=True, nonnegative=True)

		#% Variables
		s.xw, s.yw, s.theta, s.xf, s.yf, s.beta = \
            sp.symbols('xw yw theta xf yf beta', real=True)
		s.d_xw, s.d_yw, s.d_theta, s.d_xf, s.d_yf, s.d_beta = \
            sp.symbols('d_xw d_yw d_theta d_xf d_yf d_beta', real=True)
		s.dd_xw, s.dd_yw, s.dd_theta, s.dd_xf, s.dd_yf, s.dd_beta = \
            sp.symbols('dd_xw dd_yw dd_theta dd_xf dd_yf dd_beta', real=True)

		## Dependent
		#% Dependent on other variables (dep. on indep. variables)
		s.m_ft_dep = (s.m_f + s.m_b)
		s.F_wg_dep = (s.m_w * s.g)
		s.I_w_dep = (0.5 * s.m_w * s.R_w**2)
		s.beta_dep = (sp.asin((s.L_h - s.R_w) / s.L_wh))
		s.L_h_acc = (s.L_h - s.R_w)
		s.T_roll_dep = (s.mu * s.F_n * s.R_w)

		#% Of t
		s.t = sp.symbols('t')
		s.xw_t, s.yw_t, s.theta_t, s.xf_t, s.yf_t, s.beta_t = \
            sympy_function_multi(['xw yw theta xf yf beta'], s.t)
		s.d_xw_t, s.d_yw_t, s.d_theta_t, s.d_xf_t, s.d_yf_t, s.d_beta_t = \
            sympy_derivative_multi([s.xw_t, s.yw_t, s.theta_t, s.xf_t, s.yf_t, s.beta_t], s.t)
		s.dd_xw_t, s.dd_yw_t, s.dd_theta_t, s.dd_xf_t, s.dd_yf_t, s.dd_beta_t = \
            sympy_derivative_multi([s.xw_t, s.yw_t, s.theta_t, s.xf_t, s.yf_t, s.beta_t], s.t, 2)

		s.expand(expand)
		if prefix is not None:
			s.set_prefix(prefix)

	#region  ## Parameter dependencies (dep. on dep. variables)
	@property
	def m_acc(s):
		return s.m_ft + 3 * s.m_w

	@property
	def F_fg_dep(s):
		return (s.m_ft * s.g)

	@property
	def L_wf_dep(s):
		return (1. / s.m_ft * (s.m_f / 2. * s.L_wh + s.m_b / 3. * s.L_b))

	@property
	def W_wf_dep(s):
		return (s.m_b / s.m_ft * (s.W_f / 2. + s.W_b / 3.))

	@property
	def L_dep(s):
		(s.L_wf * (sp.sin(s.beta) - s.mu * sp.cos(s.beta)) + s.W_wf *
			(s.mu * sp.sin(s.beta) + sp.cos(s.beta)))

	#endregion

	## Functions
	def expand(s, params=None):
		"""Expand dependent parameters"""
		if params is None:  #@ Do nothing
			return
		elif params is True:  #@ Expand all dependent parameters
			groups = ParamGroups()
			params = groups.params_dependent

		replacements = [getattr(s, param + '_dep') for param in params]
		params = [getattr(s, key) for key in params]

		while True:
			for key, value in s.__dict__.items():
				setattr(s, key, subs_multi(value, params, replacements))

			free_symbols = [
				str(entry) for par in s.__dict__ for entry in getattr(s, par).free_symbols
			]
			free_symbols = set(free_symbols)

			if not any(par in free_symbols for par in params):
				return

	def set_prefix(s, prefix=None):
		if prefix is None:  #@ Do nothing
			return
		for name, key in s.__dict__.items():
			if key.is_Atom:
				key.name = prefix + key.name
				setattr(s, name, key)

	def get_symbolic_params(s, params):
		"""Returns list with symbolic params from string"""
		return [getattr(s, param) for param in params]


#%---------------------------------------------------------------------------
class TFsAdaptedSymbolic:  #~ Contains Symbolic transfer functions and elements + function for explicitly filling in trolley transfer function
	"""
    Inputs:
     - expand:  List of dependent params to expand.
       Options:
        * None = not expand
        * True = expand everything
        * False (def) = expand default: 'T_roll'
        * List with specific params
    - Subs_plant:  Whether to explicitly fill in trolley TF

    Contains:
    - Symbolic transfer functions and elements
    - Functions:
      - subs_plant():  explicitly fill in trolley transfer function in others and save in class (none)
      - expand: Same as input expand
    """

	def __init__(s, expand=False, subs_plant=False):
		s.A, s.A_n, s.A_a = sp.symbols("A A_n A_a", nonnegative=True)
		s.B, s.G, s.B_n, s.G_n, s.B_a, s.G_a = sp.symbols('B G B_n G_n B_a G_a', real=True)
		s.Q = sp.symbols("Q")

		s.sym = ParamsSymbolic(expand)

		s.TF_L = s.G * s.Q / (s.G_n * (1 - s.Q))
		s.TF_S = sp.simplify(1 / (s.TF_L + 1))
		s.TF_T = sp.simplify(s.TF_L / (s.TF_L + 1))

		beta = sp.asin((s.sym.L_h - s.sym.R_w) / s.sym.L_wh)
		I = s.sym.R_w**2 * (s.sym.m_ft + 3. * s.sym.m_w)
		s.TF_G = 2. / I * (1. - (s.sym.mu * s.sym.R_w) / (s.sym.L_wh * sp.cos(beta)))

		if subs_plant:
			s.subs_plant()

	def subs_plant(s):
		s.TF_L = s.TF_L.subs(s.G, s.TF_G)
		s.TF_S = s.TF_S.subs(s.G, s.TF_G)
		s.TF_T = s.TF_T.subs(s.G, s.TF_G)

	def expand(s, params=None):
		s.sym.expand(s, params)


#%---------------------------------------------------------------------------
class TFsAdaptedNumeric:
	#region  description

	"""
    Contains adapted transfer functions filled with numeric values.

    - Inputs:
      - par_type: which type of uncertain param value to fill in
          Options:
          * None: don't fill
          *'min'\'max'\'avg'\'nom'

      - expand: Whether to expand depentent variables.
          Options:
          * True:     expand all
          * False:     expand default: 'T_roll'
          * None:     don't expand
          * List with specific params to expand

      - subs_plant: Whether to expand G to function

    - Functions:
      - fill_params(function):  fill in parameter values for symbolic parameters.
    """

	#endregion

	def __init__(s, par_type=None, expand=True, subs_plant=True):

		if isinstance(expand, str):  #@ Check if single param is given
			expand = [expand]

		s.TFs = TFsAdaptedSymbolic(expand=expand, subs_plant=subs_plant)
		s.par_num = ParamsNumeric()

		#% Fill param values in TFs
		if par_type is not None:
			s.par_num.set_params_type(par_type)

	#region  ## Properties
	@property
	def G(s):
		return s.fill_params('TF_G')

	@property
	def L(s):
		return s.fill_params('TF_L')

	@property
	def S(s):
		return s.fill_params('TF_S')

	@property
	def T(s):
		return s.fill_params('TF_T')

	#endregion Properties

	## Functions
	def fill_params(s, function):
		""" Return function with params replaced by values from par_num. """
		TF = getattr(s.TFs, function)
		params_sym = TF.free_symbols  #@ Which params are not set yet
		params_sym.discard(s.TFs.Q)  #@ Remove Q from params to be replaced

		replaced = []
		replacements = []
		not_replaced = []
		for param in params_sym:
			if param == s.TFs.G_n:  #@ Get TF value
				replacement = TFsAdaptedNumeric(par_type='nom').G
			else:  #@ Get param value
				replacement = getattr(s.par_num, str(param))

			if replacement is None:
				not_replaced.append(param)
			else:
				replaced.append(param)
				replacements.append(replacement)

		TF = subs_multi(TF, replaced, replacements)

		if len(not_replaced) != 0:
			print(
				"WARNING: Whilst obtaining numeric {}: Params '{}' are not replaced since no value is set."
				.format(function, not_replaced))
		return TF
		# replacements = [getattr(s.par_num, param) for param in params]
		# params = [getattr(s.TF_sym.sym, param) for param in params]

		# fun_filled = subs_multi(getattr(s.TF_sym), params, replacements)
		# return fun_filled

	def set_params_min(s):
		s.par_num.set_params_min()

	def set_params_max(s):
		s.par_num.set_params_max()

	def set_params_avg(s):
		s.par_num.set_params_avg()

	def set_params_nom(s):
		s.par_num.set_params_nom()


#%---------------------------------------------------------------------------
class TrolleyModelTime:

	def __init__(self, s=None):
		if s is None:
			s = ParamsSymbolic()

		self.unconstrained_T_roll_explicit = (-2.0 * s.F_wg * s.I_f * s.R_w * sp.sin(s.gamma)
			+ s.I_f * s.R_w * s.d_beta**2 * s.m_ft *
			(s.L_wf * sp.cos(s.beta) - s.W_wf * sp.sin(s.beta)) - s.I_f * s.R_w *
			(s.F_fg * sp.sin(s.gamma) - s.F_hx) + 2.0 * s.I_f *
			(s.T_m - s.T_roll) + s.R_w * s.m_ft *
			(s.L_wf * sp.sin(s.beta) + s.W_wf * sp.cos(s.beta)) *
			(-s.F_fg * s.L_wf * sp.cos(s.beta + s.gamma)
			+ s.F_fg * s.W_wf * sp.sin(s.beta + s.gamma) - s.F_hx * s.L_wh * sp.sin(s.beta)
			+ s.F_hy * s.L_wh * sp.cos(s.beta) + 2.0 * s.T_m)) / (s.I_f * s.R_w *
			(s.m_ft + 3.0 * s.m_w)) * s.m_acc

		self.unconstrained_T_roll_implicit = (-2.0 * s.F_wg * s.I_f * s.R_w * s.mu * sp.cos(s.gamma)
			- 2.0 * s.F_wg * s.I_f * s.R_w * sp.sin(s.gamma)
			+ s.I_f * s.R_w * s.d_beta**2 * s.m_ft * s.mu *
			(s.L_wf * sp.sin(s.beta) + s.W_wf * sp.cos(s.beta))
			+ s.I_f * s.R_w * s.d_beta**2 * s.m_ft *
			(s.L_wf * sp.cos(s.beta) - s.W_wf * sp.sin(s.beta)) - s.I_f * s.R_w * s.mu *
			(s.F_fg * sp.cos(s.gamma) - s.F_hy) - s.I_f * s.R_w *
			(s.F_fg * sp.sin(s.gamma) - s.F_hx) + 2.0 * s.I_f * s.T_m + s.R_w * s.m_ft *
			(-s.L_wf * s.mu * sp.cos(s.beta) + s.L_wf * sp.sin(s.beta)
			+ s.W_wf * s.mu * sp.sin(s.beta) + s.W_wf * sp.cos(s.beta)) *
			(-s.F_fg * s.L_wf * sp.cos(s.beta + s.gamma)
			+ s.F_fg * s.W_wf * sp.sin(s.beta + s.gamma) - s.F_hx * s.L_wh * sp.sin(s.beta)
			+ s.F_hy * s.L_wh * sp.cos(s.beta) + 2.0 * s.T_m)) / (s.I_f * s.R_w *
			(s.m_ft + 3.0 * s.m_w)) * s.m_acc

		self.constrained_T_roll_explicit = (-s.F_fg * s.R_w * sp.sin(s.gamma) + s.F_hx * s.R_w
			- 2.0 * s.F_wg * s.R_w * sp.sin(s.gamma) + s.L_wf * s.R_w * s.d_beta**2 * s.m_ft
			/ sp.cos(s.beta) + 2.0 * s.T_m - 2.0 * s.T_roll) / (s.R_w *
			(s.m_ft + 3.0 * s.m_w)) * s.m_acc

		self.constrained_T_roll_implicit = (
			s.F_fg * s.L_wf * s.R_w * s.mu * sp.cos(s.beta + s.gamma) / sp.cos(s.beta) - s.F_fg
			* s.L_wh * s.R_w * s.mu * sp.cos(s.gamma) - s.F_fg * s.L_wh * s.R_w * sp.sin(s.gamma)
			- s.F_fg * s.R_w * s.W_wf * s.mu * sp.sin(s.beta + s.gamma) / sp.cos(s.beta)
			+ s.F_hx * s.L_wh * s.R_w * s.mu * sp.tan(s.beta) + s.F_hx * s.L_wh * s.R_w
			- 2.0 * s.F_wg * s.L_wh * s.R_w * s.mu * sp.cos(s.gamma)
			- 2.0 * s.F_wg * s.L_wh * s.R_w * sp.sin(s.gamma)
			+ s.I_f * s.R_w * s.d_beta**2 * s.mu * sp.sin(s.beta) / sp.cos(s.beta)**2
			+ s.L_wf * s.L_wh * s.R_w * s.d_beta**2 * s.m_ft / sp.cos(s.beta)
			+ s.L_wh * s.R_w * s.W_wf * s.d_beta**2 * s.m_ft * s.mu / sp.cos(s.beta) +
			2.0 * s.L_wh * s.T_m - 2.0 * s.R_w * s.T_m * s.mu / sp.cos(s.beta)) / (s.L_wh * s.R_w *
			(s.m_ft + 3.0 * s.m_w)) * s.m_acc

	def get_model_numeric(self, model, replaced=[], replacements=[]):
		model = getattr(self, model)
		sym = ParamsSymbolic()
		num = ParamsNumeric()

		#% Expand variables and fill in numeric values for known variables
		eq_names = [sym.m_ft, sym.F_fg, sym.F_wg, sym.L_wf, sym.W_wf]
		eq_values = [getattr(sym, "eq_" + str(x)) for x in eq_names]

		var_names = list(sym.knowns)
		var_values = [getattr(num, str(x)) for x in var_names]

		names = eq_names + list(var_names) + list(replaced)
		values = eq_values + var_values + replacements

		model = subs_multi(model, names, values)
		return model


##--------------------------------------------------------------------------------------
if __name__ == "__main__":
	sol, expanded = get_model_odes()

	print()
