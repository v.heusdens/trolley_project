import numpy as np
import matplotlib.pyplot as plt
from control import matlab, TransferFunction as tf
import control
import time
import copy
import warnings

def plot_data(data_pre, data_post):
    t_pre = np.linspace(0, len(data_pre)/f_s, len(data_pre), endpoint=False)
    t_post = np.linspace(0, len(data_post)/f_s, len(data_post), endpoint=False)

    fig, ax = plt.subplots(1, 1, sharex=True, squeeze=True)
    ax.stem(t_pre, data_pre, use_line_collection=True, label="before processing")
    ax.stem(t_post, data_post, use_line_collection=True, label="after processing", linefmt="C1--", markerfmt="C1o")

    ax.set_ylabel("Magnitude")
    ax.legend()
    plt.show()
    return

def process(dw):
    print("processing data-window...")

    DW_pad = np.fft.fft(dw, tw + pad)
    DW_pad_ = DW_pad*IMP_pad
    dw_pad_ = np.fft.ifft(DW_pad_)
    
    print("finished processing data-window.")
    return dw_pad_

"""Make x and t"""
f_s = 1  # sampling frequency, must be int
dur = 30  # duration in seconds
tw = 10 # samples

input_data = np.ones(dur*f_s)
input_data[0] = 0
input_data[9] = 0

"""Make filter"""
# = z-transform of 0.5*(x[n] + x[n-1])
filter = tf(np.array([1, 1]), np.array([2, 0]), 1/f_s)
t_imp, imp = control.impulse_response(filter)

pad = len(t_imp)
if pad > tw:
    raise Exception("padding length longer than time-window. Data from different data-windows will not be concatenated correctly. Please expand time-window.")
IMP_pad = np.fft.fft(imp, tw + pad)


""" Simulate real-time data acquisition """
dw = []
res = np.zeros((tw), dtype=complex)

for data in input_data:
    dw.append(data)
    print("data-window: {}".format(dw))

    if len(dw) == tw:
        print("data-window complete.")
        dw_pad_ = process(dw)

        dw_ = dw_pad_[:-pad] + res
        res[:pad] = dw_pad_[-pad:]

        plot_data(dw, dw_.real)
        print("start collecting new data window.")
        dw = []

    time.sleep(1/f_s)