import sympy as sp
import numpy as np
from sympy import S, I
from control import TransferFunction as tf
from python_scripts.data_analysis.data_plot_functions import bode_plot
from python_scripts.toolbox import *
from python_scripts.load_model_elements import *

def stability_analysis():
    print("Running stability analysis... ")

    #@ Q = 1/ (s - b)
    b = sp.symbols('b', real=True)
    G_a = TFsAdaptedNumeric('avg').G
    G_n = TFsAdaptedNumeric('nom').G
    w = 0.741773187960721 #@ Calculated from uncertainty analysis

    eq = sp.StrictGreaterThan(abs(G_a - G_n*b - G_n),     G_a * w)

    print('Objective equation:')
    print('    {} > {}'.format(*eq.args))
    sol = sp.solveset(eq, b, domain=S.Reals)
    print("Result:")
    print("    {}".format(sol))
    return sol

def stability_analysis_check(range_b):
    b = sp.symbols('b', real=True)
    omega, w = sp.symbols('omega w', nonnegative=True)
    G_a = TFsAdaptedNumeric('avg').G
    G_n = TFsAdaptedNumeric('nom').G
    w = 0.741773187960721 #@ Calculated from uncertainty analysis
    omega = 0

    Q = 1 / (omega*I - b)
    eq_inf_norm = abs(G_a * Q / (G_a * Q + G_n * (1 - Q)) * w)

    range_b_comp = sp.Complement(sp.Reals, range_b)
    b_bound = range_b_comp.boundary.args

    # Check for omega = 0 and range of b if eq_inf_norm indeed < 1
    scale = abs(b_bound[1] - b_bound[0])
    start = float(b_bound[0] - 0.2*scale)
    stop = float(b_bound[1] + 0.2*scale)
    b_vals = np.linspace(start, stop, num=100)
    print("Testing values of beta within range {}, {}.".format(start, stop))

    b_results = [eq_inf_norm.subs(b, b_val) for b_val in b_vals]

    #% check if as expected
    check = []
    false_positives = []
    false_negatives = []

    print("Checking if stability criterium holds for several values of b:")
    for b_val, b_result in zip(b_vals, b_results):
        if b_result < 1: #@ Stable
            check.append(True)
            if b_bound[0] <= b_val <= b_bound[1]: #@ False positive
                false_positives.append(b_val)
        else: #@ Instable
            check.append(False)
            if b_val < b_bound[0] or b_val > b_bound[1]: #@ False negative
                false_negatives.append(b_val)

    print("Found condition satisfactions for b:")
    print(check)

    if len(false_positives) == 0 and len(false_negatives) == 0:
        print("Test succeeded: All checked values are as expected:.")
    else:
        print("Test failed.")
        print("False positives:     {}".format(false_positives))
        print("False positives:     {}".format(false_negatives))





    print()



if __name__ == "__main__":
    # Q = 5 / (s + 5)
    sample_freq = 50
    b = 5
    Q_num = [b]
    Q_denom = [1, b]
    # Q_filter = tf(Q_num, Q_denom, 1./sample_freq)
    bode_plot(Q_num, Q_denom, b, name=True)


    print()
