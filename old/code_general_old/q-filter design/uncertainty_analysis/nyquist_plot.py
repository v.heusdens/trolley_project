import math

import numpy as np
import rospkg
import yaml
from matplotlib import pyplot as plt
from control import TransferFunction as tf
from control import StateSpace as ss
from control import nyquist
from scipy import signal
from ros_packages.trolley_description.config.load_model_params import *

m_ft2 = m_ft + 20

H_val = (mu*math.tan(beta) + 1)/(1*I_w + R_w**2*(m_ft + 2*m_w))*R_w


H_val2 = (mu*math.tan(beta) + 1)/(1*I_w + R_w**2*(m_ft + 2*m_w))*R_w
H = tf((mu*math.tan(beta) + 1)*R_w, 1*I_w + R_w**2*(m_ft2 + 2*m_w))
print("{} = {}".format(H, H_val2))

# H = tf([1],[1, 1])
real, imag, freq = nyquist(H, Plot=False)

plt.figure()
plt.plot(real, imag, 'bs', label="pos imag")
plt.plot(real, -imag, 'ro', label="neg imag")
plt.legend()
plt.grid('on')


# sys = ss("1. -2; 3. -4", "5.; 7", "6. 8", "9.")
# real, imag, freq = nyquist(sys)

plt.show()


