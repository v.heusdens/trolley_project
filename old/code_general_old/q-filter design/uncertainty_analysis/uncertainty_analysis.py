from __future__ import print_function

from pprint import pprint
import sympy as sp
from sympy.utilities.lambdify import lambdify
import numpy as np
from scipy.optimize import minimize
from matplotlib import pyplot as plt
import time
import itertools as iter

from code_general.toolbox import *
from code_general.load_model_elements import *

#% --------------------------------------------------------------------------------------
def TEST():
    x, y, phi, t = sp.symbols('x y phi t')
    l, m, I, g, Fs, Ma = sp.symbols('l m I g Fs Ma')
    s = sp.Function('s')(t)
    alpha = sp.Function('alpha')(t)
    q = [s, alpha]
    dq = derivative_multi(q, t)
    ddq = derivative_multi(q, t, 2)
    # dds = sp.diff(s, t, 2)

    T = np.array([
        s + l*sp.sin(alpha),
        -l*sp.cos(alpha),
        alpha
    ])

    dT_dq = derivative_multi(T, q)
    ddT_dqq = derivative_multi(dT_dq, q)

    c = np.squeeze(np.dot(np.squeeze(np.dot(ddT_dqq, dq)),dq)) 

    M = np.eye(3)*np.array([m, m, I])
    Mc =  np.dot(M, c)
    Mt = np.dot(dT_dq.T, np.dot(M, dT_dq))

    F = np.array([0, -m*g, 0])
    Q = [Fs, Ma]
    TMT_l = np.squeeze(np.dot(Mt, ddq))
    TMT_r = Q + np.dot(dT_dq.T, F - Mc)
    TMT = TMT_l - TMT_r
    
    print('solving...')
    sol = sp.solve(TMT, [sp.Derivative(s, t, 2), sp.Derivative(alpha, t, 2)])
    print('dds = {}'.format(sol[sp.Derivative(s, t, 2)]) )


def map_var(var_unconstr, var_min, var_max): #~ Map variables for constraint elimination
    var_avg = avg(var_min, var_max)
    var_mapped = var_avg + sp.sin(var_unconstr) * (var_max - var_avg)
    return var_mapped


def objective_function(G, sym, num): #~ Calculate symbolic optimization function
    """
    G: transfer function
    """
    ## Formulation of objective function
    f = - abs(G)

    ## Constraint elimination 
    mu_, m_b_, L_h_ = sp.symbols('mu_ m_b_ L_h_', real=True) #@ Unbounded parameters for mapping
    params_mapped = (mu_, m_b_, L_h_)

    #% Mapping of constrained to unconstrained parameters
    mu_mapped = map_var(mu_, *num.par_unc.get_lim('mu'))
    m_b_mapped = map_var(m_b_, *num.par_unc.get_lim('m_b'))
    L_h_mapped = map_var(L_h_, *num.par_unc.get_lim('L_h'))

    #% Implementing in model equation
    f_mapped = f.subs([(sym.mu, mu_mapped), (sym.m_b, m_b_mapped), (sym.L_h, L_h_mapped)])
    return f_mapped, params_mapped


def calc_parameter_uncertainty_radius(f, G, unc_params, sym, num): #~ Performs optimalization algorithm to determine uncertainty radius
    print("\n - Calculating parameter uncertainty radius...")

    if any(isinstance(x, str) for x in unc_params):
        unc_params = sym.get_symbolic_params(unc_params)
    
    #% Solve optimization problem
    vars_unc0 = num.par_unc.get_params_max() #@ Initial guess

    print("Running optimization...")
    sol = minimize(lambda x: f(*x), x0=vars_unc0, method='newton-CG', jac=lambda x: np.array(j(*x)), options={'disp': True}) 
    #@ Do not give hessian due to being unable to calculate - due to abs in objective_function function
    print("unbound solution: mu_ = {}    m_b_ = {}    L_h_ = {}".format(*sol.x))

    #% Map unconstrained values back to real parameter values
    mu_opt = map_var(sol.x[0], *num.par_unc.get_lim('mu'))
    m_b_opt = map_var(sol.x[1], *num.par_unc.get_lim('m_b'))
    L_h_opt = map_var(sol.x[2], *num.par_unc.get_lim('L_h'))
    params_unc_opt = (mu_opt, m_b_opt, L_h_opt)
    params_unc_opt = [round(par_opt, 5) for par_opt in params_unc_opt]

    print("\nBound solution: mu = {}    m_b = {}    L_h = {}".format(mu_opt, m_b_opt, L_h_opt))
    print("") #@ Tot hier gaat goed!

    #% Calculate parameter uncertainty radius
    G_lim = subs_multi(G, unc_params, params_unc_opt)
    G_a = subs_multi(G, unc_params, num.par_unc.get_params_avg())
    l = abs((G_lim - G_a)/G_a)
    print("radius l: {}".format(l))
    return l, params_unc_opt


def calc_max_added_weight(sym, num, G): #~ Calculated max added m_b to let w <= 1 
    #@ Uncertain parameters: mu, m_b, L_h
    mu_min = num.par_unc.mu_min
    mu_avg = num.par_unc.get_avg('mu')
    L_h_min = num.par_unc.L_h_min
    L_h_avg = num.par_unc.get_avg('L_h')
    m_b_min = num.par_unc.m_b_min
    m_b_max = sp.symbols('m_b_max_sym', nonnegative=True, real=True)
    m_b_avg = avg(m_b_min, m_b_max)

    G_a = G.subs([(sym.mu, mu_avg), (sym.L_h, L_h_avg), (sym.m_b, m_b_avg)])
    G_lim = G.subs([(sym.mu, mu_min), (sym.L_h, L_h_min), (sym.m_b, m_b_min)])
    l = abs((G_lim - G_a)/G_a)

    sol = sp.solve((l - 1), m_b_max)[0] #@ Solve for l = 1
    print('max total bag mass: {}'.format(sol))
    print('max added bag mass: {}'.format(sol - num.par_unc.m_b_bag))


def verify_optimization_results(sym, num, G, unc_params, opt_values): #~ Calculate plant value for various param combis and check if max value is obtained for calculated optimal param values
    print("\n - Verifying optimization results...")
    
    if isinstance(unc_params[0], str):
        unc_params = sym.get_symbolic_params(unc_params)
    
    G_opt = subs_multi(G, unc_params, opt_values)

    #% Generate param value combinations
    nr_of_points = 30

    lim_params = num.par_unc.get_params_lim()
    param_values = [np.linspace(lim[0], lim[1], nr_of_points) for lim in lim_params]
    combis = list(iter.product(*param_values))
    
    #% Calculate resulting values of G
    G_pos = [subs_multi(G, unc_params, combi) for combi in combis]
    G_opt_ = max(G_pos)
    combi_opt_ = combis[G_pos.index(G_opt_)]

    print('Checked {} possible parameter combinations.'.format(len(combis)))
    print('Max value of G calculated optimal params: {}'.format(G_opt))
    print('Max value of G from list: {}'.format(G_opt_))
    if G_opt >= G_opt_:
        print("Calculated optimum is highest. Calculated optimum verified!")
    else:
        print("Calculated optimum is NOT highest; highest value found for {} = {}. Calculated optimum debunked.".format(unc_params, combi_opt_))
    

## MAIN ##
if __name__ == "__main__":
    TFs = TFsAdaptedNumeric(m_w='m_w_tot')
    TF_G = TFs.TF_G
    groups = ParamGroups()
    sym = ParamsSymbolic()
    num = ParamsNumeric()
    # pn.m_b_max = 10.4668939715408 #@ TEST: Set calculated max to obtain l = 1

    #% Make functions for use in optimization algorithm
    f_, params_mapped = objective_function(TF_G, sym, num)
    j_ = jacobian(f_, params_mapped)

    f = lambdify(params_mapped, f_)
    j = lambdify(params_mapped, j_)

    # calc_max_added_weight(sym, num, TF_G)
    l_opt, params_opt = calc_parameter_uncertainty_radius(f, TF_G, groups.params_uncertain, sym, num)
    verify_optimization_results(sym, num, TF_G,groups.params_uncertain, opt_values=params_opt)
    

    print("")