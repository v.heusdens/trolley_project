import sympy as sp
from copy import copy
from code_general.load_model_elements import *

def print_sol(sol):
    if len(sol) == 0:
        raise Exception("No solutions found.")
        return
    for key, item in sol.items():
        print("{:<5}: {}".format(key, item))

def tf_dob_general(from_, to_): #~ Calculate any transfer function or loop transfer (y->y) of a general DOB system
    ## Symbolic variables
    P, P_n, Q = sp.symbols("P P_n Q")
    y, y_in, y_meas, xi, d, d_est, d_est_, x, x_comp, x_des = sp.symbols("y y_in y_meas xi d d_est d_est_ x x_comp x_des")
    signals = [y, y_in, y_meas, xi, d, d_est, d_est_, x, x_comp, x_des]

    eqs = [
        sp.Eq(x,        x_comp + d),
        sp.Eq(x_comp,   x_des - d_est),
        sp.Eq(d_est,    Q*d_est_),
        sp.Eq(d_est_,   y_meas/P_n - x_comp),
        sp.Eq(y_meas,   -y_in + xi),
        sp.Eq(y,        P*x),
        sp.Eq(-y_in,     y)
    ]

    ## Define signals
    signals_in = [d, xi, x_des]  
    signal_out = locals()[to_]
    if not isinstance(from_, (list, tuple)): from_ = [from_]
    signals_tf = [locals()[var] for var in from_]

    if signals_tf == [y_in]:
        eqs.pop(-1)
        signals_in.append(y_in)

    signals_impl = [sig for sig in signals if sig not in signals_in]

    ## Solve
    sol = sp.solve(eqs, signals_impl + [signal_out])
    sol_out = sol[signal_out]

    for signal_tf in signals_tf:
        replacements = [(sig, 0) for sig in signals_in if sig != signal_tf]
        sol_H = sol_out.subs(replacements)
        H = sp.simplify(sol_H/signal_tf)
        print("\nTransfer function DOB general {} --> {}: \n{}\n".format(signal_tf, signal_out, H))

def tf_dob_trolley(from_, to_): #~ Calculate any transfer function or loop transfer (y->y) of a general DOB system
    """ Calculate any transfer function or loop transfer (y->y) of a general DOB system. Naming provided in 'names_DOB_trolley.png'."""

    ## Symbolic variables
    T_m, T_m_, T_in, F_hx, F_in, F_in_, F_meas, F_meas_, F_des, F_n, alpha, alpha_in, xi, d, d_est, d_est_, d_est_b = sp.symbols('T_m T_m_ T_in F_hx F_in F_in_ F_meas F_meas_ F_des F_n alpha alpha_in xi d d_est d_est_ d_est_b')
    A, B, D, G, Q, A_n, B_n, D_n, B_ = sp.symbols('A B D G Q A_n B_n D_n B_')
    signals = [T_m, T_m_, T_in, F_hx, F_in, F_in_, F_meas, F_meas_, F_des, F_n, alpha, alpha_in, xi, d, d_est, d_est_, d_est_b]
    
    eqs = [ #@ Custom initial transfer function
        sp.Eq(alpha,    A*F_in),
        sp.Eq(F_in,     d + F_des),
        sp.Eq(F_des,    D*F_hx + T_in),
        sp.Eq(T_in,     B*T_m),
        sp.Eq(T_m,      -d_est_b),
        sp.Eq(d_est_b,  d_est/B_),
        sp.Eq(d_est,    Q*d_est_),
        sp.Eq(d_est_,   F_in_ - F_meas_ - T_m_),
        sp.Eq(T_m_,     B_n*T_m),
        sp.Eq(F_in_,    (-alpha_in + xi)/A_n),
        sp.Eq(F_meas_,   D_n*F_meas),
        sp.Eq(-alpha_in, alpha)
    ]

    ## Define signals
    signals_in = [F_meas, F_hx, d, xi]
    signal_out = locals()[to_]
    if not isinstance(from_, (list, tuple)): from_ = [from_]
    signals_tf = [locals()[var] for var in from_]

    if signals_tf == [alpha_in]: #@ Loop transfer function
        eqs.pop(-1)
        signals_in.append(alpha_in)
    
    signals_impl = [sig for sig in signals if sig not in signals_in]

    ## Solve
    sol = sp.solve(eqs, signals_impl + [signal_out])
    sol_out = sol[signal_out]

    Hs = {}
    for signal_tf in signals_tf:
        replacements = [(sig, 0) for sig in signals_in if sig != signal_tf]
        sol_H = sol_out.subs(replacements)
        H = sp.simplify(sol_H/signal_tf)
        Hs["{} to {}".format(signal_tf, signal_out)] = H
        print("Transfer function DOB custom {} --> {}: \n{}\n".format(signal_tf, signal_out, H))
    
    return H


def calc_Q_for_TF_equals_x(from_, to_, x):
    Q = sp.symbols('Q')
    TF = tf_dob_trolley(from_, to_)
    sol = sp.solve(sp.Eq(TF, x), Q)[0]
    print("{} = {} for Q = {}".format(TF, x , sol))


################################################################
if __name__ == "__main__":
    sym = ModelElementsSymbolic()
    tf_dob_general('d', 'd_est')
    tf_dob_trolley('d', 'T_in')
    # calc_Q_for_TF_equals_x('xi', 'T_m', 0)