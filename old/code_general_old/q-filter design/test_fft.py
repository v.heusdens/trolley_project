import numpy as np
import matplotlib.pyplot as plt
from control import matlab, TransferFunction as tf
import control


"""Make x and t"""
f_s = 1  # sampling frequency
dur = 10  # duration in seconds

x = np.ones(dur*f_s)
x[0] = 0


"""Make filter"""
# = z-transform of 0.5*(x[n] + x[n-1])
filter = tf(np.array([1, 1]), np.array([2, 0]), 1/f_s)
t_imp, imp = control.impulse_response(filter)


"""Filter signal"""
## Pad signal ##
pad = len(t_imp)
x_pad = np.concatenate((x, np.zeros(pad)))

## Transform to frequency domain, pay attention to proper length ##
# Input signals
X = np.fft.fft(x)
X_pad = np.fft.fft(x_pad)

# Impulse response
IMP = np.fft.fft(imp, len(X))
IMP_pad = np.fft.fft(imp, len(X_pad))

## Obtain filtered data ##
X_ = X*IMP
X_pad_ = X_pad*IMP_pad

# """Return to time-domain"""
x_ = np.fft.ifft(X_)
x_pad_ = np.fft.ifft(X_pad_)


"""Plot"""
# Make t-arrays to plot data
# linspace inputs: start, end, number of samples
t = np.linspace(0, dur, dur*f_s, endpoint=False)
t_pad = np.linspace(t[0], t[0] + dur + pad/f_s - 1, dur*f_s + pad)

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, squeeze=True)
ax1.stem(t, x, label="before processing")
ax1.stem(t, x_, label="after processing", linefmt="C1--", markerfmt="C1o")

ax1.set_ylabel("Magnitude")
ax1.set_title("Unpadded")
ax1.legend()

ax2.stem(t_pad, x_pad, label="before processing")
ax2.stem(t_pad, x_pad_, label="after processing",
         linefmt="C1--", markerfmt="C1o")

ax2.set_xlabel("Time in [s]")
ax2.set_ylabel("Magnitude")
ax2.set_title("Padded")
ax2.legend()

plt.show()
print()
