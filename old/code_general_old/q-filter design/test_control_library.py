from control import matlab, TransferFunction as tf, impulse_response
import numpy as np
from matplotlib import pyplot as plt

f = 50
s = tf.s

# q-filter
q_filter = tf(np.array([1.]), np.array([1., 2., 1.]))
q_filter_d = matlab.c2d(q_filter, 1/f, method="tustin")

# Impulse response
T, ir = impulse_response(q_filter)
T_d, ir_d = impulse_response(q_filter_d)

time_steps = T[-1]*f
t = np.arange(T[0], T[-1], 1/f)

print(f"padding time: {T[-1]}\ntime_steps: {time_steps}")

mx = max(ir)
perc = ir[-1]/mx*100
print(ir[-10:-1])
print(f"last value is {perc}% of max value of {mx}")


# Plot
# plt.plot(T, ir, label="continuous-time")
# plt.plot(T_d, ir_d, label="discrete-time")
# plt.legend()
# plt.show()

print()