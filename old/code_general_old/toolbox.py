import sympy as sp
import numpy as np

def avg(min, max):
    return (min + max) / 2.
#%--------------------------------------------------------------------------------------
def make_eq_copyable(model):
    if not isinstance(model, str):
        model = str(model)
    for inst in ["sin", "cos", "tan"]:
        model = model.replace(inst, "sp." + inst)
    for inst in ["(t)"]:
        model = model.replace(inst, '')
    return model
#%--------------------------------------------------------------------------------------
def expand_to_variables(model, vars):
    """Expands equation into coefficients of variables given in 'var' list  

    model : equation to expand  
    vars :  iterable of variables to expand to
    """
    expr_parts = {}
    expr = sp.expand(model)

    for var in vars:
        expr = sp.collect(expr, var)
        expr_parts[str(var)] = expr.coeff(var, 1)
        expr -= expr_parts[str(var)]*var
        expr_parts[str(var)] = sp.simplify(expr_parts[str(var)])
    expr_parts["rest"] = expr

    return expr_parts
#%--------------------------------------------------------------------------------------
def subs_multi(f, x, y, replacements = None): #~ subs using iterables, recursive
    """
    In f, replace x by y.
    f : objective function  
    x : variables to be replaced  
    y : variables to use as replacement  
    """
    if replacements is None:
        replacements = zip(x, y)

    if isinstance(f, list): #@ Go deeper
        f = list(f)
        for i, _ in enumerate(f):
            f[i] = subs_multi(f[i], x, y, replacements)

    else: #@ End of tree
        while f.has(*x):
            f = f.subs(replacements)
        return f

    return f
#%--------------------------------------------------------------------------------------
def derivative_multi(f, x, order=1): #~ takes the derivative of multiple equations simultaneously
    """
    f = iterable of equations  
    x = variable or iterable of variables to differentiate to  
    i = order of derivative
    """
    if not isinstance(x, (list, tuple)):
        x = [x]

    df = [[sp.diff(F, X, order) for X in x] for F in f]
    df = np.array(df)
    return df
#%--------------------------------------------------------------------------------------
def jacobian(f, x): #~ Derivatives
    """
    f = objective function  
    x = variables to differentiate to  
    """
    j = [sp.diff(f, i) for i in x] #@ symbolic jacobian
    return j
#%--------------------------------------------------------------------------------------
def hessian(f, x): #~ Double derivatives
    j = jacobian(f, x) #@ symbolic jacobian
    h = [[sp.diff(j_, i) for i in x] for j_ in j] #@ symbolic hessian   
    return h
#%--------------------------------------------------------------------------------------
def print_list(lst):
    if isinstance(lst, (list, np.ndarray)):
        for entry in lst:
            print(entry)
    elif isinstance(lst, dict):
        for key, value in lst.items():
            print('{}: {}'.format(key, value))
#%--------------------------------------------------------------------------------------
def getattr_multi(object, names):
    """
    object =    object to get attributes from
    names =     string of attribute names, separated by a space  
    """
    names = names.split()
    attributes = [ getattr(object, name) for name in names ]
    return attributes
#%--------------------------------------------------------------------------------------
def expand_strings(names):
    return [elem for string in names for elem in string.split()]
#%--------------------------------------------------------------------------------------
def sympy_function_multi(names, dep_var): #~ Make multiple sympy functions at once
    """
    Makes multiple sympy functions depending on the same variable at once. 
    names =     iterable with function name strings  
    dep_var =   symbolic variable or string
    """
    if isinstance(dep_var, str):
        dep_var = sp.symbols(dep_var)
        
    names = expand_strings(names)

    functions = [sp.Function(name)(dep_var) for name in names]
    return functions
#%--------------------------------------------------------------------------------------
def sympy_derivative_multi(functions, dep_var, amount=1): #~ Make multiple sympy functions at once
    """
    Makes multiple sympy derivatives depending on the same variables at once. 
    functions =     iterable with functions to take derivative from 
    dep_var =   symbolic variable or string
    """
    if isinstance(dep_var, str):
        dep_var = sp.symbols(dep_var)
    
    derivatives = [sp.Derivative(function, dep_var, amount) for function in functions]
    return derivatives
#%--------------------------------------------------------------------------------------
def pnorm(a, p=2):
    """Returns p-norm of sequence a. By default, p=2. 
    p must be a positive integer.
    """
    if not isinstance(a, (list, tuple, np.ndarray)):
        a = [a]

    if p == 'inf' or np.isinf(p): # Infinity norm - largest absolute value
        return max(abs(x) for x in a)
    else:
        return np.sum(x**p for x in a) ** (sp.Rational(1, p))

