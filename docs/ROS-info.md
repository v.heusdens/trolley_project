TOC

<!-- TOC -->

- [NOTES](#notes)
  - [Timeout functions](#timeout-functions)
- [DOCS/INFO](#docsinfo)
  - [ROS](#ros)
    - [Command-line tools](#command-line-tools)
    - [rostopic](#rostopic)
    - [rospy](#rospy)
  - [GAZEBO](#gazebo)
- [MESSAGE TYPES](#message-types)
  - [std_msgs](#std_msgs)
  - [geometry_msgs](#geometry_msgs)
  - [sensor_msgs](#sensor_msgs)
  - [gazebo_msgs](#gazebo_msgs)
- [PLUGINS & CONTROLLERS](#plugins--controllers)
  - [ros_control plugins](#ros_control-plugins)
    - [joint_state_controller](#joint_state_controller)
    - [joint_effort_controller](#joint_effort_controller)
  - [gazebo_plugins](#gazebo_plugins)
  - [Custom plugin](#custom-plugin)
    - [GazeboRosFT](#gazeborosft)
  - [Writing a custom plugin](#writing-a-custom-plugin)
- [Unit testing](#unit-testing)
  - [Unittest python](#unittest-python)
  - [Rostest](#rostest)
  - [rosunit](#rosunit)

<!-- /TOC -->

<br>

# NOTES

Subscriber nodes working with Gazebo: Wait for sim-time to start running (see test_template)

## Timeout functions

launch file:

- `gazebo_timeout.launch` --> `gazebo_timeout.py`  
  Uses rostime from gazebo simulation to set a timeout on all nodes started in a launch file.

      - Launch file argument: `timeout`

- `gazebo_unpause_simulation.launch` --> `gazebo_unpause_simulation.py`  
  Un-pauses gazebo simulation after specified time.

      - Launch file argument: `timeout` (optional)

- `launch_timeout.launch` --> `launch_timeout.py`  
  Sets timeout on all nodes started in a launch file, does not use gazebo time.

      - Launch file argument: `timeout` (optional)

- `test_dummy.test` --> `test_dummy.py`  
  Dummy test with specified duration, always passes when executed.  
  Used for keeping rostest running for a specified period of time.  
  Shuts down when rospy.is_shutdown() is invoked.

      - Launch file argument: `duration` (optional, default = 1)

<br><!--##########################################################-->

# DOCS/INFO

## ROS

### Command-line tools

http://wiki.ros.org/ROS/CommandLineTools

### rostopic

http://docs.ros.org/en/diamondback/api/rostopic/html/rostopic-module.html#get_topic_type

### rospy

rospy is a pure Python client library for ROS. The rospy client API enables Python programmers to quickly interface with ROS Topics, Services, and Parameters.

- Documentation: [ROS wiki - rospy/overview](http://wiki.ros.org/rospy/Overview)
- Tutorials: [ROW wiki - rospy/tutorials](http://wiki.ros.org/rospy/Tutorials)
- Module info: [ROS docs - Package rospy](http://docs.ros.org/en/jade/api/rospy/html/rospy-module.html)

<br>

## GAZEBO

- Gazebo documentation: [Gazebo 9.0 API Reference](http://osrf-distributions.s3.amazonaws.com/gazebo/api/9.0.0/index.html)
- Communication between ROS and Gazebo: [gazebosim.org: ROS-Gazebo communication](http://gazebosim.org/tutorials?tut=ros_comm&cat=connect_ros#GazeboPublishedTopics)
- [github: gazebo_ros_pkgs source code](https://github.com/ros-simulation/gazebo_ros_pkgs)

<br> <!--##########################################################-->

# MESSAGE TYPES#

## [std_msgs](http://wiki.ros.org/std_msgs)

|                          |     | MSG TYPE                                    | NAME        | INFO                                |
| ------------------------ | --- | ------------------------------------------- | ----------- | ----------------------------------- |
| **/String**              |     | string                                      | data        | _text_                              |
|                          |     |                                             |             |                                     |
| **/Float64**             |     | float64                                     | data        |                                     |
|                          |     |                                             |             |                                     |
| **/Time**                |     | time                                        | data        |                                     |
|                          |     |                                             |             |                                     |
| **/MultiArrayDimension** |     | string                                      | label       | _label of given dimension_          |
|                          |     | uint32                                      | size        | _size of given dimension_           |
|                          |     | uint32                                      | stride      | _stride of given dimension_         |
|                          |     |                                             |             |                                     |
| **/MultiArrayLayout**    |     | [std_msgs](#std_msgs)/MultiArrayDimension[] | dim         | _array of dimension properties_     |
|                          |     | uint32                                      | data_offset | _padding elements at front of data_ |
|                          |     |                                             |             |                                     |
| **Float64MultiArray**    |     | [std_msgs](#std_msgs)/MultiArrayLayout      | layout      | _data layout spec_                  |
|                          |     | float64[]                                   | data        | _data array_                        |
|                          |     |                                             |             |                                     |
| **/Header**              |     | uint32                                      | seq         | _sequence ID_                       |
|                          |     | time                                        | stamp       | _time-stamp_                        |
|                          |     | string                                      | frame_id    |                                     |

<br>

## [geometry_msgs](http://wiki.ros.org/geometry_msgs)

| TYPE                              |     | MSG TYPE                                   | NAME        | INFO                     |
| --------------------------------- | --- | ------------------------------------------ | ----------- | ------------------------ |
| **/Point**                        |     | float64                                    | x           |                          |
|                                   |     | float64                                    | y           |                          |
|                                   |     | float64                                    | z           |                          |
|                                   |     |                                            |             |                          |
| **/Vector3**                      |     | float64                                    | x           |                          |
|                                   |     | float64                                    | y           |                          |
|                                   |     | float64                                    | z           |                          |
|                                   |     |                                            |             |                          |
| **/Quaternion**                   |     | float64                                    | x           |                          |
|                                   |     | float64                                    | y           |                          |
|                                   |     | float64                                    | z           |                          |
|                                   |     | float64                                    | w           |                          |
|                                   |     |                                            |             |                          |
| **/Pose**                         |     | [Geometry_msgs](#geometry_msgs)/Point      | position    |                          |
|                                   |     | [Geometry_msgs](#geometry_msgs)/Quaternion | orientation |                          |
|                                   |     |                                            |             |                          |
| **/Twist**                        |     | [Geometry_msgs](#geometry_msgs)/Vector3    | linear      |                          |
| &ensp;_linear + angular velocity_ |     | [Geometry_msgs](#geometry_msgs)/Vector3    | angular     |                          |
|                                   |     |                                            |             |                          |
| **/Wrench**                       |     | [Geometry_msgs](#geometry_msgs)/Vector3    | force       |                          |
| &ensp;_force + torque_            |     | [Geometry_msgs](#geometry_msgs)/Vector3    | torque      |                          |
|                                   |     |                                            |             |                          |
| **/WrenchStamped**                |     | [std_msgs](#std_msgs)/Header               | header      |                          |
| &ensp;_wrench + header_           |     | [Geometry_msgs](#geometry_msgs)/Wrench     | wrench      | _force + torque vectors_ |

<br>

## [sensor_msgs](http://wiki.ros.org/sensor_msgs)

| TYPE     |     | MSG TYPE                                   | NAME                | INFO      |
| -------- | --- | ------------------------------------------ | ------------------- | --------- |
| **/Imu** |     | [std_msgs](#std_msgs)/Header               | header              |           |
|          |     | [Geometry_msgs](#geometry_msgs)/Quaternion | orientation         |           |
|          |     | [Geometry_msgs](#geometry_msgs)/Vector3    | angular_velocity    | [rad/sec] |
|          |     | [Geometry_msgs](#geometry_msgs)/Vector3    | linear_acceleration | [m/s^2]   |

<br>

## [gazebo_msgs](http://docs.ros.org/en/melodic/api/gazebo_msgs/html/msg/LinkStates.html)

| TYPE            |     | MSG TYPE                                | NAME  | INFO                                     |
| --------------- | --- | --------------------------------------- | ----- | ---------------------------------------- |
| **/LinkStates** |     | string[]                                | name  | _array of link names_                    |
|                 |     | [Geometry_msgs](#geometry_msgs)/Pose[]  | pose  | _array of desired poses in world frame_  |
|                 |     | [Geometry_msgs](#geometry_msgs)/Twist[] | twist | _array of desired twists in world frame_ |

<!--| TYPE     |     | MSG TYPE | NAME | INFO |
| -------- | --- | -------- | ---- | ---- |
|   ****   |     |          |      |      |
|          |     |          |      |      |
|          |     |          |      |      |
|          |     |          |      |      |-->

# PLUGINS & CONTROLLERS

- [Gazebosim: Plugins 101](http://gazebosim.org/tutorials?tut=plugins_hello_world&cat=write_plugin)

<br>

## ros_control plugins

- Documentation: [ROS wiki](https://wiki.ros.org/ros_control)
- Source (provides up-to-date list with controller options) [GitHub - ros_controllers](https://github.com/ros-controls/ros_controllers)
- Tutorial: [Gazebosim - ROS Control](http://gazebosim.org/tutorials?tut=ros_control&cat=connect_ros)
- Handy 3rd party overview: [rosroboticslearning - ROS Control](https://www.rosroboticslearning.com/ros-control)

<br>

### joint_state_controller

Publishes the state of all resources registered to a `hardware_interface::JointStateInterface` to a topic of type `sensor_msgs/JointState`.

<br>

### joint_effort_controller

Command a desired force/torque to joints - force/torque input is directly output to the joint.

- Documentation: [ROS docs - effort_controllers Namespace Reference](https://docs.ros.org/en/api/effort_controllers/html/namespaceeffort__controllers.html)

<br>

## gazebo_plugins

- _Documentation: [ROS wiki](http://wiki.ros.org/gazebo_plugins) (only links to other docs already included in this file)_
- Available plugins: [Gazebosim - Plugins available in gazebo_plugins](http://gazebosim.org/tutorials?tut=ros_gzplugins&cat=connect_ros#Pluginsavailableingazebo_plugins)
- Tutorial: [Gazebosim - Using Gazebo plugins with ROS](http://gazebosim.org/tutorials?tut=ros_gzplugins)

## Custom plugin

- AdLinkForce reference: [Gazebo API reference](http://osrf-distributions.s3.amazonaws.com/gazebo/api/9.0.0/classgazebo_1_1physics_1_1Link.html#a2b5d6a125fba8b6faea62de2611ef991)

<br>

### GazeboRosFT

Ros Gazebo Ros Force/Torque Sensor Plugin.
Broadcasts messages with measured force and torque on a specified joint.

- [docs.ros.org: plugin XML reference](http://docs.ros.org/en/jade/api/gazebo_plugins/html/group__GazeboRosFTSensor.html)

|                      |                             |
| -------------------- | --------------------------- |
| _Msg type:_          | geometry_msgs/WrenchStamped |
| _Reference frame:_   | CHILD link frame            |
| _Measure direction:_ | child-to-parent link        |

<br>

## Writing a custom plugin

- http://gazebosim.org/tutorials?cat=write_plugin

<br> <!--##########################################################-->

# Unit testing

High-level info: http://wiki.ros.org/Quality/Tutorials/UnitTesting  
ROS-unittests: http://wiki.ros.org/unittest

Levels:

- _Level 1._  
  Library unit test (unittest (+ rosunit)): a library unit test should test your code sans ROS (if you are having hard time testing sans ROS, you probably need to refactor your library). Make sure that your underlying functions are separated out and well tested with both common, edge, and error inputs. Make sure that the pre- and post- conditions of your methods are well tested. Also make sure that the behavior of your code under these various conditions is well documented.

- _Level 2._  
  ROS node unit test (rostest + unittest): node unit tests start up your node and test its external API, i.e. published topics, subscribed topics, and services.

- _Level 3._  
  ROS nodes integration / regression test (rostest + unittest): integration tests start up multiple nodes and test that they all work together as expected. Debugging a collection of ROS nodes is like debugging multi-threaded code: it can deadlock, there can be race conditions, etc... An integration test is often the best way of uncovering the bugs.

<br>

## Unittest (python)

https://docs.python.org/3/library/unittest.html

Simple example:

    import unittest

    class TestStringMethods(unittest.TestCase):

        def test_...(self):
            # Test method
            self.assertEqual('foo'.upper(), 'FOO')


    if __name__ == '__main__':
        unittest.main()

> TestCase methods:  
> ![Python unittest TestCase Methods](./images/python_unittest_testcase_methods.png)

<br>

## Rostest

Overview: http://wiki.ros.org/rostest  
Setup: http://wiki.ros.org/rostest/Writing  
ROS-unittests: http://wiki.ros.org/unittest

Wrapper around python unittest, needed to test nodes.

Simple example:

    #!/usr/bin/env python
    PKG = 'test_roslaunch'
    import roslib; roslib.load_manifest(PKG)  # This line is not needed with Catkin.

    import sys
    import unittest

    ## A sample python unit test
    class TestBareBones(unittest.TestCase):
        ## test 1 == 1
        def test_one_equals_one(self): # only functions with 'test_'-prefix will be run!
            self.assertEquals(1, 1, "1!=1")

    if __name__ == '__main__':
        import rostest
        rostest.rosrun(PKG, 'test_bare_bones', TestBareBones)

<br>

## rosunit

http://wiki.ros.org/rosunit

Wrapper around python unittest, allows unittest to be called from test launch file.

Simple example:

    #!/usr/bin/env python
    PKG='test_foo'
    import roslib; roslib.load_manifest(PKG)  # This line is not needed with Catkin.

    import sys
    import unittest

    ## A sample python unit test
    class TestBareBones(unittest.TestCase):

        def test_one_equals_one(self):
            self.assertEquals(1, 1, "1!=1")

    if __name__ == '__main__':
        import rosunit
        rosunit.unitrun(PKG, 'test_bare_bones', TestBareBones)
